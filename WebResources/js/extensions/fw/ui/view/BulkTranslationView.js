dojo.provide("extensions.fw.ui.view.BulkTranslationView");
dojo.require("fw.ui.view.AdvancedView");
dojo.require("fw.ui.view.TabbedViewMixin");
dojo.require("fw.ui.controller.BaseDocController");
dojo.declare("extensions.fw.ui.view.BulkTranslationView", [
		fw.ui.controller.BaseDocController, fw.ui.view.AdvancedView,
		fw.ui.view.TabbedViewMixin ], {
	getAdvancedURLParams : function() {
		var _2 = this.get("model"), _3 = _2.get("assets")
		|| [ _2.get("asset") ], _4 = [], _5 = [];
		if (_3) {
			_4 = _3;
		};
		var assetIds = "";
		dojo.forEach(_4, function(_6, i) {
			assetIds += _6.type + ":" + _6.id;
			if (i != _4.length-1){
				assetIds += ";";
			}
		});
		console.log(assetIds);
		if (assetIds) {
			return {
				ThisPage : "ClayTabletDispatcher",
				PostPage : "ClayTabletDispatcher",
				displayelement: "BulkTranslationFront",
				assetids : assetIds
			};
		} else {
			throw new Error("Cannot render BulkTranslation screen");
		}
	},
	_buildAdvancedURL : function() {
		var _8, _9 = {}, _a, _b;
		if (!this.model) {
			return;
		}
		_8 = this.model.get("assets") || [ this.model.get("asset")];
		if (!_8) {
			return;
		}
		_9 = {
			cs_environment : "ucform",
			cs_formmode : "WCM",
			viewId : this.id,
			docId : this.model.get("docid")
		};
		dojo.mixin(_9, this.getAdvancedURLParams());
		//_b = this._getSelectedTab();
		//if (_b) {
		//	dojo.mixin(_9, {
		//		_SELECTED_TAB_ : _b
		//	});
		//}
		return this.requestService.getControllerURL(
				this.GET_ADVANCED_SCREEN, _9);
	},
	show: function () {
              
               this.set('title', "Bulk Translation");
               return this.inherited(arguments);
   }
});