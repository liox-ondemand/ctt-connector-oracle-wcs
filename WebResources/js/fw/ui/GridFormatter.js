dojo.provide("extensions.fw.ui.GridFormatter");
(function() {
	dojo
			.declare(
					"extensions.fw.ui.view.GridFormatter",
					null,
					{
						translateDetail : function(_4c, _4d) {
							var _4e = this.grid, _4f = this, _50 = "&#10;", _51 = /<br\s*\/?\s*>/gi;
							if (_4e) {
								var _52 = _4e.getItem(_4d), _53 = _4e.store, _54 = _53
										.getValue(_52, "detail");
								var _56 = _54.replace(_51, _50);
								return "<span title='" + _56 + "'>" + _54 + "</span>";
							} else {
								return "";
							}
						}
					});
})();