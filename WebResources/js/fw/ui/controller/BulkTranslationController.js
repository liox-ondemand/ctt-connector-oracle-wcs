dojo.provide("extensions.fw.ui.controller.BulkTranslationController");
dojo.require("fw.ui.ObjectFactory");
dojo.require("fw.ui.controller.BaseDocController");
dojo
		.declare(
				"extensions.fw.ui.controller.BulkTranslationController",
				fw.ui.controller.BaseDocController,
				{
					disableDocumentActions : true,
					constructor : function() {
						var _1 = fw.ui.ObjectFactory;
						this.requestService = _1
								.createServiceObject("request");
						this.assetService = _1.createServiceObject("asset");
						
					},
					uninitialize : function() {
						this.view = null;
					},
					translateAsset : function(_2, _3) {
						var _4 = this, _5 = [];
						_5.push(_2.params.assetId);
						return _4.deleteService(_5, true);
					},
					translateAssets : function(_6, _7) {
						var _8 = this, _9 = dijit.byId(_6.params.gridId), _a, _b, _c = [], _d = [], _10, _11, _b = "";
						if (_9) {
							_a = _9.store;
							_11 = _9.selection.getSelected();
							if (_11.length === 0) {
								_8.view.clearMessage();
								_8.view
										.warn(fw.util
												.getString("UI/UC1/JS/CannotTranslate3"));
							} else {
								dojo
										.forEach(
												_11,
												function(_12) {
													var _13 = _a.getValue(
															_12, "asset"), _14 = _a
															.getValue(_12,
																	"name"), _15 = _a
															.getValue(_12,
																	"success");
													if (_15) {
														_d.push(_14);
													} else {
														_c
														.push({
															id : _13.id[0],
															type : _13.type[0]
														});
													}
												});

								if (_d.length) {
									_b += fw.util.getString(
											"UI/UC1/JS/CannotTranslate5", {
												"number" : _d.length
											})
											+ "<br/>";
								}
								if (_b) {
									_8.view.clearMessage();
									_8.view.displayMessage(_b, "error");
								}
								if (_c.length) {
									return _8.translationService(_c, true);
								}
							}
						}
					},
					translationService : function(_17, _18) {
						var _19 = this, def;
						this.view.feedback("Translating assets");
						return _19.assetService
								.translateAssets(_17)
								.then(
										function(_1a) {
											def = new dojo.Deferred();
											if (_19.view) {
												_19.view._updateDisplay(
														_1a.status, _18);
												_19
														.feedback(
																fw.util
																		.getString("UI/UC1/JS/DoneCheckDetails"),
																2000);												
											}
											def.resolve();
											return def.promise;
										});
					},
					_getAssets : function() {
						var _47 = [], doc = this.view.get("doc");
						var _48 = doc.get("asset");
						if (_48) {
							_47.push(_48);
						} else {
							_47 = doc.get("assets");
						}
						return _47;
					}
				});