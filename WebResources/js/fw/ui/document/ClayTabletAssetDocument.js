/*
 * Copyright (c) 2011 FatWire Corporation. All Rights Reserved.
 * Title, ownership rights, and intellectual property rights in and
 * to this software remain with FatWire Corporation. This  software
 * is protected by international copyright laws and treaties, and
 * may be protected by other law.  Violation of copyright laws may
 * result in civil liability and criminal penalties.
 */
if (!dojo._hasResource["fw.ui.document.ClayTabletAssetDocument"]) {
    dojo._hasResource["fw.ui.document.ClayTabletAssetDocument"] = true;
    dojo.provide("fw.ui.document.ClayTabletAssetDocument");
    dojo.require("fw.ui.document.AssetDocument");
    dojo.declare("fw.ui.document.ClayTabletAssetDocument", fw.ui.document.AssetDocument, {
        load: function(_1) {
            _1 = _1 || this.get("id");
            if (!_1 || !(_1 instanceof fw.ui.document.DocumentId)) {
                throw new Error("Unable to load document: no valid identifier provided");
            }
            var _2 = this.getAssetId(_1),
                _3 = this.assetService.getAssetMetadata(_2),
                _4 = this;
            return _3.then(function(_5) {
                var _6 = _4.assetManager.getAsset(_5[0], true);
                _4.set("asset", _6);
                _4.set("id", _1);
                _4.set("name", _6.name);
                _4.set("bookmarked", _6.bookmarked);
                _4.versioningStatus = _6.lockStatus;
                _4.set("sharable", _6.permissions && _6.permissions.canShare);
                _4.set("supportedType", _6.supportedType);
                _4.set("tracked", _6.tracked);
                _4.set("tname", _6.tname);
                _4.set("subtype", _6.subtype);
                _4.set("type", _6.type);
                _4.set("externalid", _6.externalid);
                var _7 = _4.appContext.getConfig("previewableTypes"),
                    _8 = dojo.indexOf(_7, _6.type) !== -1;
                _4.set("previewable", _8);
                return _4;
            });

        },
        copy: function() {
            throw new Error("This operation is not supported on this asset");
        },
        translate: function() {
            throw new Error("This operation is not supported on this asset");
        },
        checkout: function() {
            throw new Error("This operation is not supported on this asset");
        },
        undoCheckout: function() {
            throw new Error("This operation is not supported on this asset");
        },
        edit: function() {
            throw new Error("This operation is not supported on this asset");
        },
        getDefaultViewType: function() {
            var _9, _a = this.getConfig("defaultView"),
                _b, _c = "claytabletdoc",
                _d = this.get("asset");
            if (!_a) {
                console.warn("Missing 'defaultView' property in configuration. Using default Custom asset view");
                _9 = "claytabletdocview";
            } else {
                if (!_d || !_d.type) {
                    _9 = _a[_c];
                } else {
                    _b = _d.type;
                    if (!_9) {
                        _9 = _a[_b] || _a[_c];
                    }
                }
            }
            return _9;
        }
    });
}