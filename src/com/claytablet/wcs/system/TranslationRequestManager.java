/**
 * 
 */
package com.claytablet.wcs.system;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationRequest;

/**
 * @author Mike Field
 *
 */
public interface TranslationRequestManager {

	TranslationRequest createNewTranslationRequest(String siteId, String nSourceId, String nSourceLang, String nTargetLang, TranslationJob job);
	
}
