/**
 * 
 */
package com.claytablet.wcs.system;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.wcs.system.impl.ExtendedAssetIdImpl;
import com.fatwire.assetapi.data.AssetId;
import com.openmarket.xcelerate.asset.AssetIdImpl;

/**
 * @author Mike Field
 *
 */
public class Utils {

	protected static final Log LOG = LogFactory.getLog(Utils.class.getName());

	
	/**
	 * Iterates over map and returns the key for the given searchvalue. Returns the first key found
	 * for the given value (in case there is more than one entry with that value).
	 * @param map Map to search on. Must be Map<String, String>
	 * @param searchstring String to search for
	 * @return String key for given search value. Null if not found.
	 */
	public static String getKeyForThisValue(Map<String, String> map, String searchstring) {
		String mykey = null;
		Iterator<Entry<String, String>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<String,String> pairs = (Map.Entry<String,String>)iterator.next();
		    String value =  pairs.getValue();
		    String key = pairs.getKey();
		    if (searchstring.equalsIgnoreCase(value)) {
		    	mykey = key;
		    	break;
		    }
		}
		return mykey;
	}
	
	/**
	 * Converts a string formatted as Content_C:1231231231231 to an AssetId object
	 * @param typeid String with assettype and assetid
	 * @return AssetId object
	 */
	public static AssetId stringToAssetId(String typeid) {
		try {
			String[] vals = typeid.split(":");
			if (vals.length==2) {
				return new AssetIdImpl(vals[0], Long.parseLong(vals[1]));
			} else {
				return null;
			}
		} catch (Exception e) {
			LOG.error("Error parsing string " + typeid + " to convert it to an AssetId object. Exception: " + e.toString());
			return null;
		}
	}

	/**
	 * Converts a string formatted as Content_C:1231231231231 to an AssetId object
	 * @param typeid String with assettype and assetid
	 * @return AssetId object
	 */
	public static ExtendedAssetId stringToExtendedAssetId(ICS ics, String typeid) {
		AssetId aid = stringToAssetId(typeid);
		
		return aid==null? null : new ExtendedAssetIdImpl(ics, aid);
	}
}
