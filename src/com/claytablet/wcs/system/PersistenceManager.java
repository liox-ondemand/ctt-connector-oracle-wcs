/**
 * 
 */
package com.claytablet.wcs.system;

import java.util.ArrayList;
import java.util.List;

import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationRequest;

/**
 * @author Mike Field
 * @deprecated
 *
 */
public class PersistenceManager  {

	public List<TranslationJob> jobs;
	public List<TranslationRequest> trs;
	
	public PersistenceManager() {
		jobs = new ArrayList<TranslationJob>();
		trs = new ArrayList<TranslationRequest>();
	}
	
	public void saveTranslationJob(TranslationJob job) {
		jobs.add(job);
	}
	
	public List<TranslationJob> getAllTranslationJobs() {
		return jobs;
	}
	
	public void saveTranslationRequest(TranslationRequest tr) {
		trs.add(tr);
	}
	
	public List<TranslationRequest> getAllTranslationRequests() {
		return trs;
	}
}
