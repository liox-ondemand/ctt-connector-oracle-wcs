package com.claytablet.wcs.system;

public class ContentUpdateException extends Exception {
	private static final long serialVersionUID = 1L;

	public ContentUpdateException() {
		super();
	}

	public ContentUpdateException(String message, Throwable cause) {
		super(message, cause);
	}

	public ContentUpdateException(String message) {
		super(message);
	}

	public ContentUpdateException(Throwable cause) {
		super(cause);
	}
}
