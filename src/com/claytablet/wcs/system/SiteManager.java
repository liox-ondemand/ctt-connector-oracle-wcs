/**
 * 
 */
package com.claytablet.wcs.system;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

/**
 * @author Mike Field
 *
 */
public interface SiteManager {

	String getSiteIdByName(String sitename);
	Map<String, String> getAllSites();
	String getSiteNameById(String id);
}
