/**
 * 
 */
package com.claytablet.wcs.system;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

import com.fatwire.assetapi.common.AssetAccessException;
import com.fatwire.assetapi.data.AssetData;
import com.fatwire.assetapi.data.AssetDataManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;


/**
 * Lightweight DAO to load core attribute values. This object is used when sorting
 * and filtering search results.
 * @author Mike Field
 *
 */
public interface ExtendedAssetId {

	Boolean isApproved(ICS ics, String pubtargetid);
	AssetId getAssetId();
	long getId();
	String getAssetType();
	String getJobId();
	String getName();
	String getDescription();
	String getCreatedBy();
	Date getCreatedDate();
	String getUpdatedBy();
	Date getUpdatedDate();
	String getLocale();
	ICS getICS();
	
}
