package com.claytablet.wcs.system;

import java.util.List;

import com.fatwire.assetapi.def.AssetTypeDef;

/**
 * Provides a method for getting all the subtypes (definitions) of an asset type for a site.
 * @author Rick Ptasznik
 *
 */
public interface SubTypeManager {
	List<AssetTypeDef> getSubTypesForAssetType(String assetType, String pubid);
}
