/**
 * 
 */
package com.claytablet.wcs.system;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.claytablet.client.producer.TranslationContent;
import com.claytablet.client.producer.impl.TranslationContentImpl;
import com.claytablet.wcs.system.impl.ExtendedAssetIdImpl;
import com.fatwire.assetapi.common.AssetAccessException;
import com.fatwire.assetapi.data.AssetData;
import com.fatwire.assetapi.data.AssetDataManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.assetapi.data.AttributeData;
import com.fatwire.assetapi.data.MutableAssetData;
import com.fatwire.mda.Dimension;
import com.fatwire.mda.DimensionManager;
import com.fatwire.mda.DimensionableAssetInstance.DimensionParentRelationship;
import com.fatwire.mda.DimensionableAssetManager;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;
import com.openmarket.xcelerate.asset.AssetIdImpl;
import com.openmarket.xcelerate.common.DimParentRelationshipImpl;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

/**
 * A series of helpful functions to lookup translations and dependencies of a given asset.
 * Also supports the creation of a translation stub given a master asset and new locale.
 * @author Mike Field
 *
 */
public interface AssetManager {

	Map<String, String> getAllAssetTypes(ICS ics);
	List<String> getTranslatedLocales(AssetId assetid);
	Collection<AssetId> getTranslations(AssetId assetid);
	boolean assetExists(AssetId assetid);
	AssetId getTranslationMaster(AssetId assetid);
	AssetId getTranslation(AssetId assetid, String locale);
	List<AssetId> getDependencies(AssetId assetid);
	AssetId createTranslationStub(AssetId masteraid, String sourceLocale, String targetLocale, boolean suffixAssetName) throws AssetAccessException;
	void copyContent(AssetId sourceAssetId, AssetId targetAssetId, String siteid) throws AssetAccessException;
	TranslationContent getContentToTranslateIgnoreMods(AssetId aid, String siteid);
	TranslationContent getContentToTranslate(AssetId aid, String siteid, TranslationContent prevtc);
	void updateAssetFromTranslation(AssetId aid, TranslationContent tc, String username) throws ContentUpdateException;
	String getSubtype(AssetId assetid);
	List<String> getAllSubtypes(String assettype);
	List<String> getTranslatableAttributes(String assettype);
	List<String> getTranslatableAttributes(String assettype, String subtype);
	List<String> getAllAttributes(String assettype);
}
