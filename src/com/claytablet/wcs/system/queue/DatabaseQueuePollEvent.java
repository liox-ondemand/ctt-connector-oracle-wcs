package com.claytablet.wcs.system.queue;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.CTPlatformManager.ProcessingResult;
import com.claytablet.client.producer.TranslationEntity.Status;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobFilter;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.util.DiagnosticUtils;
import com.claytablet.wcs.CTObjectFactory;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.ConfigManagerImpl;
import com.claytablet.wcs.system.impl.ContextManagerImpl;
import com.claytablet.wcs.system.impl.JobManagerImpl;
import com.claytablet.wcs.system.impl.JobTimeoutProgressReporter;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.JobTimeoutProgressReporter.Breaker;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.CSRuntimeException;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
			 
public class DatabaseQueuePollEvent implements Action {
	private Log LOG = LogFactory.getLog(getClass().getName() + ":" + Thread.currentThread().getId());
	
	private JobManager jobManager;
	private TranslationEntityManager entityManager;
	@InjectForRequest private ManagerFactory managerFactory;
	@InjectForRequest private WorkflowManager workflowManager;
	
	@Override
	public void handleRequest(ICS ics) {
		if (CTObjectFactory.hasInitError()) {
			LOG.warn("Skipping DatabaseQueuePollEvent as ClayTablet connector fails to start...");
			return;
		}
		//LOG.debug("Starting DatabaseQueuePollEvent...");
		login(ics);
        try {
            if (ics.UserIsMember("xceleditor")) {
                // retrieve list of projects with translations ready for pickup
                try {
                	UserContext ctx = new UserContextImpl(ics);
                    ManagerFactorySession session = managerFactory.newGlobalAdminSession(ctx);
                    try {
	                	jobManager =  new JobManagerImpl(ics, session);
	                	entityManager = session.getTranslationEntityManager();
	                	
	        			int breakInterval = session.getConfigurationManager().get(
	        					ConfigManagerImpl.WCS_BACKGROUND_PROCESS_BREAK_INTERVAL);
	        			if (Breaker.JobSender.readyToContinue(1000L * breakInterval)) {
	        				processAllQueueEntries(session, ics);
	        			} else {
	        				LOG.info("Skipping sending jobs as breaking period from previous task is not over yet...");
	        			}
	                    session.commit(true);
                    } finally {
                    	session.close();
                    }
                } catch (Exception e) {
                    LOG.error("Failure synchronizing metadata", e);
                }
            } else {
                throw new CSRuntimeException("Event user must be logged in", 403);
            }
        } finally {

        	logout(ics);
        	//LOG.debug("Finished DatabaseQueuePollEvent.");
        }
        

    }
	
	private void processAllQueueEntries(ManagerFactorySession session, ICS ics)
	{
		int pollInterval = session.getConfigurationManager().get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_TIMEOUT);
		
		JobTimeoutProgressReporter pr = new JobTimeoutProgressReporter(Breaker.JobSender, 1000L * pollInterval);

		String task = pr.startTask("sendOutQueuedJobs", "Sending out queued jobs");
		
		try {
			// use databasequeue process all job ids in db
			TranslationJobFilter sendingFilter = new TranslationJobFilter.JobFactory().filter(
					"status='" + Status.SENDING + "' OR status='" + Status.SENDING_TO_COPY_BACK + "'");
			List<TranslationJob> jobs = entityManager.findTranslationJobs(sendingFilter);
							
			if (jobs.size()<=0) {
				LOG.debug("No job to process.");
			} else {
				LOG.info("Processing " + jobs.size() + " jobs to be sent");
				pr.addMilestone("sendJobs", "Send out queued jobs", 100);
				pr.startMilestone("sendJobs");
				pr.setupCurrentMilestone(jobs.size(), 0);
			}
			while (!jobs.isEmpty())
			{
				TranslationJob job = jobs.get(0);
				if (job.getStatus()==Status.SENDING_TO_COPY_BACK) {
					LOG.debug("Directly copy back "+job);
					if (session.getConfigurationManager().get(ConfigManagerImpl.WCS_PREVENT_MULTITHREAD_JOB_PROCESSING)) {
						LOG.debug("Setting status to PROCESSING_SENDING_TO_COPY_BACK for "+job);
						entityManager.save(job.setStatus(Status.PROCESSING_SENDING_TO_COPY_BACK));
						session.commit(false);
					}
					jobManager.copyBackJob(pr, job, entityManager);
				} else {
					String vendorid = job.getTranslationProvider().getId();
					LOG.debug("Sending job, name: "+job.getName()+" vendor id: "+vendorid+" site: "+job.getSiteId());
					if (session.getConfigurationManager().get(ConfigManagerImpl.WCS_PREVENT_MULTITHREAD_JOB_PROCESSING)) {
						LOG.debug("Setting status to PROCESSING_SENDING for "+job);
						entityManager.save(job.setStatus(Status.PROCESSING_SENDING));
						session.commit(false);
					}
					jobManager.sendJob(pr, job, entityManager, vendorid);
				}
				job = entityManager.getTranslationJob(job.getId());
				if (job.getStatus().ordinal()>=Status.SENT_TO_PLATFORM.ordinal()) {
					_addAssetsFromJobToWorkflow(job);
				} else if (job.getStatus()==Status.PROCESSING_SENDING_TO_COPY_BACK) {
					LOG.debug("Reverting status back to SENDING_TO_COPY_BACK for "+job);
					entityManager.save(job.setStatus(Status.SENDING_TO_COPY_BACK));
					session.commit(false);
				} else if (job.getStatus()==Status.PROCESSING_SENDING) {
					LOG.debug("Reverting status back to SENDING for "+job);
					entityManager.save(job.setStatus(Status.SENDING));
					session.commit(false);
				}
				
				if (!pr.reportProgress(1)) {
					break;
				}
				jobs = entityManager.findTranslationJobs(sendingFilter);
			}
			pr.completeActiveTask(task, true);
		} finally {
			if (!pr.isTaskCompleted(task)) {
				pr.completeActiveTask(task, false);
			}
		}
		if (pr.shouldContinue()) {
			LOG.debug("Resetting breaker " + Breaker.JobSender);
			Breaker.JobSender.reset();
		}
	}
	
	private void _addAssetsFromJobToWorkflow(TranslationJob job) {
		LOG.debug("Looping through translation requests looking for a workflow id");
		if (job != null) {
			for (TranslationRequest tr : job.getTranslationRequests()) {
				
				if (Utilities.goodString( tr.getTargetWorkflowId() )) {
					LOG.debug("Found workflow id " + tr.getTargetWorkflowId() );
					_addAssetToWorkflow(tr);
				}
				
			}
		} else {
			LOG.debug("Job is null");			
		}
		LOG.debug("Finished looping looking for workflow");
	}
	
	private void _addAssetToWorkflow(TranslationRequest tr) {
		
		String siteid = tr.getSiteId();
		String workflowprocid = tr.getTargetWorkflowId();
		String nativeid = tr.getNativeTargetId();
		AssetId aid = com.claytablet.wcs.system.Utils.stringToAssetId( nativeid );
		
		// createdBy in this process will default to batchuser, however
		// we will track the original TR creation user and assign the asset
		// after child asset creation within the workflow
		
		String assignToUser = tr.getCreatedUser();
		LOG.debug("_addAssetToWorkflow by user: "+assignToUser);
		
		workflowManager.addOrAdvanceAssetInWorkflow(aid, siteid, workflowprocid, assignToUser);
				
	}
	
	
	
	
    private void login(ICS ics) {
        FTValList vl = new FTValList();
        vl.setValString("ftcmd", "login");
        String batchuser = ics.GetProperty("xcelerate.batchuser", "futuretense_xcel.ini", true);
		String encryptedpass = ics.GetProperty("xcelerate.batchpass", "futuretense_xcel.ini", true);
		String batchpass = Utilities.decryptString(encryptedpass);
        vl.setValString("username", batchuser);
        vl.setValString("password", batchpass);

        ics.CatalogManager(vl);
        LOG.debug("Logged in as " + batchuser);
    }

    private void logout(ICS ics) {
        FTValList vl = new FTValList();
        vl.setValString("ftcmd", "logout");
        ics.CatalogManager(vl);
        LOG.debug("Logged out.");
    }
}
