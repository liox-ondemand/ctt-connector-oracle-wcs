package com.claytablet.wcs.system.queue;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Util.ftMessage;

import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.cs.core.db.PreparedStmt;
import com.fatwire.cs.core.db.StatementParam;
import com.fatwire.gst.foundation.controller.action.support.NoSuchMethodExceptionRuntimeException;
import com.fatwire.gst.foundation.facade.assetapi.AssetIdUtils;
import com.fatwire.gst.foundation.facade.cm.AddRow;
import com.fatwire.gst.foundation.facade.sql.Row;
import com.fatwire.gst.foundation.facade.sql.SqlHelper;
import com.fatwire.gst.foundation.facade.sql.table.TableColumn;
import com.fatwire.gst.foundation.facade.sql.table.TableCreator;
import com.fatwire.gst.foundation.facade.sql.table.TableDef;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class DatabaseJobQueue<E> extends AbstractQueue<E> {

    private ICS ics;
    private Action action;
    private TranslationEntityManager entityManager;
    
    private static final Log LOG = LogFactory.getLog(DatabaseJobQueue.class.getPackage().getName());

    public enum Action {PROCESS_JOBS, SYNC_TO_CT, DELETE_FROM_CT};

    // Column names for DB table
    public static String ID = "id"; // TableColumn.Type.ccbigint, the primary key
    // "q_entry" value is either id for TranslationJob class, or jobId for JobQueueEntry (CT id)
    public static String Q_ENTRY = "q_entry"; // TableColumn.Type.ccvarchar, length 255
    public static String INSERTION_DATE = "insertion_date"; // TableColumn.Type.ccdatetime
    public static String STATUS = "status"; // e.g. to VOID a row instead of deleting it from table, TableColumn.Type.ccvarchar, length 255
    // "q_entry_class" is either "com.claytablet.client.producer.TranslationJob" or "com.claytablet.wcs.system.queue.JobQueueEntry"
    public static String Q_ENTRY_CLASS = "q_entry_class"; // TableColumn.Type.ccvarchar, length 255
    // "action" value is the "action to take on the queue item"
    // The 'action' values don't really matter just yet ..., but probably would be at least one of "DOWNLOAD_FROM_CT" or "SYNC_TO_CT"
    public static String ACTION = "action"; // TableColumn.Type.ccvarchar, length 255
    public static String SITENAME = "sitename";
    private static boolean tableCreated = false;
    
    private static final String CT_QUEUE_TABLE = "CT_queue";
    

    private static final PreparedStmt HEAD_SELECT = new PreparedStmt("SELECT * FROM " + CT_QUEUE_TABLE + " WHERE action=? and id=(SELECT MIN(id) FROM "+CT_QUEUE_TABLE+" WHERE action=?)", Collections.singletonList(CT_QUEUE_TABLE));
    private static final PreparedStmt COUNT_SELECT= new PreparedStmt("SELECT COUNT(*) AS queuesize FROM " + CT_QUEUE_TABLE + " WHERE action=?", Collections.singletonList(CT_QUEUE_TABLE));
    private static final PreparedStmt ACTION_SELECT= new PreparedStmt("SELECT * FROM " + CT_QUEUE_TABLE + " WHERE action=? ORDER BY ID ASC", Collections.singletonList(CT_QUEUE_TABLE));
    private static final PreparedStmt Q_ENTRY_SELECT= new PreparedStmt("SELECT * FROM " + CT_QUEUE_TABLE + " WHERE action=? AND q_entry=? AND q_entry_class=? ORDER BY ID ASC", Collections.singletonList(CT_QUEUE_TABLE));

    
    static {
        HEAD_SELECT.setElement(0, CT_QUEUE_TABLE, ACTION);
        HEAD_SELECT.setElement(1, CT_QUEUE_TABLE, ACTION);
        COUNT_SELECT.setElement(0, CT_QUEUE_TABLE, ACTION);
        ACTION_SELECT.setElement(0, CT_QUEUE_TABLE, ACTION);
        Q_ENTRY_SELECT.setElement(0, CT_QUEUE_TABLE, ACTION);
        Q_ENTRY_SELECT.setElement(1, CT_QUEUE_TABLE, Q_ENTRY);
        Q_ENTRY_SELECT.setElement(2, CT_QUEUE_TABLE, Q_ENTRY_CLASS);
    }

    private DatabaseJobQueue(ICS ics, Action action, TranslationEntityManager em) {
        this.ics = ics;
        this.action = action;
        this.entityManager = em;
        
    }

    public static DatabaseJobQueue<JobQueueEntry> getProcessJobsQueue(ICS ics, TranslationEntityManager em) {
        return new DatabaseJobQueue<JobQueueEntry>(ics, Action.PROCESS_JOBS, em);
    }
    
    // not needed, for now
    /*
    public static DatabaseJobQueue<JobQueueEntry> getSyncToCTQueue(ICS ics) {
        return new DatabaseJobQueue<JobQueueEntry>(ics, Action.SYNC_TO_CT);
    }

    public static DatabaseJobQueue<AssetId> getDeleteFromCTQueue(ICS ics) {
        return new DatabaseJobQueue<AssetId>(ics, Action.DELETE_FROM_CT);
    }
	*/
    
    /**
     * Returns an Iterator for type E. For guidance, see GSF classes:
     * com.fatwire.gst.foundation.facade.sql.SqlHelper (esp the "select" method), which uses class:
     * com.fatwire.gst.foundation.facade.sql.IListIterable
     * @return
     */
    @Override
    public Iterator<E> iterator() {
    	LOG.debug("CTQueue[" + action + "]: Getting iterator");
        final StatementParam param = ACTION_SELECT.newParam();
        param.setString(0,action.name());
        Collection<E> results = new LinkedList<E>();
        for(final Row r : SqlHelper.select(ics, ACTION_SELECT, param)) {
        	results.add(instantiateQueueEntry(r.getString(Q_ENTRY_CLASS),r.getString(Q_ENTRY)));
        }
        return results.iterator();
    }

    @Override
    public int size() {
        final StatementParam param = COUNT_SELECT.newParam();
        param.setString(0,action.name());
        Row r = SqlHelper.selectSingle(ics, COUNT_SELECT, param);
        int size = (r == null) ? 0 : r.getInt("queuesize");
        LOG.debug("CTQueue[" + action + "] size is " + size);
        return size;
    }
    
    public static void dropTables(ICS ics)
    {
    	LOG.info("Dropping...");
    	new TableCreator(ics).delteTable(CT_QUEUE_TABLE);
    	LOG.info("Done drop.");
    	DatabaseJobQueue.tableCreated = false;
    }
    
    public static boolean createTables(ICS ics)
    {
    	
    	try{
	    	LOG.debug("Creating or updating CT Queue Table...");
	        final TableDef def = new TableDef(CT_QUEUE_TABLE, "", ftMessage.objecttbl);
	        
	        def.addColumn(new TableColumn(DatabaseJobQueue.ID, TableColumn.Type.ccbigint, true).setNullable(false));
	        def.addColumn(new TableColumn(DatabaseJobQueue.ACTION, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
	        def.addColumn(new TableColumn(DatabaseJobQueue.Q_ENTRY, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
	        def.addColumn(new TableColumn(DatabaseJobQueue.Q_ENTRY_CLASS, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
	        def.addColumn(new TableColumn(DatabaseJobQueue.STATUS, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
	        def.addColumn(new TableColumn(DatabaseJobQueue.INSERTION_DATE, TableColumn.Type.ccdatetime).setNullable(true));
	        def.addColumn(new TableColumn(DatabaseJobQueue.SITENAME, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
	
	        new com.fatwire.gst.foundation.facade.sql.table.TableCreator(ics).createTable(def);
	        
	    	LOG.debug("Created or updated CT Queue Table...");
	    	DatabaseJobQueue.tableCreated = true;
	    	return true;
    	}
    	catch(Exception e)
    	{
    		DatabaseJobQueue.tableCreated = false;
    		LOG.error(e);
    		LOG.error(e.getMessage());
    		return false;
    	}
    	
    }
    
    public static boolean tablesExist(ICS ics)
    {
    	return SqlHelper.tableExists(ics, CT_QUEUE_TABLE);	
    }
    

    private boolean alreadyInQueue(Action action, String q_entry, String q_entry_class) {
        final StatementParam param = Q_ENTRY_SELECT.newParam();
        param.setString(0,action.name());
        param.setString(1,q_entry);
        param.setString(2,q_entry_class);
        Collection<E> results = new LinkedList<E>();
        for(final Row r : SqlHelper.select(ics, Q_ENTRY_SELECT, param)) {
            results.add(instantiateQueueEntry(r.getString(Q_ENTRY_CLASS),r.getString(Q_ENTRY)));
        }
        return results.size() > 0;
    }

    public boolean offer(E e) {
        LOG.info("CTQueue[" + action + "] offer: " + e);
        String q_entry_class="";
        String q_entry="";
        
        if(e instanceof TranslationJob){
        	TranslationJob job = (TranslationJob)e;
        	q_entry=job.getId();
        	
        	q_entry_class="com.claytablet.client.producer.TranslationJob";
        	
        }        
        if(e instanceof JobQueueEntry){
        	JobQueueEntry lt = (JobQueueEntry)e;
        	q_entry=lt.getJobId()+":"+lt.getUser()+":"+lt.getSiteName();	
        	q_entry_class="com.claytablet.wcs.system.queue.JobQueueEntry";
        	this.SITENAME = ((JobQueueEntry) e).getSiteName();
        	
        }
        
        if("".equals(q_entry)) return false;

        if(alreadyInQueue(action, q_entry, q_entry_class)) {
            LOG.info("CTQueue[" + action + "] offer: " + e + " is already in queue.");
        }
        else {
            AddRow addRow = new AddRow(CT_QUEUE_TABLE);
            addRow.set(ACTION, action.name());
            addRow.set(Q_ENTRY, q_entry);
            addRow.set(Q_ENTRY_CLASS, q_entry_class);
            addRow.set(SITENAME, this.SITENAME);
            if(e instanceof TranslationJob)
            {
            	TranslationJob job = (TranslationJob)e;
            	
            	addRow.set(STATUS, job.getStatus().toString());
   
            	
            }
            else
            {
            	addRow.set(STATUS, "");
            }
            addRow.set(INSERTION_DATE, new java.util.Date());
            addRow.execute(ics);
            LOG.info("CTQueue[" + action + "] offer: " + e + " added to queue.");
        }
  
        return true;
    }

    
    public E poll() {
    	E head = peek();
        SqlHelper.execute(ics, CT_QUEUE_TABLE, "DELETE FROM " + CT_QUEUE_TABLE + " WHERE id=(SELECT MIN(id) FROM "+CT_QUEUE_TABLE+" WHERE action='"+action+"')");
        LOG.info("CTQueue[" + action + "] poll: Removing " + head + " from queue.");
        return head;
    }

    public E peek(){
        final StatementParam param = HEAD_SELECT.newParam();
        param.setString(0,action.name());
        param.setString(1,action.name());
        Row r = SqlHelper.selectSingle(ics, HEAD_SELECT, param);
        
        E e = null;
		if(r != null)
		{
	        try {
	            e = instantiateQueueEntry(r.getString(Q_ENTRY_CLASS),r.getString(Q_ENTRY));
	        } catch (ClassCastException e1) {
	            throw new IllegalStateException("Error instantiating object instance from CTQueue. ", e1);
	        }
	        LOG.info("CTQueue[" + action + "] peek: Returning " + e);
		}
        return e;
//        return r == null ? null : instantiateQueueEntry(r.getString(Q_ENTRY_CLASS),r.getString(Q_ENTRY));
    }

    /**
     * If qClass is "com.fatwire.assetapi.data.AssetId", create the AssetId object. OTHERWISE, run the code below to instantiate the queue entry id
     * @param qClass class name (including package) used to instantiate E object
     * @param qEntry String used to instantiate E object
     * @return instance of object defined by qClass and qEntry.
     */
    @SuppressWarnings("unchecked")
    private E instantiateQueueEntry(String qClass, String qEntry) throws ClassCastException {

        //instantiate AssetId
    	
    	if("com.claytablet.client.producer.TranslationJob".equals(qClass)){
    		// fix this todo
    		return (E) entityManager.getTranslationJob(qEntry);
    		
    		 //return (E) AssetIdUtils.createAssetId(qEntry.split(":")[0],qEntry.split(":")[1]);
    		
    	}
    	
    	if("com.claytablet.wcs.system.queue.JobQueueEntry".equals(qClass)){
    		return (E) new JobQueueEntry(qEntry);
    	}
    	
        /*
         Important / Good things to know in the below code -- using this code (aka reflection):
         - Class c = Class.forName(String)
         - Constructor ctor = c.getConstructor(Object...)
         - Object o = ctor.newInstance(Object...);
          */

        Class<?> c = null;
        try {
            c = Class.forName(qClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Object o = null;
        try {
            Constructor<?> ctor = c.getConstructor(String.class);
            o = ctor.newInstance(qEntry);
        } catch (NoSuchMethodException e) {
            throw new NoSuchMethodExceptionRuntimeException(c.getName()
                    + " should have a public constructor accepting a String.");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return (E) o;
    }
}
