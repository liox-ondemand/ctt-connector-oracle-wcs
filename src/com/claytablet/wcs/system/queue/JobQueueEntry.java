package com.claytablet.wcs.system.queue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JobQueueEntry {
	private static final Log LOG = LogFactory.getLog(JobQueueEntry.class.getPackage().getName());
	private String jobId, user, sitename;
	
	public JobQueueEntry(String id, String username, String site)
	{
		this.jobId = id;
		this.user = username;
		this.sitename = site;
	}
	
	// 0 - id
	// 1 -name
	// 2 - site
	public JobQueueEntry(String args)
	{
		if(args.split(":").length == 3)
		{
			this.jobId = args.split(":")[0];
			this.user = args.split(":")[1];
			this.sitename = args.split(":")[2];
		}
		else
		{
			LOG.error("Unable to parse arguments string: "+args+" ... Expected 3 values, delimited by ':'");
		}
	}
	
	public String getJobId()
	{
		return jobId;
	}
	public String getUser()
	{
		return user;
	}
	
	public void setUser(String s)
	{
		this.user = s;
	}
	
	public void setJobId(String s)
	{
		this.jobId = s;
	
	}
	
	public void setSiteName(String s)
	{
		this.sitename = s;
		
	}
	
	public String getSiteName()
	{
		return this.sitename;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobQueueEntry that = (JobQueueEntry) o;

        if (!jobId.equals(that.jobId)) return false;
        if (!user.equals(that.user)) return false;
        if(!sitename.equals(that.sitename)) return false;

        return true;
    }
	
	@Override
    public String toString() {
        return user + ':' + jobId;
    }
}
