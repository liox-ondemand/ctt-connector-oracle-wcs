/**
 * 
 */
package com.claytablet.wcs.system;
import java.util.*;
import java.lang.Long;


import com.claytablet.wcs.system.impl.ExtendedAssetIdImpl;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.search.query.SortOrderImpl;
import com.fatwire.search.util.SearchUtils;
import com.fatwire.cs.core.search.data.IndexData;
import com.fatwire.cs.core.search.data.ResultRow;
import com.fatwire.cs.core.search.engine.SearchEngine;
import com.fatwire.cs.core.search.engine.SearchEngineConfig;
import com.fatwire.cs.core.search.engine.SearchEngineException;
import com.fatwire.cs.core.search.engine.SearchResult;
import com.fatwire.cs.core.search.query.Operation;
import com.fatwire.cs.core.search.query.QueryExpression;
import com.fatwire.cs.core.search.query.SortOrder;
import com.fatwire.cs.core.search.source.IndexSourceConfig;
import com.fatwire.cs.core.search.source.IndexSourceMetadata;
import com.openmarket.xcelerate.asset.AssetIdImpl;
import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * A series of functions for searching and filtering assets. Supports pagination.
 * @author Mike Field
 */
public interface AssetListManager {
	
	Map<String, String> getEnabledAssetTypesInSite(String sitename);
	List<ExtendedAssetId> getAllAssetsInSite(String sitename, int startindex, int maxresults, String sortattribute, Boolean sortasc);
	List<AssetId> getAssetsByTreePage(AssetId pageassetid);
	List<AssetId> getAssetsByTreeNode(Long nodeid); 
	List<AssetId> getAssetsByFlexParent(AssetId parentassetid);
	int queryLuceneTotal(String luceneindex, List<String> assettypes, List<String> keywords, String sitename, String sortattribute, Boolean sortasc);
	List<AssetId> queryLucene(String luceneindex, 
			List<String> assettypes, 
			List<String> keywords, 
			String sitename, 
			int startindex, 
			int maxresults, 
			String sortattribute,
			Boolean sortasc);
	List<ExtendedAssetId> filterByJob(List<ExtendedAssetId> inputlist, String jobid);
	List<ExtendedAssetId> filterByModifiedDate(List<ExtendedAssetId> inputlist, Date modifiedsincedate);
	List<ExtendedAssetId> filterOnlyPublishedAssets(List<ExtendedAssetId> inputlist, String pubtargetid);
	List<AssetId> filterByAssetIdList(List<AssetId> inputlist, List<AssetId> listofassetidstoinclude);
	List<AssetId> removeFromList(List<AssetId> inputlist, AssetId assetid);
	void sortList(List<AssetId> inputlist, String sortattribute, Boolean sortasc, int startindex, int maxrecords);
}
