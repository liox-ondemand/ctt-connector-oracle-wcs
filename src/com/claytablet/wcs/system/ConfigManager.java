/**
 * 
 */
package com.claytablet.wcs.system;

import java.util.List;
import java.util.Map;

/**
 * @author Mike Field
 *
 */
public interface ConfigManager {
	public static final String ALL_ASSETTYPES_KEY = "";
	public static final String ALL_ATTRIBUTES_KEY = "";

	int getMaxItemsPerPage();
	void setMaxItemsPerPage(int maxitemsperpage);
	
	int getCTPollInterval();

	String getWorkflowStepNameResend();
	void setWorkflowStepNameResend(String stepname);
	
	String getWorkflowStepNameReview();
	void setWorkflowStepNameReview(String stepname);

	Boolean isEnabledAssetType(String siteid, String assettype);
	Boolean isEnabledSite(String siteid);
	Boolean isAllAttributesEnabledByDefault(String siteid);
	
	List<String> getEnabledAttributes(String siteid, String assetType);
	Boolean isAllAttributesEnabledByDefault(String siteid, String assetType);
	Boolean isEnabledAttribute(String siteid, String assettype, String attribute);
	void setEnabledAttributes(String siteid, String assettype, List<String> enabledAttributes);
	void setEnabledByDefault(String siteid, boolean enabled);
	void setEnabledByDefault(String siteid, String assetType);
}
