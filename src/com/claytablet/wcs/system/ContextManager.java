/**
 * 
 */
package com.claytablet.wcs.system;

import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserContext;

/**
 * @author Mike Field
 *
 */
public interface ContextManager {

	void setEntityManager(TranslationEntityManager em);
	TranslationEntityManager getEntityManager();
	void setContext(UserContext ctx);
	UserContext getContext();
	String getSitesHome();
	String getCSWebAppHome();
}
