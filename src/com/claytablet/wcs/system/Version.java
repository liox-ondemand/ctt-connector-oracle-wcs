package com.claytablet.wcs.system;

import com.claytablet.model.ProductVersion;

public enum Version implements ProductVersion {
	V100(1, 0, 0),
	V101(1, 0, 1),
	V102(1, 0, 2),
	V103(1, 0, 3),
	V104(1, 0, 4),
	V105(1, 0, 5),
	V106(1, 0, 6),
	V107(1, 0, 7),
	V108(1, 0, 8),
	V109(1, 0, 9),
	V1010(1, 0, 10),
	V110(1, 1, 0),
	V111(1, 1, 1),
	V112(1, 1, 2),
	V113(1, 1, 3),
	V114(1, 1, 4),
	V115(1, 1, 5),
	V116(1, 1, 6),
	V117(1, 1, 7),	
	;

	public static final Version CURRENT = V117;
	
	private final int major;
	private final int minor;
	private final int micro;
	
	
	private Version(int major, int minor, int micro) {
		this.major = major;
		this.minor = minor;
		this.micro = micro;
	}
	
	@Override
	public int getMajor() {
		return major;
	}

	@Override
	public int getMinor() {
		return minor;
	}

	@Override
	public int getMicro() {
		return micro;
	}

	@Override
	public String toString() {
		return String.valueOf(major) + "." + String.valueOf(minor) + "." + String.valueOf(micro);
	}
	
	/**
	 * Main method for ANT tool to execute so that the jar manifest contains the version
	 */
	public static void main(String[] args) {
		System.out.print(Version.CURRENT.toString());
	}
}
