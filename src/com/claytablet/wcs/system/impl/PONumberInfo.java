package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ConfigurationKey.Factory;

public class PONumberInfo {
	public static ConfigurationKey<Map<String, String>> PO_NUMBERS_DESCRIPTION_CONFIGS = 
			Factory.getKeyBuilder("WCS.PONumberDescriptionConfigs", Collections.<String, String>emptyMap()).
					setMultiLine(true).setHidden().key();
	
	public static ConfigurationKey<Boolean> PO_NUMBERS_REQUIRED = 
			Factory.getKeyBuilder("WCS.PONumberRequired", false).setHidden().key();
	
	static public void addPONumber(ConfigurationManager cm, String poNumber, String description) {
		Map<String, String> poConfigs = new HashMap<String, String>(cm.get(PO_NUMBERS_DESCRIPTION_CONFIGS));
		poConfigs.put(poNumber, description);
		cm.getConfigurationSet().put(PO_NUMBERS_DESCRIPTION_CONFIGS, poConfigs);
	}
	
	static public void updatePONumber(ConfigurationManager cm,
			String originalPoNumber, String poNumber, String description) {
		Map<String, String> poConfigs = new HashMap<String, String>(cm.get(PO_NUMBERS_DESCRIPTION_CONFIGS));
		poConfigs.remove(originalPoNumber);
		poConfigs.put(poNumber, description);
		cm.getConfigurationSet().put(PO_NUMBERS_DESCRIPTION_CONFIGS, poConfigs);
	}
	
	static public void removePONumber(ConfigurationManager cm, String poNumber) {
		Map<String, String> poConfigs = new HashMap<String, String>(cm.get(PO_NUMBERS_DESCRIPTION_CONFIGS));
		poConfigs.remove(poNumber);
		cm.getConfigurationSet().put(PO_NUMBERS_DESCRIPTION_CONFIGS, poConfigs);
		
	}
	
	static public List<String> getPONumbers(ConfigurationManager cm) {
		Map<String, String> poConfigs = cm.get(PO_NUMBERS_DESCRIPTION_CONFIGS);
		ArrayList<String> pos = new ArrayList<String>(poConfigs.keySet());
		Collections.sort(pos);
		return pos;
	}
	
	static public String getPONumberDescription(ConfigurationManager cm, String poNumber) {
		Map<String, String> poConfigs = cm.get(PO_NUMBERS_DESCRIPTION_CONFIGS);
		return poConfigs.get(poNumber);
	}

	public static boolean isPONumberRequired(ConfigurationManager configManager) {
		return configManager.get(PO_NUMBERS_REQUIRED);
	}

	public static void setPONumberRequired(ConfigurationManager configManager, 
			boolean poNumberRequired) {
		configManager.getConfigurationSet().put(PO_NUMBERS_REQUIRED, poNumberRequired);
	}
	
	static public List<PONumberInfo> getAllPONumberInfo(ConfigurationManager cm) {
		Map<String, String> poConfigs = cm.get(PO_NUMBERS_DESCRIPTION_CONFIGS);
		List<PONumberInfo> pos = new ArrayList<PONumberInfo>();
		for (Map.Entry<String, String> entry : poConfigs.entrySet()) {
			pos.add(new PONumberInfo(entry.getKey(), entry.getValue()));
		}
		Collections.sort(pos, new Comparator<PONumberInfo>() {
			@Override public int compare(PONumberInfo po1, PONumberInfo po2) {
				return po1.getName().compareTo(po2.getName());
			}});
		return pos;
	}
	
	private final String name;
	private final String desc;
	
	public PONumberInfo(String name, String desc) {
		super();
		this.name = name;
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}
}
