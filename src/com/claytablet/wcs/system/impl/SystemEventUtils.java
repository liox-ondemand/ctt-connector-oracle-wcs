package com.claytablet.wcs.system.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.queue.DatabaseQueuePollEvent;

public class SystemEventUtils {
	protected static final Log LOG = LogFactory.getLog(SystemEventUtils.class.getName());
	
	public enum Event {
		AsyncOp("DatabaseQueuePollEvent", "CustomElements/CT/Utils/Dispatcher", 
				DatabaseQueuePollEvent.class.getName(), 7),
		CheckMessage("CTCheckMessage", "CustomElements/CT/WEM/CheckMessages", null, 22);
		
		private String name;
		private String pageName;
		private String className;
		private int startSeconds;

		private Event(String name, String pageName, String className, int startSeconds) {
			this.name = name;
			this.pageName = pageName;
			this.className = className;
			this.startSeconds = startSeconds;
		}
		
		public boolean isCreated(ICS ics) {
			return ics.ReadEvent(name, name)!=null;
		}
		
		public boolean destroy(ICS ics) {
			if (isCreated(ics)) {
				LOG.info("Deleting event: " + name);
				return ics.DestroyEvent(name);
			} else {
				LOG.warn("Could not delete event: " + name + ", event was not found.");
				return false;
			}
		}
		
		public boolean enable(ICS ics) {
			if(isCreated(ics)) {
				LOG.info("Enabling event: " + name);
				return ics.EnableEvent(name);
			} else {
				LOG.warn("Could not enable event: " + name + ", event might not have been created/registered yet.");
				return false;
			}
		}
		
		public boolean disable(ICS ics) {
			if(isCreated(ics)) {
				LOG.info("Disabling event: " + name);
				return ics.DisableEvent(name);
			} else {
				LOG.warn("Could not disable event: " + name + ", no such event found.");
				return false;
			}
		}
		
		public boolean createOrUpdate(ICS ics, int pollInterval) {
			String runTimeString = getEventRunTimeString(pollInterval, startSeconds);
			
			LOG.info("Set " + name + " runtime to: " + runTimeString);
			
			boolean success = false;
			IList e = ics.ReadEvent(name , name);

			if (e!=null) {
				if (!ics.DestroyEvent(name)) {
		            LOG.error("Failure unregister event " + name + " errno: " + ics.GetErrno());
				}
			}
	        FTValList params = new FTValList();
			params.setValString("pagename", pageName);
			if (!StringUtils.isEmpty(className)) {
				params.setValString("action", "class:" + className);
			}
	        success = ics.AppEvent(name, "ContentServer", runTimeString, params);
	        if (success) {
	            LOG.info("Registered event " + name + " for action " + className);
	            LOG.info("Executing event on time interval: " + runTimeString);
	            
	        } else {
	            LOG.error("Failure registering event "+ name +" errno: " + ics.GetErrno());
	        }
	        if (pollInterval==0) {
	        	disable(ics);
	        }
			return success;
		}
		
		public void updateInterval(int pollInterval, ICS ics) {
			if (pollInterval==0) {
				disable(ics);
			} else {
				createOrUpdate(ics, pollInterval);
				enable(ics);
			}
		}
	}
	
	static public boolean isAsyncOpsEventCreated(ICS ics) {
		return Event.AsyncOp.isCreated(ics);
	}
	

	private static String getEventRunTimeString(int pollValue, int startSeconds)
	{
		String EVERY_60_MINUTES = "*:0:" + startSeconds + " */*/*";
		String EVERY_30_MINUTES = "*:0,30:" + startSeconds + " */*/*";
		String EVERY_10_MINUTES = "*:0,10,20,30,40,50:" + startSeconds + " */*/*";
		String EVERY_5_MINUTES = "*:0,5,10,15,20,25,30,35,40,45,50,55:" + startSeconds + " */*/*";
		String EVERY_2_MINUTES = "*:0,2,4,6,8,10,12,14,16,18,20,22,24,26,28," +
		"30,32,34,36,38,40,42,44,46,48,50,52,54,56,58:" + startSeconds + " */*/*";
		String EVERY_MINUTE = "*:*:" + startSeconds +" */*/*";
		String EVERY_30_SECONDS = "*:*:" + startSeconds + "," + (startSeconds+30) + " */*/*";
		LOG.info("working with integer: "+pollValue);
		String retVal = null;
		switch(pollValue) {
		case 0:
			retVal = EVERY_MINUTE;
			break;
		case 30:
			retVal = EVERY_30_SECONDS;
			break;
		case 60:
			retVal = EVERY_MINUTE;
			break;
		case 120:
			retVal = EVERY_2_MINUTES;
			break;
		case 300:
			retVal = EVERY_5_MINUTES;
			break;
		case 600:
			retVal = EVERY_10_MINUTES;
			break;
		case 1800:
			retVal = EVERY_30_MINUTES;
			break;
		case 3600:
			retVal = EVERY_60_MINUTES;
			break;
		
		default:
			retVal = EVERY_MINUTE;
				
		}
		
		return retVal;
			
	}
	
	static public boolean deleteAsyncOpsEvent(ICS ics)
	{
		return Event.AsyncOp.destroy(ics);
	}
	
	static public boolean createAsyncOpsEvent(ICS ics, ManagerFactorySession session)
	{
		ConfigurationManager cm = session.getConfigurationManager();
		
		int pollInterval = cm.get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_INTERVAL);

		return Event.AsyncOp.createOrUpdate(ics, pollInterval);
	}
	
	static public boolean enableAsyncOpsEvent(ICS ics) {
		return Event.AsyncOp.enable(ics);
	}
	
	static public boolean disableAsyncOpsEvent(ICS ics) {
		return Event.AsyncOp.disable(ics);
	}

	static public boolean isCheckMessageEventCreated(ICS ics) {
		return Event.CheckMessage.isCreated(ics);
	}

	static public boolean deleteCheckMessageEvent(ICS ics) {
		return Event.CheckMessage.destroy(ics);
	}
	
	static public boolean createCheckMessageEvent(ICS ics, ManagerFactorySession session)
	{
		ConfigurationManager cm = session.getConfigurationManager();
		
		int pollInterval = cm.get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_INTERVAL);
		return Event.CheckMessage.createOrUpdate(ics, pollInterval);
	}
	
	static public boolean enableCheckMessageEvent(ICS ics) {
		return Event.CheckMessage.enable(ics);
	}
	
	static public boolean disableCheckMessageEvent(ICS ics)	{
		return Event.CheckMessage.disable(ics);
	}
}