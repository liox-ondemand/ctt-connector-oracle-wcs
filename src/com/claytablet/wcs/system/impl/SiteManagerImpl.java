/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.Utils;

/**
 * @author Mike Field
 *
 */
public class SiteManagerImpl implements SiteManager {

	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	protected ICS ics;
	
	/**
	 * Default constructor
	 * @param ics Current ICS context
	 */
	public SiteManagerImpl(ICS ics) {
		this.ics = ics;
	}
	
	/**
	 * Calls getAllSites() then returns the id/key where the value equals sitename
	 * @param sitename Name of site to lookup
	 * @return String id of the site
	 */
	public String getSiteIdByName(String sitename) {
		Map<String, String> allsites = this.getAllSites();
		return Utils.getKeyForThisValue(allsites, sitename);
	}
	
	/**
	 * Returns a list of all rows in the Publication table (using ics.CallSQL)
	 * @return A hashmap of site ids and names, e.g. "1231231231230", "FirstSiteII"
	 */
	public Map<String, String> getAllSites() {
		Map<String, String> sitelist = new HashMap<String, String>();
		String sql = "SELECT id, name FROM Publication ORDER BY name";
		StringBuffer errStr = new StringBuffer();
		ics.ClearErrno();
		//IList results = ics.CallSQL(sql, null, -1, true, errStr); 
		IList results = ics.SQL("Publication", sql, null, -1, true, errStr);
		int errNum = ics.GetErrno();
		if (errNum < 0) { LOG.error("Error getting list of sites. Error " + Integer.toString(errNum) + ": " + errStr); }
		if (results != null && results.hasData()) {
			try {
				for (int i=1; i <= results.numRows(); i++)
	            {
					results.moveTo(i);
					sitelist.put( results.getValue("id"), results.getValue("name") );
	            }
			} catch (NoSuchFieldException e) {
				LOG.error("Error getting list of sites with exception: " + e);
			}
		}
		return sitelist;
	}
	
	public String getSiteNameById(String id) {
		Map<String, String> allsites = this.getAllSites();
		return allsites.get(id);
	}
	
}
