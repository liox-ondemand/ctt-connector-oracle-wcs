package com.claytablet.wcs.system.impl;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationKey.Factory;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.util.XmlUtil;

public class FreewayAnalysisCodeConfig {
	protected static final Log LOG = LogFactory.getLog(FreewayAnalysisCodeConfig.class);
	
	public static ConfigurationKey<String> WCS_FREEWAY_ANALYSISCODE_CONFIGS = 
			Factory.getXmlKeyBuilder("WCS.freewayAnalysisCodeConfigs", "", null).
					setMultiLine(true).setHidden().key();
	
	private String acName;
	private int acLevel;
	private String acDescription;
	private String acDefaultValue;
	private boolean acRequired;
	private List<String> acValues = new ArrayList<String>();
	private List<String> acValueDescriptions = new ArrayList<String>();
	
	public FreewayAnalysisCodeConfig(String acName, String acDescription,
			String acDefaultValue, boolean acRequired) {
		this(0, acName, acDescription, acDefaultValue, acRequired);
	}
	
	public FreewayAnalysisCodeConfig(int acLevel, String acName, String acDescription,
			String acDefaultValue, boolean acRequired) {
		super();
		this.acLevel = acLevel;
		this.acName = acName;
		this.acDescription = acDescription;
		this.acDefaultValue = acDefaultValue;
		this.acRequired = acRequired;
	}
	
	public int getAcLevel() {
		return acLevel;
	}
	
	public String getAcName() {
		return acName;
	}
	public String getAcDefaultValue() {
		return acDefaultValue;
	}
	public boolean isAcRequired() {
		return acRequired;
	}
	
	public String getValueDescription(String value) {
		for (int i=0; i<acValues.hashCode(); i++) { 
			String val = acValues.get(i);
			if (StringUtils.endsWith(value, val)) {
				return acValueDescriptions.get(i);
			}
		}
		return null;
	}

	public List<String> getValues() {
		return acValues;
	}

	public List<String> getValueDescriptions() {
		return acValueDescriptions;
	}

	public String getAcDescription() {
		return acDescription;
	}
	
	public void addValue(String value, String valueDescription) {
		acValues.add(value);
		acValueDescriptions.add(valueDescription);
	}

	public static List<FreewayAnalysisCodeConfig> getConfigsFromXML(Element e) {
		NodeList configs = e.getElementsByTagName("AnalysisCodeConfig");
		List<FreewayAnalysisCodeConfig> results = 
				new ArrayList<FreewayAnalysisCodeConfig>(configs.getLength());
		
		for (int i=0; i<configs.getLength(); i++) {
			Element configElem = (Element) configs.item(i);
			int acLevel = 0;
			if (!StringUtils.isEmpty(configElem.getAttribute("level"))) {
				try {
					acLevel = Integer.valueOf(configElem.getAttribute("level"));
					if (acLevel<1 || acLevel>3) {
						throw new NumberFormatException("Analysis code level must be between 1 and 3");
					}
				} catch (NumberFormatException nfe) {
					LOG.warn("'" + configElem.getAttribute("level") + "' is not a valid analysic code level", nfe);
				}
			}
			String name = configElem.getAttribute("name");
			String description = configElem.getAttribute("description");
			String defaultValue = configElem.getAttribute("defaultValue");
			boolean required = "true".equalsIgnoreCase(configElem.getAttribute("required"));
			FreewayAnalysisCodeConfig config = 
					new FreewayAnalysisCodeConfig(acLevel, name, description, defaultValue, required);
			NodeList valueEntries = configElem.getElementsByTagName("AnalysisCodeValue");
			for (int j=0; j<valueEntries.getLength(); j++) {
				Element valueEntry = (Element) valueEntries.item(j);
				config.addValue(valueEntry.getAttribute("value"), valueEntry.getAttribute("description"));
			}
			results.add(config);
		}
		return results;
	}
	
	public static void appendConfigsIntoXML(Node root, List<FreewayAnalysisCodeConfig> configs) {
		for (FreewayAnalysisCodeConfig config : configs) {
			Element e = root.getOwnerDocument().createElement("AnalysisCodeConfig");
			e.setAttribute("level", String.valueOf(config.acLevel));
			e.setAttribute("name", config.acName);
			if (!StringUtils.isEmpty(config.acDescription)) {
				e.setAttribute("description", config.acDescription);
			}
			if (!StringUtils.isEmpty(config.acDefaultValue)) {
				e.setAttribute("defaultValue", config.acDefaultValue);
			}
			e.setAttribute("required", String.valueOf(config.acRequired));
			for (int i=0; i<config.acValues.size(); i++) {
				String value = config.acValues.get(i);
				String valueDescription = config.acValueDescriptions.get(i);
				Element valueEntry = root.getOwnerDocument().createElement("AnalysisCodeValue");
				valueEntry.setAttribute("value", value);
				if (!StringUtils.isEmpty(valueDescription)) {
					valueEntry.setAttribute("description", valueDescription);
				}
				e.appendChild(valueEntry);
			}
			root.appendChild(e);
		}
	}

	protected static void saveConfig(
			ConfigurationManager cm, List<FreewayAnalysisCodeConfig> configs) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();

			Element rootElement = doc.createElement("FreewayAnalysisCodeConfigs");

			appendConfigsIntoXML(rootElement, configs);
			doc.appendChild(rootElement);
			// write the content into xml file
			cm.getGlobalConfigurationSet().put(WCS_FREEWAY_ANALYSISCODE_CONFIGS, 
					XmlUtil.toString(doc));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static FreewayAnalysisCodeConfig findFreewayAnalysisCodeConfig(
			ConfigurationManager cm, String analysisCodeName) {
		String cfgVal = cm.get(WCS_FREEWAY_ANALYSISCODE_CONFIGS);
		
		List<FreewayAnalysisCodeConfig> configs = parseConfig(cfgVal);
		
		for (FreewayAnalysisCodeConfig config : configs) {
			if (config.getAcName().equals(analysisCodeName)) {
				return config;
			}
		}
		return null;
	}

	public static List<FreewayAnalysisCodeConfig> getAllFreewayAnalysisCodeConfigs(
			ConfigurationManager cm) {
		String cfgVal = cm.get(WCS_FREEWAY_ANALYSISCODE_CONFIGS);
		
		return parseConfig(cfgVal);
	}

	private static List<FreewayAnalysisCodeConfig> parseConfig(String cfgVal) {
		if (!StringUtils.isEmpty(cfgVal)) {
			try {
				Document doc = XmlUtil.parse(cfgVal);
				
				List<FreewayAnalysisCodeConfig> configs = getConfigsFromXML((Element) doc.getFirstChild());
				return configs;
			} catch (Exception e) {
				LOG.warn("Cannot parse configuration for " + 
						WCS_FREEWAY_ANALYSISCODE_CONFIGS.getName() + ": " + cfgVal , e);
				return Collections.emptyList();
			}
		} else {
			return Collections.emptyList();
		}
	}
}
