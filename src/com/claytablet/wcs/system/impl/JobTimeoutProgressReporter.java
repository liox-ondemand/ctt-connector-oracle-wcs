package com.claytablet.wcs.system.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.claytablet.client.producer.impl.ProgressReporterImpl;

public class JobTimeoutProgressReporter extends ProgressReporterImpl {
	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	
	public enum Breaker {
		JobSender,
		CTMessagePoller;
		
		private long lastProgressTime = Long.MIN_VALUE;

		public void updateProgress() {
			lastProgressTime = System.currentTimeMillis();
		}
		
		public boolean readyToContinue(long breakInterval) {
			return breakInterval<=0L || System.currentTimeMillis() >= lastProgressTime + breakInterval;
		}

		public void reset() {
			lastProgressTime = Long.MIN_VALUE;
		}
	}
	
	public long startTime = System.currentTimeMillis();
	private long timeout;
	private Breaker breaker;
	
	public JobTimeoutProgressReporter(Breaker breaker, long timeout) {
		this.breaker = breaker;
		this.timeout = timeout;
	}
	

	@Override
	public String startTask(String taskName, String taskDescription) {
		LOG.debug("Starting task: " + taskName);
		return super.startTask(taskName, taskDescription);
	}
	
	@Override
	public boolean startMilestone(String milestoneName) {
		LOG.debug("Starting milestone: " + milestoneName);
		super.startMilestone(milestoneName);
		return shouldContinue();
	}

	public boolean shouldContinue() {
		breaker.updateProgress();
		if (timeout>0 && System.currentTimeMillis() - startTime > timeout) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public boolean reportProgress(int progressInSteps) {
		super.reportProgress(progressInSteps);
		LOG.debug("Reported progress: " + progressInSteps + ", now at " + getStateDescription());
		return shouldContinue();
	}
}