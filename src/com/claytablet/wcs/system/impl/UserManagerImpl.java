/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.UserManager;
import com.fatwire.assetapi.data.AssetId;
import com.openmarket.xcelerate.asset.AssetIdImpl;

/**
 * @author Mike Field
 *
 */
public class UserManagerImpl implements UserManager {

	private Log LOG = LogFactory.getLog(getClass().getName() + ":" + Thread.currentThread().getId());
	private ICS ics;
	
	public UserManagerImpl(ICS ics) {
		this.ics = ics;
	}
	
	@Override
	public Boolean hasCTAdminRole(String username, String sitename) {
		return hasRole(username, "CTAdmin", sitename);
	}

	@Override
	public Boolean hasCTSiteAdminRole(String username, String sitename) {
		return hasRole(username, "CTSiteAdmin", sitename);
	}

	@Override
	public Boolean hasCTUserRole(String username, String sitename) {
		return hasRole(username, "CTUser", sitename);
	}

	@Override
	public Boolean hasRole(String username, String role, String sitename) {
		// Get all roles for user
		String pubid = sitename;
		if (isSitenameNotPubId(sitename)) {
			pubid = getPubId(sitename);
		}
		List<String> roles = getUserRoles(username, pubid);
		LOG.debug("Found that rolelist contains " + role + "? Answer is: " + roles.contains(role));
		return roles.contains(role);
	}
	
	private Boolean isSitenameNotPubId(String idorsitename) {
		try {
			Long.parseLong(idorsitename);
			LOG.error("Was given a pubid not sitename: "+idorsitename);
			return false;
		} catch (Exception e) {
			return true;
		}
	}
	
	public String getPubId(String sitename) {
		SiteManager sm = new SiteManagerImpl(ics);
		return sm.getSiteIdByName(sitename);
	}
	
	public List<String> getUserRoles(String username, String siteid) {
		
		LOG.debug("Getting roles for username=" + username + " and siteid=" + siteid);
		// <userMANAGER.GETuserFROMname username="xceladmin" objvarname="u"/>
		FTValList vals = new FTValList();
        vals.put("USERNAME", username);
        vals.put("OBJVARNAME", "u" );
        ics.ClearErrno();
        ics.runTag("USERMANAGER.GETUSERFROMNAME", vals);        
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag USERMANAGER.GETUSERFROMNAME for username " + username + " with error " + errs);
        }					
		
        // <ccuser:getsiteroles name="u" site="968251170475" objvarname="roleobject"/>
		String listname = "mylist";
		vals = new FTValList();
        vals.put("NAME", "u");
        vals.put("SITE", siteid);
        vals.put("OBJVARNAME", "roleobject" );
        ics.ClearErrno();
        ics.runTag("CCUSER.GETSITEROLES", vals);        
        errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag CCUSER.GETSITEROLES for username " + username + " and site id " + siteid + " with error " + errs);
        }					
        
        // <rolelist.GETALL name="roleobject" varname="roles"/>
        // Gets list as a comma-separated string
		vals = new FTValList();
        vals.put("NAME", "roleobject");
        vals.put("VARNAME", "roles" );
        ics.ClearErrno();
        ics.runTag("ROLELIST.GETALL", vals);        
        errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag ROLELIST.GETALL for username " + username + " with error " + errs);
        }
        
        LOG.debug("Got all from rolelist and it returned: " + ics.GetVar("roles"));
        
        // Convert comma-separated string to arraylist
        String roles = ics.GetVar("roles");
        List<String> alRoles = new ArrayList<String>();
        if (!StringUtils.isEmpty(roles)) {
	        String[] saRoles = roles.split(",");
	        for (String s : saRoles) {
	        	alRoles.add(s);
	        }
        }
        return alRoles;
        
	}

	@Override
	public String getUserId(String username) {
		return username; /** TODO: Get id instead of returning username **/
	}

	public boolean isBatchUser(String username) {
		return ("DefaultReader".equals( username ) || 
				StringUtils.equals(username, ics.GetProperty("xcelerate.batchuser", "futuretense_xcel.ini", true)));
	}
}
