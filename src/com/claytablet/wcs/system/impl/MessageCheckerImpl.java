/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.CTPlatformException;
import com.claytablet.client.producer.CTPlatformManager;
import com.claytablet.client.producer.CTPlatformManager.ProcessingResult;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProgressReporter;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.MessageChecker;
import com.claytablet.wcs.system.impl.JobTimeoutProgressReporter.Breaker;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;

/**
 * @author Mike Field
 *
 */
public class MessageCheckerImpl implements MessageChecker {
	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	private ICS ics;
	private TranslationEntityManager entityManager;
	private ManagerFactorySession session;
	
	public MessageCheckerImpl(ICS ics,
			TranslationEntityManager entityManager,
			ManagerFactorySession session) {
		this.ics = ics;
		this.entityManager = entityManager;
		this.session = session;
	}
	
	@Override
	public ProcessingResult checkMessagesAndUpdateContent() {
		LOG.debug("Beginning to check Clay Tablet platform for messages");
		
		int breakTimeOut = session.getConfigurationManager().get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_TIMEOUT);
		int breakInterval = session.getConfigurationManager().get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_BREAK_INTERVAL);
		if (Breaker.CTMessagePoller.readyToContinue(1000L * breakInterval)) {
			JobTimeoutProgressReporter progressReporter = new JobTimeoutProgressReporter(
					Breaker.CTMessagePoller, 1000L * breakTimeOut);
			ProcessingResult pr = null;
			List<TranslationProvider> vendors = entityManager.getAllTranslationProviders();
			try {
				Session batchUserSession = _getLoginSessionForBatchUser();
				CTPlatformManager pm = session.getCTPlatformManager();
				pm.setProgressReporter(progressReporter);
				pr = pm.checkMessages(vendors, 
						new ContentManagerImpl(ics, ics.GetSSVar("username"), session, batchUserSession));
				LOG.debug("Finished checking Clay Tablet platform for messages");
				
				if (progressReporter.shouldContinue()) {
					LOG.debug("Resetting breaker " + Breaker.CTMessagePoller);
					Breaker.CTMessagePoller.reset();
				}
			} catch (CTPlatformException e) {
				LOG.error("Unable to check for messages from Clay Tablet platform.", e);
			}
			return pr;
		} else {
			LOG.info("Skipping this poll period as breaking period from previous poll is not over yet...");
			return ProcessingResult.empty;
		}
	}

	private Session _getLoginSessionForBatchUser() { 
        String batchuser = ics.GetProperty("xcelerate.batchuser", "futuretense_xcel.ini", true);
        String encryptedpass = ics.GetProperty("xcelerate.batchpass", "futuretense_xcel.ini", true);
        String batchpass = Utilities.decryptString(encryptedpass);
        LOG.debug("Logging in as " + batchuser);
        return SessionFactory.newSession(batchuser, batchpass);
	}
	
	

}
