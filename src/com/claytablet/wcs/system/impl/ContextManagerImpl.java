/**
 * 
 */
package com.claytablet.wcs.system.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TeamProfile;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.mock.MockUserContext;
import com.claytablet.client.util.ListUtils;
import com.claytablet.wcs.system.ContextManager;

/**
 * @author Mike Field
 *
 */
public class ContextManagerImpl implements ContextManager {

	protected static final Log LOG = LogFactory.getLog(ContextManagerImpl.class.getName());
	private UserContext context;
	private TranslationEntityManager entitymanager;
	private String sitesHome;
	private String csWebAppHome;

	public ContextManagerImpl(ICS ics, String username, ManagerFactorySession session, 
			String sitesHome, String csWebAppHome) {
		this.sitesHome = sitesHome;
		this.csWebAppHome = csWebAppHome;
		this.entitymanager = session.getTranslationEntityManager();
	}
	
	/**
	 * @param em the em to set
	 */
	public void setEntityManager(TranslationEntityManager em) {
		this.entitymanager = em;
	}

	/**
	 * @return the em
	 */
	public TranslationEntityManager getEntityManager() {
		return entitymanager;
	}

	/**
	 * @param ctx the ctx to set
	 */
	public void setContext(UserContext ctx) {
		this.context = ctx;
	}

	/**
	 * @return the ctx
	 */
	public UserContext getContext() {
		return context;
	}

	@Override
	public String getSitesHome() {
		return sitesHome;
	}

	@Override
	public String getCSWebAppHome() {
		// TODO Auto-generated method stub
		return csWebAppHome;
	}
}
