package com.claytablet.wcs.system.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.UserContext2;

public class UserContextImpl implements UserContext2 {

	protected static final Log LOG = LogFactory.getLog(UserContextImpl.class.getName());
	private String siteid;
	private UserProfileImpl userprofile;
	private ICS ics;
	
	public UserContextImpl(ICS ics) {
		this.ics = ics;
		this.siteid = ics.GetSSVar("pubid");
		if (StringUtils.isEmpty(this.siteid)) {
			String username = ics.GetSSVar("username");
			
			if (!StringUtils.isEmpty(username)) {
				SiteManagerImpl sm = new SiteManagerImpl(ics);
				UserManagerImpl um = new UserManagerImpl(ics);
				
				if (!um.isBatchUser(username)) {
					Map<String, String> allSites = sm.getAllSites();
					
					for (Map.Entry<String, String> entry : allSites.entrySet()) {
						if (um.hasCTSiteAdminRole(username, entry.getValue())) {
							this.siteid = entry.getKey();
							break;
						}
					}
				}
			}
		}
		this.userprofile = new UserProfileImpl(this.ics, this.siteid);
	}
	
	@Override
	public UserProfileImpl getUserProfile() {
		return this.userprofile;
	}

	@Override
	public String getSiteId() {
		return this.siteid;
	}

	@Override
	public String toString() {
		return "UserContextImpl [siteid=" + siteid + ", userprofile="
				+ userprofile + ", ics=" + ics + "]";
	}
}
