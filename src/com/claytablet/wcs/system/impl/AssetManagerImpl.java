/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationContent;
import com.claytablet.client.producer.ConfigurationKey.Factory;
import com.claytablet.client.producer.impl.TranslationContentImpl;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContentUpdateException;
import com.claytablet.wcs.system.ExtendedAssetId;
import com.claytablet.wcs.system.SiteManager;
import com.fatwire.assetapi.common.AssetAccessException;
import com.fatwire.assetapi.common.AssetNotExistException;
import com.fatwire.assetapi.data.AssetData;
import com.fatwire.assetapi.data.AssetDataManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.assetapi.data.AttributeData;
import com.fatwire.assetapi.data.MutableAssetData;
import com.fatwire.assetapi.def.AssetTypeDef;
import com.fatwire.assetapi.def.AssetTypeDefManager;
import com.fatwire.assetapi.def.AssetTypeDefManagerImpl;
import com.fatwire.assetapi.def.AttributeDef;
import com.fatwire.assetapi.def.AttributeTypeEnum;
import com.fatwire.mda.Dimension;
import com.fatwire.mda.DimensionManager;
import com.fatwire.mda.DimensionableAssetManager;
import com.fatwire.mda.DimensionableAssetInstance.DimensionParentRelationship;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;
import com.openmarket.framework.jsp.object.IsEmpty;
import com.openmarket.xcelerate.asset.AssetIdImpl;
import com.openmarket.xcelerate.common.DimParentRelationshipImpl;

/**
 * @author Mike Field
 *
 */
public class AssetManagerImpl implements AssetManager {
	static public ConfigurationKey<List<String>>CTWCS_NON_TRANSLATABLE_TYPES = 
			Factory.getKeyBuilder("CTWCS.nontranslatableTypes", 
					Arrays.asList( 
							"Dimension", 
							"Query", 
							"Template", 
							"DimensionSet", 
							"PageAttribut", 
							"PageDefinition", 
							"PageFilter",
							"AttrTypes",
							"HistoryVals",
							"ScalarVals",
							"Segments",
							"SiteEntry",
							"Stylesheet")
					).setHidden().key();
	
	static public ConfigurationKey<List<String>>CTWCS_NON_COPYABLE_ATTRS = 
			Factory.getKeyBuilder("CTWCS.noncopyableAttributes", 
					Arrays.asList(
							"createdby", 
							"updatedby", 
							"SPTNCode", 
							"SPTParent", 
							"SPTRank", 
							"Dimension")
					).setHidden().key();

	static public ConfigurationKey<List<String>>CTWCS_NON_TRANSLATABLE_ATTRS = 
			Factory.getKeyBuilder("CTWCS.nontranslatableAttributes", 
					Arrays.asList(
							"status",
							"createdby",
							"updatedby",
							"subtype",
							"filename",
							"path",
							"template",
							"externaldoctype",
							"fw_uid",
							"SPTNCode")
					).setHidden().key();
	
	static public ConfigurationKey<Boolean> CTWCS_ADD_TARGET_LOCALE_TO_ASSET_NAME = 
			Factory.getKeyBuilder("CTWCS.addTargetLocaleToAssetName", true).setHidden().key(); 
	
	static public ConfigurationKey<Integer> CTWCS_ASSET_NAME_LENGTH_LIMIT = 
			Factory.getKeyBuilder("CTWCS.assetNameLengthLimit", 64).setHidden().key();
	
	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	protected ICS ics;
	protected Session ses;
	private ManagerFactorySession session;
	
	
	/**
	 * Default constructor
	 * @param ics Current ICS context
	 */
	public AssetManagerImpl(ICS ics, ManagerFactorySession session, Session wcsSession) {
		this.ics = ics;
		this.ses = wcsSession;
		this.session = session;
	}
	
	/**
	 * Returns a list of all rows in the AssetType table (using ics.CallSQL)
	 * @param ics Current ICS context
	 * @return A hashmap of assettype ids and names, e.g. "1231231231230", "AVIArticle"
	 */
	public Map<String, String> getAllAssetTypes(ICS ics) {
		Map<String, String> atypelist = new HashMap<String, String>();
		String sql = "SELECT id, name FROM AssetType ORDER BY assettype";
		StringBuffer errStr = new StringBuffer();
		ics.ClearErrno();
		IList results = ics.CallSQL(sql, null, -1, true, errStr); 
		int errNum = ics.GetErrno();
		if (errNum < 0) { LOG.error("Error getting list of asset types. Error " + Integer.toString(errNum) + ": " + errStr); }
		if (results != null && results.hasData()) {
			try {
				for (int i=1; i <= results.numRows(); i++)
	            {
					results.moveTo(i);
					atypelist.put( results.getValue("id"), results.getValue("assettype") );
	            }
			} catch (NoSuchFieldException e) {
				LOG.error("Error getting list of asset types with exception: " + e);
			}
		}
		return atypelist;
	}
	
	/**
	 * First calls getTranslations() and then for each one, creates a UIAssetId object,
	 * which loads the locale. Then, puts the locale into a list.
	 * @param assetid The asset whose translated locales you're trying to find
	 * @return A list of strings of the locales (e.g. en_US, fr_FR, fr_CA)
	 */
	public List<String> getTranslatedLocales(AssetId assetid) {
		List<String> locales = new ArrayList<String>();
        for( AssetId id : getTranslations(assetid) ) {
        	ExtendedAssetId uaid = new ExtendedAssetIdImpl(ics, id);
        	locales.add(uaid.getLocale());
        }
		return locales;
	}
	
	/**
	 * Calls DimensionableAssetManager.getRelatives() to get the translations of the given asset
	 * @param assetid The asset whose translations you want
	 * @return A collection of AssetId objects of the translations
	 */
	public Collection<AssetId> getTranslations(AssetId assetid) {
        DimensionableAssetManager mgr = (DimensionableAssetManager)ses.getManager(DimensionableAssetManager.class.getName());
        return mgr.getRelatives( assetid, null, "Locale" );
	}
	
	/**
	 * Get translation master of an asset
	 * @param assetid The asset whose translations you want
	 * @return AssetId objects of the translation master, return the "assetid" itself if the asset itself is the translation master
	 */
	public AssetId getTranslationMaster(AssetId assetid) {
        DimensionableAssetManager mgr = (DimensionableAssetManager)ses.getManager(DimensionableAssetManager.class.getName());
        
        DimensionParentRelationship r = mgr.getParent(assetid, "Locale" );
        if (r==null || r.getParent()==null) {
        	return assetid;
        } else {
        	return r.getParent();
        }
	}
	
	/**
	 * Loads a specific translation for the given asset
	 * @param assetid The asset whose translation you want to look up
	 * @param locale The locale name of the desired translation (e.g. fr_FR)
	 * @return AssetId object of the translation. Null if no translation was found.
	 */
	public AssetId getTranslation(AssetId assetid, String locale) {
        DimensionableAssetManager mgr = (DimensionableAssetManager)ses.getManager(DimensionableAssetManager.class.getName());
        return mgr.getRelative( assetid, locale );
	}
	
	/**
	 * Calls ASSET.GETPUBDEPS to get a list of dependencies of depth 1. First needs to
	 * call ASSET.LOAD to get an ics handle on the asset.
	 * @param assetid Asset whose dependencies you want
	 * @return List of AssetId objects
	 */
	public List<AssetId> getDependencies(AssetId assetid) {
		List<AssetId> deps = new ArrayList<AssetId>();
		// Call <ASSET.LOAD>
		String assetname = "depasset";
		FTValList vals = new FTValList();
        vals.put("NAME", assetname);
        vals.put("TYPE", assetid.getType());
        vals.put("OBJECTID", Long.toString(assetid.getId()) );
        ics.ClearErrno();
        ics.runTag("ASSET.LOAD", vals);        
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag ASSET.LOAD to get dependents of asset " + assetid.toString() + " with error " + errs);
        }	
        
		// Call <ASSET.GETPUBDEPS>
        String listname = "deplist";
		vals = new FTValList();
        vals.put("NAME", assetname);
        vals.put("LIST", listname);
        vals.put("DEPTH", "1" );
        ics.ClearErrno();
        ics.runTag("ASSET.GETPUBDEPS", vals);        
        errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag ASSET.GETPUBDEPS for asset " + assetid.toString() + " with error " + errs);
        }			
        IList deplist = ics.GetList(listname); // The resulting list has columns otype, oid, oversion, deptype (use lowercase, not upper)
        if (deplist != null && deplist.hasData()) {
        	// Loop through list and create assetid objects for each
        	try {
				for (int i=1; i <= deplist.numRows(); i++)
	            {
					deplist.moveTo(i);
					AssetId thisdep = new AssetIdImpl( deplist.getValue("otype"), Long.parseLong(deplist.getValue("oid")) );
					deps.add( thisdep );
	            }
			} catch (NoSuchFieldException e) {
				LOG.error("Error looping through results from ASSET.GETPUBDEPS tag for asset " + assetid.toString() + " with exception: " + e);
			} 
        
        }
		return deps;
	}
	
	public boolean assetExists(AssetId assetid) {
        AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
        try {
			adm.readAttributes( assetid, Arrays.asList( new String[] {"Dimension"} ) );
			return true;
		} catch (AssetNotExistException e) {
			return false;
		} catch (AssetAccessException e) {
			return false;
		}
	}
	
	/**
	 * Creates a new asset with the same type and subtype as the master asset.
	 * Copies all attribute values and parents from master into child (except for
	 * Dimension and Ruleset. Sets the new Dimension as localename, then sets the
	 * masteraid as the dimension parent.
	 * @param masteraid Master asset to copy
	 * @param targetLocale Text string of new locale (e.g. fr_CA)
	 * @return AssetId of the new child asset
	 * @throws AssetAccessException 
	 */
	public AssetId createTranslationStub(AssetId masteraid, String sourceLocale, String targetLocale, boolean suffixAssetName) 
			throws AssetAccessException {
    	LOG.debug("Enter createTranslationStub");
    	// Load session and adm
        AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
    	LOG.debug("Session loaded.");
        // Load the master asset
    	//
    	AssetId translationMaster = getTranslationMaster(masteraid);
		ExtendedAssetId uaid = new ExtendedAssetIdImpl(ics, translationMaster);
    	Iterable<AssetData> masterDataList = adm.read( Arrays.<AssetId>asList( masteraid ) ); 
    	LOG.debug("Master asset loaded");
    	// Create a new asset as a copy of the master asset
        MutableAssetData childData = adm.newAssetData( masteraid.getType() , getSubtype(masteraid) );
        AssetData data = adm.readAttributes( masteraid, Arrays.asList( new String[] {"name"} ) );
        String assetName = data.getAttributeData("name").getData().toString();
        
        int assetNameLengthLimit = session.getConfigurationManager().get(CTWCS_ASSET_NAME_LENGTH_LIMIT);
        
        if (!StringUtils.isEmpty(assetName) && session.getConfigurationManager().get(CTWCS_ADD_TARGET_LOCALE_TO_ASSET_NAME) && suffixAssetName) {
        	assetName = getAssetNameWithLocalSuffix(sourceLocale, targetLocale, assetName, assetNameLengthLimit);
        }
        
		childData.getAttributeData( "name" ).setData( assetName );
        LOG.debug("New asset copy created from master");
        
        // Copy all fields and parents from the master asset to the child asset, except for Dimension and Ruleset
    	for ( AssetData masterData : masterDataList ) {
    		for (String attributeName : masterData.getAttributeNames() ) {	       
    			AttributeData ad = masterData.getAttributeData( attributeName );
    			if (ad != null && ad.getData() != null && 
    					childData.getAttributeData( attributeName ) != null && !"name".equals(attributeName)) {
    				childData.getAttributeData( attributeName ).setData( (Object)ad.getData() );
    			}
    		}	   
    		childData.setParents( masterData.getImmediateParents() ); // Copy the parents - getting immediate parents not getparents
    		
    		childData.getAttributeData( "Dimension" ).setData( null );	// Clear the locale for now, because we don't want to copy the master asset's locale
        	//childData.getAttributeData( "ruleset" ).setData( null );	// Clear the ruleset so we don't copy the master asset's ruleset ("ruleset" doesn't exist in 11g)
        }
    	// put the new asset under "unplaced page"
    	if (childData.getAttributeData("SPTParent")!=null) {
    		childData.getAttributeData( "SPTParent" ).setData( null );
    	}
    	if (childData.getAttributeData("SPTRank")!=null) {
    		childData.getAttributeData( "SPTRank" ).setData( 1 );
    	}
    	if (childData.getAttributeData("SPTNCode")!=null) {
    		childData.getAttributeData( "SPTNCode" ).setData( "UnPlaced" );
    	}
    	
    	LOG.debug("All fields and parents from master copied to child");
    	
        adm.insert( Arrays.<AssetData>asList( childData ));        
        
        LOG.debug("AssetDataManager insert done");
        
        // Set child's dim and dim parent
		
        DimensionManager dam = (DimensionManager)ses.getManager(DimensionManager.class. getName());
        Dimension dim = dam.loadDimension( targetLocale );
        childData.getAttributeData("Dimension").setData(Arrays.asList(new Dimension[]{dim}));
        DimensionParentRelationship dpr = new DimParentRelationshipImpl("Locale", translationMaster );
        LOG.debug("Parent relationship set!");
        childData.getAttributeData("Dimension-parent").setData(Arrays.asList(new DimensionParentRelationship[]{dpr}));
        adm.update( Arrays.<AssetData>asList( childData ));
        
        LOG.debug("AssetDataManager update completed");
        
        return childData.getAssetId();
	}

	private String getAssetNameWithLocalSuffix(String sourceLocale,
			String targetLocale, String assetName, int assetNameLengthLimit) {
		String sourceLangSuffix = getLocaleSuffix(sourceLocale);
		String targetLangSuffix = getLocaleSuffix(targetLocale);
		if (assetName.contains(sourceLangSuffix)){
			assetName = StringUtils.replace(assetName, sourceLangSuffix, targetLangSuffix);
		} else {
			assetName += " " + targetLangSuffix;
		}
		if (assetNameLengthLimit>0 && assetName.length()>assetNameLengthLimit) {
			if (assetName.endsWith(targetLangSuffix) && targetLangSuffix.length()+1 <assetNameLengthLimit) {
				assetName = assetName.substring(0, assetNameLengthLimit - targetLangSuffix.length()-1) + " " + targetLangSuffix;
			} else {
				assetName = assetName.substring(0, assetNameLengthLimit);
			}
		}
		return assetName;
	}
	
	private String getLocaleSuffix(String sourceLocale) {
		return "(" + sourceLocale + ")";
	}

	/**
	 * Load the asset and loop through each of its attributes.
	 * For each attribute of type String, add it to the TranslationContent map
	 * in the format "attributename","attributevalue"
	 * @param aid The asset to transform into a TranslationContent asset
	 * @return TranslationContent asset
	 */
	public TranslationContent getContentToTranslateIgnoreMods(AssetId aid, String siteid) {
		
		TranslationContent tc = session.getTranslationEntityManager().createTranslationContent();
	    try {
	        	// Load session and adm
	 	        AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
	        	ConfigManager cm = new ConfigManagerImpl(ics, session);
	 	        
		        // Load the master asset
	        	Iterable<AssetData> masterDataList = adm.read( Arrays.<AssetId>asList( aid ) ); 

		        // Add each String attribute to the TranslationContent map
		    	for ( AssetData masterData : masterDataList ) {
		    		for (String attributeName : masterData.getAttributeNames() ) {	
		    			if (cm.isEnabledAttribute(siteid, aid.getType(), attributeName)) {
			    			AttributeData adValue = masterData.getAttributeData( attributeName );
			    			if (adValue != null) {
				    			Object valuedata = (Object)adValue.getData();
				    			if (valuedata != null && valuedata.getClass().equals(String.class)) {
				    				tc.put(attributeName, valuedata.toString());
				    			} else if (valuedata != null && valuedata.getClass().equals(ArrayList.class)) {
				    				@SuppressWarnings("unchecked")
									JSONArray jarray = new JSONArray((ArrayList<String>)valuedata);
						    		tc.put(attributeName, jarray.toString());
				    			}
			    			}
		    			}
		    		}	   
		        }
		        
		} catch (AssetAccessException e) {
		    	LOG.error("Error creating translation content of asset " + aid.toString() + " with exception: " + e);
		    	return null;
		}		
		return tc;
	}
	
	/**
	 * Load the asset and loop through each of its attributes.
	 * For each attribute of type String, add it to the TranslationContent map
	 * in the format "attributename","attributevalue"
	 * For each attribute, first check if it was modified (i.e. is different from prevtc's value).
	 * Only if it is modified should it be added to the new map.
	 * @param aid The asset to transform into a TranslationContent asset
	 * @return TranslationContent asset
	 */
	public TranslationContent getContentToTranslate(AssetId aid, String siteid, TranslationContent prevtc) {
		
		TranslationContent tc = session.getTranslationEntityManager().createTranslationContent();
	    try {
	        	// Load session and adm
	 	        AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
	        	ConfigManager cm = new ConfigManagerImpl(ics, session);
	 	        
		        // Load the master asset
	        	Iterable<AssetData> masterDataList = adm.read( Arrays.<AssetId>asList( aid ) ); 

		        // Add each String attribute to the TranslationContent map
		    	for ( AssetData masterData : masterDataList ) {
		    		for (String attributeName : masterData.getAttributeNames() ) {
		    			if (cm.isEnabledAttribute(siteid, aid.getType(), attributeName)) {
			    			AttributeData adValue = masterData.getAttributeData( attributeName );
			    			if (adValue != null) {
				    			Object valuedata = (Object)adValue.getData();
				    			String newvaluedata = null;
				    			if (valuedata != null && valuedata.getClass().equals(String.class)) {
				    				newvaluedata = (String)valuedata;
				    			} else if (valuedata != null && valuedata.getClass().equals(ArrayList.class)) {
				    				@SuppressWarnings("unchecked")
									JSONArray jarray = new JSONArray((ArrayList<String>)valuedata); /** TODO: Encapsulate TranslationContent asset **/
				    				newvaluedata = jarray.toString();
				    			}
				    			if (newvaluedata != null) {
				    				
				    				if (prevtc != null) {
					    				String prevvalue = prevtc.get(attributeName);
					    				if (prevvalue == null || ( prevvalue != null && prevvalue.equals( newvaluedata )) ) {
					    					tc.put(attributeName, newvaluedata);
					    				}
				    				} else {
				    					tc.put(attributeName, newvaluedata);
				    				}
				    			}
			    			}
		    			}
		    		}	   
		        }
		        
		} catch (AssetAccessException e) {
		    	LOG.error("Error creating translation content of asset " + aid.toString() + " with exception: " + e);
		    	return null;
		}		
		return tc;
	}	

	public void updateAssetFromTranslation(AssetId aid, TranslationContent tc, String username)  
			throws ContentUpdateException {
		 try {
			 // Load asset
			LOG.debug("Called updateAssetFromTranslation for assetid: " + aid.toString());
			
			AssetDataManager adm = (AssetDataManager) ses.getManager(AssetDataManager.class.getName());
			Iterable<AssetData> assets = adm.read(Arrays.<AssetId> asList(aid));
			List<AssetData> sAssets = new ArrayList<AssetData>();
			for (AssetData a : assets) {

				// For each value in the TranslationContent, update the corresponding attribute
				Iterator<Entry<String, String>> iterator = tc.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry<String, String> pairs = (Map.Entry<String, String>) iterator.next();
					String value = pairs.getValue();
					String key = pairs.getKey();
					LOG.debug("Attempting to update assetid " + aid.toString() + " where " + key + "=" + value);
					AttributeData aData = a.getAttributeData(key);
					Boolean isArrayList = false;
					if (aData != null) {
						Object valuedata = (Object)aData.getData();
						if (valuedata != null && valuedata.getClass().equals(ArrayList.class)) {
							isArrayList = true;
						}					
						if (isArrayList) { // Convert JSON to array list
							try {
								JSONArray jarray = new JSONArray(value); /** TODO: Encapsulate TranslationContent asset **/
								ArrayList<String> listdata = new ArrayList<String>();
								for (int i=0; i<jarray.length(); i++) {
									listdata.add((String)jarray.get(i));
								}
								aData.setData(listdata); // Update attribute as arraylist
							} catch (JSONException e) {
								throw new ContentUpdateException(
										"Could not convert value for attribute " + key + " to JSON", e);
							}
						} else {
							aData.setData(value); // Update attribute as string
						}
					}
				}
				//a.getAttributeData("updatedby").setData(username); // Set the updatedby attribute to the user doing the polling
				sAssets.add(a);
				LOG.debug("Finished updating asset " + aid.toString());
			}
			adm.update(sAssets);
		        
		} catch (AssetAccessException e) {
			throw new ContentUpdateException("Error updating translated content of asset " + aid.toString(), e);
		}	
	}

	
	/**
	 * Calls ASSET.LOAD then ASSET.GETSUBTYPE to get the current asset's subtype
	 * @param AssetId The asset whose subtype you want
	 * @return String containing the subtype
	 */
	public String getSubtype(AssetId assetid) {
		// Call <ASSET.LOAD>
		String assetname = "thisasset";
		FTValList vals = new FTValList();
        vals.put("NAME", assetname);
        vals.put("TYPE", assetid.getType());
        vals.put("OBJECTID", Long.toString(assetid.getId()) );
        ics.ClearErrno();
        ics.runTag("ASSET.LOAD", vals);        
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag ASSET.LOAD to get subtype of asset " + assetid.toString() + " with error " + errs);
        }	
        
		// Call <ASSET.GETSUBTYPE>
		vals = new FTValList();
        vals.put("NAME", assetname);
        vals.put("OUTPUT", "subtype");
        ics.ClearErrno();
        ics.runTag("ASSET.GETSUBTYPE", vals);        
        errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag ASSET.GETSUBTYPE for asset " + assetid.toString() + " with error " + errs);
        }					
		return ics.GetVar("subtype");
	}

	public List<String> getAllSubtypes(String assettype) {
		List<String> subtypes = new ArrayList<String>();
		AssetTypeDefManager mgr = new AssetTypeDefManagerImpl(ics);
		try {
			subtypes = mgr.getSubTypes(assettype);
		} catch (AssetAccessException e) {
			LOG.error("Unable to subtypes for assettype="+assettype+" with exception: " + e);
		}
		return subtypes;	
	}
	
	@Override
	public List<String> getTranslatableAttributes(String assettype) {
		ConfigurationManager cm = session.getConfigurationManager();
		if (cm.get(CTWCS_NON_TRANSLATABLE_TYPES).contains(assettype)) {
			return Collections.emptyList();
		}
		List<String> attributes = new ArrayList<String>();
		
		List<AttributeTypeEnum> translatabletypes = new ArrayList<AttributeTypeEnum>();
		translatabletypes.add( AttributeTypeEnum.STRING );
		translatabletypes.add( AttributeTypeEnum.LARGE_TEXT );
		translatabletypes.add( AttributeTypeEnum.LIST );
				
		List<String> excluded = cm.get(CTWCS_NON_TRANSLATABLE_ATTRS);
		
		List<String> subtypes = getAllSubtypes(assettype);
		// added check below to deal with basic assets as they have no subtypes
		// Rick Ptasznik
		if (subtypes != null && subtypes.size() == 0){
			// check if its a basic asset
			AssetTypeDefManager mgr = new AssetTypeDefManagerImpl(ics);
			try {
				AssetTypeDef atypedef = mgr.findByName(assettype, null);
				if (atypedef.getProperties().getIsAssetmakerAsset()){
					// we have a basic asset
					List<AttributeDef> attrdefs = atypedef.getAttributeDefs();
					
					for (AttributeDef ad : attrdefs) {
						if (!attributes.contains( ad.getName() )) {
							
							if (translatabletypes.contains( ad.getType() ) && 
									!excluded.contains( ad.getName() )) {
								attributes.add( ad.getName() );
							}
						}
					}
				}
			} catch (AssetAccessException e) {
				LOG.error("Unable to load asset definition for assettype="+assettype+" with exception: " + e);
			}			
		} else { // end addition for basic assets
			for (String subtype : subtypes) {
				
				AssetTypeDefManager mgr = new AssetTypeDefManagerImpl(ics);
				try {
					AssetTypeDef atypedef = mgr.findByName(assettype, subtype);				
					List<AttributeDef> attrdefs = atypedef.getAttributeDefs();
					
					for (AttributeDef ad : attrdefs) {
						if (!attributes.contains( ad.getName() )) {
							if (translatabletypes.contains( ad.getType() ) && 
									!excluded.contains( ad.getName() )) {
								attributes.add( ad.getName() );
							}
						}
					}
				} catch (AssetAccessException e) {
					LOG.error("Unable to load asset definition for assettype="+assettype+" with exception: " + e);
				}
			
			}
		}
		return attributes;
	}
	
	/***
	 * another version of getTranslatableAttributes that gets the attributes for 1 specific subtype (definition) only
	 * @author Rick Ptasznik
	 */
	@Override
	public List<String> getTranslatableAttributes(String assettype, String subtype){
		ConfigurationManager cm = session.getConfigurationManager();
		if (cm.get(CTWCS_NON_TRANSLATABLE_TYPES).contains(assettype)) {
			return Collections.emptyList();
		}
		List<String> attributes = new ArrayList<String>();
		
		List<AttributeTypeEnum> translatabletypes = new ArrayList<AttributeTypeEnum>();
		translatabletypes.add( AttributeTypeEnum.STRING );
		translatabletypes.add( AttributeTypeEnum.LARGE_TEXT );
		translatabletypes.add( AttributeTypeEnum.LIST );
		
		List<String> excluded = cm.get(CTWCS_NON_TRANSLATABLE_ATTRS);
			
		AssetTypeDefManager mgr = new AssetTypeDefManagerImpl(ics);
		try {
			AssetTypeDef atypedef = mgr.findByName(assettype, subtype);
			List<AttributeDef> attrdefs = atypedef.getAttributeDefs();
			for (AttributeDef ad : attrdefs) {
				if (!attributes.contains( ad.getName() )) {
					if (translatabletypes.contains( ad.getType() ) && 
							!excluded.contains( ad.getName() )) {
						attributes.add( ad.getName() );
					}
				}
			}
		} catch (AssetAccessException e) {
			LOG.error("Unable to load asset definition for assettype="+assettype+" with exception: " + e);
		}		
		
		return attributes;
	}

	@Override
	public List<String> getAllAttributes(String assettype) {
		List<String> attributes = new ArrayList<String>();
		List<String> subtypes = getAllSubtypes(assettype);
		for (String subtype : subtypes) {
			
			AssetTypeDefManager mgr = new AssetTypeDefManagerImpl(ics);
			try {
				AssetTypeDef atypedef = mgr.findByName(assettype, subtype);
				List<AttributeDef> attrdefs = atypedef.getAttributeDefs();
				for (AttributeDef ad : attrdefs) {
					if (!attributes.contains( ad.getName() )) {
						attributes.add( ad.getName() );
					}
				}
			} catch (AssetAccessException e) {
				LOG.error("Unable to load asset definition for assettype="+assettype+" with exception: " + e);
			}
		
		}
		return attributes;
	}

	@Override
	public void copyContent(AssetId sourceAssetId, AssetId targetAssetId, String siteId) throws AssetAccessException {
		copyContent(sourceAssetId, targetAssetId, siteId, false, false, null, null);
	}

	protected void copyContent(AssetId sourceAssetId, AssetId targetAssetId, String siteId, boolean copyAll, 
			boolean suffixAssetName, String sourceLocale, String targetLocale) throws AssetAccessException {
		AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
    	LOG.debug("Session loaded.");
        // Load the master asset
    	//
    	Iterable<AssetData> sourceDataList = adm.read( Arrays.<AssetId>asList( sourceAssetId ) ); 
    	LOG.debug("Master asset loaded");
    	// Create a new asset as a copy of the master asset
		Iterable<AssetData> targetDataList = adm.read(Arrays.<AssetId> asList(targetAssetId));
        MutableAssetData childData = adm.newAssetData( sourceAssetId.getType() , getSubtype(sourceAssetId) );
        childData.setAssetId(targetAssetId);
		
    	ConfigManager cm = new ConfigManagerImpl(ics, session);
		List<String> excluded = session.getConfigurationManager().get(CTWCS_NON_COPYABLE_ATTRS);
    	
        // Copy all fields and parents from the master asset to the child asset, except for Dimension and Ruleset
    	for ( AssetData targetData : targetDataList ) {
    		for (String attributeName : targetData.getAttributeNames() ) {	    
    			AttributeData ad = targetData.getAttributeData( attributeName );
    			if (ad != null && ad.getData() != null && childData.getAttributeData( attributeName ) != null) {
    				childData.getAttributeData( attributeName ).setData( (Object)ad.getData() );
    			}
    		}
    	}
        // Copy all fields and parents from the master asset to the child asset, except for Dimension and Ruleset
		String assetType = sourceAssetId.getType();
        String assetName = null;
        
    	for ( AssetData sourceData : sourceDataList ) {
    		for (String attributeName : sourceData.getAttributeNames() ) {
    			if (!excluded.contains(attributeName)) {
	    			if ("name".equals(attributeName)) {
	    				assetName = sourceData.getAttributeData( attributeName ).getData().toString();
	    			}
					if ((copyAll || cm.isEnabledAttribute(siteId, assetType, attributeName)) &&
	    					!attributeName.equals("id") && !attributeName.equals("Dimension") && 
	    					!attributeName.equals("Dimension-parent")) {
		    			AttributeData ad = sourceData.getAttributeData( attributeName );
		    			if (ad != null && ad.getData() != null && childData.getAttributeData( attributeName ) != null) {
		    				childData.getAttributeData( attributeName ).setData( (Object)ad.getData() );
		    			}
	    			}
    			}
    		}	   
    		childData.setParents( sourceData.getImmediateParents() ); // Copy the parents - getting immediate parents not getparents
        } 
    	
    	LOG.debug("All fields and parents from master copied to child");
    	
    	if (suffixAssetName && !StringUtils.isEmpty(assetName) && !StringUtils.isEmpty(sourceLocale) && !StringUtils.isEmpty(targetLocale)) {
            int assetNameLengthLimit = session.getConfigurationManager().get(CTWCS_ASSET_NAME_LENGTH_LIMIT);
            
            if (!StringUtils.isEmpty(assetName) && session.getConfigurationManager().get(CTWCS_ADD_TARGET_LOCALE_TO_ASSET_NAME) && suffixAssetName) {
            	assetName = getAssetNameWithLocalSuffix(sourceLocale, targetLocale, assetName, assetNameLengthLimit);
            }
	    	if (assetName!=null) {
	    		LOG.debug("Setting asset name to " + assetName);
	    		childData.getAttributeData( "name" ).setData( assetName );
	    	}
    	}
        // Set child's dim and dim parent
		adm.update(Arrays.<AssetData>asList( childData ));
		
        LOG.debug("AssetDataManager update completed");
	}
	
}
