/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.wcs.system.AssetListManager;
import com.claytablet.wcs.system.ExtendedAssetId;
import com.claytablet.wcs.system.SiteManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.cs.core.search.data.IndexData;
import com.fatwire.cs.core.search.data.ResultRow;
import com.fatwire.cs.core.search.engine.SearchEngine;
import com.fatwire.cs.core.search.engine.SearchEngineConfig;
import com.fatwire.cs.core.search.engine.SearchEngineException;
import com.fatwire.cs.core.search.engine.SearchResult;
import com.fatwire.cs.core.search.query.Operation;
import com.fatwire.cs.core.search.query.QueryExpression;
import com.fatwire.cs.core.search.query.SortOrder;
import com.fatwire.cs.core.search.source.IndexSourceConfig;
import com.fatwire.cs.core.search.source.IndexSourceMetadata;
import com.fatwire.gst.foundation.facade.runtag.asset.Children;
import com.fatwire.gst.foundation.facade.runtag.asset.GetSiteNode;
import com.fatwire.gst.foundation.facade.runtag.asset.Load;
import com.fatwire.gst.foundation.facade.runtag.siteplan.ListPages;
import com.fatwire.gst.foundation.facade.runtag.siteplan.SitePlanLoad;
import com.fatwire.search.query.SortOrderImpl;
import com.fatwire.search.util.SearchUtils;
import com.openmarket.xcelerate.asset.AssetIdImpl;

/**
 * @author Mike Field
 *
 */
public class AssetListManagerImpl implements AssetListManager {

	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	protected ICS ics;
	protected QueryTypeEnum searchtype = null;
	
	/**
	 * Default constructor
	 * @param ics Current ICS context
	 */
	public AssetListManagerImpl(ICS ics) {
		this.ics = ics;
	}
	
	/**
	 * List of query types, depending on which type of query was requested.
	 * @author Mike Field
	 *
	 */
	public static enum QueryTypeEnum { ALL, BYTREE, BYPARENT }
	
	/**
	 * First calls UISiteManager.getSiteIdByName() to get the site id.
	 * Then does ics.CallElement to OpenMarket/Xcelerate/Actions/AssetMgt/EnableAssetTypePub. 
	 * This called element actually just calls this tag:
	 * <PUBLICATION.CHILDREN NAME="site" LIST="Variables.list" OBJECTTYPE="AssetType" ORDER="description"/>
	 * @param sitename
	 * @return
	 */
	public Map<String, String> getEnabledAssetTypesInSite(String sitename) {
		Map<String, String> assettypes = new HashMap<String, String>();
		SiteManager sm = new SiteManagerImpl(ics);
		String siteid = sm.getSiteIdByName(sitename);
		FTValList vals = new FTValList();
        vals.put("upcommand", "ListEnabledAssetTypes");
        vals.put("list", "EnabledAssetTypes");
        vals.put("pubid", siteid);        
        ics.ClearErrno();
        ics.CallElement("OpenMarket/Xcelerate/Actions/AssetMgt/EnableAssetTypePub", vals);
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error calling element OpenMarket/Xcelerate/Actions/AssetMgt/EnableAssetTypePub to get list of enabled asset types in site " + sitename + " with error " + errs);
        } else {
        	IList atypes = ics.GetList("EnabledAssetTypes");
        	if (atypes != null && atypes.hasData()) {
    			try {
    				for (int i=1; i <= atypes.numRows(); i++)
    	            {
    					atypes.moveTo(i);
    					assettypes.put( atypes.getValue("id"), atypes.getValue("assettype") );
    	            }
    			} catch (NoSuchFieldException e) {
    				LOG.error("Error getting list of enabled asset types with exception: " + e);
    			}
    		}
        }
		return assettypes;
	}
	
	/**
	 * Begins by calling getEnabledAssetTypesInSite().
	 * Then, queries the lucene index for all these asset types using queryLucene("Global", atypes, null, 0, 0)
	 * @param sitename Name of site
	 * @param startindex Starting row number (Set to zero if you don't need pagination)
	 * @param maxresults Max results to return (Used for pagination. Set to zero for no limit.)
	 * @param sortattribute Name of attribute to sort on. Can be null.
	 * @param sortasc Boolean for ascending or descending sort order. True if ascending; false otherwise.
	 * @return
	 */
	public List<ExtendedAssetId> getAllAssetsInSite(String sitename, int startindex, int maxresults, String sortattribute, Boolean sortasc) {
		// Get all enabled asset types
		Map<String, String> assettypes = getEnabledAssetTypesInSite(sitename);
		List<String> atypes = new ArrayList<String>();
		for (String type : assettypes.values()) { // Move from a map into an array list
			atypes.add(type);		
		}
		// Set the query type for this object
		this.searchtype = QueryTypeEnum.ALL;
		// Query all assets
		List<AssetId> cleanlist = queryLucene("Global", atypes, null, sitename, startindex, maxresults, sortattribute, sortasc);
		// For each asset in the results, create UIAssetId object and return results
		List<ExtendedAssetId> completelist = new ArrayList<ExtendedAssetId>();
		for (AssetId aid : cleanlist) {
			completelist.add( new ExtendedAssetIdImpl(ics, aid) );
		}
		return completelist;
	}
	
	public List<AssetId> getAssetsByTreePage(AssetId aid) {

		Load load = new Load();
		load.setName("thisPage");
		load.setObjectId( Long.toString(aid.getId()) );
		load.setType( aid.getType() );
		load.execute(ics);
		
		GetSiteNode gsn = new GetSiteNode();
		gsn.setName("thisPage");
		gsn.setOutput("nodeId");
		gsn.execute(ics);
		
		Long nodeid = (Utilities.goodString(ics.GetVar("nodeId"))) ? Long.parseLong(ics.GetVar("nodeId")) : null;
		
		List<AssetId> children = this.getAssetsByTreeNode(nodeid);
		if (children.isEmpty()) { LOG.error("Could not find any children for Page asset " + aid.toString()); }		
		return children;
	}
	
	public List<AssetId> getAssetsByTreeNode(Long nodeid) {
		List<AssetId> children = new ArrayList<AssetId>();

		SitePlanLoad spl = new SitePlanLoad();
		spl.setName("thisNode");
		spl.setNodeid(Long.toString(nodeid));
		spl.execute(ics);
		
		ListPages lp = new ListPages();
		//lp.setLevel(200); // Default is 10,000
		lp.setName("thisNode");
		lp.setPlacedList("childPages");
		lp.execute(ics);
		
		if (ics.GetList("childPages") != null && ics.GetList("childPages").hasData()) {
			IList childpages = ics.GetList("childPages");
			for (int i=1; i<=childpages.numRows(); i++) {
				childpages.moveTo(i);
				String type;
				String id;
				try {
					
					type = childpages.getValue("AssetType");
					id = childpages.getValue("Id");
					AssetId thisaid = new AssetIdImpl( type, Long.parseLong(id) );
					children.add(thisaid);
					
				} catch (NoSuchFieldException e) {
					LOG.error("Error getting columns AssetType and Id from ics list called childPages when getting child pages from nodeid "+Long.toString(nodeid));
				}
			}		
		}
		
		if (children.isEmpty()) {
			LOG.error("Could not find any children for Page asset with nodeid="+Long.toString(nodeid));
		}
		
		return children;
	}
	

	
	public List<AssetId> getAssetsByFlexParent(AssetId parentassetid) {
		LOG.debug("About to look for children for parentasset " + parentassetid.toString());
		ics.RegisterList("childAssets", null);
		List<AssetId> children = new ArrayList<AssetId>();
		
		FTValList args = new FTValList();
		args.removeAll();
		args.setValString("assetid", Long.toString(parentassetid.getId()) );
		args.setValString("assettype", parentassetid.getType());
		ics.CallElement("CustomElements/CT/Utils/GetFlexChildren", args);

		
		if (ics.GetList("childAssets") != null && ics.GetList("childAssets").hasData()) {
			IList childpages = ics.GetList("childAssets");
			for (int i=1; i<=childpages.numRows(); i++) {
				childpages.moveTo(i);
				String type;
				String id;
				try {
					
					type = childpages.getValue("assettype");
					id = childpages.getValue("assetid");
					AssetId thisaid = new AssetIdImpl( type, Long.parseLong(id) );
					children.add(thisaid);
					children.addAll( this.getAssetsByFlexParent(thisaid) );
					
				} catch (NoSuchFieldException e) {
					LOG.error("Error getting columns assettype and assetid from ics list called childAssets when getting flex children of asset "+parentassetid+" when calling CustomElements/CT/Utils/GetFlexParents");
				}
			}	
		} else {
			LOG.debug("CustomElements/CT/Utils/GetFlexChildren did not return any children for asset "+parentassetid);			
		}
		
		if (children.isEmpty()) {
			LOG.debug("Could not find any children for asset "+parentassetid);
		}

		
		return children;		
	}
	
	/**
	 * Uses lucene search api to return the total number of results matching the search criteria
	 * for the given search index.
	 * This method purely returns the total number of results for use in pagination.
	 * Note that it actually performs the query to get the number of results.
	 * @param luceneindex Can be "Global" or any other asset type index
	 * @param assettypes List<String> of asset types to filter.
	 * @param keywords List<String> of keywords. Can be null.
	 * @param sitename String name of site to filter. Can be null.
	 * @param sortattribute Name of attribute to sort on. Can be null.
	 * @param sortasc Boolean for ascending or descending sort order. True if ascending; false otherwise.
	 * @return
	 */
	public int queryLuceneTotal(String luceneindex, List<String> assettypes, List<String> keywords, String sitename, String sortattribute, Boolean sortasc) {
		int totalresults = 0;
		Set<String> filterTypes = new HashSet<String>(); // Build the asset types to filter for
		for (String type : assettypes) {
			filterTypes.add(type);
		}

		try {
			// Get handle to SearchEngine and SearchEngineMedata
			IndexSourceConfig con = SearchUtils.getIndexSourceConfig();
			IndexSourceMetadata metadata = con.getConfiguration(luceneindex);
			String engineName = metadata.getSearchEngineName();
			SearchEngineConfig searchConfig = SearchUtils.getSearchEngineConfig();
			SearchEngine engine = searchConfig.getEngine(engineName);

			// Search only if search term is not empty
			if (keywords != null) {
				// Execute a new search for each keyword and add results to a list
				for (String keyword : keywords) {

					// Build query expression for search term
					QueryExpression q = SearchUtils.newQuery( metadata.getDefaultSearchField(), Operation.CONTAINS, keyword);
					QueryExpression qTypes = null;
					for (String filterType : filterTypes) {
						if (qTypes == null) {
							qTypes = SearchUtils.newQuery("AssetType", Operation.EQUALS, filterType);
						} else {
							qTypes = qTypes.or("AssetType", Operation.EQUALS, filterType);
						}
					}
					// Build query expression for search term contained in listed Asset types and site
					q = q.and(qTypes);
					if (sitename != null) q = q.and("Publist", Operation.CONTAINS, sitename);  // Optional site filter
					if (sortattribute != null) { // Set sort order
						SortOrder so = new SortOrderImpl(sortattribute, sortasc);
						q.setSortOrder( Arrays.<SortOrder> asList( so ) );
					}
					
					// Execute search and retrieve results
					SearchResult<ResultRow> res = engine.search( Collections.singletonList( luceneindex ), q); 
					totalresults += res.getTotal();

				}
			} else { // If there are no keywords, search for all assets by asset type
				
				// Build query expression for search term
				QueryExpression qTypes = null;
				for (String filterType : filterTypes) {
					if (qTypes == null) {
						qTypes = SearchUtils.newQuery("AssetType", Operation.EQUALS, filterType);
					} else {
						qTypes = qTypes.or("AssetType", Operation.EQUALS, filterType);
					}
				}
				if (sitename != null) qTypes = qTypes.and("Publist", Operation.CONTAINS, sitename); // Optional site filter
				if (sortattribute != null) { // Set sort order
					SortOrder so = new SortOrderImpl(sortattribute, sortasc);
					qTypes.setSortOrder( Arrays.<SortOrder> asList( so ) );
				}
				
				// Execute search and retrieve results
				SearchResult<ResultRow> res = engine.search( Collections.singletonList( luceneindex ), qTypes);
				totalresults += res.getTotal();				
			}
		} catch (SearchEngineException e) {
			LOG.error("There was an error executing the search, with exception: " + e);
		}
		return totalresults;
	}
	
	/**
	 * Uses lucene search api to return a list of AssetIds matching the search criteria
	 * for the given search index.
	 * The maximum results returned is 1000.
	 * @param luceneindex Can be "Global" or any other asset type index
	 * @param assettypes List<String> of asset types to filter.
	 * @param keywords List<String> of keywords. Can be null.
	 * @param sitename String name of site. Can be null.
	 * @param startindex Starting row number (Set to zero if you don't need pagination)
	 * @param maxresults Max results to return (Used for pagination. Set to zero for no limit.)
	 * @param sortattribute Name of attribute to sort on. Can be null.
	 * @param sortasc Boolean for ascending or descending sort order. True if ascending; false otherwise.
	 * @return
	 */
	public List<AssetId> queryLucene(String luceneindex, 
			List<String> assettypes, 
			List<String> keywords, 
			String sitename, 
			int startindex, 
			int maxresults, 
			String sortattribute,
			Boolean sortasc) {
		List<AssetId> results = new ArrayList<AssetId>();
		Set<String> filterTypes = new HashSet<String>(); // Build the asset types to filter for
		for (String type : assettypes) {
			filterTypes.add(type);
		}

		try {
			// Get handle to SearchEngine and SearchEngineMedata
			IndexSourceConfig con = SearchUtils.getIndexSourceConfig();
			IndexSourceMetadata metadata = con.getConfiguration(luceneindex);
			String engineName = metadata.getSearchEngineName();
			SearchEngineConfig searchConfig = SearchUtils.getSearchEngineConfig();
			SearchEngine engine = searchConfig.getEngine(engineName);

			// Search only if search term is not empty
			if (keywords != null) {

				// Execute a new search for each keyword and add results to a list
				for (String keyword : keywords) {

					// Build query expression for search term
					QueryExpression q = SearchUtils.newQuery( metadata.getDefaultSearchField(), Operation.CONTAINS, keyword);
					QueryExpression qTypes = null;
					for (String filterType : filterTypes) {
						if (qTypes == null) {
							qTypes = SearchUtils.newQuery("AssetType", Operation.EQUALS, filterType);
						} else {
							qTypes = qTypes.or("AssetType", Operation.EQUALS, filterType);
						}
					}
					// Build query expression for search term contained in listed Asset types & set max rows
					q = q.and(qTypes);
					if (maxresults > 0) q.setMaxResults(maxresults);
					q.setStartIndex(startindex);
					if (sitename != null) q = q.and("Publist", Operation.CONTAINS, sitename); // Optional site filter
					if (sortattribute != null) { // Set sort order
						SortOrder so = new SortOrderImpl(sortattribute, sortasc);
						q.setSortOrder( Arrays.<SortOrder> asList( so ) );
					}
					
					// Execute search and retrieve results
					SearchResult<ResultRow> res = engine.search( Collections.singletonList( luceneindex ), q);
					for (; res.hasNext();) { // In each row, let's look for the AssetId and type
						ResultRow row = res.next();				
						IndexData idData = row.getIndexData("id");
						IndexData typeData = row.getIndexData("AssetType");
						Long id = Long.parseLong(idData.getData()); // Result's ID, TYPE, and RELEVANCE
						String type = typeData.getData();
						AssetId aid = new AssetIdImpl(type, id);
						if (!results.contains(aid)) { // Build results list
							results.add(aid);
						}
					}

				}
			} else { // If keywords is null, search just by asset type
				
				// Build query expression for search term
				QueryExpression qTypes = null;
				for (String filterType : filterTypes) {
					if (qTypes == null) {
						qTypes = SearchUtils.newQuery("AssetType", Operation.EQUALS, filterType);
					} else {
						qTypes = qTypes.or("AssetType", Operation.EQUALS, filterType);
					}
				}
				// Build query expression for search term contained in listed Asset types & set max rows
				if (maxresults > 0) qTypes.setMaxResults(maxresults);
				qTypes.setStartIndex(startindex);
				if (sitename != null) qTypes = qTypes.and("Publist", Operation.CONTAINS, sitename);  // Optional site filter
				if (sortattribute != null) { // Set sort order
					SortOrder so = new SortOrderImpl(sortattribute, sortasc);
					qTypes.setSortOrder( Arrays.<SortOrder> asList( so ) );
				}
				
				// Execute search and retrieve results
				SearchResult<ResultRow> res = engine.search( Collections.singletonList( luceneindex ), qTypes);
				for (; res.hasNext();) { // In each row, let's look for the AssetId and type
					ResultRow row = res.next();				
					IndexData idData = row.getIndexData("id");
					IndexData typeData = row.getIndexData("AssetType");
					Long id = Long.parseLong(idData.getData()); // Result's ID, TYPE, and RELEVANCE
					String type = typeData.getData();
					AssetId aid = new AssetIdImpl(type, id);
					if (!results.contains(aid)) { // Build results list
						results.add(aid);
					}
				}
			}
		} catch (SearchEngineException e) {
			LOG.error("There was an error executing the search, with exception: " + e);
		}
		return results;
	}
	
	/**
	 * Filters input list for only those assets whose jobid
	 * is the specified job id
	 * @param inputlist List of UIAssetId objects to filter
	 * @param jobid String id of the job in CT
	 * @return List of UIAssetId objects matching criteria
	 */
	public List<ExtendedAssetId> filterByJob(List<ExtendedAssetId> inputlist, String jobid) {
		List<ExtendedAssetId> outlist = new ArrayList<ExtendedAssetId>();
		for (ExtendedAssetId uaid : inputlist) {
			if (jobid.equalsIgnoreCase( uaid.getJobId() ) ) {
				outlist.add(uaid);
			}
		}
		return outlist;
	}
	
	/**
	 * Returns a list of UIAssetId objects in inputlist whose
	 * modifieddate value is AFTER the modifiedsincedate 
	 * @param inputlist List of UIAssetId objects to filter
	 * @param modifiedsincedate The date to search for
	 * @return List of UIAssetId objects
	 */
	public List<ExtendedAssetId> filterByModifiedDate(List<ExtendedAssetId> inputlist, Date modifiedsincedate) {
		List<ExtendedAssetId> outlist = new ArrayList<ExtendedAssetId>();
		for (ExtendedAssetId uaid : inputlist) {
			if ( modifiedsincedate.before( uaid.getUpdatedDate() ) ) {
				outlist.add(uaid);
			}
		}
		return outlist;
	}
	
	/**
	 * Filters the input list for only those assets that have been approved
	 * for the specified publishing destination name
	 * @param inputlist List of UIAssetId objects to filter
	 * @param pubtargetid ID of publishing target destination
	 * @return List of UIAssetId objects
	 */
	public List<ExtendedAssetId> filterOnlyPublishedAssets(List<ExtendedAssetId> inputlist, String pubtargetid) {
		List<ExtendedAssetId> outlist = new ArrayList<ExtendedAssetId>();
		for (ExtendedAssetId uaid : inputlist) {
			if ( uaid.isApproved(ics, pubtargetid) ) {
				outlist.add(uaid);
			}
		}
		return outlist;
	}
	
	/**
	 * Returns the intersection of inputlist and listofassetidstoinclude. That is,
	 * only the assetid objects that are in both lists.
	 * @param inputlist First list to filter.
	 * @param listofassetidstoinclude Second list to filter.
	 * @return A list of assetid objects.
	 */
	public List<AssetId> filterByAssetIdList(List<AssetId> inputlist, List<AssetId> listofassetidstoinclude) {
		List<AssetId> outlist = new ArrayList<AssetId>();
		for (AssetId aid : listofassetidstoinclude) {
			if (inputlist.contains(aid)) {
				outlist.add(aid);
			}
		}
		return outlist;
	}
	
	/**
	 * Removes assetid from inputlist using List.remove()
	 * @param inputlist List of assetids
	 * @param assetid One particular assetid to remove
	 * @return List of assetid objects
	 */
	public List<AssetId> removeFromList(List<AssetId> inputlist, AssetId assetid) {
		inputlist.remove(assetid);
		return inputlist;
	}
	
	
	public void sortList(List<AssetId> inputlist, String sortattribute, Boolean sortasc, int startindex, int maxrecords) {
		
	}

	
}
