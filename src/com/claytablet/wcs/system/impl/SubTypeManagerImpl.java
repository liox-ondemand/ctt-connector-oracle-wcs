package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.claytablet.wcs.system.SubTypeManager;
import com.fatwire.assetapi.def.AssetTypeDef;
import com.fatwire.assetapi.def.AssetTypeDefManager;
import com.fatwire.assetapi.def.AssetTypeDefManagerImpl;
import com.fatwire.cs.core.db.PreparedStmt;
import com.fatwire.cs.core.db.StatementParam;
import com.fatwire.gst.foundation.facade.sql.Row;
import com.fatwire.gst.foundation.facade.sql.SqlHelper;

import COM.FutureTense.Interfaces.ICS;

/**
 * 
 * @author Rick Ptasznik
 *
 */

public class SubTypeManagerImpl implements SubTypeManager {

	protected ICS ics;
	private AssetTypeDefManager defManager;
	
	private static final Log LOG = LogFactory.getLog(SubTypeManagerImpl.class.getPackage().getName());
	
	private static final PreparedStmt SELECT_DEFINITION_TYPES = new PreparedStmt("SELECT assettemplate from FlexAssetTypes where assettype=?", Collections.singletonList("FlexAssetTypes"));
	private static final PreparedStmt SELECT_DEFINITION_TYPES_FOR_PARENT = new PreparedStmt("SELECT assettemplate from FlexGroupTypes where assettype=?", Collections.singletonList("FlexGroupTypes"));
	
	static {
		SELECT_DEFINITION_TYPES.setElement(0, "FlexAssetTypes", "assettype");
		SELECT_DEFINITION_TYPES_FOR_PARENT.setElement(0, "FlexGroupTypes", "assettype");
	}
	
	public SubTypeManagerImpl(ICS ics){
		this.ics = ics;
		if (ics != null){
			this.defManager = new AssetTypeDefManagerImpl(ics);
		}
	}
	
	/**
	 * returns a list of subtypes for a given asset and site
	 * assets of type Page are treated as a special case as we need to query a different table to get those definitions
	 */
	@Override
	public List<AssetTypeDef> getSubTypesForAssetType(String assetType, String pubid) {
		
		List<AssetTypeDef> definitionList = new ArrayList<AssetTypeDef>();
		
		if (assetType.equalsIgnoreCase("Page")){
			// pages are special
			if (this.defManager != null){
				List<String> subtypes = getPageTypesForSite(pubid);
				for (String subtype : subtypes){
					
					try {
						AssetTypeDef definition = defManager.findByName(assetType, subtype);
		                if(definition!= null){
		                	definitionList.add(definition);
						}		                
					} catch (Exception ex){
						LOG.error("Error getting defManager...");
					}
				}
			}
		} else {
			// anything not a page
			if (this.defManager != null){
				List<String> defTypes = getDefinitionTypes(assetType);
				
				if (defTypes != null){
					for(String defType : defTypes){
						List<String> subtypes = getSubTypesForSite(defType,pubid);
						for (String subtype : subtypes){
							
							try {
								AssetTypeDef definition = defManager.findByName(assetType, subtype);
				                if(definition!= null){
				                	definitionList.add(definition);
								}		                
							} catch (Exception ex){
								LOG.error("Error getting defManager...");
							}
						}
					}
				}
			}
		}
		
		return definitionList;
	}
	
	/**
	 * get the asset definitions for a specific asset type and site
	 * @param deftype
	 * @param pubid
	 * @return
	 */
	private List<String> getSubTypesForSite(String deftype, String pubid){
		List<String> subtypes = new ArrayList<String>();
		
		PreparedStmt SELECT_SUBTYPES = new PreparedStmt("select id, name from " + deftype + " where id in (select assetid from assetpublication where pubid = ? and assettype= ?) and status !='VO'", Arrays.asList(deftype, "assetpublication"));
		SELECT_SUBTYPES.setElement(0, "assetpublication", "pubid");
		SELECT_SUBTYPES.setElement(1, "assetpublication", "assettype");
		
		final StatementParam param = SELECT_SUBTYPES.newParam();
		param.setString(0, pubid);
		param.setString(1, deftype);
		
		for(final Row r : SqlHelper.select(ics, SELECT_SUBTYPES, param)) {
			subtypes.add(r.getString("name"));
		}
		
		return subtypes;
	}
	
	/**
	 * get the page definitions for a site
	 * @param pubid
	 * @return
	 */
	private List<String> getPageTypesForSite(String pubid){
		List<String> subtypes = new ArrayList<String>();
		
		PreparedStmt SELECT_PAGETYPES = new PreparedStmt("select * from pagedefinition where id in (select assetid from assetpublication where pubid=? and assettype='PageDefinition') and status !='VO'", Arrays.asList("pagedefinition", "assetpublication"));
		SELECT_PAGETYPES.setElement(0, "assetpublication", "pubid");
		
		final StatementParam param = SELECT_PAGETYPES.newParam();
		param.setString(0, pubid);
		
		for(final Row r : SqlHelper.select(ics, SELECT_PAGETYPES, param)) {
			subtypes.add(r.getString("name"));
		}
		
		return subtypes;
	}

	/**
	 * get all the subtypes for an asset
	 * @param assetType
	 * @return
	 */
	private List<String> getDefinitionTypes(String assetType){
		List<String> deftypes = new ArrayList<String>();
		
		final StatementParam param = SELECT_DEFINITION_TYPES.newParam();
		param.setString(0, assetType);
		 
		for(final Row r : SqlHelper.select(ics, SELECT_DEFINITION_TYPES, param)) {
			deftypes.add(r.getString("assettemplate"));
		}
		
		if (deftypes.size() == 0){
			final StatementParam param2 = SELECT_DEFINITION_TYPES_FOR_PARENT.newParam();
			param2.setString(0, assetType);
			
			for(final Row r : SqlHelper.select(ics, SELECT_DEFINITION_TYPES_FOR_PARENT, param2)) {
				deftypes.add(r.getString("assettemplate"));
			}
		}
		
		return deftypes;
	}
}
