/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.util.DiagnosticUtils;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.assetapi.data.AssetId;
import com.openmarket.xcelerate.asset.AssetIdImpl;

/**
 * @author Mike Field
 *
 */
public class WorkflowManagerImpl implements WorkflowManager {

	protected static final Log LOG = LogFactory.getLog(WorkflowManager.class.getName());
	private ICS ics;
	private ManagerFactorySession session;
	
	public WorkflowManagerImpl(ICS ics, ManagerFactorySession session) {
		this.ics = ics;
		this.session = session;
	}
	
	/* (non-Javadoc)
	 * @see com.claytablet.wcs.system.WorkflowManager#getAllWorkflowProcesses()
	 */
	@Override
	public Map<String, String> getAllWorkflowProcesses() {
		return getAllWorkflowProcessesByAssetTypeAndSite(null, null);
	}

	/* (non-Javadoc)
	 * @see com.claytablet.wcs.system.WorkflowManager#getAllWorkflowProcessesByAssetTypeAndSite(java.lang.String, java.lang.String)
	 */
	@Override
	public Map<String, String> getAllWorkflowProcessesByAssetTypeAndSite(
			String assettype, String pubid) {
		Map<String, String> wfps = new HashMap<String, String>();
		// Call <WORKFLOWENGINE.GETALLPROCESSES>
		FTValList vals = new FTValList();
        vals.put("PREFIX", "wfp");
        if (Utilities.goodString(assettype)) { vals.put("ASSETTYPE", assettype); }
        if (Utilities.goodString(pubid)) { vals.put("PUBID", pubid); }
        ics.ClearErrno();
        ics.runTag("WORKFLOWENGINE.GETALLPROCESSES", vals);        
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag WORKFLOWENGINE.GETALLPROCESSES to get list of workflow processes with error " + errs);
        }	

        String totalwfp = ics.GetVar("wfpTotal");
        int iTotalWfp = 0;
		if (totalwfp != null) {
			iTotalWfp = Integer.parseInt(totalwfp);
		}
		for (int i=0; i<iTotalWfp; i++) {
			String wfpobject = "wfp"+i;
			
	        // Call <WORKFLOWPROCESS.GETID>
			vals = new FTValList();
	        vals.put("NAME", wfpobject);
	        vals.put("VARNAME", "wfpoid");
	        ics.ClearErrno();
	        ics.runTag("WORKFLOWPROCESS.GETID", vals);        
	        errs = ics.GetErrno();
	        if (errs < 0) {
	        	LOG.error("Error running tag WORKFLOWPROCESS.GETID to get workflow process id with error " + errs);
	        }

	        // Call <WORKFLOWPROCESS.GETPROCESSNAME>
			vals = new FTValList();
	        vals.put("NAME", wfpobject);
	        vals.put("VARNAME", "wfponame");
	        ics.ClearErrno();
	        ics.runTag("WORKFLOWPROCESS.GETPROCESSNAME", vals);        
	        errs = ics.GetErrno();
	        if (errs < 0) {
	        	LOG.error("Error running tag WORKFLOWPROCESS.GETPROCESSNAME to get workflow process name with error " + errs);
	        }
	        
	        wfps.put(ics.GetVar("wfpoid"), ics.GetVar("wfponame"));

		}
		
		return wfps;
	}

	public List<String> getStartMenuItemsForSiteAndAssetType(
			String assettype, String pubid) {
		
		List<String> smitems = new ArrayList<String>();
		String listname = "items";
		// Call <STARTMENU.GETMATCHINGSTARTITEMS>
		FTValList vals = new FTValList();
        vals.put("ITEMTYPE", "ContentForm");
        vals.put("LISTVARNAME", listname);
        vals.put("PUBID", pubid);
        ics.ClearErrno();
        ics.runTag("STARTMENU.GETMATCHINGSTARTITEMS", vals);        
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag STARTMENU.GETMATCHINGSTARTITEMS to get list of start menu items with error " + errs);
        }
  
        IList itemlist = ics.GetList(listname); // The resulting list has columns id, assettype, name, description
        if (itemlist != null && itemlist.hasData()) {
        	// Loop through list see if it is included in "assettype"
        	try {
				for (int i=1; i <= itemlist.numRows(); i++)
	            {
					itemlist.moveTo(i);
					String smassettype = itemlist.getValue("assettype");
					String smname = itemlist.getValue("name");
					if (assettype.equalsIgnoreCase( smassettype )) {
						smitems.add(smname);
					}
	            }
			} catch (Exception e) {
				LOG.error("Error looping through results from STARTMENU.GETMATCHINGSTARTITEMS tag with exception: " + e);
			} 
        
        }
        
		return smitems;	
		
		
	}

	public void addAssetToWorkflow(AssetId aid, String pubid, String procid, String assignToUser) {

		LOG.debug("Beginning to add " + aid.toString() + " to workflow process " + procid );
		FTValList vals = new FTValList();
        vals.put("c", aid.getType());
        vals.put("cid", Long.toString(aid.getId()));
        vals.put("pubid", pubid);        
        vals.put("workflowprocid", procid); 
        LOG.warn("assignToUser value is: "+assignToUser);
        if(assignToUser != null)
        {
        	LOG.info("Setting assignToUser to "+assignToUser);
        	vals.put("assignToUser", assignToUser);
        }
        
        ics.ClearErrno();
        ics.CallElement("CustomElements/CT/AddToWorkflow", vals);
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error calling element CustomElements/CT/AddToWorkflow to add asset " + aid.toString() + " to workflowprocid: " + procid + " with error " + errs);
        }
		LOG.debug("Finished adding " + aid.toString() + " to workflow process " + procid );
	}
	
	
	public void addOrAdvanceAssetInWorkflow(AssetId aid, String siteid, String workflowprocid, String assignToUser) {
		
		// Is in this workflow already? If so, advance in workflow
		if ( isInWorkflow(aid, siteid, workflowprocid) ) {
			String nextstepid = getNextStepId(aid, siteid);
			advanceAssetInWorkflow(aid, siteid, nextstepid, assignToUser);
		
		// If not, add to workflow
		} else {

			addAssetToWorkflow(aid, siteid, workflowprocid, assignToUser);
		
		}

	}
	
	public void advanceAssetInWorkflowUnknownNextStep(AssetId aid, String siteid, String assignToUser) {
		
		String nextstepid = getNextStepId(aid, siteid);
		advanceAssetInWorkflow(aid, siteid, nextstepid, assignToUser);
		
	}
	
	public void advanceAssetInWorkflow(AssetId aid, String siteid, String nextstepid, String assignToUser) {
		DiagnosticUtils.trace(LOG, "Advancing asset " + aid.toString() + " in workflow to " + nextstepid);
		String batchuser = ics.GetProperty("xcelerate.batchuser", "futuretense_xcel.ini", true);
		String encryptedpass = ics.GetProperty("xcelerate.batchpass", "futuretense_xcel.ini", true);
		String batchpass = Utilities.decryptString(encryptedpass);
		
		LOG.debug("Advancing asset " + aid.toString() + " in workflow");
		FTValList args = new FTValList();
		args.removeAll();
		args.setValString("id", Long.toString(aid.getId()));
		args.setValString("AssetType", aid.getType());
		args.setValString("pubid", siteid);
		args.setValString("batchuser", batchuser);
		args.setValString("batchpass", batchpass);
		args.setValString("nextstepid", nextstepid);
		
		ics.CallElement("CustomElements/CT/AdvanceAssetInWorkflow", args);
		LOG.debug("Finished advancing " + aid.toString() + " in workflow");
	}
	
	public String getNextStepId(AssetId aid, String siteid) {
		
		ConfigManager cm = new ConfigManagerImpl(ics, session);
		String stepNameReject = cm.getWorkflowStepNameReview();
		String stepNameResend = cm.getWorkflowStepNameResend();
		return getNextStepIdFromChoices(aid, stepNameReject, stepNameResend, siteid);
		
	}
	
	public String getNextStepIdFromChoices(AssetId aid, String stepname1, String stepname2, String siteid) {
		
		FTValList vals = new FTValList();
        vals.put("AssetType", aid.getType());
        vals.put("id", Long.toString(aid.getId()));
        vals.put("pubid", siteid); 
        vals.put("stepname1", stepname1); 
        vals.put("stepname2", stepname2); 
        ics.ClearErrno();
        ics.CallElement("CustomElements/CT/GetNextWorkflowStepIdByName", vals);
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error calling element CustomElements/CT/GetNextWorkflowStepIdByName to find next legal workflow step for the asset " + aid.toString() + " with error " + errs);
        }
        LOG.debug("Found stepid=" + ics.GetVar("stepid"));
        return ics.GetVar("stepid");
	}
	
	public void loginAsBatchUser() {

		String batchuser = ics.GetProperty("xcelerate.batchuser", "futuretense_xcel.ini", true);
		if (batchuser != null && !batchuser.equals(ics.GetSSVar("username"))) {
			
			String encryptedpass = ics.GetProperty("xcelerate.batchpass", "futuretense_xcel.ini", true);
			String batchpass = Utilities.decryptString(encryptedpass);
	
			FTValList vals = new FTValList();
	        vals.put("USERNAME", batchuser);
	        vals.put("PASSWORD", batchpass);
	        ics.ClearErrno();
	        ics.runTag("USER.LOGIN", vals);        
	        int errs = ics.GetErrno();
	        if (errs < 0) {
	        	LOG.error("Error running tag USER.LOGIN to login as the batch user with error " + errs);
	        } else {
	        	LOG.debug("Successfully logged in as batch user (" + batchuser + ")");
	        }
		} else {
        	LOG.debug("Already logged in as batchuser (" + batchuser + "). No need to login again.");
		}
		
	}
	
	public Boolean isInWorkflow(AssetId aid, String siteid, String workflowprocid) {
		
		Boolean isInWorkflow = false;
		FTValList vals = new FTValList();
        vals.put("AssetType", aid.getType());
        vals.put("id", Long.toString(aid.getId()));
        vals.put("pubid", siteid);        
        vals.put("workflowprocid", workflowprocid); 
        ics.ClearErrno();
        ics.CallElement("CustomElements/CT/IsAssetInWorkflow", vals);
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error calling element CustomElements/CT/IsAssetInWorkflow to check if asset " + aid.toString() + " is in workflowprocid: " + workflowprocid + " with error " + errs);
        }
		if (Utilities.goodString(ics.GetVar("isInWorkflow")) && "true".equalsIgnoreCase(ics.GetVar("isInWorkflow"))) {
			isInWorkflow = true;
		}
		LOG.debug("Is asset " + aid.toString() + " in workflow? Answer: " + isInWorkflow.toString());
		return isInWorkflow;
		
	}

}
