/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.CTPlatformException;
import com.claytablet.client.producer.CTPlatformManager;
import com.claytablet.client.producer.ContentCopyBackManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProgressReporter;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.TranslationContent;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.Utils;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.assetapi.common.AssetAccessException;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;
import com.openmarket.xcelerate.asset.AssetIdImpl;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.impl.ProgressReporterImpl;
import com.claytablet.client.producer.impl.TranslationContentImpl;
import com.claytablet.client.util.ListUtils;
import com.claytablet.model.event.metadata.IMetadataGroup;

/**
 * @author Mike Field
 *
 */
public class JobManagerImpl implements JobManager {
	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	protected Session icssession;
	private ManagerFactorySession mfsession;
	protected ICS ics;
	private AssetManagerImpl am;
	private TranslationEntityManager em;
	private WorkflowManager wfm;

	/**
	 * Default constructor
	 */
	public JobManagerImpl(ICS ics, ManagerFactorySession mfsession) { 
		this.ics = ics;
		this.icssession = SessionFactory.getSession(ics);
		this.mfsession = mfsession;
	}
	
	/**
	 * If any of the translation requests is set to "PENDING"
	 * return true.
	 */
	public Boolean hasPendingTRs(TranslationJob job) {
		Boolean retval = false;
		for (TranslationRequest tr : job.getTranslationRequests()) {
			if (TranslationEntity.Status.PENDING.equals(tr.getStatus())) {
				retval = true;
				break;
			}
		}
		return retval;
	}
	
	@Override
	public void sendJob(ProgressReporter pr, TranslationJob job, TranslationEntityManager tem, String vendorid) {
		this.em = tem;
		this.am = new AssetManagerImpl(ics, mfsession, SessionFactory.getSession());
		this.wfm = new WorkflowManagerImpl(ics, mfsession);

		String task = pr.startTask("Send OWCS translation job", "Send out job: " + job.getName());
		pr.addMilestone("createTargetAsset", "Create target assets", 80);
		pr.addMilestone("sendJob", "Send out translaiton job", 20);
		pr.startMilestone("createTargetAsset");
		try {
			List<TranslationRequest> reqs = job.getTranslationRequests();
			pr.setupCurrentMilestone(reqs.size(), 0);
			int processedAssets = 0;
			for (TranslationRequest tr : reqs) {
				if (!pr.reportProgress(1)) {
					LOG.info("Creating target asset for job " + job.getName() + 
							" is interrupted after " + processedAssets + " assets have been processed.");
					pr.completeActiveTask(task, true);
					return;
				}
				if (tr.getSubmittedTranslationContent()==null || tr.getNativeTargetId()==null ||
						!am.assetExists(Utils.stringToAssetId(tr.getNativeTargetId()))) {
					setTargetIdContentAndWorkflow(tr);
				}
				processedAssets++;
				if (processedAssets % 10 == 0) {
					mfsession.commit(false);
				}
			}
	
			pr.startMilestone("sendJob");
			try {
				CTPlatformManager pm = mfsession.getCTPlatformManager();
				pm.setProgressReporter(pr);
				pm.sendJob(job);
				pr.completeActiveTask(task, true);
			} catch (CTPlatformException e) {
				LOG.error("Error sending " + job, e);
				pr.completeActiveTask(task, false);
			}
		} finally {
			if (!pr.isTaskCompleted(task)) {
				pr.completeActiveTask(task, false);
			}
		}
	}
	
	private void setTargetIdContentAndWorkflow(TranslationRequest tr) {
				
		String nativeTargetId = tr.getNativeTargetId();
		String nativeSourceId = tr.getNativeSourceId();
		String[] assetidparts = nativeSourceId.split(":");
		AssetId sourceAssetId = new AssetIdImpl(assetidparts[0], Long.parseLong(assetidparts[1]));

		// Set nativeTargetId if it hasn't already been set
				
		if ( (Utilities.goodString(nativeTargetId) && am.assetExists(Utils.stringToAssetId(nativeTargetId)))
				|| setTargetId(tr, sourceAssetId)) {		
			// Set the source content just before sending the job
			setSourceContent(tr, sourceAssetId);
		}
	}
	
	private boolean setTargetId(TranslationRequest tr, AssetId sourceAssetId) {
		try {
			// Check if there is a WCS asset already for this target
			String nativeTargetId = null;
			List<String> translatedLangs = am.getTranslatedLocales(sourceAssetId);
			String sourceLang = tr.getNativeSourceLanguage();
			String targetLang = tr.getNativeTargetLanguage();
			// If there is a WCS asset for this target language, set nativeTargetId
			// in the TR
			if (translatedLangs.contains(targetLang)) {
				AssetId transAssetId = am.getTranslation(sourceAssetId, targetLang);
				if (transAssetId != null) {
					nativeTargetId = transAssetId.getType() + ":" + Long.toString(transAssetId.getId());
					LOG.debug("The TR " + tr.getId() + " didn't have nativeTargetId set, but the WCS asset " + transAssetId.toString() + " already existed for this target.");
				}
	
				// If no WCS translation exists, create translation stub in WCS and
				// set nativeTargetId
			} else {
				AssetId transAssetId;
					transAssetId = am.createTranslationStub(sourceAssetId, sourceLang, targetLang, true);
					if (transAssetId != null) {
						nativeTargetId = transAssetId.getType() + ":" + Long.toString(transAssetId.getId());
						LOG.debug("Created new WCS asset for " + targetLang + ":" + transAssetId.toString());
					}
			}
			if (Utilities.goodString(nativeTargetId)) {
				tr.setNativeTargetId(nativeTargetId);
				em.save(tr);
				LOG.debug("Set native target id to " + nativeTargetId);
				return true;
			} else {
				LOG.error("Could not set native target id for TR " + tr.getId());
				tr.setError("Could not set native target id");
				em.save(tr);
				return false;
			}
		} catch (Exception e) {
			LOG.error("Cannot create target asset for " + tr, e);
			tr.setError("Cannot create target asset", e);
			em.save(tr);
			return false;
		}
	}
	
	private void setSourceContent(TranslationRequest tr, AssetId sourceAssetId) {
		String siteidstring = tr.getSiteId();
		TranslationContent tc = am.getContentToTranslateIgnoreMods(sourceAssetId, siteidstring);
		tr.setSubmittedSourceContent(tc);
		em.save(tr);
	}

	@Override
	public void copyBackJob(ProgressReporter pr, TranslationJob job, TranslationEntityManager tem) {
		try {
			this.em = tem;
			this.am = new AssetManagerImpl(ics, mfsession, SessionFactory.getSession());
			this.wfm = new WorkflowManagerImpl(ics, mfsession);
			CTPlatformManager pm = mfsession.getCTPlatformManager();
			pm.setProgressReporter(pr);
			pm.copyBackJob(job, new CopyBackManager());
		} catch (CTPlatformException e) {
			LOG.error("Error copying back " + job, e);
		}
	}

	private class CopyBackManager implements ContentCopyBackManager {
		@Override
		public String copySourceToTarget(TranslationRequest tr, boolean allowOverwrite) throws Exception {
			String nativeSourceId = tr.getNativeSourceId();
			String[] assetidparts = nativeSourceId.split(":");
			AssetId sourceAssetId = new AssetIdImpl(assetidparts[0], Long.parseLong(assetidparts[1]));
			
			List<String> translatedLangs = am.getTranslatedLocales(sourceAssetId);
			String sourceLang = tr.getNativeSourceLanguage();
			String targetLang = tr.getNativeTargetLanguage();
			String nativeTargetId = null;
			// If there is a WCS asset for this target language, set nativeTargetId
			// in the TR
	    	ConfigManager cm = new ConfigManagerImpl(ics, mfsession);
			boolean suffixAssetName =  !cm.isEnabledAttribute(tr.getSiteId(), sourceAssetId.getType(), "name");
			
			if (translatedLangs.contains(targetLang)) {
				AssetId transAssetId = am.getTranslation(sourceAssetId, targetLang);
				if (transAssetId != null) {
					nativeTargetId = transAssetId.getType() + ":" + Long.toString(transAssetId.getId());
					if (!allowOverwrite) {
						throw new Exception("Cannot copy back " + tr + " as we cannot overwrite existing target asset (" + nativeTargetId + ")");
					} else {
						am.copyContent(sourceAssetId, transAssetId, tr.getSiteId(), true, suffixAssetName, sourceLang, targetLang);
					}
				}
			} else {
				// If no WCS translation exists, create translation stub in WCS and
				// set nativeTargetId
				AssetId transAssetId = am.createTranslationStub(sourceAssetId, sourceLang, targetLang, suffixAssetName);
				
				if (transAssetId != null) {
					nativeTargetId = transAssetId.getType() + ":" + Long.toString(transAssetId.getId());
					LOG.debug("Created new WCS asset for " + targetLang + ":" + transAssetId.toString());
				}
			}
			return nativeTargetId;
		}
		
	}
}
