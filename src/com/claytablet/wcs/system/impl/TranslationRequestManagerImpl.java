/**
 * 
 */
package com.claytablet.wcs.system.impl;

import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.wcs.system.TranslationRequestManager;

/**
 * @author Mike Field
 *
 */
public class TranslationRequestManagerImpl implements TranslationRequestManager {

	public TranslationEntityManager em;
	private String previewUrlPrefix = "http://clay-tablet.com/preview/";
	
	public TranslationRequestManagerImpl(TranslationEntityManager em) {
		this.em = em;
	}
	
	public TranslationRequest createNewTranslationRequest(String siteId, String nSourceId, String nSourceLang, String nTargetLang, TranslationJob job) {
		TranslationRequest tr = em.createTranslationRequest(nSourceId, nSourceLang, nTargetLang);
		tr.setSourcePreviewUrl(getPreviewUrl(
				tr.getNativeSourceId(), tr.getNativeSourceLanguage()));
		if (job!=null) {
			tr.setTranslationJob(job);
		}
		return tr;
	}
	
	private String getPreviewUrl(String nativeId, String nativeLanguage) {
		return getPreviewUrlPrefix() + nativeId + "/" + nativeLanguage;
	}


	/**
	 * @param previewUrlPrefix the previewUrlPrefix to set
	 */
	public void setPreviewUrlPrefix(String previewUrlPrefix) {
		this.previewUrlPrefix = previewUrlPrefix;
	}


	/**
	 * @return the previewUrlPrefix
	 */
	public String getPreviewUrlPrefix() {
		return previewUrlPrefix;
	}	
	
}
