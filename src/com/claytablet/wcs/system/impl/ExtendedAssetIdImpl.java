/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import com.fatwire.mda.Dimension;
import com.fatwire.mda.DimensionableAssetManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

import com.claytablet.wcs.system.ExtendedAssetId;
import com.fatwire.assetapi.common.AssetAccessException;
import com.fatwire.assetapi.data.AssetData;
import com.fatwire.assetapi.data.AssetDataManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;

/**
 * @author Mike Field
 *
 */
public class ExtendedAssetIdImpl implements ExtendedAssetId {

	protected static final Log LOG = LogFactory.getLog(ExtendedAssetId.class.getName());

	// When adding a new attribute/field to this object, 
	// be sure to add a public field AND add the attribute name to the
	// string array called wcsattributenames.
	private AssetId assetid;
	private long id;
	private String assettype;
	private String jobid;
	private String name;
	private String description;
	private String createdby;
	private Date createddate;
	private String updatedby;
	private Date updateddate;
	private String locale;
	private ICS ics;
	private String[] wcsattributenames = { "name", "description", 
				"createdby", "createddate", "updatedby", "updateddate" };
	
	
	/**
	 * Default constructor. Calls getWCSAttributes(), getPublishHistory() and getCTAttributes() to 
	 * set all the relevant values for the given AssetId.
	 * @param aid AssetId object who attributes should be loaded.
	 */
	public ExtendedAssetIdImpl(ICS ics, AssetId aid) {
		this.setAssetId(aid);
		this.setId(getAssetId().getId());
		this.setAssetType(getAssetId().getType());
		this.setICS(ics);
		_getWCSAttributes();
		_getCTAttributes();
	}
	
	/**
	 * Uses AssetAPI to load core attribute values.
	 */
	private void _getWCSAttributes() {
		try {
			Session ses = SessionFactory.getSession();
	        AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
            AssetData data = adm.readAttributes( getAssetId(), Arrays.asList( wcsattributenames ) );
            setName(data.getAttributeData("name").getData().toString());
            setDescription((data.getAttributeData("description") != null && data.getAttributeData("description").getData() != null) ? data.getAttributeData("description").getData().toString() : "");
            setCreatedBy((data.getAttributeData("createdby") != null && data.getAttributeData("createdby").getData() != null) ? data.getAttributeData("createdby").getData().toString() : "");
            setUpdatedBy((data.getAttributeData("updatedby") != null && data.getAttributeData("updatedby").getData() != null) ? data.getAttributeData("updatedby").getData().toString() : "");
            setCreatedDate((data.getAttributeData("createddate") != null && data.getAttributeData("createddate").getData() != null) ? (Date)data.getAttributeData("createddate").getData() : null);
            setUpdatedDate((data.getAttributeData("updateddate") != null && data.getAttributeData("updateddate").getData() != null) ? (Date)data.getAttributeData("updateddate").getData() : null);
            setLocale( retrieveLocale() );
            
		} catch (AssetAccessException e) {
        	LOG.error("Error getting WCS attributes for asset " + getAssetId().toString() + " with exception: " + e);
        }		
	}
	
	/**
	 * Runs the ApprovedAssets.GetAssetStatus XML tag using the syntax 
	 * <ApprovedAssets.GetAssetStatus TARGET="Variables.target" ID="Variables.assetid" TYPE="Variables.assettype" LISTVARNAME="approvedList"/>
	 * An example of this tag is in the element OpenMarket/Xcelerate/Actions/AssetMgt/ApprovalStatus
	 * This method determines if the asset has been approved for the specified pubtargetid.
	 * First loads the pubtargetid then loads a handle onto the asset.
	 * @param ics The current ICS context
	 * @param pubtargetid ID of the publish destination
	 * @return True if it has been approved, false otherwise.
	 */
	public Boolean isApproved(ICS ics, String pubtargetid) {
		Boolean isApproved = false;
		
		// Load list of destinations this asset has been approved for
		FTValList vals = new FTValList();
        vals.put("TARGET", pubtargetid);
        vals.put("ID", assetid);
        vals.put("TYPE", assettype);
        vals.put("LISTVARNAME", "approvedlist"); // Returns a list with columns named "status", "fstate", "reason", "locked"
        ics.ClearErrno();
        ics.runTag("ApprovedAssets.GetAssetStatus", vals);        
        
        int errs = ics.GetErrno();
        if (errs < 0) {
        	LOG.error("Error running tag ApprovedAssets.GetAssetStatus to get approval status for assetid " + assetid.toString() + " with error " + errs);
        } else {
        	IList approvallist = ics.GetList("approvedlist");
        	if (approvallist != null && approvallist.hasData()) {
    			try {
    				for (int i=1; i <= approvallist.numRows(); i++)
    	            {
    					approvallist.moveTo(i);
    					/** Approval status can be "A" for approved, "C" for changed, 
    					 * "H" for held and "M" for unclear/may be held or approved
    					 * (due to dependent asset changes in dependencies)
    					 */
    					if ("A".equalsIgnoreCase( approvallist.getValue("status") )) {
    						isApproved = true;
    						break;
    					}
    	            }
    			} catch (NoSuchFieldException e) {
    				LOG.error("Error understanding the response from ApprovedAssets.GetAssetStatus tag for asset " + assetid.toString() + " with exception: " + e);
    			}
    		}
        }			
		return isApproved;
	}
	
	/**
	 * Uses CT connector to load CT attribute values.
	 * TODO Complete this
	 */
	private void _getCTAttributes() {
		this.setJobId(null);
	}

	/**
	 * Uses DimensionManager to load the locale of the current assetid
	 * @return String of the locale (e.g. fr_CA)
	 */
    private String retrieveLocale() {
    	if (!LocaleManagerImpl.isDimsEnabledForType(ics, this.assettype)) {
    		return null;
    	}
    	
        Collection<Dimension> dims;
        try {
            Session session = SessionFactory.getSession(getICS());
            DimensionableAssetManager dam = (DimensionableAssetManager) session.getManager(DimensionableAssetManager.class.getName());
            dims = dam.getDimensionsForAsset(assetid);
        } catch (Exception e) {
            LOG.debug("Exception locating dimensions for asset " + assetid + ".  Returning null.", e);
            return null;
        }

        // each asset can have only one dimension of subtype locale, which is all we care about.
        for (Dimension dim : dims) {
            if ("locale".equalsIgnoreCase(dim.getGroup())) { // ignore case in case of typos setting up dimensions
                String locale = dim.getName();
                if (LOG.isTraceEnabled()) {
                    LOG.trace("Found locale " + locale + " for asset " + assetid);
                }
                return locale;
            }
        }

        // we don't usually expect to ever find asset with dimensions that aren't locales on a multilingual website
        // mostly because the concept of a non-locale dimension is not officially supported, but tell the user anyway.
        LOG.debug("Found dimensions for asset " + assetid + " but none of them was a locale.  Dimensions: " + dims);
        return null;
    }

    /**
	 * @param assetid the assetid to set
	 */
	public void setAssetId(AssetId assetid) {
		this.assetid = assetid;
	}

	/**
	 * @return the assetid
	 */
	public AssetId getAssetId() {
		return assetid;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param assettype the assettype to set
	 */
	public void setAssetType(String assettype) {
		this.assettype = assettype;
	}

	/**
	 * @return the assettype
	 */
	public String getAssetType() {
		return assettype;
	}

	/**
	 * @param jobid the jobid to set
	 */
	public void setJobId(String jobid) {
		this.jobid = jobid;
	}

	/**
	 * @return the jobid
	 */
	public String getJobId() {
		return jobid;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param createdby the createdby to set
	 */
	public void setCreatedBy(String createdby) {
		this.createdby = createdby;
	}

	/**
	 * @return the createdby
	 */
	public String getCreatedBy() {
		return createdby;
	}

	/**
	 * @param createddate the createddate to set
	 */
	public void setCreatedDate(Date createddate) {
		this.createddate = createddate;
	}

	/**
	 * @return the createddate
	 */
	public Date getCreatedDate() {
		return createddate;
	}

	/**
	 * @param modifiedby the modifiedby to set
	 */
	public void setUpdatedBy(String modifiedby) {
		this.updatedby = modifiedby;
	}

	/**
	 * @return the modifiedby
	 */
	public String getUpdatedBy() {
		return updatedby;
	}

	/**
	 * @param modifieddate the modifieddate to set
	 */
	public void setUpdatedDate(Date modifieddate) {
		this.updateddate = modifieddate;
	}

	/**
	 * @return the modifieddate
	 */
	public Date getUpdatedDate() {
		return updateddate;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param ics the ics to set
	 */
	public void setICS(ICS ics) {
		this.ics = ics;
	}

	/**
	 * @return the ics
	 */
	public ICS getICS() {
		return ics;
	}
	
}
