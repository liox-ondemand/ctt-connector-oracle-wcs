/**
 * 
 */
package com.claytablet.wcs.system.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TargetContentManager;
import com.claytablet.client.producer.TranslationContent;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ContentUpdateException;
import com.claytablet.wcs.system.Utils;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.system.Session;

/**
 * @author Mike Field
 *
 */
public class ContentManagerImpl implements TargetContentManager {
	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	protected ICS ics;
	private String username;
	private ManagerFactorySession session;
	private AssetManagerImpl assetMgr;

	public ContentManagerImpl(ICS ics, String username, ManagerFactorySession session, Session wcsSession) {
		this.ics=ics;		
		this.username = username;
		this.session = session;
		LOG.debug("Created ContentManagerImpl successfully");
		this.assetMgr = new AssetManagerImpl(ics, session, wcsSession);
	}
	
	/* (non-Javadoc)
	 * @see com.claytablet.client.producer.ContentManager#updateContent(java.lang.String, com.claytablet.client.producer.TranslationContent)
	 */
	@Override
	public void updateTargetContent(TranslationRequest tr, TranslationContent content) throws ContentUpdateException {
 
		String siteId = tr.getSiteId();
		String nativeId = tr.getNativeTargetId();
		String targetWorkflowId = tr.getTargetWorkflowId();
		
		AssetId aid = Utils.stringToAssetId(nativeId);
		LOG.debug("Called ContentManagerImpl.updateContent for assetid " + aid.toString());
		LOG.debug("Content to update is: " + content.toString());
		LOG.debug("Updating content by username=" + this.username);
		assetMgr.updateAssetFromTranslation(aid, content, this.username);
		
		if (Utilities.goodString(targetWorkflowId)) { 
			WorkflowManager wm = new WorkflowManagerImpl(ics, session);
			wm.advanceAssetInWorkflowUnknownNextStep(aid, siteId, tr.getCreatedUser());
		}
		
	}
	

}
