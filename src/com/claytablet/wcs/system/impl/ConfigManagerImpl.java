/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.UserContext;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.SiteManager;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;
import com.claytablet.client.producer.ConfigurationKey.Factory;

/**
 * @author Mike Field
 *
 */
public class ConfigManagerImpl implements ConfigManager {

	private static final String TYPE_ATTR_SEP = ":";
	private static final String VALUES_SEP = ",";
	protected static final Log LOG = LogFactory.getLog(ConfigManager.class.getName());
	protected Session ses;
	protected ICS ics;
	private UserContext ctx;
	private ManagerFactorySession session;
	private static ConfigurationKey<String> WCS_ENABLED_ATTRIBUTES = 
			Factory.getKeyBuilder("WCS.enabledAttributes", "").setHidden().key();
	public static ConfigurationKey<Integer> WCS_MAX_ITEMS_PER_PAGE = 
			Factory.getKeyBuilder("WCS.maxItemsPerPage", 10).setLabel("Default items per page").
					setDescription("Maximum number of table items to show per page").setSection("UI Configuration").
					setValueOptions(new Integer[]{10, 20, 50, 100}, true).key();
	private static ConfigurationKey<String> WCS_WORKFLOW_STEPNAME_RESEND = 
			Factory.getKeyBuilder("WCS.workflowStepnameResend", "CT: Resend for Translation").setLabel("Resend workflow step name").
					setDescription("Name of workflow step name for resending asset").setSection("Workflow Configuration").key();
	private static ConfigurationKey<String> WCS_WORKFLOW_STEPNAME_REVIEW = 
			Factory.getKeyBuilder("WCS.workflowStepnameReview", "CT: Send for Approval").setLabel("Review workflow step name").
					setDescription("Name of workflow step name for reviewing asset").setSection("Workflow Configuration").key();

	public static ConfigurationKey<Integer> WCS_BACKGROUND_PROCESS_INTERVAL = 
			Factory.getKeyBuilder("WCS.backgroundProcessInterval", 0).setLabel("CT background process interval").
					setDescription("Interval in seconds at which the connector processes jobs in the background to send to the Clay Tablet platform. Set to 0 to stop sending jobs.").
					setSection(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL.getSection()).setUnit("seconds").
					setDefault(60).setValueOptions(new Integer[]{0, 30, 60, 120, 300, 600, 1800, 3600}, true).key();

	public static ConfigurationKey<Integer> WCS_BACKGROUND_PROCESS_TIMEOUT = 
			Factory.getKeyBuilder("WCS.backgroundThreadsTimeout", 0).setLabel("CT background threads timeout").
					setDescription("Timeout in seconds that the connector background thread can work before it must break and return control to WCS system. "
							+ "These background threads process job sending and CT platform messages. Set to 0 to allow background threads to work without breaking.").
					setSection(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL.getSection()).setUnit("seconds").
					setDefault(0).key();
	
	public static ConfigurationKey<Integer> WCS_BACKGROUND_PROCESS_BREAK_INTERVAL = 
			Factory.getKeyBuilder("WCS.backgroundThreadsBreakInterval", 0).setLabel("CT background threads break interval").
					setDescription("Break time in seconds that the connector background thread must wait before start processing again "
							+ "if previous execution time exceeded the \"CT background threads timeout\" setting. "
							+ "These background threads process job sending and CT platform messages. Set to 0 if there should be no break interval.").
					setSection(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL.getSection()).setUnit("seconds").
					setDefault(0).key();
	
	public static ConfigurationKey<Boolean> WCS_PREVENT_MULTITHREAD_JOB_PROCESSING = 
			Factory.getKeyBuilder("WCS.preventMultiThreadJobProcessing", true).setLabel("CT prevent multithread job processing").
					setDescription("Use extra locking to prevent multithread job processing").setHidden().
					setSection(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL.getSection()).setDefault(true).key();
	
	static { 
		// override CT default configuration keys to provide alternative metadata 
		ConfigurationKey.Factory.getOverrideKeyBuilder(ProducerCommonConfigs.CT_MAX_REQS_PER_ASSET).
				setLabel("Assets per file").
				setDescription("Maximum assets each file sent out for translation can contains").key();

		ConfigurationKey.Factory.getOverrideKeyBuilder(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL).
				setDefault(60).setValueOptions(new Integer[]{0, 30, 60, 120, 300, 600, 1800, 3600}, true).key();
	}
	/**
	 * Default constructor
	 */
	public ConfigManagerImpl(ICS ics, ManagerFactorySession session) { 
		this.ics = ics;
		this.ses = SessionFactory.getSession(ics);
		this.session = session;
		this.ctx = new UserContextImpl(ics);
	}
	
	
	@Override
	public Boolean isEnabledAssetType(String siteid, String assettype) {
		return !getEnabledAttributes(siteid, assettype).isEmpty();
	}

	@Override
	public Boolean isEnabledSite(String siteid) {
		ManagerFactorySession sudoSession = null;
		try {
			sudoSession = sudoIfNecessary(siteid);
			String configvalue = sudoSession.getConfigurationManager().get(WCS_ENABLED_ATTRIBUTES);
			return !StringUtils.isEmpty(configvalue);
		} catch (Exception e) {
			LOG.debug("Cannot check if site " + siteid + " is enabled for translation, assume not", e);
			return false;
		} finally {
			if (sudoSession!=session && sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	@Override
	public List<String> getEnabledAttributes(String siteid, String assetType) {
		ManagerFactorySession sudoSession = sudoIfNecessary(siteid);
		try {
			String configValue = sudoSession.getConfigurationManager().get(WCS_ENABLED_ATTRIBUTES);
			Map<String, List<String>> resultMap = convertStringToMap(configValue);
			if (resultMap.containsKey(assetType)) {
				return resultMap.get(assetType);
			} else if (isAllAttributesEnabledByDefault(resultMap)) {
				AssetManagerImpl am = new AssetManagerImpl(ics, sudoSession, SessionFactory.getSession());
				return am.getTranslatableAttributes(assetType);
			} else {
				return Collections.emptyList();
			}
		} finally {
			if (sudoSession!=session && sudoSession!=null) {
				sudoSession.close();
			}
		}
	}


	private ManagerFactorySession sudoIfNecessary(String siteid) {
		if (StringUtils.equals(session.getSiteId(), siteid)) {
			return session;
		} else {
			return session.sudo(siteid);
		}
	}


	private boolean isAllAttributesEnabledByDefault(Map<String, List<String>> resultMap) {
		return resultMap.containsKey(ALL_ASSETTYPES_KEY) && 
				resultMap.get(ALL_ASSETTYPES_KEY).contains(ALL_ATTRIBUTES_KEY);
	}

	@Override
	public Boolean isEnabledAttribute(String siteid, String assettype, String attribute) {
		List<String> enabledAttributes = getEnabledAttributes(siteid, assettype);
		return enabledAttributes.contains(attribute) || enabledAttributes.contains(ALL_ATTRIBUTES_KEY);
	}

	@Override
	public void setEnabledAttributes(String siteid, String assettype,
			List<String> enabledAttributes) {
		ManagerFactorySession sudoSession = sudoIfNecessary(siteid);
		try {
			ConfigurationManager cm = sudoSession.getConfigurationManager();
			String configValue = cm.get(WCS_ENABLED_ATTRIBUTES);
			Map<String, List<String>> resultMap = convertStringToMap(configValue);
			if (!enabledAttributes.isEmpty() ||
					isAllAttributesEnabledByDefault(resultMap)) {
				resultMap.put(assettype, enabledAttributes);
			} else {
				resultMap.remove(assettype);
			}
			cm.getConfigurationSet().put(WCS_ENABLED_ATTRIBUTES, convertMapToString(resultMap));
		} finally {
			if (sudoSession!=session && sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	@Override
	public Boolean isAllAttributesEnabledByDefault(String siteid) {
		ManagerFactorySession sudoSession = sudoIfNecessary(siteid);
		try {
			String configValue = sudoSession.getConfigurationManager().get(WCS_ENABLED_ATTRIBUTES);
			Map<String, List<String>> resultMap = convertStringToMap(configValue);
			return isAllAttributesEnabledByDefault(resultMap);
		} finally {
			if (sudoSession!=session && sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	@Override
	public Boolean isAllAttributesEnabledByDefault(String siteid, String assetType) {
		ManagerFactorySession sudoSession = sudoIfNecessary(siteid);
		try {
			ConfigurationManager cm = sudoSession.getConfigurationManager();
			String configValue = cm.get(WCS_ENABLED_ATTRIBUTES);
			Map<String, List<String>> resultMap = convertStringToMap(configValue);
			return !resultMap.containsKey(assetType) && isAllAttributesEnabledByDefault(resultMap);
		} finally {
			if (sudoSession!=session && sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	@Override
	public void setEnabledByDefault(String siteid, boolean enabled) {
		ManagerFactorySession sudoSession = sudoIfNecessary(siteid);
		try {
			ConfigurationManager cm = sudoSession.getConfigurationManager();
			String configValue = cm.get(WCS_ENABLED_ATTRIBUTES);
			Map<String, List<String>> resultMap = convertStringToMap(configValue);
			if (enabled) {
				resultMap.put(ALL_ASSETTYPES_KEY, Collections.singletonList(ALL_ATTRIBUTES_KEY));
			} else {
				resultMap.remove(ALL_ASSETTYPES_KEY);
			}
			cm.getConfigurationSet().put(WCS_ENABLED_ATTRIBUTES, convertMapToString(resultMap));
		} finally {
			if (sudoSession!=session && sudoSession!=null) {
				sudoSession.close();
			}
		}
	}
	
	@Override
	public void setEnabledByDefault(String siteid, String assetType) {
		ManagerFactorySession sudoSession = sudoIfNecessary(siteid);
		try {
			ConfigurationManager cm = sudoSession.getConfigurationManager();
			String configValue = cm.get(WCS_ENABLED_ATTRIBUTES);
			Map<String, List<String>> resultMap = convertStringToMap(configValue);
			if (isAllAttributesEnabledByDefault(resultMap)) {
				resultMap.remove(assetType);
				cm.getConfigurationSet().put(WCS_ENABLED_ATTRIBUTES, convertMapToString(resultMap));
			} else {
				throw new IllegalStateException("EnabledByDefault flag is not set for site: " + siteid);
			}
		} finally {
			if (sudoSession!=session && sudoSession!=null) {
				sudoSession.close();
			}
		}
	}
	
	private String convertMapToString(Map<String, List<String>> resultMap) {
		StringBuffer buf = new StringBuffer();
		for (Map.Entry<String, List<String>> entry : resultMap.entrySet()) {
			List<String> value = entry.getValue();
			if (value.isEmpty()) {
				buf.append(entry.getKey()).append(TYPE_ATTR_SEP).append(VALUES_SEP);
			} else {
				for (String attr : value) {
					buf.append(entry.getKey()).append(TYPE_ATTR_SEP).append(attr).append(VALUES_SEP);
				}
			}
		}
		return buf.toString();
	}


	private Map<String, List<String>> convertStringToMap(String s) {
		Map<String, List<String>> resultMap = new HashMap<String, List<String>>();
		if (Utilities.goodString(s) && !StringUtils.isEmpty(s.trim())) {
			String[] parts = s.split(VALUES_SEP);
			for (String part : parts) {
				if (part.trim().equals(TYPE_ATTR_SEP)) {
					// enable all attributes by default
					addToMap(resultMap, ALL_ASSETTYPES_KEY, ALL_ATTRIBUTES_KEY);
				} else {
					String subParts[] = part.split(TYPE_ATTR_SEP);
					if (subParts.length==1) {
						addToMap(resultMap, subParts[0], null);
					} else if (subParts.length>1){
						addToMap(resultMap, subParts[0], subParts[1]);
					}
				}
			}
		}
		return resultMap;
	}
	
	private void addToMap(Map<String, List<String>> resultMap, String assetType, String attribute) {
		List<String> attrList = resultMap.get(assetType);
		if (attrList==null) {
			attrList = new ArrayList<String>();
			resultMap.put(assetType, attrList);
		}
		if (attribute!=null && !attrList.contains(attribute) && 
				(!assetType.equals(ALL_ASSETTYPES_KEY) || !attrList.contains(ALL_ATTRIBUTES_KEY))) {
			attrList.add(attribute);
		}
	}

	@Override
	public int getMaxItemsPerPage() {
		return session.getConfigurationManager().get(WCS_MAX_ITEMS_PER_PAGE);
	}

	@Override
	public void setMaxItemsPerPage(int maxitemsperpage) {
		session.getConfigurationManager().getGlobalConfigurationSet().put(WCS_MAX_ITEMS_PER_PAGE, maxitemsperpage);
	}

	@Override
	public String getWorkflowStepNameResend() {
		return session.getConfigurationManager().get(WCS_WORKFLOW_STEPNAME_RESEND);
	}

	@Override
	public void setWorkflowStepNameResend(String stepname) {

		session.getConfigurationManager().getGlobalConfigurationSet().put(WCS_WORKFLOW_STEPNAME_RESEND, stepname);

	}

	@Override
	public String getWorkflowStepNameReview() {
		return session.getConfigurationManager().get(WCS_WORKFLOW_STEPNAME_REVIEW);
	}

	@Override
	public void setWorkflowStepNameReview(String stepname) {

		session.getConfigurationManager().getGlobalConfigurationSet().put(WCS_WORKFLOW_STEPNAME_REVIEW, stepname);

	}
	
	public int getCTPollInterval(){
		return session.getConfigurationManager().get(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL);
	}


	public void initPoReferenceConfig() {
		ConfigurationManager cm = session.getConfigurationManager();
		
		 if (!cm.containsKey(FreewayAnalysisCodeConfig.WCS_FREEWAY_ANALYSISCODE_CONFIGS)) {
			 FreewayAnalysisCodeConfig cfg = new FreewayAnalysisCodeConfig(
					 "Priority", "Job Priority", "Medium", true);
			 cfg.addValue("High", "High priority job");
			 cfg.addValue("Medium", "Medium priority job");
			 cfg.addValue("Low", "Low priority job");
			 FreewayAnalysisCodeConfig.saveConfig(cm, Collections.singletonList(cfg));
		 }
	}
}
