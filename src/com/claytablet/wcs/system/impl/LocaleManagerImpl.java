/**
 * 
 */
package com.claytablet.wcs.system.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ConfigurationSet;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.ConfigurationKey.Factory;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.LocaleManager;
import com.fatwire.assetapi.common.AssetAccessException;
import com.fatwire.assetapi.common.AssetNotExistException;
import com.fatwire.assetapi.data.AssetData;
import com.fatwire.assetapi.data.AssetDataManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.assetapi.data.AttributeData;
import com.fatwire.assetapi.data.MutableAssetData;
import com.fatwire.assetapi.query.Condition;
import com.fatwire.assetapi.query.ConditionFactory;
import com.fatwire.assetapi.query.OpTypeEnum;
import com.fatwire.assetapi.query.Query;
import com.fatwire.assetapi.query.SimpleQuery;
import com.fatwire.mda.Dimension;
import com.fatwire.mda.DimensionManager;
import com.fatwire.mda.DimensionableAssetInstance.DimensionParentRelationship;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;
import com.openmarket.xcelerate.asset.AssetIdImpl;
import com.openmarket.xcelerate.common.DimParentRelationshipImpl;

/**
 * @author Mike Field
 *
 */
public class LocaleManagerImpl implements LocaleManager {

	private Log LOG = LogFactory.getLog(getClass().getName()  + ":" + Thread.currentThread().getId());
	protected Session ses;
	protected ICS ics;
	private ManagerFactorySession session;
	private ConfigurationManager cm;
	
	public static ConfigurationKey<List<String>> WCS_HIDDEN_LOCALES = 
			Factory.getKeyBuilder("WCS.HiddenLocales", Collections.<String>emptyList()).setHidden().
					setLabel("Hidden Locales").setDescription("Locales that are hidden by default.").key();
	
	/**
	 * Default constructor
	 */
	public LocaleManagerImpl(ICS ics, ManagerFactorySession session) { 
		this.ics = ics;
		this.ses = SessionFactory.getSession(ics);
		this.session = session;
		this.cm = session.getConfigurationManager();
	}
	
	public List<String> getWCSLocalesForSite(String sitename) {
		List<String> locales = getAllWCSLocales();
		List<String> results = new ArrayList<String>(locales.size());
		
		for (String locale : locales) {
			if (getSharedSitesForLocale(locale).contains(sitename)) {
				results.add(locale);
			}
		}
		return results;
	}
	
	/**
	 * Loads all locales currently in WCS using getAllWCSLocalesAndIds
	 * @return A list of locale names (e.g. en_US) as strings
	 */
	public List<String> getAllWCSLocales() {
		List<String> locales = new ArrayList<String>();
		Map<String, String> mLocales = getAllWCSLocalesAndIds();
		Iterator<Entry<String, String>> iterator = mLocales.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<String,String> pairs = (Map.Entry<String,String>)iterator.next();
			locales.add(pairs.getValue());
		}
		return locales;
	}
	
	/**
	 * Loads all locales currently in WCS using the ASSET.LIST tag.
	 * @return A map of ids (e.g. 1231231231230) and locale names (e.g. en_US) as strings
	 */
	public Map<String, String> getAllWCSLocalesAndIds() {
		Map<String, String> locales = new HashMap<String, String>();
		// Call <ASSET.LIST>
		String listname = "thisasset";
		FTValList vals = new FTValList();
		vals.put("LIST", listname);
		vals.put("TYPE", "Dimension");
		vals.put("EXCLUDEVOIDED", "true");
		ics.ClearErrno();
		ics.runTag("ASSET.LIST", vals);
		int errs = ics.GetErrno();
		if (errs < 0) {
			LOG.error("Error running tag ASSET.LIST to get list of locales, with error "
					+ errs);
		}
		if (ics.GetList(listname) != null && ics.GetList(listname).hasData()) {
			IList localelist = ics.GetList(listname);
			try {
				AssetManager am = new AssetManagerImpl(ics, session, SessionFactory.getSession());
				for (int i = 1; i <= localelist.numRows(); i++) {
					localelist.moveTo(i);
					// Before adding this Dimension to the list,
					// check its subtype is "Locale"
					String id = localelist.getValue("id");
					AssetId thisDim = new AssetIdImpl("Dimension", Long.parseLong(id));
					if ("Locale".equalsIgnoreCase( am.getSubtype(thisDim) )) {
						locales.put(localelist.getValue("id"), localelist.getValue("name"));
					}
				}
			} catch (NoSuchFieldException e) {
				LOG.error("Error getting list of locales with exception: " + e);
			}
		}
		return locales;
	}
	
	@Override
	public Map<String, String> getAllWCSLocalesAndDescs() {
		Map<String, String> locales = new HashMap<String, String>();
		// Call <ASSET.LIST>
		String listname = "thisasset";
		FTValList vals = new FTValList();
		vals.put("LIST", listname);
		vals.put("TYPE", "Dimension");
		vals.put("EXCLUDEVOIDED", "true");
		ics.ClearErrno();
		ics.runTag("ASSET.LIST", vals);
		int errs = ics.GetErrno();
		if (errs < 0) {
			LOG.error("Error running tag ASSET.LIST to get list of locales, with error "
					+ errs);
		}
		if (ics.GetList(listname) != null && ics.GetList(listname).hasData()) {
			IList localelist = ics.GetList(listname);
			try {
				AssetManager am = new AssetManagerImpl(ics, session, SessionFactory.getSession());
				for (int i = 1; i <= localelist.numRows(); i++) {
					localelist.moveTo(i);
					// Before adding this Dimension to the list,
					// check its subtype is "Locale"
					String id = localelist.getValue("id");
					AssetId thisDim = new AssetIdImpl("Dimension", Long.parseLong(id));
					if ("Locale".equalsIgnoreCase( am.getSubtype(thisDim) )) {
						locales.put(localelist.getValue("name"), localelist.getValue("description"));
					}
				}
			} catch (NoSuchFieldException e) {
				LOG.error("Error getting list of locales with exception: " + e);
			}
		}
		return locales;
	}

	@Override
	public Map<String,String> getWCSLocalesAndDescsForSite(String sitename) {
		Map<String, String> locales = getAllWCSLocalesAndDescs();
		Map<String, String> results = new HashMap<String, String>(locales.size()*2);
		
		for (Map.Entry<String, String> localeEntry : locales.entrySet()) {
			if (getSharedSitesForLocale(localeEntry.getKey()).contains(sitename)) {
				results.put(localeEntry.getKey(), localeEntry.getValue());
			}
		}
		return results;
	}
	
	/**
	 * For each asset in assetlist, calls setLocaleForAsset()
	 * @param assetlist List of AssetId objects
	 * @param locale String value of locale, e.g. "en_US"
	 * @param makeMaster Boolean Makes each asset the master asset in their own translation set
	 */
	public void setLocaleForAssets(List<AssetId> assetlist, String locale, Boolean makeMaster) {
		for (AssetId aid : assetlist) {
			setLocaleForAsset(aid, locale, makeMaster);
		}
	}
	
	/**
	 * Sets locale for the current asset. Optionally sets this asset as the master.
	 * @param assetid AssetId of the current asset
	 * @param locale String representation of the locale (e.g. en_US)
	 * @param makeMaster Boolean Makes the current asset the master asset in the translation set
	 */
	public void setLocaleForAsset(AssetId assetid, String locale, Boolean makeMaster) {
		try {
	        AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
	        Iterable<AssetData> assets = adm.read(Arrays.<AssetId> asList( assetid ));
			List<AssetData> sAssets = new ArrayList<AssetData>();
			for (AssetData a : assets) {
				sAssets.add(a);
				DimensionManager dam = (DimensionManager) ses.getManager(DimensionManager.class.getName());
				Dimension dim = dam.loadDimension(locale);
				a.getAttributeData("Dimension").setData( Arrays.asList(new Dimension[] { dim }) );
				if (makeMaster) {
					DimensionParentRelationship dpr = new DimParentRelationshipImpl( "Locale", a.getAssetId() );
					a.getAttributeData("Dimension-parent").setData( Arrays.asList(new DimensionParentRelationship[] { dpr }));
				}
			}
			adm.update( sAssets );
		} catch (AssetAccessException e) {
        	LOG.error("Error adding locale " + locale + " to asset " + assetid.toString() + " with exception: " + e);
        }
	}
	
	
	/**
	 * Create a new locale and share it to the given sites
	 * @param localename Name of new locale (e.g. en_US)
	 * @param sitenames List of site names (not their ids). Can be a singleton list, e.g. Arrays.asList( "FirstSiteII" )
	 * @return True if no error, otherwise false. Exception written to log.
	 */
	public Boolean addLocaleAndShareToSites(String localename, List<String> sitenames) {
        try {
        	AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
        	MutableAssetData d = adm.newAssetData( "Dimension", "" );
        	d.getAttributeData( "name" ).setData( localename );
        	d.getAttributeData( "Publist" ).setData( sitenames );
        	adm.insert( Arrays.<AssetData>asList( d ));
        } catch (AssetAccessException e) {
        	LOG.error("Error creating locale " + localename + " with exception: " + e);
        	return false;
        }
		return true;
	}
	
	/*
	 * Not used. Locales are automatically available in WCS for every asset type in a given site	
	 * @param locale
	 * @param assettype
	 * private void enableLocaleForAssetType(String locale, String assettype) { }
	 */
	
	/**
	 * Gets the locale's ID as a string
	 * @param locale Locale name
	 * @return A string representation of the locale's id. E.g. 1231231231230
	 */
	public String getLocaleIdByName(String locale) {
		String thislocaleid = null;
		try {
			AssetDataManager mgr = (AssetDataManager) ses.getManager(AssetDataManager.class.getName());
			Condition c = ConditionFactory.createCondition("name", OpTypeEnum.EQUALS, locale);
			Query query = new SimpleQuery("Dimension", "Locale", c, Collections.singletonList("name"));
			query.getProperties().setIsBasicSearch( true );
			for (AssetData data : mgr.read(query)) {
				thislocaleid = Long.toString(data.getAssetId().getId());
	        	//LOG.info("Found id " + thislocaleid + " for locale " + locale);
				break;
			}
        	//LOG.info("Finished looping to get an id for locale " + locale);
		} catch (AssetAccessException e) {
        	LOG.error("Error getting locale " + locale + " with exception: " + e);
        }
		return thislocaleid;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getSharedSitesForLocale(String localename) {
		//LOG.info("About to get id for locale " + localename);
		List<String> sharedsites = new ArrayList<String>();
		String localeid = getLocaleIdByName(localename);
		//LOG.info("Found id " + localeid + " for locale " + localename);
		if (Utilities.goodString(localeid)) {
			AssetId locale = new AssetIdImpl( "Dimension", Long.parseLong(localeid));
	        try {
	        	AssetDataManager adm = (AssetDataManager) ses.getManager( AssetDataManager.class.getName() );
	        	AssetData data = adm.readAttributes( locale, Arrays.asList( "Publist" ) );
	        	if (data.getAttributeData( "Publist" ) != null) {
	        		sharedsites = (List<String>) data.getAttributeData( "Publist" ).getData();
	        		
	        		LOG.debug("Found sharedsites " + sharedsites + " for locale " + localename);
	        	}
	        } catch (AssetAccessException e) {
	        	LOG.error("Error loading list of shared sites for locale " + localename + " with exception: " + e);
	        }		
		} else {
			LOG.error("Unable to get asset id for locale " + localename);
		}
		return sharedsites;
	}
	
	@SuppressWarnings("unchecked")
	public void enableLocaleForSite(String locale, String sitename)
	{
		String localeid = getLocaleIdByName(locale);
		if(localeid != null)
		{
			try{
				AssetId assetId = new AssetIdImpl("Dimension", Long.parseLong(localeid)); 
				AssetDataManager adm = (AssetDataManager) ses.getManager(AssetDataManager.class.getName());
				//AssetData data = adm.readAttributes(assetId, Arrays.asList("Publist"));
				MutableAssetData mdata = null;
				for( MutableAssetData md : adm.readForUpdate(Collections.singletonList(assetId)))
				{
					LOG.debug("setting mutable data! ");
					mdata = md;
				}
				if(mdata.getAttributeData("Publist") != null)
				{
					LOG.debug("mdata Asset Type Def .name(): "+mdata.getAssetTypeDef().getName());
					List <String> sharedSites = (List<String>) mdata.getAttributeData( "Publist" ).getData();
					LOG.debug("sharedSites [pre-add]: "+ sharedSites);
					sharedSites.add(sitename);
					LOG.debug("sharedSites [post-add]: "+ sharedSites);
					
					List <String> dblCheckSites = (List<String>) mdata.getAttributeData( "Publist" ).getData();
					LOG.debug("a. checking current value " + dblCheckSites);
										
					mdata.getAttributeData("Publist").setData(new ArrayList<String>(sharedSites));
					
					sharedSites = (List<String>) mdata.getAttributeData( "Publist" ).getData();
					LOG.debug("b. checking new values"+ sharedSites);
					
					LOG.debug("setting AttributeData");
					LOG.debug("Collections.singletonList((AssetData)mdata) "+Collections.singletonList((AssetData)mdata));
					
					List<String> recheck = (List<String>) mdata.getAttributeData("Publist").getData();
					LOG.debug("rechecking string list: "+recheck);
					
					LOG.debug("mdata output: " +mdata.getAttributeData("Publist").getData().toString());
					List <AssetData> passThis = Collections.singletonList((AssetData)mdata);
					LOG.debug("passThis size: "+passThis.size());
					LOG.debug(passThis.toString());
					if(adm != null && passThis.size() > 0){
						LOG.debug("adm is not null, proceding with update");
						AssetData tempy = (AssetData) passThis.get(0);
						List<String> templist = (List<String>)tempy.getAttributeData("Publist").getData();
						LOG.debug("About to update attribute data is: "+templist.toString());
						adm.update(passThis);
						sharedSites = (List<String>) mdata.getAttributeData( "Publist" ).getData();
						LOG.debug("JestyTest : "+sharedSites);
						LOG.debug("doing update...!");
						this.getSharedSitesForLocale(locale);
						LOG.info("Successfully enabled locale '" + locale + "' for site " + sitename);
					} else {
						LOG.debug("adm was null of update list was not larger than 0");
					}
					
				}
			} catch(Exception e) {
				LOG.error(e.getMessage());
				LOG.error(ExceptionUtils.getStackTrace(e));
			}
		}
	}
	
	/**
	 * Loads a locale and adds sitename to the current list of sites the locale is shared to.
	 * First calls getLocaleIdByName() to load the locale's id.
	 * @param locale Name of locale (e.g. en_US)
	 * @param sitename Name of site (e.g. FirstSiteII)
	 */
	@Deprecated
	public void enableLocaleForSite(String locale, String sitename, boolean flag) {
		LOG.debug("Begin EnableLocaleForSite " + locale+ " "+ sitename);
		String thislocaleid = getLocaleIdByName(locale); // Get the id of the locale
		if (thislocaleid != null) {
			try {

				LOG.debug("Processing locale id "+thislocaleid);
	            
				Boolean alreadySharedToThisSite = false;
				AssetDataManager adm = (AssetDataManager) ses.getManager(AssetDataManager.class.getName());
				//Iterable<AssetData> assets = adm.read(Arrays.<AssetId> asList(new AssetIdImpl("Dimension", Long.parseLong(thislocaleid))));
				Iterable<MutableAssetData> assets = adm.readForUpdate(Arrays.<AssetId> asList(new AssetIdImpl("Dimension", Long.parseLong(thislocaleid))));
				List<AssetData> sAssets = new ArrayList<AssetData>();
				for (AssetData a : assets) {
					sAssets.add(a);
					AttributeData attrData = a.getAttributeData( "Publist" );
					LOG.debug("attributedata publist toString(): "+attrData.getData().toString());
		            @SuppressWarnings("unchecked")
					List<Object> publist = attrData.getDataAsList();
		            LOG.debug("publist size outside of for:loop "+publist.size());
		            // Check if sitename is already in list
		            for (Object o : publist) {
		            	LOG.debug("Checking sitename against: "+o.toString());
		            	if (sitename.equalsIgnoreCase(o.toString())) {
		            		LOG.debug("Already shared to this site");
		            		alreadySharedToThisSite = true;
		            		break;
		            	}
		            	
		            }
		            if (!alreadySharedToThisSite) {
		            	publist.add(sitename);
		            
		            	a.getAttributeData("Publist").setDataAsList(publist);
		            	
		            	
		            	
		            	
		            	LOG.debug("**** manual updating asset in loop.");
		            }
		            
				}
				if (!alreadySharedToThisSite) {
					LOG.debug("sAssets to update size: "+sAssets.size());
					adm.update(sAssets);
					
					LOG.debug("Updating asset.");
				}
			} catch (AssetNotExistException e) {
				LOG.error("Error sharing locale " + locale + " to site " + sitename + " with exception: " + e);
			} catch (AssetAccessException e) {
				LOG.error("Error getting locale " + locale + " to site " + sitename + " with exception: " + e);
			}
		}
		LOG.debug("End of EnableLocaleForSite");
	}
	
	public void updateLocaleNameAndDescription(Long localeid, String newname, String newdesc) {
		
		if (localeid != null && Utilities.goodString(newname) && Utilities.goodString(newdesc)) {
			try {
				AssetDataManager adm = (AssetDataManager) ses.getManager(AssetDataManager.class.getName());
				Iterable<AssetData> assets = adm.read(Arrays.<AssetId> asList(new AssetIdImpl("Dimension", localeid)));
				List<AssetData> sAssets = new ArrayList<AssetData>();
				for (AssetData a : assets) {
					sAssets.add(a);
		           	a.getAttributeData("name").setData( newname );
		           	a.getAttributeData("description").setData( newdesc );
				}
				adm.update(sAssets);
				//LOG.info("Updated locale " + Long.toString(localeid) + " set name="+newname+" and description="+newdesc);
			} catch (AssetNotExistException e) {
				LOG.error("Error updating locale " + Long.toString(localeid) + " with exception: " + e);
			} catch (AssetAccessException e) {
				LOG.error("Error getting locale " + Long.toString(localeid) + " with exception: " + e);
			}	
		} else {
			LOG.error("Unable to update locale because either localeid, newname or newdesc is empty");
		}
	}

	@Override
    public boolean isDimsEnabledForType(String type) {
		return isDimsEnabledForType(ics, type);
	}
	
    public static boolean isDimsEnabledForType(ICS ics, String type) {
        /*
         <ASSETTYPE.LOAD NAME="theAssetType" TYPE="Variables.assettype"/>
	     <ASSETTYPE.SCATTER NAME="theAssetType" PREFIX="at"/>
		 <IF COND="Variables.at:usedimension=F">
        */
        FTValList vl = new FTValList();
        vl.setValString("NAME","myAssetType");
        vl.setValString("TYPE", type);
        ics.runTag("ASSETTYPE.LOAD", vl);

        vl.removeAll();
        vl.setValString("NAME","myAssetType");
        vl.setValString("PREFIX", "myAssetTypePfx");
        ics.runTag("ASSETTYPE.SCATTER", vl);

        return  !"F".equalsIgnoreCase(ics.GetVar("myAssetTypePfx:usedimension"));
    }

	@Override
    public void forceEnableDimensionForType(String type) {
		forceEnableDimensionForType(ics, type);
	}
	
    public static void forceEnableDimensionForType(ICS ics, String type) {
        /*
         <ASSETTYPE.LOAD NAME="theAssetType" TYPE="Variables.assettype"/>
	     <ASSETTYPE.SCATTER NAME="theAssetType" PREFIX="at"/>
	     <ICS.SETVAR NAME="at:usedimension" VALUE="T" />
		 <ASSETTYPE.GATHER NAME="theAssetType" PREFIX="at"/>
         <ASSETTYPE.SAVE NAME="theAssetType"/>
        */
        FTValList vl = new FTValList();
        vl.setValString("NAME","myAssetType");
        vl.setValString("TYPE", type);
        ics.runTag("ASSETTYPE.LOAD", vl);

        vl.removeAll();
        vl.setValString("NAME","myAssetType");
        vl.setValString("PREFIX", "myAssetTypePfx");
        ics.runTag("ASSETTYPE.SCATTER", vl);

        ics.SetVar("myAssetTypePfx:usedimension", "T");

        vl.removeAll();
        vl.setValString("NAME","myAssetType");
        vl.setValString("PREFIX", "myAssetTypePfx");
        ics.runTag("ASSETTYPE.GATHER", vl);

        vl.removeAll();
        vl.setValString("NAME","myAssetType");
        ics.runTag("ASSETTYPE.SAVE", vl);
    }

	@Override
	public boolean isLocaleHidden(String locale) {
		return cm.get(WCS_HIDDEN_LOCALES).contains(locale);
	}

	@Override
	public void setLocaleHidden(String locale, boolean hidden) {
		ConfigurationSet gsc = cm.getGlobalConfigurationSet();
		List<String> hiddenLocales = gsc.get(WCS_HIDDEN_LOCALES);
		if (hidden) {
			if (hiddenLocales==null) {
				hiddenLocales = new ArrayList<String>(1);
			}
			if (!hiddenLocales.contains(locale)) {
				hiddenLocales.add(locale);
				gsc.put(WCS_HIDDEN_LOCALES, hiddenLocales);
			}
		} else {
			if (hiddenLocales!=null && hiddenLocales.contains(locale)) {
				hiddenLocales.remove(locale);
				gsc.put(WCS_HIDDEN_LOCALES, hiddenLocales);
			}
		}
	}
}
