package com.claytablet.wcs.system.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.UserProfile;
import com.claytablet.client.producer.UserProfile2;

public class UserProfileImpl implements UserProfile2 {

	protected static final Log LOG = LogFactory.getLog(UserProfileImpl.class.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String sitename;
	private SiteManagerImpl sm;
	private String siteid;
	private UserManagerImpl um;
	private String userid;
	private Boolean hasAdminRole;
	private Boolean hasGlobalAdminRole;
	private List<String> userroles;
	private ICS ics;
	private boolean isSystemEventUser = false;
	
	public UserProfileImpl(ICS ics, String siteid) {
		this.ics = ics;
		this.sm = new SiteManagerImpl(ics);
		this.um = new UserManagerImpl(ics);
		this.username = ics.GetSSVar("username");
		this.userid = um.getUserId(this.username);

		boolean batchUser = um.isBatchUser(this.username);
		if (batchUser || "fwadmin".equals(this.username)) {
			this.hasGlobalAdminRole = true;
		} else {
			this.hasGlobalAdminRole = um.hasCTAdminRole(this.username, "AdminSite");
		}

		this.siteid = siteid;
		if (Utilities.goodString(siteid)) {
			this.sitename = sm.getSiteNameById( this.siteid );
			this.hasAdminRole = "DefaultReader".equals( this.username ) || "fwadmin".equals(this.username) || 
					um.hasCTSiteAdminRole(this.username, this.sitename);
			this.userroles = um.getUserRoles(this.username, this.siteid);
		} else {
			isSystemEventUser = batchUser;
		}
	}
	
	@Override
	public String getName() {
		return this.username;
	}
	
	@Override
	public String getId() {
		return this.userid;
	}

	@Override
	public boolean isAdmin() {
		
		if (isGlobalAdmin()) {
			return true; // Do this because the SystemEvent's user doesn't have a site assigned to it
		} else {
			
			if (hasAdminRole == null && Utilities.goodString( ics.GetSSVar("pubid"))) {
				this.siteid = ics.GetSSVar("pubid");
				this.sm = new SiteManagerImpl(ics);
				this.um = new UserManagerImpl(ics);
				this.sitename = sm.getSiteNameById( this.siteid );
				this.hasAdminRole = um.hasCTSiteAdminRole(this.username, this.sitename);
			}
			return hasAdminRole==null? false : hasAdminRole;
		}
	}

	@Override
	public boolean isAdmin(String siteId) {
		return isGlobalAdmin() || um.hasCTSiteAdminRole(this.username, sm.getSiteNameById(siteId));
	}
	
	@Override
	public List<String> getUserRoles() {
		if (this.userroles == null && Utilities.goodString( ics.GetSSVar("pubid"))) {
			this.siteid = ics.GetSSVar("pubid");
			this.um = new UserManagerImpl(ics);
			this.userroles = um.getUserRoles(this.username, this.siteid);
		}
		return this.userroles;
	}

	@Override
	public List<String> getUserRoles(String siteId) {
		return um.getUserRoles(this.username, siteId);
	}
	
	@Override
	public boolean isGlobalAdmin() {
		if (isSystemEventUser) {
			return true; // Do this because the SystemEvent's user doesn't have a site assigned to it
		} else {
			return hasGlobalAdminRole==null? false : hasGlobalAdminRole;
		}
	}
	
	@Override
	public String toString() {
		return "UserProfileImpl [username=" + username + ", sitename="
				+ sitename + ", siteid=" + siteid + ", userid=" + userid
				+ ", hasAdminRole=" + hasAdminRole + ", userroles=" + userroles
				+ ", isSystemEventUser=" + isSystemEventUser + "]";
	}

}
