/**
 * 
 */
package com.claytablet.wcs.system;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.CTPlatformManager.ProcessingResult;
import com.claytablet.client.producer.TranslationEntityManager;

/**
 * @author Mike Field
 *
 */
public interface MessageChecker {
	ProcessingResult checkMessagesAndUpdateContent();
}
