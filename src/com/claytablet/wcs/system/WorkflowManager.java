/**
 * 
 */
package com.claytablet.wcs.system;

import java.util.List;
import java.util.Map;

import com.fatwire.assetapi.data.AssetId;

/**
 * @author Mike Field
 *
 */
public interface WorkflowManager {

	Map<String, String> getAllWorkflowProcesses();
	Map<String, String> getAllWorkflowProcessesByAssetTypeAndSite(String assettype, String pubid);
	List<String> getStartMenuItemsForSiteAndAssetType(String assettype, String pubid);
	void addAssetToWorkflow(AssetId aid, String pubid, String procid, String assignToUser);
	void addOrAdvanceAssetInWorkflow(AssetId aid, String siteid, String workflowprocid, String assignToUser);
	void advanceAssetInWorkflow(AssetId aid, String siteid, String nextstepid, String assignToUser);
	String getNextStepId(AssetId aid, String siteid);
	Boolean isInWorkflow(AssetId aid, String siteid, String workflowprocid);
	void advanceAssetInWorkflowUnknownNextStep(AssetId aid, String siteid, String assignToUser);
	String getNextStepIdFromChoices(AssetId aid, String stepname1, String stepname2, String siteid);
	
}
