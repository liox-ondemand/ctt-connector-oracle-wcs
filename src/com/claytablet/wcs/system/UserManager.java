/**
 * 
 */
package com.claytablet.wcs.system;

import java.util.List;

/**
 * @author Mike Field
 *
 */
public interface UserManager {

	Boolean hasCTAdminRole(String username, String sitename);
	Boolean hasCTSiteAdminRole(String username, String sitename);
	Boolean hasCTUserRole(String username, String sitename);
	Boolean hasRole(String username, String role, String sitename);
	String getPubId(String sitename);
	String getUserId(String username);
	List<String> getUserRoles(String username, String siteid);
}
