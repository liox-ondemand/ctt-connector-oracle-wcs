package com.claytablet.wcs.system;
import java.util.*;
import java.util.Map.Entry;
import java.lang.Long;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;

import com.fatwire.assetapi.common.AssetAccessException;
import com.fatwire.assetapi.common.AssetNotExistException;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.mda.*;
import com.fatwire.mda.DimensionableAssetInstance.DimensionParentRelationship;
import com.fatwire.system.*;
import com.fatwire.assetapi.data.*;
import com.fatwire.assetapi.query.*;
import com.openmarket.xcelerate.asset.AssetIdImpl;
import com.openmarket.xcelerate.common.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * A series of functions for creating and assigning locales to assets.
 * both of which are simple utility functions used on the UI screens.
 * @author Mike Field
 */
public interface LocaleManager {
	List<String> getAllWCSLocales();
	List<String> getWCSLocalesForSite(String sitename);
	Map<String, String> getAllWCSLocalesAndIds();
	void setLocaleForAssets(List<AssetId> assetlist, String locale, Boolean makeMaster);
	void setLocaleForAsset(AssetId assetid, String locale, Boolean makeMaster);
	Boolean addLocaleAndShareToSites(String localename, List<String> sitenames);
	String getLocaleIdByName(String locale);
	void enableLocaleForSite(String locale, String sitename);
	
	List<String> getSharedSitesForLocale(String localename);
	void updateLocaleNameAndDescription(Long localeid, String newname, String newdesc);
	
	boolean isDimsEnabledForType(String type);
	void forceEnableDimensionForType(String type);
	
	Map<String, String> getAllWCSLocalesAndDescs();
	Map<String, String> getWCSLocalesAndDescsForSite(String sitename);
	
	boolean isLocaleHidden(String locale);
	void setLocaleHidden(String locale, boolean hidden);
}
