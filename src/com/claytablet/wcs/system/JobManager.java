/**
 * 
 */
package com.claytablet.wcs.system;

import com.claytablet.client.producer.ProgressReporter;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;

/**
 * @author Mike Field
 *
 */
public interface JobManager {
	
	Boolean hasPendingTRs(TranslationJob job);
	void sendJob(ProgressReporter pr, TranslationJob job, TranslationEntityManager tem, String vendorid);	
	void copyBackJob(ProgressReporter pr, TranslationJob job, TranslationEntityManager tem);
}
