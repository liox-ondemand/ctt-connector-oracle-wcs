package com.claytablet.wcs.ui;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobFilter;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.Utils;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIJobRequestCopyManager implements Action {
	protected static final Log LOG = LogFactory.getLog(UIJobRequestCopyManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;
	
	private TranslationEntityManager entityManager;

	public void handleRequest(ICS ics) {
		String app = ics.GetVar("app");
		String jobid = ics.GetVar("jobid");
		
		String siteId = ics.GetVar("siteId");
		ManagerFactorySession sudoSession = null;
	
		try {
			if ("WEM".equals(app)) {
				if (StringUtils.isEmpty(siteId)) {
					session = session.sudoGlobalAdminSession();
				} else {
					session = session.sudo(siteId);
				}
				sudoSession = session;
				model.add("siteid", siteId);
			}
			entityManager = session.getTranslationEntityManager();
		
			String targetJobId = ics.GetVar("targetJobId");
			String targetJobName = ics.GetVar("targetJobName");
			String reqIdStr = ics.GetVar("reqIds");
			String[] reqIds = StringUtils.isEmpty(reqIdStr)? new String[0] : reqIdStr.split(" ");
			
			TranslationJob targetJob;
			
			if (Utilities.goodString(targetJobId)) {
				targetJob = entityManager.getTranslationJob(targetJobId);
			} else {
				targetJob = entityManager.save(entityManager.createTranslationJob(targetJobName));
			}
			copyTranslationRequest(reqIds, targetJob);
			model.add("targetJob", targetJob);
			model.add("jobs", entityManager.findTranslationJobs(
					TranslationJobFilter.factory.filter("status='CREATED'")));
			model.add("jobid", jobid);
		} finally {
			if (sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	private void copyTranslationRequest(String[] reqIds, TranslationJob targetJob) {
		for (String id : reqIds) {
			TranslationRequest tr = entityManager.getTranslationRequest(id);
			TranslationRequest newTr = entityManager.createTranslationRequest(tr.getNativeSourceId(), 
							tr.getNativeSourceLanguage(), tr.getNativeTargetLanguage()).
						setName(tr.getName()).setTargetWorkflowId(tr.getTargetWorkflowId()).
						setTranslationJob(targetJob);
			
			AssetId taid = assetManager.getTranslation(Utils.stringToAssetId(tr.getNativeSourceId()), 
					tr.getNativeTargetLanguage());
			if (taid!=null) {
				newTr.setNativeTargetId(taid.toString());
			}
			entityManager.save(newTr);
		}
	}

}

	