package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.claytablet.client.producer.ManagerFactory.DbType;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.wcs.system.SiteManager;

public class UITranslationRequestFilter extends UIFilterImpl {
	private int totalRequestCount;
	private List<String> assetTypes = new ArrayList<String>();
	private List<String> sourceLanganges = new ArrayList<String>();
	private List<String> targetLanganges = new ArrayList<String>();
	private List<String> statuses = new ArrayList<String>();
	protected List<String> users = new ArrayList<String>();
	protected List<String> siteIds = new ArrayList<String>();
	protected List<FilterField> fields = new ArrayList<FilterField>();

	protected UITranslationRequestFilter() {
	}
	
	public UITranslationRequestFilter(DbType dbType, List<TranslationRequest> reqs, String currentFilter) {
		init(dbType, reqs);
		
		addNewFilter(currentFilter);
	}

	public UITranslationRequestFilter(DbType dbType, TranslationJob job) {
		this(dbType, job, "");
	}
	
	public UITranslationRequestFilter(DbType dbType, TranslationJob job, String currentFilter) {
		init(dbType, job.getTranslationRequests());
		
		if (job.getStatus()!=TranslationEntity.Status.CREATED) {
			if (statuses.size()>1) {
				Collections.sort(statuses, new Comparator<String>() {
					@Override
					public int compare(String paramT1, String paramT2) {
						return TranslationEntity.Status.valueOf(paramT1).ordinal() -
								TranslationEntity.Status.valueOf(paramT2).ordinal();
					}
				});
				fields.add(new FilterField("Status", "status", 
						Operator.Util.addModifier( 
								Operator.Util.limitOperands(StringOperators.equals, statuses),
										StringOperationModifier.matchcase)));
			} else if (statuses.size()==1) {
				fields.add(new FilterField("Status: " + statuses.get(0), "", Collections.<Operator<?>>emptyList()));
			}
		}
		addNewFilter(currentFilter);
	}

	protected void init(DbType dbType, List<TranslationRequest> reqs) {
		totalRequestCount = reqs.size();
		Set<String> assetTypeSet = new HashSet<String>();
		Set<String> sourceLangSet = new HashSet<String>();
		Set<String> targetLangSet = new HashSet<String>();
		Set<String> statusSet = new HashSet<String>();
		Set<String> userSet = new HashSet<String>();
		Set<String> siteIdSet = new HashSet<String>();
		
		for (TranslationRequest req : reqs) {
			String nativeSourceId = req.getNativeSourceId();
			int sepIndex = nativeSourceId.indexOf(':');
			if (sepIndex>0) {
				assetTypeSet.add(nativeSourceId.substring(0, sepIndex));
			}
			sourceLangSet.add(req.getNativeSourceLanguage());
			targetLangSet.add(req.getNativeTargetLanguage());
			statusSet.add(req.getStatus().toString());
			userSet.add(req.getCreatedUser());
			siteIdSet.add(req.getSiteId());
		}
		assetTypes.addAll(assetTypeSet);
		sourceLanganges.addAll(sourceLangSet);
		targetLanganges.addAll(targetLangSet);
		statuses.addAll(statusSet);
		users.addAll(userSet);
		siteIds.addAll(siteIdSet);
		
		fields.add(new FilterField("Asset Name", "name", 
				Arrays.<Operator<?>>asList(new Operator[]{
						StringOperators.contains, StringOperators.matches, StringOperators.likes} )));
		
		if (assetTypes.size()>1) {
			Collections.sort(assetTypes);
			fields.add(new FilterField("Asset Type", "nativeSourceId",
					Operator.Util.limitOperands(AssetTypeNameOperators.eq, assetTypes)));
		} else if (assetTypes.size()==1) {
			fields.add(new FilterField("Asset Type: " + assetTypes.get(0), "", Collections.<Operator<?>>emptyList()));
		}
		if (sourceLanganges.size()>1) {
			Collections.sort(sourceLanganges);
			fields.add(new FilterField("Source Language", "nativeSourceLanguage",
					Operator.Util.addModifier(
							Operator.Util.limitOperands(StringOperators.equals, sourceLanganges),
									StringOperationModifier.matchcase)));
		} else if (sourceLanganges.size()==1) {
			fields.add(new FilterField("Source Language: " + sourceLanganges.get(0), "", Collections.<Operator<?>>emptyList()));
		}
		if (targetLanganges.size()>1) {
			Collections.sort(targetLanganges);
			fields.add(new FilterField("Target Language", "nativeTargetLanguage", 
					Operator.Util.addModifier(
							Operator.Util.limitOperands(StringOperators.equals, targetLanganges),
									StringOperationModifier.matchcase)));
		} else if (targetLanganges.size()==1) {
			fields.add(new FilterField("Target Language: " + targetLanganges.get(0), "", Collections.<Operator<?>>emptyList()));
		}
		fields.add(new FilterField("Last Updated Date", "updatedDate", 
				Arrays.<Operator<?>>asList(DateOperators.values())));
		if (users.size()>1) {
			Collections.sort(users);
			fields.add(new FilterField("Created User", "createdUser",
					Operator.Util.addModifier(
							Operator.Util.limitOperands(StringOperators.equals, users),
									StringOperationModifier.matchcase)));
		} else if (users.size()==1) {
			fields.add(new FilterField("Created User: " + users.get(0), "", 
					Collections.<Operator<?>>emptyList()));
		}
	}
	
	@Override
	public List<? extends Field> getFields() {
		return fields;
	}
	
	@Override
	public List<? extends Field> getAvailableFields() {
		List<Field> availableFields = new ArrayList<Field>(fields);
		List<? extends FieldCondition> fieldConditions = getFieldConditions();
		
		for (FieldCondition fieldCondition : fieldConditions) {
			if (fieldCondition.getField().getOperators().size()==1 &&
					!fieldCondition.getField().getOperators().get(0).getAcceptedOperands().isEmpty()) {
				availableFields.remove(fieldCondition.getField());
			}
		}
		return availableFields;
	}
	
	public int getTotalRequestCount() {
		return totalRequestCount;
	}

	public class FilterField implements UIFilter.Field  {
		private String displayName;
		private String colName;
		private List<Operator<?>> operators;

		FilterField(String displayName, String colName, List<UIFilter.Operator<?>> operators) {
			this.displayName = displayName;
			this.colName = colName;
			this.operators = operators;
		}
		
		FilterField(String displayName, String colName, UIFilter.Operator<?> operator) {
			this(displayName, colName, Collections.<Operator<?>>singletonList(operator));
		}
		
		@Override
		public String getDisplayName() {
			return displayName;
		}

		@Override
		public String getColName() {
			return colName;
		}

		@Override
		public List<Operator<?>> getOperators() {
			return operators;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + UITranslationRequestFilter.this.hashCode();
			result = prime * result
					+ ((colName == null) ? 0 : colName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FilterField other = (FilterField) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (colName == null) {
				if (other.colName != null)
					return false;
			} else if (!colName.equals(other.colName))
				return false;
			return true;
		}

		private UITranslationRequestFilter getOuterType() {
			return UITranslationRequestFilter.this;
		}
	}

	public static enum AssetTypeNameOperators implements Operator<String> {
		eq("=");
		
		private OperatorMetadata metadata;
		
		private AssetTypeNameOperators(String displayName) {
			metadata = OperatorMetadata.Impl.createMetadata().setDisplayName(displayName).
					setValidator(OperatorValidator.string); 
		}

		@Override
		public OperatorMetadata getMetadata() {
			return metadata;
		}
	
		@Override
		public String getWhereClause(String colName, List<String> operands,
				List<? extends OperatorModifier> modifiers) {
			StringBuffer condition = new StringBuffer();
			for (String operand : operands) {
				String operandEscaped = StringOperators.escapeLikeOperand(operand) + "%";
				if (condition.length()>0) {
					condition.append(" OR ");
				}
				condition.append(colName + " like '" +operandEscaped + "' ESCAPE '\\'");
			}
			return condition.toString();
		}
	
		@Override
		public String parseOperand(String str) {
			return str;
		}
	
		@Override
		public String operandToString(String operand) {
			return operand;
		}
		
		@Override
		public List<String> getAcceptedOperands() {
			return Collections.emptyList();
		}
	}
	
	static public class WEMQueue extends UITranslationRequestFilter {
		private SiteManager siteManager;

		public WEMQueue(DbType dbType, List<TranslationRequest> reqs, 
				String currentFilter, SiteManager siteManager) {
			super();
			this.siteManager = siteManager;
			init(dbType, reqs);
			addNewFilter(currentFilter);
		}

		@Override
		protected void init(DbType dbType, List<TranslationRequest> reqs) {
			super.init(dbType, reqs);
			
			if (siteIds.size()>1) {
				Map<String, String> siteNameMap = new HashMap<String, String>(siteIds.size()*2);

				for (String siteId : siteIds) {
					siteNameMap.put(siteManager.getSiteNameById(siteId), siteId);
				}
				fields.add(new FilterField("Site", "siteId",
						new ChoiceOperator<String>(siteNameMap, StringOperators.equals)));
			} else if (siteIds.size()==1) {
				fields.add(new FilterField("Site: " + siteManager.getSiteNameById(siteIds.get(0)), "", 
						Collections.<Operator<?>>emptyList()));
			}
		}
	}
}
