package com.claytablet.wcs.ui;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.lspconnector.lionbridge.FreewaySimpleClient;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIProviderValidationPostManager implements Action {
	private enum Action {
		ValidateProviderConnection,
		ValidateAccountKey,
	}
	protected static final Log LOG = LogFactory.getLog(UINewJobPostManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private ConfigurationManager configManager;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		switch (Action.valueOf(ics.GetVar("action"))) {
			case ValidateAccountKey:
				validateAccountKey(ics);
				break;
			
			case ValidateProviderConnection:
				validateProviderConnection(ics);
				break;
		}
	}

	private void validateProviderConnection(ICS ics) {
		switch (TranslationProvider.Type.valueOf(ics.GetVar("providertype"))) {
			case LIONBRIDGE_FREEWAY:
				validateFreewayConnection(ics.GetVar("host"), 
						ics.GetVar("user"), ics.GetVar("password"));
				break;
			default:
				model.add("error", "");
				model.add("message", "No need to validate provider");
				break;
		}
	}

	private void validateFreewayConnection(String host, String user, String password) {
		if (StringUtils.isEmpty(host) ||
				StringUtils.isEmpty(user) ||
				StringUtils.isEmpty(password) ) {
			model.add("message", "Validation failed");
			model.add("error", "Freeway login information not fully configured");
		} else {
			FreewaySimpleClient fsc = new FreewaySimpleClient();
			try {
				String proxyHost = configManager.get(ProducerCommonConfigs.CT_PROXY_HOST);
				
				if (!StringUtils.isEmpty(proxyHost)) {
					fsc.setProxyValues(proxyHost, configManager.get(ProducerCommonConfigs.CT_PROXY_PORT), 
							configManager.get(ProducerCommonConfigs.CT_PROXY_USER), 
							configManager.get(ProducerCommonConfigs.CT_PROXY_PASSWORD), 
							configManager.get(ProducerCommonConfigs.CT_PROXY_DOMAIN));
				}
				fsc.getFreewayAnalysisCodes(host, user, password);
				
				model.add("error", "");
				model.add("message", "Validated successfully");
			} catch (Exception e) {
				String msg = "Cannot retrieve analysis code information from " + host;
				LOG.warn(msg, e);
				model.add("message", "Validation failed");
				model.add("error", msg + ": " + String.valueOf(e));
			}
		}
		
	}

	private void validateAccountKey(ICS ics) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Not implemented yet");
	}
}
