/**
 * 
 */
package com.claytablet.wcs.ui;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

/**
 * @author Mike Field
 *
 */
public class UIConfigManager implements Action {

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest private ConfigManager configManager;
	@InjectForRequest private TranslationRequestManager translationRequestManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;

	@Override
	public void handleRequest(ICS ics) {
			String pagename = ics.GetVar("pagename");
			model.add("mypagename", pagename);
			/** Do something here **/
	}

}
