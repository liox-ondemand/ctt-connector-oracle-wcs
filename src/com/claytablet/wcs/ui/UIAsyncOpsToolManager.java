package com.claytablet.wcs.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.IList;
import COM.FutureTense.Interfaces.Utilities;
import COM.FutureTense.Util.ftMessage;

import com.claytablet.wcs.system.queue.DatabaseJobQueue;
import com.claytablet.wcs.system.queue.DatabaseQueuePollEvent;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.sql.table.TableColumn;
import com.fatwire.gst.foundation.facade.sql.table.TableDef;

public class UIAsyncOpsToolManager implements Action {

	@InjectForRequest private ICS ics;
	@InjectForRequest public Model model;
	
	protected static final Log LOG = LogFactory.getLog(UIAsyncOpsToolManager.class.getName());
	
	
	
	private static final String ACTION_CLASS = DatabaseQueuePollEvent.class.getName();
	private static final String EVENT_NAME = "DatabaseQueuePollEvent";
	private static final String RUN_TIMES = "*:*:45 */*/*"; 	// every minute
											 
	
	@Override
	public void handleRequest(ICS ics) {
		String pagename = ics.GetVar("pagename");
		String action = ics.GetVar("action");
		
		LOG.info("handling action: "+ (Utilities.goodString(action)?action:"none"));
		
		if(Utilities.goodString(ics.GetVar("action")))
		{
			doPost(ics);
			
		}
		ics.SetVar("message", getEventStatus());
		ics.SetVar("dbqueuestatus", getDatabaseTableStatus());
		ics.SetVar("tableexists", String.valueOf(DatabaseJobQueue.tablesExist(ics)));
		ics.SetVar("eventflag", getEventFlag());
    }
	
	private String getDatabaseTableStatus()
	{
		String message;
		if(DatabaseJobQueue.tablesExist(ics))
		{
			message = "Job queue tables exist and have been created.";
			
		}
		else
		{
			message = "Job Queue tables have not been created yet.";
		}
		return message;
	}
	
	private String getEventFlag()
	{
		String check = getEventStatus();
		String ret = "Empty";
		LOG.info("check val: "+check);
		if(check.equalsIgnoreCase("No event created or registered."))
		{
			ret = "NONE";
		}
		else if(check.equalsIgnoreCase("Enabled"))
		{
			ret = "ENABLED";
		}
		else if(check.equalsIgnoreCase("Disabled"))
		{
			ret = "DISABLED";
		}
		return ret;
	}
	
	private String getEventStatus()
	{
		IList e = ics.ReadEvent(EVENT_NAME, EVENT_NAME);
		if(e==null)
		{
			return "No event created or registered.";
		}
		try {
			/*
			LOG.info("IList item has "+ e.numColumns() + " columns.");
			for(int i=0; i<e.numColumns(); i++)
			{
				LOG.info("Col "+i+": "+e.getColumnName(i));
				LOG.info(" -- value: "+e.getValue(e.getColumnName(i)));
				
			}
			*/
			return (e.getValue("enabled").equals("true")?"Enabled":"Disabled");
			
		}
		catch(Exception ex)
		{
			
			ex.printStackTrace();
			LOG.error(ex.getMessage());
			return "Err: Unknown";
		}
		
	}
	
	private void doPost(ICS ics)
	{
		IList listy = ics.QueryEvents(null, null, null, null);
		int j=1;
		if(null != listy && listy.hasData())
		{
			LOG.info("System Events found: "+listy.numRows());	
	    	if (listy !=null && listy.numRows() > 0) {
	    		do {
	    			LOG.info("Event #"+j);
		    			for(int i=0; i<listy.numColumns(); i++)
		    			{
		    				try {
		    					LOG.info(listy.getColumnName(i) +": " +listy.getValue(listy.getColumnName(i)) );
		    				}
		    				catch(Exception e)
		    				{
		    					e.printStackTrace();
		    				}
		    				
		    			}	   
		    			j++;
	    			}
	            while (listy.moveToRow(IList.next, 0));
	         }
	    	
		}
		

        if (hasPrivs(ics) && Utilities.goodString(ics.GetVar("action"))) {
        	
        	String action = ics.GetVar("action");
        	
        	if(action.equals("CreateEvent"))
        	{
        		if(createAsyncOpsEvent())
        		{
        			model.add("errormessage","Created and registered the new Async Operations event with WCS");
        		}
        		else
        		{
        			model.add("errormessage","Unable to create and register Async Operations Event to the system");
        		}
        	}
        	else if(action.equals("EnableEvent"))
			{
        		if(enableAsyncOpsEvent())
        		{
        			model.add("errormessage","Enabling: "+EVENT_NAME);
        		}
        		else
        		{
        			model.add("errormessage","There was an error enabling the event: "+EVENT_NAME);
        		}
			}
        	else if(action.equals("DisableEvent"))
			{
        	
        		if(disableAsyncOpsEvent())
        		{
        			model.add("errormessage","Disabling: "+EVENT_NAME);
        		}
        		else
        		{
        			model.add("errormessage","There was an error disabling the event: "+EVENT_NAME);
        		}
			}
        	else if(action.equals("DeleteEvent"))
			{
        		if(deleteAsyncOpsEvent())
        		{
        			model.add("errormessage","Deleting: "+EVENT_NAME);
        		}
        		else
        		{
        			model.add("errormessage","There was an error deleting the event: "+EVENT_NAME);
        		}
			}
        	else if(action.equals("CreateTables"))
        	{
        		this.createCTQueueTable();
        	}
        	else if(action.equals("DropTables"))
        	{
        		LOG.info("Attempting to drop tables...");
        		this.dropCTQueueTables();
        	}
        	else {
        		model.add("errormessage","An invalid action was received from the Asynchronous Operations Tool.");
        		LOG.error("An invalid action was received from the Asynchronous Operations Tool.");
        		
        	}
        	
            
        } else {
        	if(ics.GetVar("action") == null)
    		{
        		LOG.info("No Action was specified from the loaded page.");
    		}
        	else
        	{	
        		model.add("errormessage","Current logged in user is not authorized to access this functionality. Note: This page requires SiteGod permissions");
        	
        	}
            //throw new CSRuntimeException("Current logged in user is not authorized to access this functionality.", 403);
        }
	}
	
	private boolean dropCTQueueTables()
	{
		if(DatabaseJobQueue.tablesExist(ics))
		{
			LOG.info("Tables exist, executing static drop function...");
			DatabaseJobQueue.dropTables(ics);
			return true;
		}
		else
		{
			model.add("errormessage", "Nothing to delete, job queue tables do not exist");
			return false;
		}
	}
	private boolean deleteAsyncOpsEvent()
	{
		if(this.eventExists())
		{
			LOG.info("Deleting event: "+EVENT_NAME);
			return ics.DestroyEvent(EVENT_NAME);
		}
		else
		{
			LOG.warn("Could not delete event: "+EVENT_NAME+", event was not found.");
			model.add("errormessage","Could not delete event: "+EVENT_NAME+", event was not found.");
			return false;
		}
	}
	
	private boolean createAsyncOpsEvent()
	{
		boolean success = false;
		IList e = ics.ReadEvent(EVENT_NAME, EVENT_NAME);
		
        if (e == null || ics.GetErrno() < 0) {
            FTValList params = new FTValList();
            params.setValString("pagename", "CustomElements/CT/Utils/Dispatcher");
            params.setValString("action", "class:"+ACTION_CLASS);
            success = ics.AppEvent(EVENT_NAME, "ContentServer", RUN_TIMES, params);
            if (success) {
                LOG.info("Registered event "+EVENT_NAME+" for action "+ACTION_CLASS);
                LOG.info("Executing event on time interval: "+RUN_TIMES);
                
            } else {
                LOG.error("Failure registering event "+EVENT_NAME+" errno: "+ics.GetErrno());
            }
        } else {
            LOG.info("Not registering event "+EVENT_NAME+" because it is already registered.");
            success = true;
        }
		return success;
	}
	
	private boolean createCTQueueTable() {
		boolean ret = false; 
		if(!DatabaseJobQueue.tablesExist(ics)){
			ret = DatabaseJobQueue.createTables(ics); 
			if(!ret)
			{
				model.add("errormessage", "Could not successfully create database table for asynchronous operations.");
			} else {
    			LOG.info("Created tables for job queue.");
			}
		}
		return ret;
    }
	

	
	private boolean enableAsyncOpsEvent()
	{
		if(eventExists())
		{
			LOG.info("Enabling event: "+EVENT_NAME);
			return ics.EnableEvent(EVENT_NAME);
		}
		else
		{
			LOG.warn("Could not enable event: "+EVENT_NAME+", event might not have been created/registered yet.");
			model.add("errormessage","Could not enable event: "+EVENT_NAME+", event might not have been created/registered yet.");
			return false;
		}
		
	}
	
	private boolean disableAsyncOpsEvent()
	{
		if(eventExists())
		{
			LOG.info("Disabling event: "+EVENT_NAME);
			return ics.DisableEvent(EVENT_NAME);
		}
		else
		{
			LOG.warn("Could not disable event: "+EVENT_NAME+", no such event found.");
			model.add("errormessage","Could not disable event:" + EVENT_NAME + ", no such event found.");
			return false;
		}
	}

    private boolean hasPrivs(ICS ics) {
        return ics.UserIsMember("SiteGod");
    }
	private boolean eventExists()
	{
		IList e = ics.ReadEvent(EVENT_NAME, EVENT_NAME);
		if(e!=null)
		{
			
			return true;
		}
		return false;
	}

}
