package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.ui.UIFilter.Field;
import com.claytablet.wcs.ui.UIFilter.FieldCondition;
import com.claytablet.wcs.ui.UIFilter.Operator;
import com.claytablet.wcs.ui.UIFilter.SingleCondition;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UITranslationRequestFilterManager implements Action {
	protected static final Log LOG = LogFactory.getLog(
			UITranslationRequestFilterManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest private SiteManager siteManager;
	
	private TranslationEntityManager entityManager;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String jobid = ics.GetVar("jobid");
		String currentFilter = ics.GetVar("currentFilter");
		String newFilter = ics.GetVar("newFilter");
		String app = ics.GetVar("app");
		
		String siteId = ics.GetVar("siteId");
		ManagerFactorySession sudoSession = null;
	
		try {
			if ("WEM".equals(app)) {
				if (StringUtils.isEmpty(siteId)) {
					session = session.sudoGlobalAdminSession();
				} else {
					session = session.sudo(siteId);
				}
				sudoSession = session;
				model.add("siteid", siteId);
			}
			entityManager = session.getTranslationEntityManager();
			
			TranslationJob job = StringUtils.isEmpty(jobid)? null : entityManager.getTranslationJob(jobid);
			UITranslationRequestFilter filter;
			
			if (job!=null) { 
				filter = new UITranslationRequestFilter(session.getDbType(), job, currentFilter);
			} else if ("WEM".equals(app)) {
				filter = new UITranslationRequestFilter.WEMQueue(session.getDbType(), 
						entityManager.getAllQueuedTranslationRequests(), currentFilter,
						siteManager);
			} else {
				filter = new UITranslationRequestFilter(session.getDbType(), 
						entityManager.getAllQueuedTranslationRequests(), currentFilter);
			}
			filter.addNewFilter(newFilter);
			model.add("job", job);
			model.add("filter", filter);
			if (!StringUtils.isEmpty(newFilter)) {
				try {
					JSONObject obj = new JSONObject(newFilter);
					model.add("newFilterJson", obj);
				} catch (JSONException e) {
					LOG.warn("Cannot parse new filter", e);
				}
			}
			List<? extends Field> fields = filter.getFields();
			List<FieldInfo> fieldInfos = new ArrayList<FieldInfo>();
			
			for (Field field : fields) {
				FieldCondition condition = filter.getFieldCondition(field);
				fieldInfos.add(new FieldInfo(field, condition));
			}
			model.add("allFields", fieldInfos);
			model.add("currentFilterJson", StringEscapeUtils.escapeHtml(filter.getJson()));
		} finally {
			if (sudoSession!=null) {
				sudoSession.close();
			}
		}
	}
	
	public class FieldInfo {
		private Field field;
		private FieldCondition fieldCondition;
		private List<String> availableOperands = new ArrayList<String>();
		
		private FieldInfo(Field field, FieldCondition fieldCondition) {
			this.field = field;
			this.fieldCondition = fieldCondition;
			if (field.getOperators().size()==1) {
				Operator<?> operator = field.getOperators().get(0);
				List<?> operands = new ArrayList<Object>(operator.getAcceptedOperands());
				
				if (fieldCondition!=null) {
					for (Iterator<?> iterator = operands.iterator(); iterator.hasNext();) {
						Object operand = (Object) iterator.next();
						for (SingleCondition<?> sc : fieldCondition.getSingleConditions()) {
							if (sc.getOperands().contains(operand)) {
								iterator.remove();
							}
							break;
						}
					}
				}
				for (Object operand : operands) {
					availableOperands.add(Operator.Util.operandToString(operator, operand));
				}
			}
		}
		
		public Field getField() {
			return field;
		}
		
		public FieldCondition getCondition() {
			return fieldCondition;
		}

		public List<String> getAvailableOperands() {
			return availableOperands;
		}
	}
}
