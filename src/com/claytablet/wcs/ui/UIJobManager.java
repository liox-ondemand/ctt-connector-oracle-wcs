/**
 * 
 */
package com.claytablet.wcs.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobFilter;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.TranslationEntity.Status;
import com.claytablet.client.producer.impl.platform.CTPlatformUtil;
import com.claytablet.client.util.ListUtils;
import com.claytablet.model.event.metadata.IMetadataGroup;
import com.claytablet.model.event.metadata.custom.freeway.FreewayMetadataGroup;
import com.claytablet.model.event.metadata.impl.JobMetadata;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.ExtendedAssetId;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.PONumberInfo;
import com.claytablet.wcs.system.queue.DatabaseJobQueue;
import com.claytablet.wcs.system.queue.JobQueueEntry;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * @author Mike Field
 *
 */
public class UIJobManager implements Action {
	enum ACTION {
		save,
		send,
		delete,
		archive,
		unarchive,
		copyback("copy back");
		
		private String displayName = name();

		private ACTION() {
		}
		
		private ACTION(String displayName) {
			this.displayName = displayName;
		}
		
		public String getDisplayName() {
			return displayName;
		}
	}

	protected static final Log LOG = LogFactory.getLog(UIJobManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ConfigManager cm;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest public Model model;
	@InjectForRequest public ManagerFactorySession session;
	
	private TranslationEntityManager entityManager;
	private ConfigurationManager configManager;
	
	public void handleRequest(ICS ics) {
		String pagename = ics.GetVar("pagename");
		String app = ics.GetVar("app");
		
		String siteId = ics.GetVar("siteId");
		ManagerFactorySession sudoSession = null;

		try {
			if ("WEM".equals(app)) {
				if (StringUtils.isEmpty(siteId)) {
					session = session.sudoGlobalAdminSession();
				} else {
					session = session.sudo(siteId);
				}
				sudoSession = session;
			}
			entityManager = session.getTranslationEntityManager();
			configManager = session.getConfigurationManager();
			
			model.add("siteid", siteId);
			if ("CustomElements/CT/JobPost".equals(pagename)) {
				doJobPost();
			} else {
				doJobFront();
			}
		} finally {
			if (sudoSession!=null) {
				sudoSession.close();
			}
		}
	}
	
	private void doJobFront() {
		String jobid = ics.GetVar("jobid");
		model.add("message", ics.GetVar("message"));
		model.add("showheaderfooter", ics.GetVar("showheaderfooter"));
		model.add("pagesize", cm.getMaxItemsPerPage());
		if (Utilities.goodString(jobid)) {
			TranslationJob job = entityManager.getTranslationJob( jobid );
			String vendormetadata = "";
			if (job.getProviderMetadata() != null) {
				vendormetadata = job.getProviderMetadata().toXml();
			}			
			String siteName = siteManager.getSiteNameById(job.getSiteId());
			model.add("job", job); 
			model.add("jobsitename", siteName);
			model.add("jobdescription", UIUtils.escapeHtmlWithNewLine(job.getDescription()));
			model.add("providerMetadata", StringEscapeUtils.escapeHtml(vendormetadata));
			model.add("translationRequests", job.getTranslationRequests());
			model.add("translationVendors", entityManager.getAllTranslationProviders());
			model.add("showteam", configManager.get(ProducerCommonConfigs.CT_ENABLE_TEAM_MANAGEMENT));
			_setupPaginationVariables(job.getTranslationRequests());
			
			model.add("poNumbers", PONumberInfo.getAllPONumberInfo(configManager));
			
			model.add("poRequired", PONumberInfo.isPONumberRequired(configManager) &&
					!PONumberInfo.getPONumbers(configManager).isEmpty());
			model.add("jobs", findOwnJob(jobid));
		}
	}

	private List<TranslationJob> findOwnJob(String thisJobId) {
		List<TranslationJob> jobs = entityManager.findTranslationJobs(
				TranslationJobFilter.factory.filter("status='CREATED'"));
		if (session.isSudoSession()) {
			List<TranslationJob> ownJobs = new ArrayList<TranslationJob>();
			for (TranslationJob job : jobs) {
				if (job.getCreatedUser().equals(session.getUserProfile().getId()) &&
						job.getId()!=thisJobId) {
					ownJobs.add(job);
				}
			}
			return ownJobs;
		} else {
			return jobs;
		}
	}
	
	private void _setupPaginationVariables( List<TranslationRequest> list ) {
//		int page_maxperpage = configManager.getMaxItemsPerPage();
		int page_maxperpage = 5;
		int page_firstrecord = 0;
		if (Utilities.goodString(ics.GetVar("page_firstrecord"))) {
			page_firstrecord = Integer.parseInt( ics.GetVar("page_firstrecord") );
		}
		int page_totalrecords = list.size();
		List<Integer> page_startrecords = new ArrayList<Integer>();
		for (int i=0; i < list.size(); i=i+page_maxperpage ) {
			page_startrecords.add(i);
		}
	
		model.add("page_maxperpage", page_maxperpage);
		model.add("page_firstrecord", page_firstrecord);
		model.add("page_lastrecord", page_firstrecord + page_maxperpage - 1);
		model.add("page_totalrecords", page_totalrecords);
		model.add("page_startrecords", page_startrecords);
		
	}
	
	private void doJobPost() {
		// Setup variables from form post
		String jobid = ics.GetVar("jobid");
		String duedate = ics.GetVar("duedate");
		String vendorid = ics.GetVar("vendor");
		String vendorname = "";
		String jobname = ics.GetVar("jobname");
		String jobdesc = ics.GetVar("jobdesc");
		String poRef = ics.GetVar("ponumber");
		String sendonlymodified = ics.GetVar("sendonlymodified");
		String actionStr = ics.GetVar("action");
		ACTION action = ACTION.save;
		
		if (!StringUtils.isEmpty(actionStr)) {
			action = ACTION.valueOf(actionStr);
		}
		// Setup rest of the variables
		FTValList args = new FTValList();
		args.removeAll();
		args.setValString("app", ics.GetVar("app"));
		String message;

		try {
			// If the form was posted, update the job
			if (Utilities.goodString(jobid)) {
				
				args.setValString("showheaderfooter", "true");
				args.setValString("jobid", jobid );
				
				TranslationJob job = entityManager.getTranslationJob( jobid );
				args.setValString("jobname", job.getName() );
				args.setValString("siteId", job.getSiteId() );
				args.setValString("jobstatus", 
						job.getStatus().getDisplayName() + (job.isArchived()? "(Archived)" : "") );
				
				//archive button was clicked
				if (action==ACTION.delete) {
					if (job.getStatus()!=TranslationEntity.Status.CREATED) {
						message = "Cannot delete a job after it has been sent out.";
						args.setValString("message", message);
						args.setValString("jobid", jobid );
						args.setValString("jobname", job.getName() );
						args.setValString("jobstatus", job.getStatus().toString() );
						args.setValString("vendorname", vendorname );
					} else {
						entityManager.deleteTranslationJob(jobid);
						message = "Job has been successfully deleted.";
						args.setValString("message", message);
						args.setValString("jobid", "" );
						args.setValString("jobname", job.getName() );
						args.setValString("jobstatus", "Deleted" );
						args.setValString("vendorname", "" );
					}
				} else {
					if (action==ACTION.archive){
						LOG.debug("archiving jobid:"+jobid);
						job.setArchived(true);
						message = "Job successfully archived";
					} else if (action==ACTION.unarchive){
						LOG.debug("unarchiving jobid:"+jobid);
						job.setArchived(false);
						message = "Job successfully unarchived";
					} else {
						message = "Job successfully updated";
					}
		
					// Format date
					Date dDueDate = new Date();
					
					if (Utilities.goodString(duedate)) {
						SimpleDateFormat[] formats = new SimpleDateFormat[] {
								new SimpleDateFormat("yyyy-MM-dd"),
								new SimpleDateFormat("yyyy/MM/dd"),
								new SimpleDateFormat("MM/dd/yyyy"),
								new SimpleDateFormat("MM-dd-yyyy"),
						};
						for (SimpleDateFormat formatter : formats) {
							try {
								dDueDate = formatter.parse(duedate);
								job.setDueDate( dDueDate );
								break;
							} catch (ParseException e) {
								// ignore error
							}
						}
					}
					
					if (Utilities.goodString(vendorid)) {
						// Get vendor id
						TranslationProvider vendor = entityManager.getTranslationProvider(vendorid);
						
						if (vendor!=null) {
							vendorname = vendor.getName();
							// Update job
							job.setTranslationProvider(vendor);
							IMetadataGroup mdg = UIProviderMetadataManager.createMetadataGroup(ics);
							if (mdg!=null) {
								job.setProviderMetadata(mdg);
							} else {
								LOG.debug("provider no metadata");
							}
						}
					} else if (StringUtils.isEmpty(vendorid)) {
						job.setTranslationProvider(null);
					}
					if (Utilities.goodString(jobname)) {
						job.setName(jobname);
					}
					if (Utilities.goodString(jobdesc)) {
						job.setDescription(jobdesc);
					}
					job.setPoReference(poRef);
					
					//save every time
					job.setTranslateModifiedOnly("true".equalsIgnoreCase(sendonlymodified) ||
							"1".equals(sendonlymodified) || "on".equalsIgnoreCase(sendonlymodified));
					
					job = entityManager.save(job);
							
					args.setValString("jobname", job.getName() );
					args.setValString("jobstatus", 
							job.getStatus().getDisplayName() + (job.isArchived()? "(Archived)" : "") );
					
					// If "sendnow" was checked, send job
					if (action==ACTION.send || action==ACTION.copyback) {
						if (action==ACTION.send) {
							if (PONumberInfo.isPONumberRequired(configManager) && 
									!PONumberInfo.getPONumbers(configManager).isEmpty() && 
									!Utilities.goodString(poRef)) {
								throw new IllegalArgumentException("PO number is required");
							}
							
							job.setStatus(Status.SENDING);
						} else {
							if (validateLicense()) {
								job.setStatus(Status.SENDING_TO_COPY_BACK);
							} else {
								args.setValString("errorMessage", "Cannot process copy back job without a valid license id, " +
										"please contact your administrator to install a valid ClayTablet license");
							}
						}
						job = entityManager.save(job);
						
						message = "Job successfully updated, and will be sent out shortly";
					}
					args.setValString("message", message);
					args.setValString("jobid", jobid );
					args.setValString("jobname", job.getName() );
					args.setValString("jobstatus", 
							job.getStatus().getDisplayName() + (job.isArchived()? "(Archived)" : "") );
					args.setValString("vendorname", vendorname );
				}
			}
		} catch (Exception e) {
			args.setValString("errorMessage", "Failed to " + action.getDisplayName() + 
					" job: " + String.valueOf(e));
		}
		ics.InsertPage("CustomElements/CT/JobPostSummary", args);	
	}
	
	private boolean validateLicense() throws Exception {
		try {
			ConfigurationManager cm = session.getConfigurationManager();
			String licenseId = cm.get(ProducerCommonConfigs.CT_LICENSE_ID);
			if (!StringUtils.isEmpty(licenseId)) {
				CTPlatformUtil.getKeyHelper(session).getLicenseKeysInfo(licenseId);
				return true;
			} else {
				LOG.error(ProducerCommonConfigs.CT_LICENSE_ID + " has not been configured");
				return false;
			}
		} catch (Exception e) {
			LOG.error("Cannot validate Clay Tablet license id", e);
			return false;
		}
	}

	private void _addAssetsFromJobToWorkflow(TranslationJob job) {
		LOG.debug("Looping through translation requests looking for a workflow id");
		if (job != null) {
			for (TranslationRequest tr : job.getTranslationRequests()) {
				
				if (Utilities.goodString( tr.getTargetWorkflowId() )) {
					LOG.debug("Found workflow id " + tr.getTargetWorkflowId() );
					_addAssetToWorkflow(tr);
				}
				
			}
		} else {
			LOG.debug("Job is null");			
		}
		LOG.debug("Finished looping looking for workflow");
	}
	
	private void _addAssetToWorkflow(TranslationRequest tr) {
		
		String siteid = tr.getSiteId();
		String workflowprocid = tr.getTargetWorkflowId();
		String nativeid = tr.getNativeTargetId();
		AssetId aid = com.claytablet.wcs.system.Utils.stringToAssetId( nativeid );
		workflowManager.addOrAdvanceAssetInWorkflow(aid, siteid, workflowprocid, null);
				
	}
	
	private IMetadataGroup _createMetadataGroup(TranslationProvider vendor) {
		FreewayMetadataGroup freewayMetadataGroup = new FreewayMetadataGroup();
		if (vendor != null && vendor.getName().equals(TranslationProvider.Type.LIONBRIDGE_FREEWAY)) {
			String instruction = ics.GetVar("special_instruction");
			String analysis1name = ics.GetVar("analysis1_name");
			String analysis1value = ics.GetVar("analysis1_value");
			String analysis2name = ics.GetVar("analysis2_name");
			String analysis2value = ics.GetVar("analysis2_value");
			String analysis3name = ics.GetVar("analysis3_name");
			String analysis3value = ics.GetVar("analysis3_value");
			freewayMetadataGroup.setSpecialInstruction(instruction);
			freewayMetadataGroup.setAnalysisCodeValue(1, analysis1name, analysis1value);
			freewayMetadataGroup.setAnalysisCodeValue(2, analysis2name, analysis2value);
			freewayMetadataGroup.setAnalysisCodeValue(3, analysis3name, analysis3value);
		}
		return freewayMetadataGroup;
	}
}
