package com.claytablet.wcs.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.CTPlatformManager;
import com.claytablet.client.producer.CTPlatformManager.ProcessingResult;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.impl.ContentManagerImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.system.SessionFactory;

public class UISubmitTranslatedFilesManager implements Action {
	protected static final Log LOG = LogFactory.getLog(UISubmitTranslatedFilesManager.class.getName());
	@InjectForRequest private ICS ics;
	@InjectForRequest public Model model;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest private SiteManager siteManager;

	public void handleRequest(ICS ics) {
		File originalFile = new File(ics.GetVar("originalfile"));
		model.add("originalfile", originalFile.getName());
		String uploadedFile = ics.GetVar("uploadedfile");
		File file;
		
		if (StringUtils.isEmpty(uploadedFile) || !(file = new File(uploadedFile)).isFile()) {
			model.add("errorMessage", "Failed to load translated file");
		} else {
			String ext = FilenameUtils.getExtension(file.getName());
			ManagerFactorySession adminSession = session.sudoGlobalAdminSession();
			FileInputStream translatedFileStream = null;
			try {
				translatedFileStream = new FileInputStream(file);
				TranslationEntityManager em = adminSession.getTranslationEntityManager();
				ProcessingResult pr = null;
				if (ext.equalsIgnoreCase("XML")) {
					// import XML file directly
					CTPlatformManager pm = adminSession.getCTPlatformManager();
					pr = pm.importTranslatedFile(translatedFileStream,
							new ContentManagerImpl(ics, ics.GetSSVar("username"), 
									adminSession, SessionFactory.getSession()));
				} else if (ext.equalsIgnoreCase("ZIP")) {
					// import ZIP file package
					CTPlatformManager pm = adminSession.getCTPlatformManager();
					pr = pm.importTranslatedFilePackage(new ZipInputStream(translatedFileStream),
							new ContentManagerImpl(ics, ics.GetSSVar("username"), 
									adminSession, SessionFactory.getSession()));
				} else {
					model.add("errorMessage", "Failed to load file with unsupport extension '" + ext + "'");
				}
				if (pr!=null) {
					model.add("result", pr);
					List<TranslationJob> jobs = new ArrayList<TranslationJob>();
					Map<String, String> siteNames = new HashMap<String, String>();
					
					for (String jobId : pr.getUpdatedJobIds()) {
						TranslationJob job = em.getTranslationJob(jobId);
						String trSiteName;
						
						if (siteNames.containsKey(job.getSiteId())) {
							trSiteName = siteNames.get(job.getSiteId());
						} else {
							trSiteName = siteManager.getSiteNameById(job.getSiteId());
							siteNames.put(job.getSiteId(), trSiteName);
						}
						
						TranslationJobExt.FullInfo jobEx = TranslationJobExt.Factory.getFullInfo(
								job, em.getTranslationJobDetailStats(job.getId()), trSiteName);
						if (job!=null) {
							jobs.add(jobEx);
						}
					}
					model.add("jobs", jobs);
				}
			} catch (Exception e) {
				String msg = "Failed to import translated file '" + originalFile + "'";
				model.add("errorMessage", msg + ": " + e);
				LOG.error(msg, e);
			} finally {
				IOUtils.closeQuietly(translatedFileStream);
				file.delete();
				adminSession.close();
			}
		}
	}
}
