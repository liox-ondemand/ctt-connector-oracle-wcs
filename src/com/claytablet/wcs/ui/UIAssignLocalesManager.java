/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.IList;
import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserContext;
import com.claytablet.wcs.system.AssetListManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.UserManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.assetapi.def.AssetTypeDef;
import com.fatwire.assetapi.def.AssetTypeDefManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.AssetIdUtils;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAsset;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;
import com.fatwire.gst.foundation.facade.mda.DimensionUtils;
import com.fatwire.gst.foundation.facade.runtag.asset.AssetList;
import com.fatwire.gst.foundation.facade.runtag.asset.AssetLoadById;
import com.fatwire.gst.foundation.facade.runtag.asset.AssetSave;
import com.fatwire.mda.Dimension;
import com.fatwire.mda.DimensionableAssetInstance;
import com.claytablet.wcs.system.impl.AssetListManagerImpl;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.UserManagerImpl;
import com.claytablet.wcs.ui.UIUtils.SiteInfo;

import COM.FutureTense.Interfaces.Utilities;
import COM.FutureTense.Util.IterableIListWrapper;

/**
 * @author Mike Field
 *
 */
public class UIAssignLocalesManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UIAssignLocalesManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest public Model model;

	private UserContextImpl ctx;
	
	public void handleRequest(ICS ics) {
		ctx = new UserContextImpl(ics);
		
		String pagename = ics.GetVar("pagename");
		model.add("mypagename", pagename);
		
		//Get the full list of Locales in the System
		if(!Utilities.goodString(ics.GetVar("step"))){
			_setupLists();
		}
		
		//Display the list of assets w/o a Locale
		if("2".equals(ics.GetVar("step"))){
			model.add("step", "2");
			
			String locale = ics.GetVar("selectedlocale");
			model.add("locale",locale);
			
			String site = ics.GetVar("selectedsite");
			String pubid =
//                        localeManager.getLocaleIdByName(site);
                    siteManager.getSiteIdByName(site);

            LOG.debug ("handleRequest pubid=" +pubid);
            model.add("pubid",pubid);
			model.add("site",site);
			
			String assettypes = ics.GetVar("selectedassettypes");
			
			if (assettypes==null) {
				assettypes="";
			}
			//Get the list of Asset Candidates for Update
			model.add("assetlist", _getAssetsForLocaleAssignment(assettypes,pubid));
			
		}
		
		//Save the Assets with the selected Locale
		if("3".equals(ics.GetVar("step"))){
			
			String locale = ics.GetVar("selectedlocale");
			model.add("locale",locale);
			
			String site = ics.GetVar("selectedsite");
			model.add("site",site);
			
			String pubid = localeManager.getLocaleIdByName(site);
			
			String assetsforupdate = ics.GetVar("assetlist");
			if(Utilities.goodString(assetsforupdate)){
				//Update Assets
				model.add("assetlist", _assignLocaleToAssets(assetsforupdate,locale));
				model.add("formstatus", "complete");
			}
			model.add("step", '3');
		}

	}
	
	private void _setupLists() {
		
		model.add("eligiblelocales", _getEligibleLocales());
		model.add("eligiblesites",_getSites());
		model.add("enabledassettypes", Collections.emptyList());
		
	}
	
	private List<String> _getEnabledAssetTypes(String sitename){
		AssetListManager assetListManager = new AssetListManagerImpl(ics);
		
		ArrayList<String> assetTypes = new ArrayList<String>();
		Map<String,String> assetMap = assetListManager.getEnabledAssetTypesInSite(sitename);
		LOG.info("Getting asset types for site: "+sitename);
		assetTypes.addAll(assetMap.values());
		for(String s:assetTypes){
			LOG.info("... Found enabled asset type: "+s);
		}
		return assetTypes;
	}
	
	private List<String> _getEligibleLocales() {
		ArrayList<String> loc = new ArrayList<String>();
		loc.addAll(localeManager.getAllWCSLocales());
		Collections.sort(loc);
		return loc;
	}
	
	private List<SiteLocaleInfo> _getSites() {
		ArrayList<SiteLocaleInfo> sites = new ArrayList<SiteLocaleInfo>();
		Map<String,String> allsites = siteManager.getAllSites();
		UserManager um = new UserManagerImpl(ics);
	
		for (Map.Entry<String, String> site : allsites.entrySet())
		{
			String siteName = site.getValue();
			
			if(ctx.getUserProfile().isAdmin(site.getKey()) && _isDimensionEnabled(siteName)){
				sites.add(new SiteLocaleInfo(siteName, localeManager.getWCSLocalesForSite(siteName)));
			}
		}
		
		Collections.sort(sites);

		return sites;
	}
	
	public class SiteLocaleInfo implements Comparable<SiteLocaleInfo> {
		private final String name;
		private final List<String> locales;
		
		public SiteLocaleInfo(String name, List<String> locales) {
			super();
			this.name = name;
			this.locales = locales;
		}

		public String getName() {
			return name;
		}
		
		public List<String> getLocales() {
			return locales;
		}

		@Override
		public int compareTo(SiteLocaleInfo otherSite) {
			return name.compareTo(otherSite.name);
		}
	}
	
	boolean _isDimensionEnabled(String site){
		AssetListManager assetListManager = new AssetListManagerImpl(ics);
		boolean isDimEnabled=false;

		Map<String,String> enabledAssetTypes = assetListManager.getEnabledAssetTypesInSite(site);
		if(enabledAssetTypes!=null && enabledAssetTypes.containsValue("Dimension")){
			isDimEnabled=true;
		}
	 return isDimEnabled;
	
	}
	
	private List<Map<String, Object>> _getAssetsForLocaleAssignment(String assettypes, String pubid) {
        //LOG.debug("_getAssetsForLocaleAssignment... assettypes:"+assettypes);
        String[] ats = assettypes.split(";");
        
        List<Map<String, Object>> assetsForAssignment = new ArrayList<Map<String,Object>>();
        
        for(String type_for : ats) {
            String type = type_for.trim();
            AssetList al = new AssetList();
            al.setExcludeVoided(true);
            al.setType(type);
            //LOG.debug("_getAssetsForLocaleAssignment... setType:"+type);
    
            if(Utilities.goodString(pubid)) {
                al.setPubid(pubid);
                //LOG.debug("_getAssetsForLocaleAssignment... setPubid:"+pubid);
            }
            al.setList("assetList");
            al.execute(ics);
           
            if(ics.GetList("assetList") != null && ics.GetList("assetList").hasData()) {
                for (IList row : new IterableIListWrapper(ics.GetList("assetList"))) {
                    String cid = null;
                    try {
                        cid = row.getValue("id");
                    } catch (NoSuchFieldException e) {
                        throw new RuntimeException("Error getting asset 'id' from Asset List results.");
                    }
                    AssetId id = AssetIdUtils.createAssetId(type, cid);

                    // get the asset's locale
                    String assetLocale = _getLocale(id);
                    // if it doesn't have a locale already then grab it as a candidate for update
                    if(StringUtils.isBlank(assetLocale)) {
                    	assetsForAssignment.add(_getAsset(id));
                    }
                }
            }
            else {
                if(Utilities.goodString(pubid))
                    LOG.warn("NO assets found in pubid " + pubid + " for assettype=" + type);
                else
                    LOG.warn("NO assets found for assettype=" + type); 
            }
        }
        return assetsForAssignment;
    }
	
	private List<Map<String, Object>> _assignLocaleToAssets(String assets, String locale) {
        String[] ats = assets.split(";");
        
        List<Map<String, Object>> updatedAssets = new ArrayList<Map<String,Object>>();
        
        for(String asset : ats) {
    	
            AssetId id = AssetIdUtils.createAssetId(asset.split(":")[0], asset.split(":")[1]);

            // get the asset's locale (it shouldn't)
            String assetLocale = _getLocale(id);
            // if it doesn't have a locale already then update
            if(StringUtils.isBlank(assetLocale)) {
            	ics.ClearErrno();
        		Dimension dim = DimensionUtils.getDimensionForName(ics, locale);
        		_loadAsset(id);
                _setLocale("asset", dim);
                _saveAsset("asset");
                if(ics.GetErrno()==0){
                	updatedAssets.add(_getAsset(id));
                }else{
                	LOG.warn("Error updating Asset=" + asset + " with Locale="+locale+" Error No="+ics.GetErrno()); 
                }
            }
        }
        return updatedAssets;
    }

    private String _getLocale(AssetId id) {
        ics.RemoveVar("CTlocale:NAME");
        FTValList list = new FTValList();
        list.setValString("type", id.getType());
        list.setValString("objectid", Long.toString(id.getId()));
        list.setValString("prefix", "CTlocale");
        ics.runTag("ASSET.GETLOCALE", list);
        return ics.GetVar("CTlocale:NAME");
    }
    
    private void _setLocale(String assetObjName, Dimension dim) {
        DimensionableAssetInstance childDimAi = (DimensionableAssetInstance)ics.GetObj(assetObjName);
        Collection<Dimension> childDimList = new ArrayList<Dimension>();
        childDimList.add(dim);
        childDimAi.setDimensions(childDimList);
    }
    
    private Map<String,Object> _getAsset(AssetId id) {
    	TemplateAsset asset = assetDao.read(id);
    	Map<String,Object> a = new HashMap<String, Object>();
        a.put("id", asset.getAssetId().getId());
        a.put("type", asset.getAssetId().getType());
        a.put("name", asset.asString("name"));
        a.put("description", asset.asString("description"));
        return a;
    }
    
    private void _loadAsset(AssetId id) {
        AssetLoadById assetLoadById = new AssetLoadById();
        assetLoadById.setAssetType(id.getType());
        assetLoadById.setAssetId(id.getId());
        assetLoadById.setName("asset");
        assetLoadById.setEditable(true);
        assetLoadById.execute(ics);
    }

    private void _saveAsset(String assetObjName) {
        AssetSave assetSave = new AssetSave();
        assetSave.setName(assetObjName);
        assetSave.execute(ics);
    }
}
