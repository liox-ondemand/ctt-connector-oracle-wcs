package com.claytablet.wcs.ui;

import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.util.DelegateProxyFactory;

public interface TranslationRequestExt extends SiteNameAware {
	String getExtStatus();

	interface FullInfo extends TranslationRequest, TranslationRequestExt {}
	
	public class Factory {
		static public FullInfo getFullInfo(TranslationRequest req, final String siteName) {
			return DelegateProxyFactory.mashup(FullInfo.class, req, new Impl(req, siteName));
		}
	}
	public class Impl implements SiteNameAware {
		final private String siteName;
		final private String extStatus;
		
		public Impl(TranslationRequest req, String siteName) {
			this.siteName = siteName;
			this.extStatus = req.isInError()? "Error -" + req.getStatus() : req.getStatus().toString();
		}

		public String getSiteName() {
			return siteName;
		}
		
		public String getExtStatus() {
			return extStatus;
		}
	}
}