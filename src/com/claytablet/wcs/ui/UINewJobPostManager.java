package com.claytablet.wcs.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TeamProfile;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.util.ListUtils;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

public class UINewJobPostManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UINewJobPostManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		createJobAndReturnJobId(ics);
	}
	
	private void createJobAndReturnJobId(ICS ics) {

		String jobname = ics.GetVar("jobname"); // E.g. "Job 1"
		String jobdesc = ics.GetVar("jobdesc"); // E.g. "First Job"
		String teamname = ics.GetVar("teamname"); // E.g. "T-editor"
		String jobid = "";

		// If all required fields are present, create new job
		if (Utilities.goodString(jobname)) {

			TranslationJob job = entityManager.createTranslationJob(jobname);
			if (Utilities.goodString(teamname)) {
				TeamProfile tf = ListUtils.findByName(entityManager.getAllTeamProfiles(), teamname);
				job.setTeam( tf );
			}
			job.setDescription(jobdesc);
			job = entityManager.save(job);
			jobid = job.getId();
			ics.SetVar("jobid", jobid);
			LOG.debug("Created new job with id=" + jobid);
			
		} else {
			
			LOG.error("Tried to create a new job but some of the required fields were missing. jobname=" + jobname);
			
		}
		
	}

}
