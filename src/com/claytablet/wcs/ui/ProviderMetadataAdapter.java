package com.claytablet.wcs.ui;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.model.event.metadata.IMetadataGroup;

public interface ProviderMetadataAdapter {
	static final String PROVIDERTYPE_ARG = "providertype";
	static final String NEW_ELEMENT_NAME_ARG = "newElementName";
	static final String EDITABLE_ARG = "editable";
	static final String PROVIDERMETADATA_ARG = "providermetadata";
	static final String PROVIDERID_ARG = "providerid";
	
	TranslationProvider.Type getProviderType();
	String getElementName();
	IMetadataGroup createMetadataGroup(ICS ics);
}
