package com.claytablet.wcs.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationImportResult;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.impl.ConfigManagerImpl;
import com.claytablet.wcs.system.impl.SystemEventUtils;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIImportConfigurationManager implements Action {
	protected static final Log LOG = LogFactory.getLog(UIImportConfigurationManager.class.getName());
	@InjectForRequest private ICS ics;
	@InjectForRequest public Model model;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest private SiteManager siteManager;

	public void handleRequest(ICS ics) {
		String originalFile = ics.GetVar("originalfile");
		model.add("originalfile", originalFile);
		String uploadedFile = ics.GetVar("uploadedfile");
		boolean merge = "true".equalsIgnoreCase(ics.GetVar("merge"));
		File file;
		
		if (StringUtils.isEmpty(uploadedFile) || !(file = new File(uploadedFile)).isFile()) {
			model.add("errorMessage", "Failed to load configuration file");
		} else {
			String ext = FilenameUtils.getExtension(file.getName());
			ManagerFactorySession adminSession = session.sudoGlobalAdminSession();
			FileInputStream translatedFileStream = null;
			try {
				translatedFileStream = new FileInputStream(file);
				ConfigurationImportResult cir = null;
				if (ext.equalsIgnoreCase("XML")) {
					// import XML file directly
					ConfigurationManager cm = adminSession.getConfigurationManager();
					if (merge) {
						cir = cm.mergeConfiguration(IOUtils.toString(translatedFileStream, "UTF-8"), 
								siteManager.getAllSites().keySet(), merge);
					} else {
						cir = cm.importConfiguration(IOUtils.toString(translatedFileStream, "UTF-8"), 
								siteManager.getAllSites().keySet());
					}
					// update system events' polling interval
					Integer pollInterval = cm.get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_INTERVAL);
					SystemEventUtils.Event.AsyncOp.updateInterval(pollInterval, ics);
					pollInterval = cm.get(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL);
					SystemEventUtils.Event.CheckMessage.updateInterval(pollInterval, ics);
					
					List<String> siteIds = new ArrayList<String>(cir.getImportedSites().length);
					for (String siteId : cir.getImportedSites()) {
						if (StringUtils.isEmpty(siteId)) {
							siteIds.add("Global configuration");
						} else {
							siteIds.add(siteManager.getSiteNameById(siteId));
						}
					}
					model.add("sites", siteIds);
				} else {
					model.add("errorMessage", "Failed to load file with unsupport extension '" + ext + "'");
				}
				if (cir!=null) {
					model.add("result", cir);
				}
			} catch (Exception e) {
				String msg = "Failed to import configuration file '" + originalFile + "'";
				model.add("errorMessage", msg + ": " + e);
				LOG.error(msg, e);
			} finally {
				IOUtils.closeQuietly(translatedFileStream);
				file.delete();
				adminSession.close();
			}
		}
	}
}
