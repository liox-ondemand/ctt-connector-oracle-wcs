/**
 * 
 */
package com.claytablet.wcs.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.util.DiagnosticUtils;
import com.claytablet.wcs.CTObjectFactory;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.UserManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * @author Mike Field
 *
 */
public class UIAuthorizationManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UIAuthorizationManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest public Model model;
	
	@Override
	public void handleRequest(ICS ics) {
		if (CTObjectFactory.hasInitError()) {
			model.add("isSiteEnabled", false);
			model.add("isUserAuthorized", false);
		} else {
			try {
				CTObjectFactory ctFactory = new CTObjectFactory(ics);
				ConfigManager configManager = ctFactory.createConfigManager(ics);
				SiteManager siteManager = ctFactory.createSiteManager(ics);
				UserManager userManager = ctFactory.createUserManager(ics);
				String username = ics.GetSSVar("username");
				String pubid = ics.GetSSVar("pubid");
				String sitename = siteManager.getSiteNameById(pubid);
				Boolean authorized = userManager.hasCTUserRole(username, sitename) || 
						userManager.hasCTSiteAdminRole(username, sitename);
				model.add("isUserAuthorized", authorized);
				if (authorized) {
					model.add("isSiteEnabled", configManager.isEnabledSite(pubid));
				} else {
					model.add("isSiteEnabled", false);
				}
			} catch (Throwable t) {
				LOG.debug("Cannot get managers from CTObjectFactory, ignoring...", t);
				model.add("isSiteEnabled", false);
				model.add("isUserAuthorized", false);
			}
		}
	}
	
}
