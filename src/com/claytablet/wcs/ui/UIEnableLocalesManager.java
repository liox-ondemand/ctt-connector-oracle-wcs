package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserContext;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.fatwire.assetapi.common.SiteAccessException;
import com.fatwire.assetapi.data.AssetData;
import com.fatwire.assetapi.data.AssetDataManager;
import com.fatwire.assetapi.data.AssetDataManagerImpl;
import com.fatwire.assetapi.data.MutableAssetData;
import com.fatwire.assetapi.site.Site;
import com.fatwire.assetapi.site.SiteInfo;
import com.fatwire.assetapi.site.SiteManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;
import com.fatwire.system.SessionFactory;

/**
 * @author Mike Field
 *
 */
public class UIEnableLocalesManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UIEnableLocalesManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest private ConfigManager configManager;
	@InjectForRequest private TranslationRequestManager translationRequestManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest private ManagerFactory managerFactory;
	@InjectForRequest public Model model;
	
	@SuppressWarnings("null")
	public void handleRequest(ICS ics) {
		
		String action = ics.GetVar("action");

		if ( Utilities.goodString(action) && "enable".equalsIgnoreCase(action) ) {

			// Enable new locale and add mapping
			
			String wcslocale = ics.GetVar("dimension_name");
			String ctlocale = ics.GetVar("mapped_value");
			_enableNewLocale();
			_addMapping(wcslocale, ctlocale);
			
		} else if ( Utilities.goodString(action) && "update".equalsIgnoreCase(action)) { 
		
			// Update mapped locales
			_updateValues();
			
		}

		// Once form processing is complete, load locale list, site list and locale mappings
		_loadLocalesAndMappings();
		
	}
	
	private void _updateValues() {
		SiteManager siteManager = getSiteManager();
		
		boolean showHidden = "true".equalsIgnoreCase(ics.GetVar("show_hidden"));
		List<String> wcslocales = localeManager.getAllWCSLocales();
		for (String wcslocale : wcslocales) {
			try {
				String isHidden = ics.GetVar("is_hidden_" + wcslocale);

				// only process locale changes
				if (showHidden || !"true".equalsIgnoreCase(isHidden)) {
					String ctlocale = ics.GetVar("mapped_value_" + wcslocale);
					String newname = ics.GetVar("newname_" + wcslocale);
					String newdesc = ics.GetVar("newdesc_" + wcslocale);
					String edited = ics.GetVar("edited_" + wcslocale);
					String addedSites = ics.GetVar("added_sites_" + wcslocale);
					String sLocaleId = localeManager.getLocaleIdByName(wcslocale);
					String hidden = ics.GetVar("hidden_" + wcslocale);
					if (Utilities.goodString(newname) && Utilities.goodString(newdesc) && 
							Utilities.goodString(sLocaleId) && "true".equalsIgnoreCase(edited)) {
						Long localeid = Long.parseLong(sLocaleId);
						localeManager.updateLocaleNameAndDescription(localeid, newname, newdesc);
						_removeMapping(wcslocale);
						_addMapping(newname, ctlocale);
						if (!wcslocale.equals(newname)) {
							localeManager.setLocaleHidden(wcslocale, false);
						}
						localeManager.setLocaleHidden(newname, Utilities.goodString(hidden));
					}
					if (Utilities.goodString(addedSites)) {
						List<String> siteNames = Arrays.asList(addedSites.split(","));
						
					    List<SiteInfo> sitesList = null;
				        sitesList = siteManager.list();
				        
						ArrayList<String> pubList = new ArrayList<String>();
						
						if (sitesList != null) {
							for (int i=0;i< sitesList.size();i++ ){
								String curr = sitesList.get(i).getName();
								LOG.info("Processing "+curr+" ...");
								if(siteNames.contains(curr)) {
									LOG.info("Adding "+curr+" to pubList.");
									pubList.add(((SiteInfo)sitesList.get(i)).getName());
								} else {
									LOG.info("NOT adding "+curr+" to pubList.");
								}
							}	
							
							List<Site> sites = siteManager.read(pubList);
							for (Site site : sites) {
								enableDimensionOnSite(siteManager, site);
							}
						}
						for (String siteName : siteNames) {
							if (siteName!=null) {
								siteName = siteName.trim();
							}
							if (!StringUtils.isEmpty(siteName)) {
								localeManager.enableLocaleForSite(wcslocale, siteName);
							}
						}
					}
				}
			} catch (Exception e) {
				LOG.error("Error enabling locale :" + wcslocale, e);
			}
		}
		
	}
	
	private void _enableNewLocale() {
		
		String dimdescr = ics.GetVar("dimension_descr");
		String sUsername = ics.GetSSVar("username");
		String dimname = ics.GetVar("dimension_name");
		String submittedSites = ics.GetVar("sitename");
		
		Map <String, Boolean> submittedSitesMap = new HashMap<String, Boolean>();
		if(Utilities.goodString(submittedSites)) {
			LOG.info("sitesSubmitted are: "+submittedSites);
			submittedSitesMap = buildSubmittedSitesMap(submittedSites);
		}

		if( Utilities.goodString(dimname) && Utilities.goodString(dimdescr) ){
			//do insert and redirect to Post template
			
			AssetDataManager assetDataMgr = new AssetDataManagerImpl(ics);
			try{

				MutableAssetData ad = assetDataMgr.newAssetData("Dimension","Locale");
				ad.getAttributeData("name").setData(dimname);
				ad.getAttributeData("description").setData(dimdescr);						
				ad.getAttributeData("createdby").setData(sUsername);
				Calendar calendar = Calendar.getInstance();
				java.util.Date now = calendar.getTime();
				ad.getAttributeData("createddate").setData(now);
				ad.getAttributeData("updatedby").setData(sUsername);
				ad.getAttributeData("updateddate").setData(now);
				ad.getAttributeData("subtype").setData("Locale");
				SiteManager siteManager = getSiteManager();
			    List<SiteInfo> sitesList = null;
			    try {
			        sitesList = siteManager.list();
			    } catch (SiteAccessException e) {
			        LOG.debug("Error getting Site list:"+e);
			    }
				ArrayList<String> pubList = new ArrayList<String>();
				
				if (sitesList != null) {
					for (int i=0;i< sitesList.size();i++ ){
						String curr = sitesList.get(i).getName();
						LOG.info("Processing "+curr+" ...");
						if(submittedSitesMap.containsKey(curr)) {
							LOG.info("Adding "+curr+" to pubList.");
							pubList.add(((SiteInfo)sitesList.get(i)).getName());
						} else {
							LOG.info("NOT adding "+curr+" to pubList.");
						}
					}	
					ad.getAttributeData("Publist").setData(pubList);
					
					List<Site> sites = siteManager.read(pubList);
					for (Site site : sites) {
						enableDimensionOnSite(siteManager, site);
					}
				}
				ArrayList<AssetData> adlist = new ArrayList<AssetData>();
				adlist.add(ad);
				//Attempt and insert
				assetDataMgr.insert(adlist);

			} catch (Exception e) {
				
				LOG.error("Error Inserting Dimension:"+e);
			}
			
			model.add("msg", "The locale "+dimname+" was successfully created.");

		}
		
	}

	private void enableDimensionOnSite(SiteManager siteManager, Site site)
			throws SiteAccessException {
		// enable dimension on the site
		List<String> assetTypes = site.getAssetTypes();
		if (!assetTypes.contains("Dimension") || !assetTypes.contains("DimensionSet")) {
			assetTypes = new ArrayList<String>(assetTypes);
			if (!assetTypes.contains("Dimension")) {
				assetTypes.add("Dimension");
			}
			if (!assetTypes.contains("DimensionSet")) {
				assetTypes.add("DimensionSet");
			}
			site.setAssetTypes(assetTypes);
			siteManager.update(Collections.singletonList(site));
		}
	}
	
	private void _addMapping(String wcslocale, String ctlocale) {
		
		if (Utilities.goodString(wcslocale) && Utilities.goodString(ctlocale)) {
			
			UserContext userContext = new UserContextImpl(ics);			
			ManagerFactorySession adminSession = managerFactory.newGlobalAdminSession(userContext);
			try {
				adminSession.getLanguageMappingManager().addLanguageMapping(wcslocale, ctlocale);
				//LOG.debug("Added language mapping where "+wcslocale+"="+ctlocale);
			} finally {
				adminSession.close();
			}			
			
		}
		//LOG.info("Finished trying to add language mapping where "+wcslocale+"="+ctlocale);
		
	}
	
	private void _removeMapping(String wcslocale) {
		
		if (Utilities.goodString(wcslocale)) {
			
			UserContext userContext = new UserContextImpl(ics);			
			ManagerFactorySession adminSession = managerFactory.newGlobalAdminSession(userContext);
			try {
				adminSession.getLanguageMappingManager().removeLanguageMapping(wcslocale);
				//LOG.info("Removed language mapping for locale "+wcslocale);
			} finally {
				adminSession.close();
			}			
			
		}
		LOG.info("Finished trying to remove language mapping for locale "+wcslocale);
		
	}

	
	private Map<String,String> getLanguageMappings( List<String> wcslocales ) {
		
		UserContext userContext = new UserContextImpl(ics);			
		Map<String,String> mappings = new HashMap<String,String>();
		ManagerFactorySession adminSession = managerFactory.newGlobalAdminSession(userContext);
		try {
			for (String s : wcslocales) {
				String mappedval = adminSession.getLanguageMappingManager().nativeLangToCTLang(s);
				mappings.put(s, mappedval);
				//LOG.debug("Found mapping "+s+"="+mappedval);
			}	
		} finally {
			adminSession.close();
		}
		return mappings;
		
	}
	
	private Map<String, List<String>> getSharedSites( List<String> wcslocales ) {
		Map<String,List<String>> sharedsites = new HashMap<String,List<String>>();
		for (String s : wcslocales) {
			List<String> sites = localeManager.getSharedSitesForLocale( s );
			sharedsites.put(s, sites);
			LOG.debug("The locale "+s+" is shared to: "+sites);
		}	
		return sharedsites;		
	}
	
	private Map<String, List<String>> getAvailableSites( List<String> allSites, 
			Map<String, List<String>> sharedSites ) {
		Map<String,List<String>> availableSites = new HashMap<String,List<String>>();
		for (String locale : sharedSites.keySet()) {
			List<String> sites = localeManager.getSharedSitesForLocale( locale );
			List<String> availSites = new ArrayList<String>(allSites.size());
			for (String site : allSites) {
				if (!sites.contains(site)) {
					availSites.add(site);
				}
			}
			availableSites.put(locale, availSites);
		}	
		return availableSites;		
	}
	
	private void _loadLocalesAndMappings() {
		
		List<String> allLocales = localeManager.getAllWCSLocales();
		List<String> locales = new ArrayList<String>();
		List<String> hiddenLocales = new ArrayList<String>();

		for (String locale : allLocales) {
			if (localeManager.isLocaleHidden(locale)) {
				hiddenLocales.add(locale); 
			} else {
				locales.add(locale);
			}
		}
		Collections.sort(locales);
		Collections.sort(hiddenLocales);
		model.add("locales", locales);
		model.add("hiddenlocales", hiddenLocales);
		List<String> sitesList = getSitesList();
		model.add("siteslist", sitesList);
		model.add("languagemappings", getLanguageMappings(allLocales) );
		Map<String, List<String>> sharedSites = getSharedSites(allLocales);
		model.add("sharedsites", sharedSites);
		model.add("availablesites", getAvailableSites(sitesList, sharedSites));
		model.add("localeHiddenMap", getLocaleHiddenMap(allLocales));
		model.add("showHidden", "true".equalsIgnoreCase(ics.GetVar("show_hidden")));
	}
	
	private Map<String,Boolean> getLocaleHiddenMap( List<String> wcslocales ) {
		
		Map<String,Boolean> mappings = new HashMap<String,Boolean>();
		for (String locale : wcslocales) {
			mappings.put(locale, localeManager.isLocaleHidden(locale));
		}	
		return mappings;
		
	}
	
	private Map<String, Boolean> buildSubmittedSitesMap(String s)
	{
		Map <String, Boolean> ret = new HashMap<String,Boolean>();
		String [] split = s.split(";");
		for(String c:split)
		{
			ret.put(c, new Boolean(true));
		}
		return ret;
	}
	
	private List<String> getSitesList(){
		LOG.info("Getting SitesList...");
		SiteManager sm = getSiteManager();
	    List<SiteInfo> sitesList = null;
	    
	    try {
	        sitesList = sm.list();
	    } catch (SiteAccessException e) {
	        LOG.debug("Error getting Site list:"+e);
	    }
	    
		ArrayList<String> pubList = new ArrayList<String>();
		if(sitesList != null)
		{
			for (int i=0;i< sitesList.size();i++ ){
				pubList.add(((SiteInfo)sitesList.get(i)).getName());
			
			}
			LOG.info("Siteslist has "+pubList.size()+" values.");
			return pubList;
		}
		LOG.info("Siteslist has no values.");
		return null;
		
	}

	private SiteManager getSiteManager() {
		SiteManager sm = (SiteManager) SessionFactory.getSession(ics).getManager(SiteManager.class.getName());
		return sm;
	}

}
