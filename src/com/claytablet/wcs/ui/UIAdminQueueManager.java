/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.TranslationEntity.Status;
import com.claytablet.wcs.CTObjectFactory;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

/**
 * @author Mike Field
 *
 */
public class UIAdminQueueManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UIAdminQueueManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;
	/*TO DO: Replace with something other than a workaround.*/
	//@InjectForRequest private ManagerFactory managerFactory; 
	
	public void handleRequest(ICS ics) {
			String pagename = ics.GetVar("pagename");
			if ("CustomElements/CT/WEM/QueuePost".equals(pagename)) {
				doQueuePost();
			} else {
				doQueueFront();
			}
	}
	
	private void doQueueFront() {
		
		try{
			/*TO DO: Replace with something other than a workaround.*/
			
			LOG.info("Updating factory generation and admin session to reflect and make use of new CTObjectFactory");
			// ManagerFactory factory = ManagerFactory.Init.newMockManagerFactory(); 
			CTObjectFactory factory = new CTObjectFactory(ics);
			
			UserContext ctx = new UserContextImpl(ics);
			
			//ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
			ManagerFactorySession adminSession = factory.createManagerFactorySession(ics);
			
			TranslationEntityManager em = adminSession.getTranslationEntityManager();
			model.add("translationRequests", em.getAllQueuedTranslationRequests());
			
			//only jobs not sent yet
			List<TranslationJob> jobs = new ArrayList<TranslationJob>();
			for(TranslationJob job:em.getAllTranslationJobs()){
				Status currentstatus = job.getStatus();
				if(job.getStatus().isActive() && !job.isCompleted()){
					jobs.add(job);
				}
			
			}
			model.add("jobs", jobs);
			model.add("message",ics.GetVar("message"));
			
		}finally{
			session.close(); 
		}
	}
	
	private void doQueuePost() {
		try{
			
			String action =  ics.GetVar("action");
			String formpostval_trids = ics.GetVar("trids");
			String formpostval_jobid = ics.GetVar("jobid");
			
			String[] trids = formpostval_trids.split(";");
			String jobname = null;
			FTValList args = new FTValList();
			
			/*TO DO: Replace with something other than a workaround.*/
			//ManagerFactory factory = ManagerFactory.Init.newMockManagerFactory(); 
			CTObjectFactory factory = new CTObjectFactory(ics);
			UserContext ctx = new UserContextImpl(ics);
			ManagerFactorySession adminSession = factory.createManagerFactorySession(ics);
			TranslationEntityManager em = adminSession.getTranslationEntityManager();
			
			//Remove TR
			if (Utilities.goodString(action) && "remove".equals(action)){
				//Do the Remove Translation Request for the ids checked off
				for (String trid : trids) {
					em.deleteTranslationRequest(trid);
				}
				
		      	model.add("translationRequests", em.getAllQueuedTranslationRequests());
		      	args.removeAll();
		      	args.setValString("message", "Translation requests successfully removed.");
		      	
		      	ics.InsertPage("CustomElements/CT/WEM/QueueFront", args);	

			}
			//Add TR to Job
			if (Utilities.goodString(action) && "addtojob".equals(action) && Utilities.goodString(formpostval_jobid)){
				TranslationJob job = em.getTranslationJob(formpostval_jobid);
				//Do the Remove Translation Request for the ids checked off
				for (String trid : trids) {
					TranslationRequest tr = em.getTranslationRequest(trid);
					
					tr.setTranslationJob(job);
					tr = em.save(tr);
				}

		      	model.add("translationRequests", em.getAllQueuedTranslationRequests());
		      	args.removeAll();
		      	
		      	args.setValString("message","Translation requests successfully added to "+job.getName()+".");
		      	ics.InsertPage("CustomElements/CT/WEM/QueueFront", args);	

			}
		}finally{
			session.close(); 
		}
		
	}
	
	

}
