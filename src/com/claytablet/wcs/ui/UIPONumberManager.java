package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserContext;
import com.claytablet.wcs.system.AssetListManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.UserManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.PONumberInfo;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.UserManagerImpl;
import com.claytablet.wcs.ui.UIUtils.SiteInfo;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

public class UIPONumberManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UIPONumberManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ConfigurationManager configManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		UserContextImpl ctx = new UserContextImpl(ics);
		
		String pagename = ics.GetVar("pagename");

		Map<String, String> allSites = siteManager.getAllSites();
		List<SiteInfo> sites = new ArrayList<SiteInfo>();
		
		for (Map.Entry<String, String> entry : allSites.entrySet()) {
			if (ctx.getUserProfile().isAdmin(entry.getKey())) {
				sites.add(new SiteInfo(entry.getKey(), entry.getValue()));
			}
		}
		Collections.sort(sites);
		model.add("sites", sites);
		
		String siteId = ctx.getSiteId();
		model.add("cursiteid", siteId);
		model.add("requireponumber", PONumberInfo.isPONumberRequired(configManager));
	}
}
