package com.claytablet.wcs.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * Abstract action class that automatically closes the ManagerFactorySession.
 * @author Tony Field
 * @since September 24, 2014
 */
public abstract class AbstractAction implements Action {

	@InjectForRequest private ManagerFactorySession mfs;

	/**
	 * Final implementation of handleRequest which manages the lifecycle of delicate
	 * objects.
	 * @param ICS
 	 */
	public final void handleRequest(ICS ics) {
		try {
			handleRequest();
		} finally {
			if (mfs != null && mfs.isOpen()) {
				mfs.close();
			}
		}
	}

	/**
	 * Primary action worker method.
	 */
	public abstract void handleRequest();
}
