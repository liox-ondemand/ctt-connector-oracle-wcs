/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Factory;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.action.support.IcsFactoryUtil;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

import com.fatwire.gst.foundation.facade.assetapi.asset.ScatteredAsset;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAsset;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

import com.fatwire.gst.foundation.facade.mda.LocaleService;


/**
 * @author Mike Field
 *
 */
public class UIViewTranslationsManager implements Action {
	
	protected static final Log LOG = LogFactory.getLog(UIViewTranslationsManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
			String pagename = ics.GetVar("pagename");
			String assettype = ics.GetVar("c");
			String assetid = ics.GetVar("cid");
			
			TemplateAsset asset = assetDao.readCurrent();
			
			
			model.add("c", assettype);
			model.add("cid", assetid);
			
			model.add("asset", new ScatteredAsset(asset.getDelegate()));
			
			model.add("mypagename", pagename);
		
			AssetId aId = assetDao.createAssetId(assettype,assetid);
			
			Factory factory = IcsFactoryUtil.getFactory(ics);
	        LocaleService localeService = factory.getObject("localeService", LocaleService.class);
	        
	        Map<String,Object> translatedAsset;
	        List<Map<String,Object>> translatedAssetsList = new ArrayList<Map<String,Object>>();
			
			List<AssetId> translationsList = (List<AssetId>) assetManager.getTranslations(aId);
			for(AssetId id: translationsList ){
				translatedAsset = new HashMap<String,Object>();
				
				TemplateAsset ta = assetDao.read(id);
				
				translatedAsset.put("name", ta.asString("name"));
				translatedAsset.put("locale", localeService.getLocaleForAsset(id).getName());
				translatedAsset.put("id",id.getId()+"");
				translatedAsset.put("type",id.getType());
				
				translatedAssetsList.add(translatedAsset);
			}
			
			model.add("translatedassets", translatedAssetsList);

		
	}

}