package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.wcs.system.AssetListManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.SubTypeManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.assetapi.def.AssetTypeDef;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

public class UIWCSAssetTypesManager implements Action {
	enum ACTION {
		SelectAttrs,
		DeselectAttrs,
		EnableDefault,
		DisableDefault,
	}
	
	protected static final Log LOG = LogFactory.getLog(UIWCSConfigManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest private ConfigManager configManager;
	@InjectForRequest private TranslationRequestManager translationRequestManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private AssetListManager assetListManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;
	@InjectForRequest private SubTypeManager subTypeManager;
	
	public void handleRequest(ICS ics) {
		String siteId = ics.GetVar("siteid");
		String translateAll = ics.GetVar("translateall");
		String selectedType = ics.GetVar("selectedType");
		String actionStr = ics.GetVar("action");
		String selectedAttrs = ics.GetVar("selectedAttrs");
		String selectedDefinition = ics.GetVar("selectedDefinition");
		
		if (!StringUtils.isEmpty(translateAll)) {
			configManager.setEnabledByDefault(siteId, Boolean.valueOf(translateAll));
		}
		if (!StringUtils.isEmpty(actionStr) && !StringUtils.isEmpty(selectedType)) {
			ACTION action = ACTION.valueOf(actionStr);
			
			switch (action) {
			case SelectAttrs:
				List<String> attrsSelected = Arrays.asList(selectedAttrs.split(","));
				attrsSelected.remove("");
				List<String> enabledAttrs = new ArrayList<String>(
						configManager.getEnabledAttributes(siteId, selectedType));
				enabledAttrs.addAll(attrsSelected);
				configManager.setEnabledAttributes(siteId, selectedType, enabledAttrs); 
				break;							
			case DeselectAttrs:
				List<String> attrsDeselected = Arrays.asList(selectedAttrs.split(","));
				attrsDeselected.remove("");
				List<String> unselectedAttrs = 
						configManager.isAllAttributesEnabledByDefault(siteId, selectedType)?
								new ArrayList<String>(assetManager.getTranslatableAttributes(selectedType)) :
								new ArrayList<String>(configManager.getEnabledAttributes(siteId, selectedType));
				unselectedAttrs.removeAll(attrsDeselected);
				configManager.setEnabledAttributes(siteId, selectedType, unselectedAttrs); 
				break;
			case EnableDefault:
				configManager.setEnabledByDefault(siteId, selectedType); 
				break;
			case DisableDefault:
				configManager.setEnabledAttributes(siteId, selectedType, 
						Collections.<String>emptyList());
				break;
			}
		}
		String siteName = siteManager.getSiteNameById(siteId);
		List<AssetTypeInfo> assetTypes = new ArrayList<AssetTypeInfo>();
		List<AssetTypeInfo> assetTypesNotTranslatable = new ArrayList<AssetTypeInfo>();
		List<String> types = new ArrayList<String>(
				assetListManager.getEnabledAssetTypesInSite(siteName).values());
		Collections.sort(types);
		
		// figure out subtypes here
		List<String> subTypeNames = new ArrayList<String>();
		if (Utilities.goodString(selectedType)){
			List<AssetTypeDef> defs = this.subTypeManager.getSubTypesForAssetType(selectedType, siteId);
			
			for(AssetTypeDef d : defs){
				subTypeNames.add(d.getSubtype());
			}
		}		
		model.add("subTypeNames", subTypeNames);
		if (!Utilities.goodString(selectedDefinition)){
			selectedDefinition = "";
		}
		
		if (selectedDefinition.equals("Any")){
			model.add("selectedDefinition", "Any");
			selectedDefinition = "";
		} else {
			model.add("selectedDefinition", selectedDefinition);
		}
		
		for (String assetType : types) {
			AssetTypeInfo assetTypeInfo = new AssetTypeInfo(siteId, assetType);
			if (assetTypeInfo.getTranslatableAttributes().size()>0) {
				assetTypes.add(assetTypeInfo);
			
				if (assetType.equals(selectedType)) {
					if (Utilities.goodString(selectedDefinition)){
						assetTypeInfo = new AssetTypeInfo(siteId, assetType, selectedDefinition);
					}
					model.add("selectedtype", assetTypeInfo);
				}
			} else {
				assetTypesNotTranslatable.add(assetTypeInfo);
			}
		}
		model.add("assettypes", assetTypes);		
		model.add("assettypes_without_attr", assetTypesNotTranslatable);
		model.add("translateall", configManager.isAllAttributesEnabledByDefault(siteId));
	}
	
	public class AssetTypeInfo {
		private final String name;
		final private List<String> translatableAttributes;
		final private List<String> selectedAttributes = new ArrayList<String>();
		final private List<String> unselectedAttributes = new ArrayList<String>();
		private boolean translateAll;
		
		public AssetTypeInfo(String siteid, String name) {
			super();
			this.translateAll = configManager.isAllAttributesEnabledByDefault(siteid, name);
			this.name = name;			
			
			translatableAttributes = assetManager.getTranslatableAttributes(name);
			
			for (String attr : getTranslatableAttributes()) {
				if (configManager.isEnabledAttribute(siteid, name, attr)) {
					selectedAttributes.add(attr);
				} else {
					unselectedAttributes.add(attr);
				}
			}
		}
		
		public AssetTypeInfo(String siteid, String name, String selectedDefinition) {
			super();
			this.translateAll = configManager.isAllAttributesEnabledByDefault(siteid, name);
			this.name = name;			
			
			translatableAttributes = assetManager.getTranslatableAttributes(name, selectedDefinition);
			
			for (String attr : getTranslatableAttributes()) {
				if (configManager.isEnabledAttribute(siteid, name, attr)) {
					selectedAttributes.add(attr);
				} else {
					unselectedAttributes.add(attr);
				}
			}
		}

		public boolean isTranslateAll() {
			return translateAll;
		}
		
		public String getName() {
			return name;
		}

		public List<String> getTranslatableAttributes() {
			return translatableAttributes;
		}

		public List<String> getSelectedAttributes() {
			return selectedAttributes;
		}
		
		public List<String> getUnselectedAttributes() {
			return unselectedAttributes;
		}
	}
}
