package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationEntity.Status;
import com.claytablet.client.producer.TranslationJobDetailStats;
import com.claytablet.client.util.DelegateProxyFactory;

public interface TranslationJobExt extends SiteNameAware  {
	Map<Status, Integer> getRequestCountsByStatusMap(); 
	Map<Status, Integer> getRequestErrorCountsByStatusMap(); 
	List<Status> getRequestStatuses(); 
	int getRequestCount();
	int getRequestErrorCount();
	
	interface FullInfo extends TranslationJob, TranslationJobExt {	}

	public class Factory {
		static public FullInfo getFullInfo(TranslationJob job, 
				TranslationJobDetailStats stats, final String sitename) {
			return DelegateProxyFactory.mashup(FullInfo.class, job, new Impl(job, stats, sitename));
		}
	}
	
	public class Impl implements TranslationJobExt {
		final Map<Status, Integer> countsMap;
		final Map<Status, Integer> errorCountsMap;
		final List<Status> statuses;
		private String sitename;
		private int requestCount;
		private int requestErrorCount;
		
		public Impl(TranslationJob job, TranslationJobDetailStats stats, final String sitename) {
			this.sitename = sitename;
			countsMap = stats.getTranslationRequestsCountMap();
			errorCountsMap = stats.getTranslationRequestsErrorCountMap();
			statuses = new ArrayList<Status>(countsMap.keySet());
			Collections.sort(statuses);
			requestCount = stats.getTranslationRequestsCount();
			requestErrorCount = stats.getTranslationRequestsErrorCount();
		}
		
		@Override public String getSiteName() { return sitename; }
		@Override public Map<Status, Integer> getRequestCountsByStatusMap() { 
			return countsMap; 
		}
		@Override public List<Status> getRequestStatuses() {
			return statuses;
		}
		@Override
		public int getRequestCount() {
			return requestCount;
		}
		@Override
		public Map<Status, Integer> getRequestErrorCountsByStatusMap() {
			return errorCountsMap;
		}

		public int getRequestErrorCount() {
			return requestErrorCount;
		}
	}
}
