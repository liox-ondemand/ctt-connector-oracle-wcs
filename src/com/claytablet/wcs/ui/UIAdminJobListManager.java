/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntity.Status;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobStats;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.util.DelegateProxyFactory;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.UserManager;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.UserManagerImpl;
import com.claytablet.wcs.ui.UIUtils.SiteInfo;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * @author Mike Field
 * Manages JobList for the WEM app
 */
public class UIAdminJobListManager implements Action {
	private enum ACTION {
		archive,
		unarchive
	}

	protected static final Log LOG = LogFactory.getLog(UIAdminJobListManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ConfigManager cm;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest public Model model;
	
	private TranslationEntityManager entityManager;
	
	public void handleRequest(ICS ics) {
		String pagename = ics.GetVar("pagename");

		String app = ics.GetVar("app");
		String siteId = ics.GetVar("selectSite");
		
		if ("WEM".equals(app)) {
			UserContextImpl ctx = new UserContextImpl(ics);

			if (StringUtils.isEmpty(siteId)) {
				siteId = ctx.getSiteId();
			}
			model.add("cursiteid", siteId);
			
			Map<String, String> allSites = siteManager.getAllSites();
			List<SiteInfo> sites = new ArrayList<SiteInfo>();
			
			for (Map.Entry<String, String> entry : allSites.entrySet()) {
				if (ctx.getUserProfile().isAdmin(entry.getKey())) {
					sites.add(new SiteInfo(entry.getKey(), entry.getValue()));
				}
			}
			Collections.sort(sites);
			model.add("sites", sites);
		}
			
		model.add("message", ics.GetVar("message"));
		
		model.add("mypagename", pagename);
		model.add("pagesize", cm.getMaxItemsPerPage());
		model.add("showArchived", ics.GetVar("showarchived"));
	}
	
	private void doJobListPost(ACTION action) {
		int successCount = 0;
		int errorCount = 0;
		TranslationJob job = null;
		String[] jobIds;
		
		switch (action) {
		case archive:
			String toArchiveJobIdStr = ics.GetVar("to_archive_jobids");
			jobIds = StringUtils.isEmpty(toArchiveJobIdStr)? new String[0] : toArchiveJobIdStr.split(",");
			for (String jobId : jobIds) {
				try {
					job = entityManager.getTranslationJob(jobId);
					entityManager.save(job.setArchived(true));
					successCount ++;
				} catch (Exception e) {
					errorCount ++;
					LOG.error("Cannot archive TranslationJob '" + (job==null? jobId: job) + "'", e);
				}
			}
			if (errorCount>0) {
				String msg = "Failed to archived " + errorCount + " translation jobs.";
				if (successCount>0) {
					msg += "\nSuccessfully archived " + successCount + " translation jobs.";
				}
				model.add("errorMessage", msg);
			} else if (successCount>0) {
				model.add("archiveMessage", "Successfully archived " + successCount + " translation jobs.");
			}
			break;
		case unarchive:
			String toUnarchiveJobIdStr = ics.GetVar("to_unarchive_jobids");
			jobIds = StringUtils.isEmpty(toUnarchiveJobIdStr)? new String[0] : toUnarchiveJobIdStr.split(",");
			for (String jobId : jobIds) {
				try {
					job = entityManager.getTranslationJob(jobId);
					entityManager.save(job.setArchived(false));
					successCount ++;
				} catch (Exception e) {
					errorCount ++;
					LOG.error("Cannot unarchive TranslationJob '" + (job==null? jobId: job) + "'", e);
				}
			}
			if (errorCount>0) {
				String msg = "Failed to unarchived " + errorCount + " translation jobs.";
				if (successCount>0) {
					msg += "\nSuccessfully unarchived " + successCount + " translation jobs.";
				}
				model.add("errorMessage", msg);
			} else if (successCount>0) {
				model.add("unarchiveMessage", "Successfully unarchived " + successCount + " translation jobs.");
			}
			break;
		}
	}
	
	private void showJobList(boolean showArchived) {
		List <TranslationJob> allUsersJobs = entityManager.getAllTranslationJobs();
		
		model.add("message", ics.GetVar("message"));
		
		List <TranslationJobExt.FullInfo> activejobs = new ArrayList<TranslationJobExt.FullInfo>();
		List <TranslationJobExt.FullInfo> inactivejobs = new ArrayList<TranslationJobExt.FullInfo>();
		
		LOG.info("Processing a job list of size: "+ allUsersJobs.size());
		
		Map<String, String> siteNames = new HashMap<String, String>();
		for(TranslationJob job : allUsersJobs)
		{
			Status currentStatus = job.getStatus();
			
			String trSiteName;
			if (siteNames.containsKey(job.getSiteId())) {
				trSiteName = siteNames.get(job.getSiteId());
			} else {
				trSiteName = siteManager.getSiteNameById(job.getSiteId());
				siteNames.put(job.getSiteId(), trSiteName);
			}
			
			TranslationJobExt.FullInfo jobEx = TranslationJobExt.Factory.getFullInfo(
					job, entityManager.getTranslationJobDetailStats(job.getId()), trSiteName); 
			if(currentStatus.isActive()) {
				//job is active
				activejobs.add(jobEx);
			} else {
				//job is inactive
				inactivejobs.add(jobEx);
			}
		}
		
		model.add("activejobs",activejobs);
		model.add("inactivejobs",inactivejobs);
		
		TranslationJobStats jobStatus = entityManager.getTranslationJobStats();
		model.add("archivedjobscount",jobStatus.getArchivedJobsCount());
		
		if (showArchived) {
			List<TranslationJob> archivedJobs = entityManager.getArchivedTranslationJobs();
			List<TranslationJobExt.FullInfo> archivedJobsEx = 
					new ArrayList<TranslationJobExt.FullInfo>(archivedJobs.size());
			
			for (TranslationJob job : archivedJobs) {
				String trSiteName;
				if (siteNames.containsKey(job.getSiteId())) {
					trSiteName = siteNames.get(job.getSiteId());
				} else {
					trSiteName = siteManager.getSiteNameById(job.getSiteId());
					siteNames.put(job.getSiteId(), trSiteName);
				}
				TranslationJobExt.FullInfo jobEx = TranslationJobExt.Factory.getFullInfo(
						job, entityManager.getTranslationJobDetailStats(job.getId()), trSiteName); 
				archivedJobsEx.add(jobEx);
			}
			model.add("archivedjobs", archivedJobsEx);
		}
	}
}
