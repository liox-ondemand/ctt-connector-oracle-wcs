/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TmUpdateRequest;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.TranslationEntity.Status;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobFilter;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.TranslationRequestFilter;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.Utils;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;


/**
 * @author Mike Field
 *
 */
public class UITranslationStatusManager implements Action {
	
	protected static final Log LOG = LogFactory.getLog(UITranslationStatusManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
			String pagename = ics.GetVar("pagename");
			final String assettype = ics.GetVar("c");
			final String assetid = ics.GetVar("cid");
			Long longAssetId = Long.parseLong(assetid);
			
			String doAction = ics.GetVar("doAction");
			String reqIds = ics.GetVar("reqIds");
			
			model.add("c", assettype);
			model.add("cid", assetid);
			model.add("mypagename", pagename);
			
			// handle action
			
			if(doAction != null && !doAction.trim().equalsIgnoreCase(""))
			{
				processRequestIds(doAction, reqIds);
			}

			TranslationRequestFilter filter1 = new TranslationRequestFilter() {
				@Override public String getWhereClause() {
					return "nativeSourceId='" + assettype + ":" + assetid + "'"; 
				}

				@Override public String getOrderByClause() {
					return "updatedDate DESC";
				}
				
				@Override public int getMaxResults() { return UNLIMITED_RESULTS; }
			};
			
			TranslationRequestFilter filter2 = new TranslationRequestFilter() {
				@Override public String getWhereClause() {
					return "nativeTargetId='" + assettype + ":" + assetid + "'";
				}

				@Override public String getOrderByClause() {
					return "updatedDate DESC";
				}
				
				@Override public int getMaxResults() { return UNLIMITED_RESULTS; }
			};
			
			TranslationStatusSegment activeSegment = new TranslationStatusSegment("Active"); 
			TranslationStatusSegment waitingApprovalSegment = new TranslationStatusSegment("WaitingApproval"); 
			TranslationStatusSegment inactiveSegment = new TranslationStatusSegment("Inactive");
			
			AssetId aId = assetDao.createAssetId(assettype, longAssetId);
			int index = 0;
			//returned list of status
			List<Map<String,String>> statusList = new ArrayList<Map<String,String>>();
			
			for (TranslationRequestFilter filter : new TranslationRequestFilter[]{filter1, filter2}) {
				List <TranslationRequest> trList = entityManager.findTranslationRequests(filter);
				
				List<Map<String,Object>> waitingApprovalList = new ArrayList<Map<String,Object>>();
				List<Map<String,Object>> activeList = new ArrayList<Map<String,Object>>();
				List<Map<String,Object>> inactiveList = new ArrayList<Map<String,Object>>();
				LOG.info("entityManager returned "+trList.size()+" translation requests.");
				
				for(TranslationRequest tr: trList)
				{
					LOG.info("Processing translation request with id: "+tr.getId());
					Status status = tr.getStatus();
					
					// ignore archived requests
					if(!tr.isArchived())	
					{
						TranslationJob tJob = tr.getTranslationJob();
						
						if (tr.getStatus()==TranslationEntity.Status.REVIEW_TRANSLATION) {
							waitingApprovalList.add(getTableRowMap(tJob, tr));
						} else if(tr.getStatus().isActive()) {
							// job is active
							activeList.add(getTableRowMap(tJob, tr));
						} else {
							// Status is Completed t.f. job is inactive								 
							inactiveList.add(getTableRowMap(tJob, tr));
						}
						
						HashMap<String,String> assetInfo = new HashMap<String,String>();
						assetInfo.put("name", pagename);
						assetInfo.put("type", assettype);
						assetInfo.put("id", tr.getId());
						assetInfo.put("status",status.name()); 
						statusList.add(assetInfo);
					}
				}
				
				if (index==0) {
					waitingApprovalSegment.setFromList(waitingApprovalList);
					activeSegment.setFromList(activeList);
					inactiveSegment.setFromList(inactiveList);
				} else {
					waitingApprovalSegment.setToList(waitingApprovalList);
					activeSegment.setToList(activeList);
					inactiveSegment.setToList(inactiveList);
				}
				index ++;
			}
			model.add("statuslist", statusList);
			model.add("segments", Arrays.asList(new TranslationStatusSegment[]{
					waitingApprovalSegment, activeSegment, inactiveSegment
			}));
	}
	
	private void processRequestIds(String doAction, String ids)
	{
		List<String> splitIds = parseRequestIdString(ids);
		
		if(doAction.equalsIgnoreCase("update"))
		{
			List<TranslationRequest> requests = new ArrayList<TranslationRequest>();
			
			for(int i=0; i<splitIds.size(); i++)
			{
				TranslationRequest tr = entityManager.getTranslationRequest(splitIds.get(i));
				requests.add(tr);
				
			}
			
			/** TODO: client-producer to process request not yet implemented **/
			//TmUpdateRequest tmupdate = entityManager.createTmUpdateRequest();
			//tmupdate.setTranslationRequests(requests);
			
		}
		else if(doAction.equalsIgnoreCase("accept"))
		{
			for(int i=0; i<splitIds.size(); i++)
			{
				TranslationRequest tr = entityManager.getTranslationRequest(splitIds.get(i));
				if (tr.getStatus()==TranslationEntity.Status.REVIEW_TRANSLATION) {
					entityManager.save(tr.setCompleted());
				}
			}
		}
	}
	
	private List<String> parseRequestIdString(String ids)
	{
		List<String> translationIds = new ArrayList<String>();
		String [] id_split = ids.split("\\|");
		
		for(int i = 0; i < id_split.length; i++)
		{
			if(!id_split[i].trim().equalsIgnoreCase(""))
			{
				translationIds.add(id_split[i]);
			}
		}
		
		return translationIds;
	
	}
	
	private HashMap <String, Object> getTableRowMap(TranslationJob j, TranslationRequest tr)
	{
		HashMap<String,Object> assetInfo = new HashMap<String,Object>();
		
		if (j!=null) {
			assetInfo.put("jobid", cleanStringValue(j.getId()));
			assetInfo.put("jobname", cleanStringValue(j.getName()));
			if(j.getTranslationProvider() != null)
			{
				assetInfo.put("provider", cleanStringValue(j.getTranslationProvider().getName()));
			}
			else
			{
				assetInfo.put("provider", "");
			}
			assetInfo.put("jobstatus", cleanStringValue(j.getStatus().toString()));
			assetInfo.put("jobstatusdisplayname", cleanStringValue(j.getStatus().getDisplayName()));
			if(j.getSubmittedDate() != null)
			{
				assetInfo.put("submitteddate", j.getSubmittedDate());
			}
			
		}
		assetInfo.put("srclocale", cleanStringValue(tr.getNativeSourceLanguage()));
		assetInfo.put("targetlocale", cleanStringValue(tr.getNativeTargetLanguage()));
		
		assetInfo.put("updateddate", tr.getUpdatedDate());
		
		assetInfo.put("txstatus",cleanStringValue(tr.getStatus().name())); 
		assetInfo.put("txstatusdisplayname",cleanStringValue(tr.getStatus().getDisplayName())); 
		assetInfo.put("acceptedstatus", cleanStringValue(getAcceptedStatus(tr)));
		assetInfo.put("acceptedstatusdisplayname", cleanStringValue(getAcceptedStatusDisplayName(tr)));
		
		
		assetInfo.put("requestid", cleanStringValue(tr.getId()));
				
		assetInfo.put("sourceassetid", Utils.stringToAssetId(cleanStringValue(tr.getNativeSourceId())));
		assetInfo.put("targetassetid", Utils.stringToAssetId(cleanStringValue(tr.getNativeTargetId())));
		assetInfo.put("assetname", cleanStringValue(tr.getName()));
		
		
		return assetInfo;
	
	}
	
	private String cleanStringValue(String val)
	{
		if(Utilities.goodString(val))
		{
			return val;
		}
		else
		{
			return "";
		}
	}
	
	// TODO verify accepted status criteria
	private String getAcceptedStatus(TranslationRequest tr)
	{
		TranslationEntity.Status s = tr.getStatus();
		
		if(s.equals(TranslationEntity.Status.TRANSLATION_REJECTED) || s.equals(TranslationEntity.Status.COMPLETED) || s.equals(TranslationEntity.Status.COMPLETED_NO_NEED_TO_TRANSLATE))
		{
			return s.toString();
		}
		return "";
	}
	
	private String getAcceptedStatusDisplayName(TranslationRequest tr)
	{
		TranslationEntity.Status s = tr.getStatus();
		
		if(s.equals(TranslationEntity.Status.TRANSLATION_REJECTED) || s.equals(TranslationEntity.Status.COMPLETED) || s.equals(TranslationEntity.Status.COMPLETED_NO_NEED_TO_TRANSLATE))
		{
			return s.getDisplayName();
		}
		return "";
	}

	public class TranslationStatusSegment {
		private final String segmentType;
		private List<Map<String,Object>> fromList;
		private List<Map<String,Object>> toList;
		
		public TranslationStatusSegment(String segmentType) {
			super();
			this.segmentType = segmentType;
		}

		public String getType() {
			return segmentType;
		}

		public List<Map<String,Object>> getFromList() {
			return fromList;
		}

		void setFromList(List<Map<String,Object>> fromList) {
			this.fromList = fromList;
		}

		public List<Map<String,Object>> getToList() {
			return toList;
		}

		void setToList(List<Map<String,Object>> toList) {
			this.toList = toList;
		}
		
	}
}
