package com.claytablet.wcs.ui;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIJobRequestDetailManager	implements Action {
	protected static final Log LOG = LogFactory.getLog(UIJobManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private ManagerFactorySession session;
	
	private TranslationEntityManager entityManager;
	@InjectForRequest private JobManager jobManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest public Model model;

	public void handleRequest(ICS ics) {
		String reqId = ics.GetVar("requestid");
		String app = ics.GetVar("app");
		
		String siteId = ics.GetVar("siteId");
		ManagerFactorySession sudoSession = null;
	
		try {
			if ("WEM".equals(app)) {
				if (StringUtils.isEmpty(siteId)) {
					session = session.sudoGlobalAdminSession();
				} else {
					session = session.sudo(siteId);
				}
				sudoSession = session;
				model.add("siteid", siteId);
			}
			entityManager = session.getTranslationEntityManager();
	
			if (Utilities.goodString(reqId)) {
				TranslationRequest request = entityManager.getTranslationRequest(reqId);
				if (request!=null) {
					model.add("tr", TranslationRequestExt.Factory.getFullInfo(request, null));
				}
			}
		} finally {
			if (sudoSession!=null) {
				sudoSession.close();
			}
		}
	}
}
