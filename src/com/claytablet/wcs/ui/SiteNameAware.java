package com.claytablet.wcs.ui;

public interface SiteNameAware {
	public String getSiteName();
}