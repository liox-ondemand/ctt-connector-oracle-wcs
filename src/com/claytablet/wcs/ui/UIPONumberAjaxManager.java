package com.claytablet.wcs.ui;

import org.apache.commons.lang.StringUtils;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.impl.PONumberInfo;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIPONumberAjaxManager implements Action {
	enum ACTION {
		setPoRequired,
		addPoNumber,
		updatePoNumber,
		removePoNumber,
	}
	
	@InjectForRequest private ICS ics;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String siteId = ics.GetVar("siteid");
		String action = ics.GetVar("action");
		ACTION actionEnum = null;
		
		if (!StringUtils.isEmpty(action)) {
			actionEnum = ACTION.valueOf(action);
		}
		
		ManagerFactorySession sudoSession = session.sudo(siteId);
		try {
			ConfigurationManager cm = sudoSession.getConfigurationManager();
			
			if (actionEnum!=null) {
				switch (actionEnum) {
				case setPoRequired:
					setPoRequired(cm);
					break;
				case addPoNumber:
					addPoNumber(cm);
					break;
				case updatePoNumber:
					updatePoNumber(cm);
					break;
				case removePoNumber:
					removePoNumber(cm);
					break;
				}
			}
			model.add("requireponumber", PONumberInfo.isPONumberRequired(cm));
			model.add("poNumbers", PONumberInfo.getAllPONumberInfo(cm));
		} finally {
			if (sudoSession!=null) sudoSession.close();
		}
		model.add("siteid", siteId);
	}

	private void removePoNumber(ConfigurationManager cm) {
		String poNumber = ics.GetVar("name");
		PONumberInfo.removePONumber(cm, poNumber);
		model.add("deletemessage", "PO Number '" + poNumber + "' has been removed.");
	}

	private void updatePoNumber(ConfigurationManager cm) {
		String originalPoNumber = ics.GetVar("originalname");
		String poNumber = ics.GetVar("name");
		String poDesc = ics.GetVar("desc");
		PONumberInfo.updatePONumber(cm, originalPoNumber, poNumber, poDesc);
		model.add("savedPoNumber", poNumber);
		model.add("savemessage", "Change saved.");
	}

	private void addPoNumber(ConfigurationManager cm) {
		String poNumber = ics.GetVar("name");
		String poDesc = ics.GetVar("desc");
		PONumberInfo.addPONumber(cm, poNumber, poDesc);
		model.add("savedPoNumber", poNumber);
		model.add("savemessage", "Change saved.");
	}

	private void setPoRequired(ConfigurationManager cm) {
		String requirePO = ics.GetVar("requireponumber");
		PONumberInfo.setPONumberRequired(cm, "true".equalsIgnoreCase(requirePO));
		model.add("defaultchangemessage", "Change saved.");
	}
}