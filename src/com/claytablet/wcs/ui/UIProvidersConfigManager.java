/**
 * 
 */
package com.claytablet.wcs.ui;

import static com.claytablet.client.producer.ProducerCommonConfigs.CT_LICENSE_ID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJobStats;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.impl.platform.CTPlatformUtil;
import com.claytablet.license.key.KeyHelper;
import com.claytablet.license.key.LicenseKeyInfo;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * @author Mike Field
 *
 */
public class UIProvidersConfigManager implements Action {

	private enum ACTION {
		Save,
		Delete,
		Update
	}
	
	protected static final Log LOG = LogFactory.getLog(UIProvidersConfigManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactory factory;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String pagename = ics.GetVar("pagename");
		if ("CustomElements/CT/WEM/ProvidersConfigPost".equals(pagename)) {
			doProvidersConfigPost();
		} else {
			doProvidersConfigFront();
		}
	}

	private void doProvidersConfigFront() {
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		try {
			TranslationEntityManager em = adminSession.getTranslationEntityManager();
			List<TranslationProvider> allProviders = em.getAllTranslationProviders();
			
			ConfigurationManager cm = adminSession.getConfigurationManager();
			String licenseId = cm.get(ProducerCommonConfigs.CT_LICENSE_ID);
			List<String> accountKeys = new ArrayList<String>();
			List<String> disabledAccountKeys = new ArrayList<String>();
			int totalActiveJobs = 0;
			boolean needSetLicenseId = StringUtils.isEmpty(licenseId);
			try {
				for (TranslationProvider provider : allProviders) {
					if (needSetLicenseId && !StringUtils.isEmpty(provider.getLicenseId())) {
						licenseId = provider.getLicenseId();
					}
					TranslationJobStats stats = em.getTranslationProviderJobStats(provider.getId());
					totalActiveJobs += stats.getActiveJobsCount();
					provider.attachJobStats(stats);
				}
				if (!StringUtils.isEmpty(licenseId)) {
					getAccountKeys(adminSession, licenseId, accountKeys, disabledAccountKeys);
					if (needSetLicenseId) {
						cm.getGlobalConfigurationSet().put(ProducerCommonConfigs.CT_LICENSE_ID, licenseId);
					}
				} 
			} catch (Exception e) {
				LOG.error("Failed to get list of LicenseKeyInfo for LicenseId:" + licenseId, e);		 
			}
			model.add("providers", allProviders);
			model.add("providertypes", Arrays.asList(TranslationProvider.Type.values()));
			model.add("accountkeys", accountKeys);
			model.add("disabledaccountkeys", disabledAccountKeys);
			model.add("licenseid", licenseId);
			model.add("totalactivejobs", totalActiveJobs);
		} finally {
			adminSession.close();
		}
	}

	private void getAccountKeys(ManagerFactorySession session, String licenseId, List<String> accountKeys,
			List<String> disabledAccountKeys) throws Exception {
		List<LicenseKeyInfo> licenseKeyInfoList = new ArrayList<LicenseKeyInfo>();
		licenseKeyInfoList = CTPlatformUtil.getKeyHelper(session).getLicenseKeysInfo(licenseId);
		
		if (accountKeys!=null && disabledAccountKeys!=null) {
			for (LicenseKeyInfo licenseKeyInfo : licenseKeyInfoList) {
				if (!licenseKeyInfo.getKeyId().toLowerCase().contains("provider")) {
					if (licenseKeyInfo.getIsUsed()) {
						disabledAccountKeys.add(licenseKeyInfo.getKeyId());
					} else {
						accountKeys.add(licenseKeyInfo.getKeyId());
					}
				} 
			}
			if (!accountKeys.isEmpty() || !disabledAccountKeys.isEmpty()) {
				model.add("validatedLicenseId", licenseId);
			}
		}
	}

	private void doProvidersConfigPost() {
		switch (ACTION.valueOf(ics.GetVar("action"))) {
		case Save:
			if (StringUtils.isEmpty(ics.GetVar("providerid"))) {
				saveNewProvider();
			} else {
				saveProvider();
			}
			break;
		
		case Delete:
			deleteProvider();
			break;
		
		case Update:
			updateLicenseId();
			break;
		}

		FTValList args = new FTValList();
		args.removeAll();
		args.setValString("message", ics.GetVar("message"));
		args.setValString("errormessage", ics.GetVar("errormessage"));
		args.setValString("updateproviderid", ics.GetVar("updateproviderid"));
		ics.InsertPage("CustomElements/CT/WEM/ProvidersConfigFront", args);	
	}

	private void updateLicenseId() {
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		
		try {
			ConfigurationManager cm = adminSession.getConfigurationManager();
			String licenseId = ics.GetVar("licenseid");
			if (licenseId==null) {
				licenseId="";
			}
			// test if license id is valid
			List<String> accountKeys = new ArrayList<String>();
			List<String> disabledAccountKeys = new ArrayList<String>();
			if (!StringUtils.isEmpty(licenseId)) {
				getAccountKeys(adminSession, licenseId, accountKeys, disabledAccountKeys);
			}
			if (StringUtils.isEmpty(licenseId) || !accountKeys.isEmpty() || !disabledAccountKeys.isEmpty()) {
				// update all existing providers with new account key
				TranslationEntityManager em = adminSession.getTranslationEntityManager();
				int providerUpdated = 0;
				for (TranslationProvider p : em.getAllTranslationProviders()) {
					em.save(p.setAccountKey(licenseId, ""));
					providerUpdated ++;
				}
				cm.getGlobalConfigurationSet().put(CT_LICENSE_ID, licenseId);
				
				if (StringUtils.isEmpty(licenseId)) {
					ics.SetVar("message", "ClayTablet license id removed successfully. " +
							(providerUpdated<=0? "" : 
								"Also updated " + providerUpdated + " existing provider accounts. "
										+ "You need to setup license id and account keys again before using them to send out jobs"));
				} else {
					ics.SetVar("message", "ClayTablet license id updated successfully. " +
							(providerUpdated<=0? "" : 
								"Also updated " + providerUpdated + " existing provider accounts. "
										+ "You need to setup account keys for them before using them to send out jobs"));
				}
			} else {
				ics.SetVar("errormessage", "Fail to update license id: cannot find "
						+ "any valid accounts associated with license id '" + licenseId + "'");
			}
		} catch (Exception e) {
			ics.SetVar("errormessage", "Failed to update license id: " + String.valueOf(e));
			LOG.error("Failed to update CLayTablet license id. ", e);
		} finally {
			adminSession.close();
		}
	}

	private void deleteProvider() {
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		try {
			String providerId = ics.GetVar("providerid");
			TranslationEntityManager em = adminSession.getTranslationEntityManager();
			em.deleteTranslationProvider(providerId);
			ics.SetVar("message", "Provider '" + ics.GetVar("original_name") + "' has been deleted successfully.");
		} catch (Exception e) {
			ics.SetVar("errormessage", "Failed to delete provider '" + 
					ics.GetVar("original_name") + "'. " + String.valueOf(e));
			LOG.error("Failed to delete provider '" + ics.GetVar("original_name") + "'. ", e);
		} finally {
			adminSession.close();
		}
	}

	private void saveProvider() {
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		try {
			String id = ics.GetVar("providerid");
			String name = ics.GetVar("providername");
			TranslationProvider.Type type = TranslationProvider.Type.valueOf(ics.GetVar("providertype"));
			String accountKey = ics.GetVar("accountkey");
			String licenseId = ics.GetVar("licenseid");
			String host = ics.GetVar("providerhost");
			String user = ics.GetVar("provideruser");
			String password = ics.GetVar("providerpassword");

			TranslationEntityManager em = adminSession.getTranslationEntityManager();

			TranslationProvider provider = em.getTranslationProvider(id);
			provider.setName(name);
			provider.setProviderType(type);
			provider.setAccountKey(licenseId, accountKey);
			if (type.isExtendedConfig()) {
				provider.setHostName(host).setLoginUser(user).setPassword(password);
			}
			provider = em.save(provider);
			ics.SetVar("updatedproviderid", provider.getId());
			ics.SetVar("message", "Provider '" + name + "' has been updated successfully.");
		} catch (Exception e) {
			ics.SetVar("errormessage", "Failed to update provider '" + 
					ics.GetVar("original_name") + "'. " + String.valueOf(e));
			LOG.error("Failed to update provider '" + ics.GetVar("original_name") + "'. ", e);
		} finally {
			adminSession.close();
		}
	}

	private void saveNewProvider() {
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		try {
			String name = ics.GetVar("providername");
			TranslationProvider.Type type = TranslationProvider.Type.valueOf(ics.GetVar("providertype"));
			String accountKey = ics.GetVar("accountkey");
			String licenseId = ics.GetVar("licenseid");
			String host = ics.GetVar("providerhost");
			String user = ics.GetVar("provideruser");
			String password = ics.GetVar("providerpassword");

			TranslationEntityManager em = adminSession.getTranslationEntityManager();

			TranslationProvider provider = em.createTranslationProvider(name, type);
			provider.setAccountKey(licenseId, accountKey);
			if (type.isExtendedConfig()) {
				provider.setHostName(host).setLoginUser(user).setPassword(password);
			}
			provider = em.save(provider);
			ics.SetVar("updatedproviderid", provider.getId());
			ics.SetVar("message", "Provider '" + name + "' has been created successfully.");
		} catch (Exception e) {
			ics.SetVar("errormessage", "Failed to create new provider. " + String.valueOf(e));
			LOG.error("Failed to create new provider.", e);
		} finally {
			adminSession.close();
		}
	}
}
