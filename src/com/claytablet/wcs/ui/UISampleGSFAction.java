/**
 * 
 */
package com.claytablet.wcs.ui;

/**
 * @author Mike Field
 *
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.wcs.system.LocaleManager;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

public class UISampleGSFAction implements Action {
    /**
     * Inject an ICS into the action for convenience
     */
    @InjectForRequest
    public ICS ics;
    @InjectForRequest
    private LocaleManager localeManager;
	protected static final Log LOG = LogFactory.getLog(UISampleGSFAction.class.getName());

    /**
     * Provide a place to put the data that will be passed into the page context for use with expression language
     */
    @InjectForRequest
    public Model model;

    /**
     * Provide a DAO that allows an asset to be easily mapped
     */
    @InjectForRequest
    protected TemplateAssetAccess templateAssetAccess;

    //@InjectForRequest
    //protected MyAssetDao myAssetDao;

    public void handleRequest(ICS ics) {
        // get the asset id corresponding to the ICS variables c, cid
        AssetId id = templateAssetAccess.currentId();
        LOG.error("***** UISampleGSFAction: id = " + id.toString() + " *****");

        // load ArticleDetail page and add it to the page context
        //model.add("asset", myAssetDao.loadArticlePage(id));
    }
}