package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobDetailStats;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.TranslationRequestFilter;
import com.claytablet.client.util.DelegateProxyFactory;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.ConfigManagerImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIJobRequestsManager implements Action {
	private enum ACTION {
		pagination,
		removeRequest,
		approveRequest,
	}
	
	protected static final Log LOG = LogFactory.getLog(UIJobManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private ManagerFactorySession session;
	
	private TranslationEntityManager entityManager;
	@InjectForRequest private JobManager jobManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest public Model model;

	public void handleRequest(ICS ics) {
		String jobid = ics.GetVar("jobid");
		String orderByClause = ics.GetVar("orderby");
		String whereClause = ics.GetVar("where");
		String app = ics.GetVar("app");
		
		String siteId = ics.GetVar("siteId");
		ManagerFactorySession sudoSession = null;
	
		try {
			if ("WEM".equals(app)) {
				if (StringUtils.isEmpty(siteId)) {
					session = session.sudoGlobalAdminSession();
				} else {
					session = session.sudo(siteId);
				}
				sudoSession = session;
				model.add("siteid", siteId);
			}
			entityManager = session.getTranslationEntityManager();
			
			ACTION action = ACTION.pagination;
			
			if (Utilities.goodString(ics.GetVar("action"))) {
				action = ACTION.valueOf(ics.GetVar("action"));
				String reqIdStr = ics.GetVar("reqIds");
				String[] reqIds = StringUtils.isEmpty(reqIdStr)? new String[0] : reqIdStr.split(" ");
				
				switch (action) {
				case removeRequest:
					removeTranslationRequest(reqIds);
					break;
				case approveRequest:
					approveTranslationRequest(reqIds);
					break;
				default:
					break;
				}
			}
			
			List<TranslationRequest> requests;
			if (Utilities.goodString(jobid)) {
				TranslationJob job = entityManager.getTranslationJob( jobid );
				
				requests = entityManager.findTranslationRequests(jobid, 
						TranslationRequestFilter.factory.filter(whereClause, orderByClause));
				String siteName = siteManager.getSiteNameById(job.getSiteId());
				TranslationJobDetailStats stats = entityManager.getTranslationJobDetailStats(jobid);
				model.add("job", TranslationJobExt.Factory.getFullInfo(job, stats, siteName));
				model.add("job_totalrecords", stats.getTranslationRequestsCount());
				model.add("translationRequests", getRequestsWithExtraInfo(requests, siteName));
			} else {
				requests = entityManager.getAllQueuedTranslationRequests();
				
				model.add("job_totalrecords", requests.size());
				
				if (Utilities.goodString(orderByClause) || Utilities.goodString(whereClause)) {
					requests = entityManager.findQueuedTranslationRequests( 
							TranslationRequestFilter.factory.filter(whereClause, orderByClause));
					model.add("translationRequests", getRequestsWithExtraInfo(requests, null));
				} else {
					model.add("translationRequests", getRequestsWithExtraInfo(requests, null));
				}
			}
				
			int firstRec = Integer.valueOf(ics.GetVar("firstrec"));
			int pageSize = Integer.valueOf(ics.GetVar("pagesize"));
			
			if (Utilities.goodString(orderByClause)) {
				int sep = orderByClause.lastIndexOf(' ');
				if (sep>0) {
					model.add("sort_by", orderByClause.substring(0, sep).trim());
					String sortDir = orderByClause.substring(sep).trim().toUpperCase();
					model.add("sort_dir", sortDir);
					model.add("sort_dir_rev", sortDir.equals("ASC")? "DESC" : "ASC");
				} else {
					model.add("sort_by", orderByClause);
					model.add("sort_dir", "ASC");
					model.add("sort_dir_rev", "DESC");
				}
			}
			_setupPaginationVariables(requests, pageSize, firstRec);
		} finally {
			if (sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	private List<TranslationRequestExt.FullInfo> getRequestsWithExtraInfo(
			List<TranslationRequest> requests, String siteName) {
		Map<String, String> siteNames = new HashMap<String, String>();
		
		List<TranslationRequestExt.FullInfo> list = 
				new ArrayList<TranslationRequestExt.FullInfo>(requests.size());
		for (TranslationRequest tr : requests) {
			String trSiteName = siteName;
			if (siteName==null) {
				if (siteNames.containsKey(tr.getSiteId())) {
					trSiteName = siteNames.get(tr.getSiteId());
				} else {
					trSiteName = siteManager.getSiteNameById(tr.getSiteId());
					siteNames.put(tr.getSiteId(), trSiteName);
				}
			}
			list.add(TranslationRequestExt.Factory.getFullInfo(tr, trSiteName));
		}
		return list;
	}

	private void removeTranslationRequest(String[] reqIds) {
		for (String id : reqIds) {
			entityManager.deleteTranslationRequest(id);
		}
		model.add("updateReqIds", true);
	}

	private void approveTranslationRequest(String[] reqIds) {
		for (String id : reqIds) {
			TranslationRequest tr = entityManager.getTranslationRequest(id);
			if (tr!=null) {
				entityManager.save(tr.setCompleted());
			}
		}
		model.add("updateReqIds", true);
	}

	private void _setupPaginationVariables( List<?> list, int maxPerPage, int firstrec) {
		int page_totalrecords = list.size();
		int total_pages = (page_totalrecords + maxPerPage -1) / maxPerPage;
		int pageLastRec = firstrec + maxPerPage - 1;
		if (pageLastRec>=page_totalrecords) {
			pageLastRec=page_totalrecords-1;
		}
		int cur_page = firstrec / maxPerPage;
		
		List<Integer> page_startrecords = new ArrayList<Integer>();
		if (total_pages<=12) {
			for (int i=0; i < list.size(); i=i+maxPerPage ) {
				page_startrecords.add(i);
			}
		} else {
			int i;
			for (i=0; i < 2; i++ ) {
				page_startrecords.add(i*maxPerPage);
			}
			int nextPage = cur_page-2;
			if (nextPage>total_pages-7) {
				nextPage = total_pages-7;
			}
			if (i<nextPage) {
				page_startrecords.add(-1 * ((int)(i+nextPage)/2)*maxPerPage);
				i=nextPage;
			} 
			for (int j=0; j<5; j++, i++) {
				page_startrecords.add(i*maxPerPage);
			}
			if (i<total_pages-2) {
				page_startrecords.add(-1 * ((int)(i+total_pages-2)/2)*maxPerPage);
				i = total_pages-2;
			}
			for (; i<total_pages; i++) {
				page_startrecords.add(i*maxPerPage);
			}
		}
		model.add("page_firstrec", firstrec);
		model.add("pagesize", maxPerPage);
		model.add("page_lastrec", pageLastRec);
		model.add("page_maxperpage", maxPerPage);
		model.add("page_totalrecords", page_totalrecords);
		model.add("page_startrecords", page_startrecords);
		model.add("page_size_options", 
				Arrays.asList(ConfigManagerImpl.WCS_MAX_ITEMS_PER_PAGE.getValueOptions()));
	}
}
