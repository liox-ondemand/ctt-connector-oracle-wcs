package com.claytablet.wcs.ui;

public interface UIFilterCondition {
	String getWhereClause();
}