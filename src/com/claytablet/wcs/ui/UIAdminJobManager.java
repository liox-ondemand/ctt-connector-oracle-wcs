/**
 * 
 */
package com.claytablet.wcs.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory; 

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.util.ListUtils;
import com.claytablet.model.event.metadata.IMetadataGroup;
import com.claytablet.model.event.metadata.custom.freeway.FreewayMetadataGroup;
import com.claytablet.wcs.CTObjectFactory;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.queue.DatabaseJobQueue;
import com.claytablet.wcs.system.queue.JobQueueEntry;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

/**
 * @author Mike Field
 *
 */
public class UIAdminJobManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UIAdminJobManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest public Model model;
	@InjectForRequest private JobManager jobManager;
	
	public void handleRequest(ICS ics) {
			
			String pagename = ics.GetVar("pagename");
			if ("CustomElements/CT/WEM/JobPost".equals(pagename)) {
				doJobPost();
				
			} else {
				doJobFront();
			}

	}
	
	private void doJobFront() {
		String jobid = ics.GetVar("jobid");
		model.add("message", ics.GetVar("message"));
		model.add("showheaderfooter", ics.GetVar("showheaderfooter"));
		if (Utilities.goodString(jobid)) {
			TranslationJob job = entityManager.getTranslationJob( jobid );
			String vendormetadata = "";
			if (job.getProviderMetadata() != null) {
				IMetadataGroup img = job.getProviderMetadata();
				vendormetadata = img.toString(); // TO DO: ASK YU LI WHAT TO DO WITH THIS
			}			
			model.add("job", job);
			model.add("vendorMetadata", vendormetadata);
			model.add("translationRequests", job.getTranslationRequests());
			
			model.add("translationVendors", entityManager.getAllTranslationProviders());
		}
	}
	
	private void doJobPost() {
		// Setup variables from form post
		
		String action=ics.GetVar("action");
		
		String jobid = ics.GetVar("jobid");
		String duedate = ics.GetVar("duedate");
		String vendorname = ics.GetVar("vendor");

		
		String jobname = ics.GetVar("jobname");
		String jobdescription = ics.GetVar("jobdescription");

		// Setup rest of the variables
		String message = "";
		CTObjectFactory factory = new CTObjectFactory(ics);
		
		UserContext ctx = new UserContextImpl(ics);
		
		//ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		ManagerFactorySession adminSession = factory.createManagerFactorySession(ics);
		
		TranslationEntityManager em = adminSession.getTranslationEntityManager();
		TranslationJob job = em.getTranslationJob( jobid );

		if(Utilities.goodString(ics.GetVar("action")) && "removeselected".equals(ics.GetVar("action")))
		{
			
			
			String trids = ics.GetVar("trids");
			LOG.info("trids found: "+trids);
			
			if(Utilities.goodString(trids))
			{
				String [] splitIds = trids.split(";");
				for(String i:splitIds)
				{
					LOG.info("Attempting to remove translation request with id: "+i);
					try{
						
						em.deleteTranslationRequest(i);
					}
					catch(Exception e)
					{
						LOG.error(e.getMessage());
						e.printStackTrace();
					}
				}
			}
					
				
			
		}
		else 
		{

		String vendorid = "";

		
			if((Utilities.goodString(action) && ("savejob".equals(action) || "sendjob".equals(action)))){
				message = "Job successfully updated";
				// If the form was posted, update the job
				if (Utilities.goodString(jobid)) {
	
					if (Utilities.goodString(jobname)) {
						job.setName(jobname);
					}
					
					if (Utilities.goodString(jobdescription)) {
						job.setDescription(jobdescription);
					}
	
					// Format date
					Date dDueDate = new Date();
					if (Utilities.goodString(duedate)) {
						try {
							SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
							dDueDate = formatter.parse(duedate);
							job.setDueDate( dDueDate );
						} catch (ParseException e) {
		
						}
					}
					
					if (Utilities.goodString(vendorname)) {
						// Get vendor id
						TranslationProvider vendor = ListUtils.findByName(entityManager.getAllTranslationProviders(), vendorname);
						
						if (vendor!=null) {
							// Update job
							vendorid = vendor.getId();
							job.setTranslationProvider(vendor);
							IMetadataGroup img = _createMetadataGroup(vendor);
							
							/** TO DO: Work with Yu Li here. 
							 * Currently getting exception java.io.NotSerializableException: com.claytablet.model.event.metadata.custom.freeway.FreewayMetadataGroup
							 * job.setVendorMetadata(img);
							 *  
							 */
						}
					}
				}
			}
			
			//archive button was clicked
			if (Utilities.goodString(action) && "archivejob".equals(action)){
				
				job.setArchived(true);
				message = "Job successfully archived";
			}
			
			
			//save every time
			LOG.trace("Saving the job.");
			job = entityManager.save(job);
			LOG.trace("Save completed.");
					
			// send job button was clicked
			if (Utilities.goodString(action) && "sendjob".equals(action)){

				job = entityManager.save(job.setStatus(TranslationEntity.Status.SENDING));
				message = "Job successfully updated, and will be sent out shortly";
			}
		
			
			
			
			FTValList args = new FTValList();
			args.removeAll();
			args.setValString("message", message);
			args.setValString("showheaderfooter", "true");
			args.setValString("jobid", jobid );
			args.setValString("jobname", job.getName() );
			args.setValString("jobstatus", job.getStatus().toString() );
			args.setValString("vendorname", vendorname );
			ics.InsertPage("CustomElements/CT/WEM/JobFront", args);	
		}
	}
	
	private IMetadataGroup _createMetadataGroup(TranslationProvider vendor) {
		FreewayMetadataGroup freewayMetadataGroup = new FreewayMetadataGroup();
		if (vendor != null && vendor.getName().equals(TranslationProvider.Type.LIONBRIDGE_FREEWAY)) {
			String instruction = ics.GetVar("special_instruction");
			String analysis1name = ics.GetVar("analysis1_name");
			String analysis1value = ics.GetVar("analysis1_value");
			String analysis2name = ics.GetVar("analysis2_name");
			String analysis2value = ics.GetVar("analysis2_value");
			String analysis3name = ics.GetVar("analysis3_name");
			String analysis3value = ics.GetVar("analysis3_value");
			freewayMetadataGroup.setSpecialInstruction(instruction);
			freewayMetadataGroup.setAnalysisCodeValue(1, analysis1name, analysis1value);
			freewayMetadataGroup.setAnalysisCodeValue(2, analysis2name, analysis2value);
			freewayMetadataGroup.setAnalysisCodeValue(3, analysis3name, analysis3value);
		}
		return freewayMetadataGroup;
	}

}
