/**
 * 
 */
package com.claytablet.wcs.ui;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserContext;
import static com.claytablet.client.producer.ProducerCommonConfigs.CT_ENABLE_TEAM_MANAGEMENT;

import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

/**
 * @author Mike Field
 *
 */
public class UIProfilesConfigManager implements Action {
	private enum ACTION {
		Disable,
		Enable,
	}
	
	protected static final Log LOG = LogFactory.getLog(UIProvidersConfigManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactory factory;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String pagename = ics.GetVar("pagename");
		if ("CustomElements/CT/WEM/ProfilesConfigPost".equals(pagename)) {
			doProfilesConfigPost();
		} else {
			doProfilesConfigFront();
		}
			
		
	}

	private void doProfilesConfigFront() {
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		
		try {
			model.add("teamenabled", adminSession.getConfigurationManager().get(CT_ENABLE_TEAM_MANAGEMENT));
		} finally {
			adminSession.close();
		}
	}

	private void doProfilesConfigPost() {
		switch (ACTION.valueOf(ics.GetVar("action"))) {
		case Disable:
			setTeamManagementEnabled(false);
			break;
		
		case Enable:
			setTeamManagementEnabled(true);
			break;
		}

		FTValList args = new FTValList();
		args.removeAll();
		args.setValString("errormessage", ics.GetVar("errormessage"));
		ics.InsertPage("CustomElements/CT/WEM/ProfilesConfigFront", args);	
	}

	private void setTeamManagementEnabled(boolean enabled) {
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		
		try {
			adminSession.getConfigurationManager().getGlobalConfigurationSet().put(
					CT_ENABLE_TEAM_MANAGEMENT, enabled);
		} catch (Exception e) {
			ics.SetVar("errormessage", "Failed to " + (enabled? "enable" : "disable") + " team management.");
			LOG.debug("Failed to update CLayTablet license id. ", e);
		} finally {
			adminSession.close();
		}
	}
}
