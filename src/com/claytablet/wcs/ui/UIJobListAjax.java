package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobFilter;
import com.claytablet.client.producer.TranslationJobStats;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.TranslationRequestFilter;
import com.claytablet.client.util.DelegateProxyFactory;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.ConfigManagerImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIJobListAjax implements Action {
	private enum TYPE {
		active,
		inactive,
		archived
	}
	
	private enum ACTION {
		pagination,
		archive,
		unarchive
	}
	
	protected static final Log LOG = LogFactory.getLog(UIJobManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private ManagerFactorySession session;
	
	private TranslationEntityManager entityManager;
	private ConfigurationManager configManager;
	@InjectForRequest private JobManager jobManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest public Model model;

	public void handleRequest(ICS ics) {
		String typeStr = ics.GetVar("type");
		
		String orderByClause = ics.GetVar("orderby");
		String whereClause = ics.GetVar("where");
		String app = ics.GetVar("app");
		String siteId = ics.GetVar("siteId");
		ManagerFactorySession sudoSession = null;
	
		TYPE type = Utilities.goodString(typeStr)? TYPE.valueOf(typeStr) : TYPE.active;
		
		try {
			if ("WEM".equals(app)) {
				if (StringUtils.isEmpty(siteId)) {
					session = session.sudoGlobalAdminSession();
				} else {
					session = session.sudo(siteId);
				}
				sudoSession = session;
			}
			entityManager = session.getTranslationEntityManager();
			configManager = session.getConfigurationManager();
			
			ACTION action = ACTION.pagination;
			
			if (Utilities.goodString(ics.GetVar("action"))) {
				action = ACTION.valueOf(ics.GetVar("action"));
				String jobIdStr = ics.GetVar("jobIds");
				String[] jobIds = StringUtils.isEmpty(jobIdStr)? new String[0] : jobIdStr.split(" ");
				
				switch (action) {
				case archive:
					archiveTranslationJobs(jobIds);
					break;
				case unarchive:
					unarchiveTranslationJobs(jobIds);
					break;
				default:
					break;
				}
			}
			
			List<TranslationJob> jobs;
			
			TranslationJobFilter filter = TranslationJobFilter.factory.filter(whereClause, orderByClause);
			switch (type) {
			case archived:
				jobs = entityManager.findArchivedTranslationJobs(filter);
				break;
				
			case inactive:
				TranslationJobFilter inactiveEntityFilter = getEntityStatusFilter(false);
				filter = TranslationJobFilter.factory.and(filter, inactiveEntityFilter);
				jobs = entityManager.findTranslationJobs(filter);
				model.add("archivedjobscount", entityManager.getTranslationJobStats().getArchivedJobsCount());
				break;
			
			case active:
			default:
				TranslationJobFilter activeEntityFilter = getEntityStatusFilter(true);
				filter = TranslationJobFilter.factory.and(filter, activeEntityFilter); 
				jobs = entityManager.findTranslationJobs(filter);
				break;
			}
			
			model.add("job_totalrecords", jobs.size());
			
			int firstRec = Integer.valueOf(ics.GetVar("firstrec"));
			int pageSize = Integer.valueOf(ics.GetVar("pagesize"));
			
			_setupPaginationVariables(jobs, pageSize, firstRec);
			
			List<TranslationJob> jobExs = new ArrayList<TranslationJob>(jobs.size());
			
			Map<String, String> siteNames = new HashMap<String, String>();
			for (int i = 0; i < jobs.size(); i++) {
				TranslationJob job = jobs.get(i);
				if (i<firstRec || i>=firstRec + pageSize) {
					jobExs.add(job);
				} else {
					// only load extra job information for job that is displayed on the page
					String trSiteName;
					if (siteNames.containsKey(job.getSiteId())) {
						trSiteName = siteNames.get(job.getSiteId());
					} else {
						trSiteName = siteManager.getSiteNameById(job.getSiteId());
						siteNames.put(job.getSiteId(), trSiteName);
					}
					
					jobExs.add(TranslationJobExt.Factory.getFullInfo(
							job, entityManager.getTranslationJobDetailStats(job.getId()), trSiteName)); 
				}
			}
			
			if (Utilities.goodString(orderByClause)) {
				int sep = orderByClause.lastIndexOf(' ');
				if (sep>0) {
					model.add("sort_by", orderByClause.substring(0, sep).trim());
					String sortDir = orderByClause.substring(sep).trim().toUpperCase();
					model.add("sort_dir", sortDir);
					model.add("sort_dir_rev", sortDir.equals("ASC")? "DESC" : "ASC");
				} else {
					model.add("sort_by", orderByClause);
					model.add("sort_dir", "ASC");
					model.add("sort_dir_rev", "DESC");
				}
			}
			model.add("jobs", jobExs);
			model.add("total_jobs", jobExs.size());
			model.add("type", typeStr);
			model.add("siteid", siteId);
		} finally {
			if (sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	private TranslationJobFilter getEntityStatusFilter(boolean active) {
		String condition = "";
		
		for (TranslationEntity.Status status : TranslationEntity.Status.values()) {
			if (active == status.isActive()) {
				String newCondition = "status='" +status.name() + "'";
				if (!StringUtils.isEmpty(condition)) {
					condition+= " OR ";
				}
				condition += newCondition;
			}
		}
		return TranslationJobFilter.factory.filter(condition);
	}

	private void archiveTranslationJobs(String[] jobIds) {
		for (String id : jobIds) {
			TranslationJob job = entityManager.getTranslationJob(id);
			job.setArchived(true);
			entityManager.save(job);
		}
		model.add("updateJobIds", true);
	}

	private void unarchiveTranslationJobs(String[] jobIds) {
		for (String id : jobIds) {
			TranslationJob job = entityManager.getTranslationJob(id);
			job.setArchived(false);
			entityManager.save(job);
		}
		model.add("updateJobIds", true);
	}

	private void _setupPaginationVariables( List<?> list, int maxPerPage, int firstrec) {
		int page_totalrecords = list.size();
		int total_pages = (page_totalrecords + maxPerPage -1) / maxPerPage;
		int pageLastRec = firstrec + maxPerPage - 1;
		if (pageLastRec>=page_totalrecords) {
			pageLastRec=page_totalrecords-1;
		}
		int cur_page = firstrec / maxPerPage;
		
		List<Integer> page_startrecords = new ArrayList<Integer>();
		if (total_pages<=12) {
			for (int i=0; i < list.size(); i=i+maxPerPage ) {
				page_startrecords.add(i);
			}
		} else {
			int i;
			for (i=0; i < 2; i++ ) {
				page_startrecords.add(i*maxPerPage);
			}
			int nextPage = cur_page-2;
			if (nextPage>total_pages-7) {
				nextPage = total_pages-7;
			}
			if (i<nextPage) {
				page_startrecords.add(-1 * ((int)(i+nextPage)/2)*maxPerPage);
				i=nextPage;
			} 
			for (int j=0; j<5; j++, i++) {
				page_startrecords.add(i*maxPerPage);
			}
			if (i<total_pages-2) {
				page_startrecords.add(-1 * ((int)(i+total_pages-2)/2)*maxPerPage);
				i = total_pages-2;
			}
			for (; i<total_pages; i++) {
				page_startrecords.add(i*maxPerPage);
			}
		}
		model.add("page_firstrec", firstrec);
		model.add("pagesize", maxPerPage);
		model.add("page_lastrec", pageLastRec);
		model.add("page_maxperpage", maxPerPage);
		model.add("page_totalrecords", page_totalrecords);
		model.add("page_startrecords", page_startrecords);
		model.add("page_size_options", 
				Arrays.asList(ConfigManagerImpl.WCS_MAX_ITEMS_PER_PAGE.getValueOptions()));
	}
}
