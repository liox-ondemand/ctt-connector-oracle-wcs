package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public abstract class UIFilterImpl implements UIFilter {
	private static final Log LOG = LogFactory.getLog(UIFilterImpl.class.getName());
	
	private final List<FieldConditionImpl> conditions = new ArrayList<FieldConditionImpl>();
	
	@Override
	public List<? extends FieldCondition> getFieldConditions() {
		return conditions;
	}
	
	@Override
	public String getWhereClause() {
		StringBuffer condition = new StringBuffer();
		for (FieldCondition fc : conditions) {
			if (fc.getSingleConditions().size()>0) {
				String cond = fc.getWhereClause();
				
				if (!StringUtils.isEmpty(cond)) {
					if (condition.length()>0) {
						condition.append(" AND ");
					}
					condition.append("(").append(cond).append(")");
				}
			}
		}
		return condition.toString();
	};

	public FieldConditionImpl getFieldCondition(Field field) {
		for (FieldConditionImpl fieldCondition : conditions) {
			if (fieldCondition.getField().getDisplayName().equals(field.getDisplayName())) {
				return fieldCondition;
			}
		}
		return null;
	}
	
	public FieldConditionImpl addFieldCondition(Field field) {
		for (FieldConditionImpl fieldCondition : conditions) {
			if (fieldCondition.getField().equals(field)) {
				return fieldCondition;
			}
		}
		FieldConditionImpl fieldCondition = new FieldConditionImpl(field);
		conditions.add(fieldCondition);
		
		return fieldCondition;
	}
	
	protected Field findField(String fieldName) {
		for (Field field : getFields()) {
			if (field.getDisplayName().equals(fieldName)) {
				return field;
			}
		}
		return null;
	}
	
	public void addNewFilter(String newFilter) {
		if (!StringUtils.isEmpty(newFilter)) {
			try {
				JSONObject obj = new JSONObject(newFilter);
				JSONArray fields = obj.getJSONArray("fields");
				for (int i=0; i<fields.length(); i++) {
					JSONObject field = fields.getJSONObject(i);
					String fieldName = field.getString("name");
					Field filterField = findField(fieldName);
					
					JSONArray operators = field.getJSONArray("operators");
					
					if (filterField!=null && operators.length()>0) {
						FieldConditionImpl condition = addFieldCondition(filterField);
						for (int j=0; j<operators.length(); j++) {
							JSONObject operator = operators.getJSONObject(j);
							if (!operator.has("removed") || !operator.getBoolean("removed")) {
								int operatorIndex = operator.getInt("operator");
								Operator<?> op = filterField.getOperators().get(operatorIndex);
								JSONArray operands = operator.getJSONArray("operands");
								List<String> operandsList = new ArrayList<String>(operands.length());
								for (int k=0; k<operands.length(); k++) {
									String operand = operands.getString(k);
									if (!StringUtils.isEmpty(operand)) {
										operandsList.add(operand);
									}
								}
								condition.addSingleCondition(op, operandsList);
							}
						}
					}
				}
			} catch (JSONException e) {
				LOG.warn("Cannot parse filter", e);
			}
		}
	}

	public String getJson() {
		JSONObject json = new JSONObject();
		JSONArray jsonFields = new JSONArray();

		try {
			for (Field field : getFields()) {
				FieldConditionImpl fieldCondition = getFieldCondition(field);
				if (fieldCondition==null) {
					fieldCondition = new FieldConditionImpl(field);
				}
				jsonFields.put(fieldCondition.getJsonObject());
			}
			json.put("fields", jsonFields);
		} catch (JSONException e) {
			LOG.warn("Cannot get JSON string for filter", e);
			return "";
		}
		return json.toString();
	}

	protected static class FieldConditionImpl implements FieldCondition {
		private Field field;
		private final List<SingleConditionImpl<?>> singleConditions = 
				new ArrayList<SingleConditionImpl<?>>();

		FieldConditionImpl(Field field) {
			this.field = field;
		}
		
		public JSONObject getJsonObject() throws JSONException {
			JSONObject jsonObj = new JSONObject();
			JSONArray operators = new JSONArray();
			
			jsonObj.put("name", field.getDisplayName());
			for (SingleConditionImpl<?> singleCondition : singleConditions) {
				operators.put(singleCondition.getJsonObject(field));
			}
			jsonObj.put("operators", operators);
			return jsonObj;
		}

		@Override
		public String getWhereClause() {
			StringBuffer condition = new StringBuffer();
			for (SingleConditionImpl<?> singleCondition : singleConditions) {
				String whereClause = singleCondition.getWhereClause(field.getColName());
				
				if (whereClause.length()>0) {
					if (condition.length()>0) {
						condition.append(" OR ");
					}
					condition.append("(").append(whereClause).append(")");
				}
			}
			return condition.toString();
		}

		@Override
		public Field getField() {
			return field;
		}

		@Override
		public List<SingleConditionImpl<?>> getSingleConditions() {
			return singleConditions;
		}

		@SuppressWarnings("unchecked")
		public <T> SingleConditionImpl<T> addSingleCondition(Operator<T> op, List<String> operandsList) {
			if (!op.getAcceptedOperands().isEmpty()) {
				for (SingleConditionImpl<?> sc : this.singleConditions) {
					if (sc.getOperator().equals(op)) {
						sc.addOperands(operandsList);
					}
					return (SingleConditionImpl<T>) sc;
				}
			}
			SingleConditionImpl<T> singleCondition = new SingleConditionImpl<T>(op, operandsList);
			singleConditions.add(singleCondition);
			return singleCondition;
		}
	}
	
	protected static class SingleConditionImpl<T> implements SingleCondition<T> {
		private Operator<T> operator;
		private List<T> operands;
		private List<? extends OperatorModifier> operatorModifiers;

		public SingleConditionImpl(Operator<T> operator, T ... operands) {
			this(operator, Collections.<OperatorModifier>emptyList(), operands);
		}
		
		public void addOperands(List<String> operandsList) {
			operands = new ArrayList<T>(operands);
			for (String operand : operandsList) {
				operands.add(operator.parseOperand(operand));
			}
		}

		public JSONObject getJsonObject(Field field) throws JSONException {
			JSONObject jsonObj = new JSONObject();
			JSONArray operands = new JSONArray();
			
			jsonObj.put("operator", getOperatorIndex(field, operator));
			for (String operand : getOperandStrings()) {
				operands.put(operand);
			}
			jsonObj.put("operands", operands);
			return jsonObj;
		}

		private int getOperatorIndex(Field field, Operator<T> operator) throws JSONException {
			for (int i=0; i<field.getOperators().size(); i++) {
				if (field.getOperators().get(i)==operator) {
					return i;
				}
			}
			throw new JSONException("Cannot find operator " + operator.getMetadata().getDisplayName());
		}

		public SingleConditionImpl(Operator<T> operator, List<String> operands) {
			this.operator = operator;
			this.operatorModifiers = Collections.<OperatorModifier>emptyList();
			this.operands = new ArrayList<T>(operands.size());
			for (String operand : operands) {
				this.operands.add(operator.parseOperand(operand));
			}
		}
		
		public SingleConditionImpl(Operator<T> operator,
				List<? extends OperatorModifier> operatorModifiers,
				T ... operands) {
			this.operator = operator;
			this.operatorModifiers = operatorModifiers;
			if (!operator.getMetadata().isOperandsCountLimited() && 
					operands.length!=operator.getMetadata().getOperandsCount()) {
				throw new  IllegalArgumentException("Operator " + 
						operator.getMetadata().getDisplayName() + " expect " + 
						operator.getMetadata().getOperandsCount() + " operands, but " +
						operands.length + " are supplied");
			}
			this.operands = Arrays.<T>asList(operands); 
		}

		@Override
		public String getWhereClause(String colName) {
			return operator.getWhereClause(colName, operands, operatorModifiers);
		}

		@Override
		public boolean isNot() {
			return false;
		}

		@Override
		public Operator<T> getOperator() {
			return operator;
		}

		@Override
		public List<? extends OperatorModifier> getOperatorModifiers() {
			return operatorModifiers;
		}

		@Override
		public T getOperand() {
			return operands.get(0);
		}

		@Override
		public List<T> getOperands() {
			return Collections.unmodifiableList(operands);
		}

		@Override
		public List<String> getOperandStrings() {
			List<String> operandStrings = new ArrayList<String>(operands.size());
			for (T t : operands) {
				operandStrings.add(operator.operandToString(t));
			}
			return Collections.unmodifiableList(operandStrings);
		}
	}
}
