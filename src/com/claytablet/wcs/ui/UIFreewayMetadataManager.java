package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.client.producer.TranslationProvider.Type;
import com.claytablet.lspconnector.lionbridge.AnalysisCode;
import com.claytablet.lspconnector.lionbridge.FreewaySimpleClient;
import com.claytablet.model.event.metadata.IMetadataGroup;
import com.claytablet.model.event.metadata.MetadataFactory;
import com.claytablet.model.event.metadata.custom.freeway.FreewayMetadataGroup;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.impl.FreewayAnalysisCodeConfig;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIFreewayMetadataManager implements Action {
	protected static final Log LOG = LogFactory.getLog(UIFreewayMetadataManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private JobManager jobManager;
	@InjectForRequest private ConfigurationManager configManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String providerId = ics.GetVar(ProviderMetadataAdapter.PROVIDERID_ARG);
		boolean editable = Boolean.valueOf(ics.GetVar(ProviderMetadataAdapter.EDITABLE_ARG));
		String metadataStr = ics.GetVar(ProviderMetadataAdapter.PROVIDERMETADATA_ARG);
		
		FreewayMetadataGroup fmdg = null;
		
		if (!StringUtils.isEmpty(metadataStr)) {
			try {
				IMetadataGroup mdg = MetadataFactory.getMetadataGroupFromXml(metadataStr);
				fmdg = FreewayMetadataGroup.convert2FreewayMetadataGroup(mdg);
			} catch (Exception e) {
				LOG.warn("Cannot parse Freeway metadata", e);
			}
		}
		
		TranslationProvider provider = entityManager.getTranslationProvider(providerId);
		LOG.debug("UIFreewayMetadataManager.doMetadatFrount: providerid=" + ics.GetVar("providerid"));
		
		List<AnalysisCodeValue> acsv = new ArrayList<AnalysisCodeValue>(3);

		if (provider!=null && provider.getProviderType()==Type.LIONBRIDGE_FREEWAY) {
			if (editable) {
				if (StringUtils.isEmpty(provider.getHostName()) ||
						StringUtils.isEmpty(provider.getLoginUser()) ||
						StringUtils.isEmpty(provider.getPassword()) ) {
					List<FreewayAnalysisCodeConfig> faccs = 
							FreewayAnalysisCodeConfig.getAllFreewayAnalysisCodeConfigs(configManager);
					
					if (!faccs.isEmpty()) {
						for (FreewayAnalysisCodeConfig facc : faccs) {
							acsv.add(new AnalysisCodeValue(facc, 
									fmdg==null? "" : fmdg.getAnalysisCodeValue(facc.getAcLevel())));
						}
						model.add("error", "");
					} else {
						model.add("error", "Freeway login information is not configured, cannot obtain analysis code");
					}
				} else {
					FreewaySimpleClient fsc = new FreewaySimpleClient();
					try {
						String proxyHost = configManager.get(ProducerCommonConfigs.CT_PROXY_HOST);
						
						if (!StringUtils.isEmpty(proxyHost)) {
							fsc.setProxyValues(proxyHost, configManager.get(ProducerCommonConfigs.CT_PROXY_PORT), 
									configManager.get(ProducerCommonConfigs.CT_PROXY_USER), 
									configManager.get(ProducerCommonConfigs.CT_PROXY_PASSWORD), 
									configManager.get(ProducerCommonConfigs.CT_PROXY_DOMAIN));
						}
						List<AnalysisCode> acs = fsc.getFreewayAnalysisCodes(
								provider.getHostName(), provider.getLoginUser(), provider.getPassword());
						
						for (AnalysisCode ac : acs) {
							acsv.add(new AnalysisCodeValue(configManager, ac, 
									fmdg==null? "" : fmdg.getAnalysisCodeValue(ac.getLevel())));
						}
						model.add("error", "");
					} catch (Exception e) {
						String msg = "Cannot retrieve analysis code information from " + provider.getHostName();
						LOG.warn(msg, e);
						model.add("error", msg + ": " + String.valueOf(e));
					}
				}
			} else if (fmdg!=null) {
				for (int i=1; i<=3; i++) {
					acsv.add(new AnalysisCodeValue(configManager, fmdg.getAnalysisCodeName(i), i,
							fmdg.getAnalysisCodeValue(i)));
				}
				model.add("error", "");
			} else {
				model.add("error", "");
			}
			
			ics.SetVar(ProviderMetadataAdapter.EDITABLE_ARG, String.valueOf(editable));
		} else {
			LOG.warn("Provider is not Freeway: " + provider);
		}
		model.add("AnalysisCodes", acsv);
		model.add("editable", String.valueOf(editable));
		model.add("acsummary", acsv.isEmpty()? "N/A" : "");
	}
	
	public static class AnalysisCodeValue {
		private final String name;
		private final String value;
		private final String description;
		private final String valueDescription;
		private final boolean required;
		private final int level;
		private final List<String> codeNames;
		private final List<String> codeDescriptions;

		private AnalysisCodeValue(FreewayAnalysisCodeConfig facc, String value) {
			this.name = facc.getAcName();
			this.level = facc.getAcLevel();
			this.codeNames = new ArrayList<String>(facc.getValues());
			this.codeDescriptions = new ArrayList<String>(facc.getValueDescriptions());
			
			if (StringUtils.isEmpty(value)) {
				value = facc.getAcDefaultValue();
			}
			this.value = value;
			this.description = facc.getAcDescription();
			this.valueDescription = facc.getValueDescription(value);
			this.required = facc.isAcRequired();
		}

		private AnalysisCodeValue(ConfigurationManager configManager, AnalysisCode ac, String value) {
			this.name = ac.getName();
			this.level = ac.getLevel();
			this.codeNames = ac.getCodeNames();
			this.codeDescriptions = new ArrayList<String>(codeNames.size());
			
			FreewayAnalysisCodeConfig facc = 
					FreewayAnalysisCodeConfig.findFreewayAnalysisCodeConfig(configManager, name);
			if (facc!=null && StringUtils.isEmpty(value)) {
				value = facc.getAcDefaultValue();
			}
			this.value = value;
			this.description = facc==null? null : facc.getAcDescription();
			this.valueDescription = facc==null? null : facc.getValueDescription(value);
			this.required = facc==null? false : facc.isAcRequired();
			
			for (int i=0; i<codeNames.size(); i++) {
				if (facc!=null) {
					codeDescriptions.add(facc.getValueDescription(codeNames.get(i)));
				} else {
					codeDescriptions.add(null);
				}
			}
			
		}

		private AnalysisCodeValue(ConfigurationManager configManager, String name, int level, String value) {
			this.name = name;
			this.level = level;
			this.codeNames = Collections.<String>emptyList();
			this.codeDescriptions = Collections.<String>emptyList();
			this.value = value;
			FreewayAnalysisCodeConfig facc = 
					FreewayAnalysisCodeConfig.findFreewayAnalysisCodeConfig(configManager, name);
			this.description = facc==null? null : facc.getAcDescription();
			this.valueDescription = facc==null? null : facc.getValueDescription(value);
			this.required = facc==null? false : facc.isAcRequired();
		}

		public String getName() {
			return name;
		}

		public List<String> getCodeNames() {
			return codeNames;
		}

		public List<String> getCodeDescriptions() {
			return codeDescriptions;
		}

		public String getValue() {
			return value;
		}
		
		public int getLevel() {
			return level;
		}

		public String getValueDescription() {
			return valueDescription;
		}

		public String getDescription() {
			return description;
		}

		public boolean isRequired() {
			return required;
		}
	}
	
	public static class Adapter implements ProviderMetadataAdapter {
		@Override
		public Type getProviderType() {
			return Type.LIONBRIDGE_FREEWAY;
		}

		@Override
		public String getElementName() {
			return "MetadataFreeway";
		}

		@Override
		public IMetadataGroup createMetadataGroup(ICS ics) {
			FreewayMetadataGroup freewayMetadataGroup = new FreewayMetadataGroup();
			String analysis1name = ics.GetVar("analysiscode1_name");
			String analysis1value = ics.GetVar("analysiscode1_value");
			String analysis2name = ics.GetVar("analysiscode2_name");
			String analysis2value = ics.GetVar("analysiscode2_value");
			String analysis3name = ics.GetVar("analysiscode3_name");
			String analysis3value = ics.GetVar("analysiscode3_value");
			if (!StringUtils.isEmpty(analysis1name)) {
				freewayMetadataGroup.setAnalysisCodeValue(1, analysis1name, analysis1value);
			}
			if (!StringUtils.isEmpty(analysis2name)) {
				freewayMetadataGroup.setAnalysisCodeValue(2, analysis2name, analysis2value);
			}
			if (!StringUtils.isEmpty(analysis3name)) {
				freewayMetadataGroup.setAnalysisCodeValue(3, analysis3name, analysis3value);
			}
			return freewayMetadataGroup;
		}
	}
}
