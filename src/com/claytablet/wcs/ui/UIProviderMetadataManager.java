package com.claytablet.wcs.ui;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.client.producer.TranslationProvider.Type;
import com.claytablet.model.event.metadata.IMetadataGroup;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.SiteManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIProviderMetadataManager implements Action {
	protected static final Log LOG = LogFactory.getLog(UIProviderMetadataManager.class.getName());

	private static final Map<String, ProviderMetadataAdapter> adaptersMap = 
			new HashMap<String, ProviderMetadataAdapter>();
	
	static {
		ProviderMetadataAdapter[] adapters = new ProviderMetadataAdapter[] {
				new UIFreewayMetadataManager.Adapter(),
		};
		
		for (ProviderMetadataAdapter adapter : adapters) {
			adaptersMap.put(adapter.getProviderType().name(), adapter);
		}
	}
	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private JobManager jobManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String providerId = ics.GetVar(ProviderMetadataAdapter.PROVIDERID_ARG);
		String providerMetadata = ics.GetVar(ProviderMetadataAdapter.PROVIDERMETADATA_ARG);
		String editable = ics.GetVar(ProviderMetadataAdapter.EDITABLE_ARG);
		boolean isCurrentProvider = Boolean.valueOf(ics.GetVar("iscurrentprovider"));
		
		TranslationProvider provider = entityManager.getTranslationProvider(providerId);
		
		if (provider!=null) { 
			String providerType = provider.getProviderType().name();
			model.add(ProviderMetadataAdapter.PROVIDERTYPE_ARG, providerType);
			if (isCurrentProvider) {
				model.add(ProviderMetadataAdapter.PROVIDERMETADATA_ARG, providerMetadata);
			}
			model.add(ProviderMetadataAdapter.EDITABLE_ARG, editable);
			
			ProviderMetadataAdapter adapter = adaptersMap.get(providerType);
			
			if (adapter!=null) {
				model.add(ProviderMetadataAdapter.NEW_ELEMENT_NAME_ARG, adapter.getElementName());
				model.add(ProviderMetadataAdapter.PROVIDERID_ARG, providerId);
			} else {
				model.add(ProviderMetadataAdapter.NEW_ELEMENT_NAME_ARG, "");
			}
		}
	}

	public static IMetadataGroup createMetadataGroup(ICS ics2) {
		String providerType= ics2.GetVar(ProviderMetadataAdapter.PROVIDERTYPE_ARG);
		try {
			if (!StringUtils.isEmpty(providerType)) {
				ProviderMetadataAdapter adapter = adaptersMap.get(providerType);
				
				if (adapter!=null) {
					return adapter.createMetadataGroup(ics2);
				}
			}
		} catch (Exception e) {
			LOG.warn("Cannot create provider metadata for " + providerType, e);
		}
		return null;
	}
}
