/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;













import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobFilter;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.ExtendedAssetId;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.Utils;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.ExtendedAssetIdImpl;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.CSRuntimeException;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.ScatteredAsset;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAsset;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;
import com.fatwire.mda.Dimension;
import com.openmarket.xcelerate.asset.AssetIdImpl;

/**
 * @author Mike Field
 *
 */
public class UITranslationWizard implements Action {
	
	protected static final Log LOG = LogFactory.getLog(UITranslationWizard.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private ConfigManager configManager;
	@InjectForRequest private ConfigurationManager ctConfigManager;
	@InjectForRequest private TranslationRequestManager translationRequestManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
			LOG.debug("Handling request...");
			String pagename = ics.GetVar("pagename");
			if ("CustomElements/CT/TranslatePost".equals(pagename)) {
				doTranslatePost();
			} else {
				doTranslateFront();
			}
	}

	private void doTranslateFront() {
		LOG.debug("Doing a TranslateFront");
		String formval_assetids = ics.GetVar("assetids");
		String formval_c = ics.GetVar("c");
		String formval_cid = ics.GetVar("cid");
		AssetId firstid = null;
		String[] assetids = null;
		int numberOfAssets = 1;
		Boolean isSingleAsset = false;
		Boolean goodAssetList = true;	
		
		if (Utilities.goodString(formval_assetids)) {
			
			assetids = formval_assetids.split(";");
			numberOfAssets = assetids.length;
			if (assetids.length > 0) {
				firstid = Utils.stringToAssetId(assetids[0]);
				if (assetids.length == 1) {
					isSingleAsset = true;
				}
				else
				{
					//more than one assetid - check locale values exist
					for(String id : assetids)
					{
						ExtendedAssetIdImpl checkasset = new ExtendedAssetIdImpl(ics, Utils.stringToAssetId(id));
						if(checkasset.getLocale() == null)
						{
							goodAssetList = false;
						}
					}
				}
				
			} else {
				LOG.error("assetids length was zero and its value is"+formval_assetids);
			}
			
		} else if (Utilities.goodString(formval_c) && Utilities.goodString(formval_cid)) {
			
			firstid = new AssetIdImpl( formval_c, Long.parseLong(formval_cid) );
			isSingleAsset = true;
			ics.SetVar("assetids", formval_c+":"+formval_cid);
			LOG.debug("Set firstid as a single asset to: " + firstid.toString());
			
		} else {
			
			LOG.error("CustomElements/CT/TranslateFront requires either assetids or c and cid to be passed to it. The values were c="+formval_c+" and cid="+formval_cid+" and assetids="+formval_assetids);
			
		}
		 
		

		if (firstid != null) {
				
			ExtendedAssetId firstasset = new ExtendedAssetIdImpl( ics, firstid );
			String currentLocale="";
			if(goodAssetList) {
				currentLocale = firstasset.getLocale();
			}
			
			if (currentLocale==null) {
				currentLocale = "";
			}

			model.add("numberOfAssets", numberOfAssets);
			model.add("currentLocale", currentLocale);
			model.add("currentAssetType", firstid.getType());
			setupLocales(currentLocale);
			model.add("workflow", workflowManager.getAllWorkflowProcessesByAssetTypeAndSite(firstid.getType(), null));
			model.add("jobs", entityManager.findTranslationJobs(
					TranslationJobFilter.factory.filter("status='CREATED'")));
			model.add("teams", entityManager.getAllTeamProfiles());
			model.add("showteam", ctConfigManager.get(ProducerCommonConfigs.CT_ENABLE_TEAM_MANAGEMENT));
			
			if (isSingleAsset) {
					
				List<String> translatedLocales = assetManager.getTranslatedLocales(firstid);
				String sTranslatedLocales = translatedLocales.toString();
				sTranslatedLocales = sTranslatedLocales.replace("[", "").replace("]","");
				model.add("asset", firstasset);
				model.add("translatedLocales", sTranslatedLocales);
				model.add("translatedLocalesList", translatedLocales);
					
			}
			
		} else {
				
			LOG.error("At least one assetid must be specified in the form variable 'assetids'");
				
		}
	}
	
	private void setupLocales(String currentLocale) {
		List<String> loc = new ArrayList<String>();
		List<String> hiddenLocales = new ArrayList<String>();
		String siteName = siteManager.getSiteNameById(ics.GetSSVar("pubid"));
		Map<String, String> locales = localeManager.getWCSLocalesAndDescsForSite(siteName);
		for (Map.Entry<String, String> localeEntry : locales.entrySet()) {
			if (!currentLocale.equals(localeEntry.getKey())) {
				if (localeManager.isLocaleHidden(localeEntry.getKey())) {
					hiddenLocales.add(localeEntry.getKey());
				} else {
					loc.add(localeEntry.getKey());
				}
			}
		}
		Collections.sort(loc);
		Collections.sort(hiddenLocales);
		model.add("eligibleLocales", loc);
		model.add("hiddenLocales", hiddenLocales);
		model.add("localeDetails", locales);
	}

	private void doTranslatePost() {
		
		if (Utilities.goodString(ics.GetVar("assetids")) && (Utilities.goodString(ics.GetVar("selectedlocales")) || 
				Utilities.goodString(ics.GetVar("hidden_selectedlocales")))) {
			List<AssetId> assetsToTranslate = _getAssetsToTranslate();
			TranslationJob job = _getJobIfAny();
			_createTranslationRequests(assetsToTranslate, job);
			_showPostResults(job);
		} else {
			_processError();
		}
		
	}
	
	private void _processError() {
		if (!Utilities.goodString(ics.GetVar("assetids"))) {
			model.add("error", "You must specify an asset id to translate.");
		} else if (!Utilities.goodString(ics.GetVar("selectedlocales"))) {
			model.add("error", "You must select at least one target language.");
		}
		String formvals_assetids = ics.GetVar("assetids");
		AssetId aid = Utils.stringToAssetId(formvals_assetids);
		model.add("c", aid.getType());
		model.add("cid", Long.toString(aid.getId()));
		LOG.debug("Setting c and cid to " + aid.toString());
	}
	
	private void _createTranslationRequests(List<AssetId> assetsToTranslate, TranslationJob job) {
		String formvals_targetlangs = ics.GetVar("selectedlocales");
		if (formvals_targetlangs==null) {
			formvals_targetlangs = "";
		}
		
		String formvals_hiddenTargetlangs = ics.GetVar("hidden_selectedlocales");
		if (formvals_hiddenTargetlangs==null) {
			formvals_hiddenTargetlangs = "";
		}
		
		String formvals_workflowprocid = ics.GetVar("workflowprocid");
		String formvals_siteid = ics.GetVar("siteid");
		String sitename = siteManager.getSiteNameById( formvals_siteid );
		String[] targetlangs = formvals_targetlangs.split(";");
		String[] hiddenTargetlangs = formvals_hiddenTargetlangs.split(";");
		
		List<String> targetLangList = new ArrayList<String>(Arrays.asList(targetlangs));
		targetLangList.addAll(Arrays.asList(hiddenTargetlangs));
		
		// For each asset to translate, create a translation request
		for (AssetId aid : assetsToTranslate) {		
			ExtendedAssetId eid = new ExtendedAssetIdImpl(ics, aid);
			String srcLocale = eid.getLocale();
			if (!StringUtils.isEmpty(srcLocale)) {
				for (String targetlang : targetLangList) {
					// Add translation request
					if (!StringUtils.isEmpty(targetlang)) {
						TranslationRequest tr = translationRequestManager.createNewTranslationRequest( 
								sitename, aid.toString() , srcLocale, targetlang, job);
						tr.setName(eid.getName());
						// If workflow id was specified, add it to the request
						if (Utilities.goodString(formvals_workflowprocid)) {
							tr.setTargetWorkflowId( formvals_workflowprocid );
						}
						AssetId taid = assetManager.getTranslation(aid, targetlang);
						if (taid!=null) {
							tr.setNativeTargetId(taid.toString());
						}
						entityManager.save(tr);
					}
				}
			} else {
				LOG.warn("Asset " + eid.getAssetType() + ":" + aid.toString() + " has no locale, skipping...");
			}
		}
	}
	
	// If jobid was specified, set job id
	private TranslationJob _getJobIfAny() {
		TranslationJob job = null;
		String formvals_jobid = ics.GetVar("jobid");
		if (Utilities.goodString(formvals_jobid) && !"queue".equalsIgnoreCase(formvals_jobid)) {
			if ("newjob".equalsIgnoreCase(formvals_jobid)) {
				job = entityManager.save(entityManager.createTranslationJob(ics.GetVar("newjobname")));
			} else {
				job = entityManager.getTranslationJob(formvals_jobid);
			}
		}	
		return job;
	}
	
	private List<AssetId> _getAssetsToTranslate() {
		
		String formvals_assetids = ics.GetVar("assetids");
		String[] assetids = formvals_assetids.split(";");
		String formvals_getdeps = ics.GetVar("sendwithdependencies");
		
		// Add assetids to an array list for easy looping
		List<AssetId> assetsToTranslate = new ArrayList<AssetId>();
		for (String s : assetids) {
			if (s.indexOf(":") > -1) {
				String type = s.split(":")[0].trim();
				String id = s.split(":")[1].trim();
				AssetId aid = new AssetIdImpl(type, Long.parseLong(id));
				assetsToTranslate.add(aid);
			}
		}

		// If "send with dependencies" was selected, get dependencies
		// and add to list of assets to translate
		if ("1".equalsIgnoreCase(formvals_getdeps)) {
			List<AssetId> workinglist = new ArrayList<AssetId>();
			String pubid = ics.GetSSVar("pubid");
			for (AssetId aid : assetsToTranslate) {
				for (AssetId depaid : assetManager.getDependencies(aid) ) {
					if ( configManager.isEnabledAssetType( pubid, depaid.getType() ) ) { // Only add to list if this is an enabled asset type
						workinglist.add( depaid  );
					}
				}
			}
			assetsToTranslate.addAll( workinglist );
			assetsToTranslate = _removeDuplicates(assetsToTranslate); // Remove duplicates from assetsToTranslate		
		}
		return assetsToTranslate;
	}
	
	private List<AssetId> _removeDuplicates(List<AssetId> allids) {
		List<AssetId> uniqueids = new ArrayList<AssetId>();
		Set<AssetId> tempset = new HashSet<AssetId>();
		for (AssetId aid : allids) {
			if (tempset.add(aid)) {
				uniqueids.add(aid);
			}
		}
		return uniqueids;
	}
	
	private void _showPostResults(TranslationJob job) {

		// If no jobid was specified, move to queue screen
		if (job == null) {
				
			FTValList args = new FTValList();
  			args.removeAll();
  			args.setValString("message", "Translation requests successfully added to the queue.");
			args.setValString("showheaderfooter", "true");
			ics.InsertPage("CustomElements/CT/QueueFront", args);
			//model.add("message", "Translation requests successfully added to the queue.");
			//model.add("showheaderfooter", "true");
			//model.add("nextpage", "CustomElements/CT/QueueFront");
				
		} else { // If a jobid was specified, show job details screen
				
			FTValList args = new FTValList();
  			args.removeAll();
  			args.setValString("message", "Translation requests successfully added to the job");
			args.setValString("showheaderfooter", "true");
			args.setValString("jobid", job.getId() );
			ics.InsertPage("CustomElements/CT/JobFront", args);	
			//model.add("message", "Translation requests successfully added to the job.");
			//model.add("showheaderfooter", "true");
			//model.add("jobid", job.getId() );
			//model.add("nextpage", "CustomElements/CT/JobFront");
		}	
	}

}