package com.claytablet.wcs.ui;

import static com.claytablet.client.util.NotNullUtils.notNull;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactory.DbType;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.db.AbsDatabaseSchemaManager;
import com.claytablet.client.producer.impl.jpa.JpaManagerFactory;
import com.claytablet.client.util.db.DbUtil;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIDeveloperToolManager implements Action {
	private enum ACTION {
		DropDB,
		RecreateDB,
	}
	
	protected static final Log LOG = LogFactory.getLog(UIProvidersConfigManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactory factory;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		UserContext ctx = new UserContextImpl(ics);
		if (!ctx.getUserProfile().isGlobalAdmin()) {
			throw new UnsupportedOperationException("Only administrator can perform this operation");
		} else {
			String pagename = ics.GetVar("pagename");
			if ("CustomElements/CT/WEM/DeveloperToolPost".equals(pagename)) {
				doDeveloperToolPost();
			} else {
				doDeveloperToolFront();
			}
		}
	}

	private void doDeveloperToolFront() {
		Connection conn = null;
		try {
			model.add("message", "TestMessage: " + ics.GetVar("message"));
			model.add("errormessage", ics.GetVar("errormessage"));
			conn = getConnection();
			DatabaseMetaData metadata = conn.getMetaData();
			List<String> dbInfos = new ArrayList<String>();
			dbInfos.add("DatabaseProductName: " + metadata.getDatabaseProductName());
			dbInfos.add("DatabaseProductVersion: " + metadata.getDatabaseProductVersion());
			dbInfos.add("DriverName: " + metadata.getDriverName());
			dbInfos.add("DriverVersion: " + metadata.getDriverVersion());
			dbInfos.add("URL: " + metadata.getURL());
			dbInfos.add("Username: " + metadata.getUserName());
			AbsDatabaseSchemaManager sm = (AbsDatabaseSchemaManager)
					(DbType.fromDriverName(metadata.getDriverName()).getSchemaManager());
			dbInfos.add("CT Database version: " + sm.getDbVersion(conn));
			model.add("dbinfos", dbInfos);
		} catch (Exception e) {
			model.add("errormessage", String.valueOf(e));
			LOG.debug("Failed to retrive ClayTablet DB info. ", e);
		} finally {
			DbUtil.silentlyClose(conn, null, null);
		}
	}

	private Connection getConnection() {
		EntityManager em = ((JpaManagerFactory)factory).getEntityManager();
		Session session = (Session) em.getDelegate();
		@SuppressWarnings("deprecation")
		Connection conn = notNull(session.connection());
		return conn;
	}

	private void doDeveloperToolPost() {
		switch (ACTION.valueOf(ics.GetVar("action"))) {
		case DropDB:
			dropDb();
			break;
		
		case RecreateDB:
			recreateDb();
			break;
		}
	}

	private void dropDb() {
		Connection conn = null;
		try {
			conn = getConnection();
			DatabaseMetaData metadata = conn.getMetaData();
			AbsDatabaseSchemaManager sm = (AbsDatabaseSchemaManager)
					(DbType.fromDriverName(metadata.getDriverName()).getSchemaManager());
			sm.dropDB(conn);
			ics.SetVar("message", "ClayTablet database has been dropped successfully");
		} catch (Exception e) {
			ics.SetVar("errormessage", "Failed to drop ClayTablet DB: " + String.valueOf(e));
			LOG.debug("Failed to drop ClayTablet DB. ", e);
		} finally {
			DbUtil.silentlyClose(conn, null, null);
		}
	}

	private void recreateDb() {
		Connection conn = null;
		try {
			conn = getConnection();
			DatabaseMetaData metadata = conn.getMetaData();
			AbsDatabaseSchemaManager sm = (AbsDatabaseSchemaManager)
					(DbType.fromDriverName(metadata.getDriverName()).getSchemaManager());
			sm.dropDB(conn);
			sm.createDB(conn);
			UserContext ctx = new UserContextImpl(ics);
			ManagerFactorySession session = factory.newGlobalAdminSession(ctx);
			try {
				session.getConfigurationManager().getGlobalConfigurationSet().put(
						ProducerCommonConfigs.CT_ENABLE_TEAM_MANAGEMENT, false);
			} finally {
				session.close();
			}
			ics.SetVar("message", "ClayTablet database has been recreated recreated successfully");
		} catch (Exception e) {
			ics.SetVar("errormessage", "Failed to recreate ClayTablet DB" + String.valueOf(e));
			LOG.debug("Failed to recreate ClayTablet DB. ", e);
		} finally {
			DbUtil.silentlyClose(conn, null, null);
		}
	}
}
