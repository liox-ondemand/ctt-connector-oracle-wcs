/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationKey;
import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.UserContext;
import com.claytablet.wcs.system.impl.ConfigManagerImpl;
import com.claytablet.wcs.system.impl.SystemEventUtils;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * @author Mike Field
 *
 */
public class UICTConfigManager implements Action {

	private static final String HIDDEN_SYSTEM_CONFIGURATION = "Hidden System Configuration";
	protected static final Log LOG = LogFactory.getLog(UIProvidersConfigManager.class.getName());

	private enum ACTION {
		Update,
	}
	
	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactory factory;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String pagename = ics.GetVar("pagename");
		if ("CustomElements/CT/WEM/CTConfigPost".equals(pagename)) {
			doCTConfigPost();
		} else {
			doCTConfigFront();
		}
	}

	private void doCTConfigFront() {
		UserContext ctx = new UserContextImpl(ics);
		Map<String, ConfigurationSection> sections = new HashMap<String, ConfigurationSection>();
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		try {
			ConfigurationManager cm = adminSession.getConfigurationManager();
			for (ConfigurationKey<?> key : ConfigurationKey.Factory.getKnownKeys()) {
				if (!"true".equals(ics.GetVar("showProtected")) && 
						(key.getName().equals(ProducerCommonConfigs.CT_ENABLE_TEAM_MANAGEMENT.getName()) ||
						key.getName().equals(ProducerCommonConfigs.CT_MANUAL_KEY_MANAGEMENT.getName()))) {
					continue;
				}
				if ("true".equals(ics.GetVar("showHidden")) || !key.isHidden()) {
					addToSections(sections, key, cm.getString(key));
				}
			}
			ConfigurationSection hiddenSection = sections.remove(HIDDEN_SYSTEM_CONFIGURATION);
			ArrayList<ConfigurationSection> modelSections = 
					new ArrayList<ConfigurationSection>(sections.values());
			Collections.sort(modelSections);
			if (hiddenSection!=null) {
				modelSections.add(hiddenSection);
			}
			model.add("sections", modelSections);
			
		} finally {
			adminSession.close();
		}
	}

	private void addToSections(Map<String, ConfigurationSection> sections,
			ConfigurationKey<?> key, String value) {
		String sectionName = key.isHidden()? HIDDEN_SYSTEM_CONFIGURATION :key.getSection();
		ConfigurationSection section = sections.get(sectionName);
		
		if (section==null) {
			section = new ConfigurationSection(sectionName);
			sections.put(sectionName, section);
		}
		section.addEntry(key, value);
	}

	private void doCTConfigPost() {
		switch (ACTION.valueOf(ics.GetVar("action"))) {
		case Update:
			updateConfig();
			break;
		}

		FTValList args = new FTValList();
		args.removeAll();
		if (!StringUtils.isEmpty(ics.GetVar("showHidden"))) {
			args.setValString("showHidden", ics.GetVar("showHidden"));
		}
		if (!StringUtils.isEmpty(ics.GetVar("showProtected"))) {
			args.setValString("showProtected", ics.GetVar("showProtected"));
		}
		args.setValString("message", ics.GetVar("message"));
		args.setValString("errormessage", ics.GetVar("errormessage"));
		args.setValString("updatedkey", ics.GetVar("updatedkey"));
		args.setValString("newvalue", ics.GetVar("newvalue"));
		ics.InsertPage("CustomElements/CT/WEM/CTConfigFront", args);	
	}

	private void updateConfig() {
		String keyName = ics.GetVar("keyname");
		String value = ics.GetVar("value");
		
		if (!StringUtils.isEmpty(keyName)) {
			ConfigurationKey<?> key = ConfigurationKey.Factory.getKey(keyName);
			if (key!=null) {
				UserContext ctx = new UserContextImpl(ics);
				ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
				try {
					ConfigurationManager cm = adminSession.getConfigurationManager();
					if (value==null) {
						cm.getGlobalConfigurationSet().remove(key);
					} else {
						cm.getGlobalConfigurationSet().putString(key, value);
					}
					ics.SetVar("message", "successfully updated.");
					// Update poll intervals
					if (keyName.equals(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_INTERVAL.getName())) {
						// modify Async ops system event
						Integer pollInterval = cm.get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_INTERVAL);
						SystemEventUtils.Event.AsyncOp.updateInterval(pollInterval, ics);
					} else if (keyName.equals(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL.getName())) {
						Integer pollInterval = cm.get(ProducerCommonConfigs.CT_PLATFORM_POLL_INTERVAL);
						SystemEventUtils.Event.CheckMessage.updateInterval(pollInterval, ics);
					}
				} catch (Exception e) {
					ics.SetVar("errormessage", 
							StringEscapeUtils.escapeHtml("Cannot update configuration: " + String.valueOf(e)));
					ics.SetVar("newvalue", StringEscapeUtils.escapeHtml(value));
					LOG.debug("Cannot update " + keyName, e);
				} finally {
					adminSession.close();
				}
			}
			ics.SetVar("updatedkey", keyName);
			
			
		}
	}
	
	public class ConfigurationSection implements Comparable<ConfigurationSection>{
		private final String name;
		private final List<ConfigurationEntry> entries = new ArrayList<ConfigurationEntry>();
		
		private ConfigurationSection(String name) {
			this.name = name;
		}

		private void addEntry(ConfigurationKey<?> key, String value) {
			entries.add(new ConfigurationEntry(key, value));
		}

		public List<ConfigurationEntry> getEntries() {
			Collections.sort(entries);
			return entries;
		}

		public String getName() {
			return name;
		}

		@Override
		public int compareTo(ConfigurationSection otherSection) {
			return name.compareTo(otherSection.name);
		}
	}
	
	public class ConfigurationEntry implements Comparable<ConfigurationEntry>{
		final private ConfigurationKey<?> key;
		final private String value;
		
		private ConfigurationEntry(ConfigurationKey<?> key, String value) {
			this.key = key;
			this.value = value;
		}

		public ConfigurationKey<?> getKey() {
			return key;
		}

		public String getValue() {
			return value;
		}

		@Override
		public int compareTo(ConfigurationEntry otherEntry) {
			return key.getLabel().compareTo(otherEntry.key.getLabel());
		}
	}
}
