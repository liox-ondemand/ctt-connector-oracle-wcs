/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.wcs.system.AssetListManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.ExtendedAssetId;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.Utils;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.ExtendedAssetIdImpl;
import com.fatwire.assetapi.data.AssetId;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;
import com.openmarket.xcelerate.asset.AssetIdImpl;


/**
 * @author Mike Field
 *
 */
public class UIBulkTranslationManager implements Action {
	
	protected static final Log LOG = LogFactory.getLog(UIBulkTranslationManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private AssetListManager assetListManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest private ConfigManager configManager;
	@InjectForRequest private TranslationRequestManager translationRequestManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;
	
	
	public void handleRequest(ICS ics) {
		
		if (Utilities.goodString( ics.GetVar("action")) && "bulktranslate".equalsIgnoreCase( ics.GetVar("action") )) {
			model.add("showform", false);
			_doPost();
		}
		else
		{
			// initial load form shows after clicking context menu
			// 'Bulk Translate'
			String rawAssetIds = ics.GetVar("allassetids");
			if (!Utilities.goodString(rawAssetIds)) {
				rawAssetIds = ics.GetVar("assetids");
			}
			List<ExtendedAssetId> assetIds = parseAssetIdObjects(rawAssetIds);
			model.add("showform", true);
			_showForm(assetIds);
		}
	}
	
	private List<ExtendedAssetId> parseAssetIdObjects(String idsIn)
	{
		List<ExtendedAssetId> retVal = new ArrayList<ExtendedAssetId>();
		String [] idList = idsIn.split(";");
		for(String curr : idList)
		{
			
			String [] fields = curr.split(":");
			LOG.info("processing curr: "+fields[0]+" : "+fields[1]);
			
			if(fields.length == 2)
			{
				AssetIdImpl theasset = new AssetIdImpl(fields[0], Long.parseLong(fields[1]));
				ExtendedAssetId eid = new ExtendedAssetIdImpl(ics, theasset);
				retVal.add(eid);	
			}
			else
			{
				LOG.error("Unable to parse Asset ID fields, expecting AssetType:AssetId - could not parse string value: ["+fields.toString()+"]");
			}
		}
		
		if(retVal.size() >0)
		{
			return retVal;
		}
		else
		{
			LOG.info("No valid Asset IDs were found from string input: ["+idsIn+"]");
			return null;
		}
	}
	
	private void _showForm(List<ExtendedAssetId> idsIn)
	{
		List<String> eligibleLocales = new ArrayList<String>();
		List<ExtendedAssetId> simpleassetlist = new ArrayList<ExtendedAssetId>(idsIn);
		List<ExtendedAssetId> assetlist = new ArrayList<ExtendedAssetId>();
		List<String> assettypes = new ArrayList<String>();
		//configure filters
		String formval_locale = ics.GetVar("localeFilter");
		String formval_assettypes = ics.GetVar("assetTypeFilter");
		String[] selectedAssetTypes = (formval_assettypes != null) ? formval_assettypes.split(";") : null;
		List<String> listSelectedAssetTypes = new ArrayList<String>();
		if (selectedAssetTypes != null) {
			for (String s : selectedAssetTypes) {
				listSelectedAssetTypes.add(s);
			}
		}
		Boolean hasLocaleFilter = Utilities.goodString(ics.GetVar("localeFilter"));
		Boolean hasAssetTypeFilter = Utilities.goodString(ics.GetVar("assetTypeFilter"));
		
		for (ExtendedAssetId eid : simpleassetlist) {
			
			// Create filtered list of assets
			if (hasAssetTypeFilter && hasLocaleFilter) {
				if (listSelectedAssetTypes.contains( eid.getAssetType() ) && formval_locale.equals( eid.getLocale() )) {
					assetlist.add(eid);
				}
			} else if (hasAssetTypeFilter && !hasLocaleFilter) {
				if (listSelectedAssetTypes.contains( eid.getAssetType() ) ) {
					assetlist.add(eid);
				}
			} else if (hasLocaleFilter && !hasAssetTypeFilter) {
				if (formval_locale.equals( eid.getLocale() )) {
					assetlist.add(eid);
				}
			} else if (!Utilities.goodString(formval_locale) && !Utilities.goodString(formval_assettypes)) {
				assetlist.add(eid);
			}
			

			// Create list of assettypes
			if (!assettypes.contains( eid.getAssetType() )) {
				assettypes.add( eid.getAssetType() );
			}

			// Create list of eligible locales
			if (Utilities.goodString( eid.getLocale() ) && !eligibleLocales.contains( eid.getLocale() )) {
				eligibleLocales.add( eid.getLocale() );
			}

		}
		
		_validateAssetList(assetlist);
		_validateAssetList(assetlist);
		
		model.add("assetTypes", assettypes);
		model.add("eligibleLocales", eligibleLocales );		
		model.add("allassetlist", idsIn);
		model.add("assetlist", assetlist);
		
		_setupPaginationVariables(assetlist);
		
	}
	
	private void _setupPaginationVariables( List<ExtendedAssetId> assetlist ) {
		int page_maxperpage = configManager.getMaxItemsPerPage();
		int page_firstrecord = 0;
		if (Utilities.goodString(ics.GetVar("page_firstrecord"))) {
			page_firstrecord = Integer.parseInt( ics.GetVar("page_firstrecord") );
		}
		int page_totalrecords = assetlist.size();
		List<Integer> page_startrecords = new ArrayList<Integer>();
		for (int i=0; i < assetlist.size(); i=i+page_maxperpage ) {
			page_startrecords.add(i);
		}
	
		model.add("page_maxperpage", page_maxperpage);
		model.add("page_firstrecord", page_firstrecord);
		model.add("page_lastrecord", page_firstrecord + page_maxperpage - 1);
		model.add("page_totalrecords", page_totalrecords);
		model.add("page_startrecords", page_startrecords);
		
		if (Utilities.goodString(ics.GetVar("assetids"))) {
			String[] saSelectedassetids = ics.GetVar("assetids").split(";");
			List<String> selectedassetids = new ArrayList<String>();
			for (String s : saSelectedassetids) {
				selectedassetids.add(s);
			}
			model.add("selectedassetids", selectedassetids);
		}
	}
	
	private void _doPost()
	{
		if (Utilities.goodString(ics.GetVar("assetids"))) {
			FTValList args = new FTValList();
			args.removeAll();
			args.setValString("assetids", ics.GetVar("assetids"));
			LOG.info("Passing assetids to TranslateFront with value=" + ics.GetVar("assetids"));
			ics.InsertPage("CustomElements/CT/TranslateFront", args);
		} else {
			_processError();
		}		
	}
	
	private void _processError()
	{
		if (!Utilities.goodString(ics.GetVar("assetids"))) {
			model.add("error", "You must specify an asset id to translate.");
		}
		String formvals_assetids = ics.GetVar("assetids");
		AssetId aid = Utils.stringToAssetId(formvals_assetids);
		model.add("c", aid.getType());
		model.add("cid", Long.toString(aid.getId()));
		LOG.debug("Setting c and cid to " + aid.toString());
		ics.CallElement("CustomElements/CT/ErrorPage", null);
		
	}

	private void _validateAssetList(List<ExtendedAssetId> assetlist) {
		
		String previouslocale = null;
		Boolean hasDifferentLocales = false;
		Boolean hasUnassignedLocales = false;
		for (ExtendedAssetId eid : assetlist) {
			
			// Validate if all locales are the same
			if ( previouslocale != null && !previouslocale.equals( eid.getLocale() )) {
				hasDifferentLocales = true;
			}
			
			// Validate all assets have a locale
			previouslocale = eid.getLocale();
			if ( eid.getLocale() == null || "".equals( eid.getLocale() ) ) {
				hasUnassignedLocales = true;
			}
			
			model.add("hasDifferentLocales", hasDifferentLocales );
			model.add("hasUnassignedLocales", hasUnassignedLocales );
			
		}
		
	}
}