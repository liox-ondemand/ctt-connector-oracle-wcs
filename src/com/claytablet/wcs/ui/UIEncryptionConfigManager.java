package com.claytablet.wcs.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.EncryptionManager;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.impl.EncryptionManagerImpl;
import com.claytablet.util.crypt.EncryptionTool;
import com.claytablet.util.crypt.KeyStoreType;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIEncryptionConfigManager implements Action {
	static List<String> keyTypes = new ArrayList<String>();
	
	protected static final Log LOG = LogFactory.getLog(UIAdminJobListManager.class.getName());
	private static final String ORIGINAL_PASSWORD = "--Original Password--";

	static {
		addKeyType(keyTypes, "AES", 128);
		addKeyType(keyTypes, "AES", 192);
		addKeyType(keyTypes, "AES", 256);
	}
	
	private enum ACTION {
		update,
		clear,
		generate
	}


	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;

	public void handleRequest(ICS ics) {
		String actionStr = ics.GetVar("action");
		ACTION action = null;
		
		if (!StringUtils.isEmpty(actionStr)) {
			try {
				action = ACTION.valueOf(actionStr);
			} catch (Exception e) {
				LOG.debug("Ignore unknown action: " + action, e);
			}
		}
		
		ManagerFactorySession gaSession = session.sudoGlobalAdminSession();
		EncryptionManager encm = gaSession.getEncryptionManager();
		
		try {
			boolean updated = true;
			if (action!=null) {
				switch (action) {
				case update:
					updated = updateConfig(encm);
					break;
				case clear:
					clearConfig(encm);
					break;
				case generate:
					if (generateKey(encm)) {
						updated = updateConfig(encm);
					} else {
						updated = false;
					}
					break;
				}
			}
			String keyStoreType = encm.getKeyStoreType();
			String keyStorePath = encm.getKeyStorePath();
			String keyStorePassword = StringUtils.isEmpty(encm.getKeyStorePassword())? "" : ORIGINAL_PASSWORD;
			String keyAlias = encm.getKeyAlias();
			String keyPassword = StringUtils.isEmpty(encm.getKeyPassword())? "" : ORIGINAL_PASSWORD;
			model.add("original_keystoreType", keyStoreType);
			model.add("original_keystorePath", keyStorePath);
			model.add("original_keystorePassword", keyStorePassword);
			model.add("original_keyAlias", keyAlias);
			model.add("original_keyPassword", keyPassword);
			if (encm instanceof EncryptionManagerImpl) {
				Exception error = ((EncryptionManagerImpl)encm).getError();
				if (error!=null) {
					model.add("load_error", "Failed to load encryption key " + keyAlias + " from key store " + 
								keyStorePath + " (" + String.valueOf(error) );
				}
			}
			
			// model.add("keystoreTypes", Arrays.asList(KeyStoreType.typeNames()));
			model.add("keystoreTypes", Collections.singletonList(KeyStoreType.JCEKS.typeName()));
			
			if (updated) {
				model.add("keystoreType", keyStoreType);
				model.add("keystorePath", keyStorePath);
				model.add("keystorePassword", keyStorePassword);
				model.add("keyAlias", keyAlias);
				model.add("keyPassword", keyPassword);
			}
			
			model.add("hasUnsavedChanges", !updated);
			model.add("keyTypes", keyTypes);
		} finally {
			gaSession.close();
		}
	}

	private boolean generateKey(EncryptionManager encm) {
		String keystoreType = ics.GetVar("keystoreType");
		String keyType = ics.GetVar("keyType");
		String keystorePath = ics.GetVar("keystorePath");
		String keystorePassword = ics.GetVar("keystorePassword");
		String keyAlias = ics.GetVar("keyAlias");
		String keyPassword = ics.GetVar("keyPassword");
		
		File file = new File(keystorePath);
		boolean keyStoreCreated = false;
		try {
			String actualKeystorePassword = ORIGINAL_PASSWORD.equals(keystorePassword)?
				encm.getKeyStorePassword() : keystorePassword;
				
			String actualKeyPassword = ORIGINAL_PASSWORD.equals(keyPassword)?
					encm.getKeyPassword() : (StringUtils.isEmpty(keyPassword)? keystorePassword : keyPassword);
			
			KeyStore keyStore;  
			 
			if (file.exists()) {
				keyStore = EncryptionTool.Factory.loadKeyStore(
						keystoreType, actualKeystorePassword, new FileInputStream(keystorePath));
			} else {
				keyStore = EncryptionTool.Factory.createKeyStore(
						keystoreType, actualKeystorePassword, new FileOutputStream(keystorePath));
				keyStoreCreated = true;
			}
			if (keyStore.isKeyEntry(keyAlias)) {
				throw new Exception("Key '" + keyAlias + "' already exists in keystore");
			} else {
				SecretKey secretKey = generateAndTestKey("AES", Integer.valueOf(keyType));
				KeyStore.SecretKeyEntry keyStoreEntry = new KeyStore.SecretKeyEntry(secretKey);
			    PasswordProtection pw = new PasswordProtection(actualKeyPassword.toCharArray());
			    keyStore.setEntry(keyAlias, keyStoreEntry, pw);    
			    keyStore.store(new FileOutputStream(file), actualKeystorePassword.toCharArray());				
			}
			return true;
		} catch (Exception e) {
			if (keyStoreCreated && file.exists()) {
				// delete created file
				file.delete();
			}
			model.add("keystoreType", keystoreType);
			model.add("keystorePath", keystorePath);
			model.add("keystorePassword", keystorePassword);
			model.add("keyAlias", keyAlias);
			model.add("keyPassword", keyPassword);
			model.add("keyType", keyType);
			model.add("error_message", "Failed to generate encryption key: " + String.valueOf(e));
			LOG.error("Failed to generate encryption key", e);
			return false;
		}
	}

	private boolean updateConfig(EncryptionManager encm) {
		String keystoreType = ics.GetVar("keystoreType");
		String keystorePath = ics.GetVar("keystorePath");
		String keystorePassword = ics.GetVar("keystorePassword");
		String keyAlias = ics.GetVar("keyAlias");
		String keyPassword = ics.GetVar("keyPassword");
		try {
			String actualKeystorePassword = ORIGINAL_PASSWORD.equals(keystorePassword)?
				encm.getKeyStorePassword() : keystorePassword;
				
			String actualKeyPassword = ORIGINAL_PASSWORD.equals(keyPassword)?
					encm.getKeyPassword() : (StringUtils.isEmpty(keyPassword)? keystorePassword : keyPassword);
					
			encm.setKeyConfig(keystoreType, keystorePath, actualKeystorePassword, keyAlias, actualKeyPassword);
			model.add("message", "Successfully updated encryption key configuration.");
			return true;
		} catch (Exception e) {
			model.add("keystoreType", keystoreType);
			model.add("keystorePath", keystorePath);
			model.add("keystorePassword", keystorePassword);
			model.add("keyAlias", keyAlias);
			model.add("keyPassword", keyPassword);
			model.add("error_message", "Failed to update encryption key configuration: " + String.valueOf(e));
			LOG.error("Failed to update encryption key configuration", e);
			return false;
		}
	}

	private void clearConfig(EncryptionManager encm) {
		try {
			encm.clearKeyConfig();
			model.add("message", "Encrytion key setting has been cleared.");
		} catch (Exception e) {
			model.add("errorMessage", "Failed to clear encrytion key setting: " + String.valueOf(e));
			LOG.error("Failed to clear encrytion key setting", e);
		}
	}

	static private void addKeyType(List<String> keyTypes, String algo, int length) {
		try {
			generateAndTestKey(algo, length);
        	String len = String.valueOf(length);
	        if (!keyTypes.contains(len)) {
				keyTypes.add(len);
	        }
		} catch (Exception e) {
			LOG.debug("Key type " + algo + "(" + length + ") not supported", e);
		}
	}

	private static SecretKey generateAndTestKey(String algo, int length) throws GeneralSecurityException {
		KeyGenerator keyGen = KeyGenerator.getInstance(algo);
		keyGen.init(length); 
		SecretKey key = keyGen.generateKey();
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		IvParameterSpec iv = generateNewIv();
		cipher.init(Cipher.ENCRYPT_MODE, key, iv);
		return key;
	}

	static private IvParameterSpec generateNewIv() {
        SecureRandom random = new SecureRandom();
        // The IV is 16 bytes log, because each AES block is 16 bytes long.
        // This is true regardless of the selected key size.
        byte[] buffer = new byte[16];
        random.nextBytes(buffer);
        return new IvParameterSpec(buffer);
    }
}
