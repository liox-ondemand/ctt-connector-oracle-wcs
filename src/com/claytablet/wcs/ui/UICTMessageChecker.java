/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.CTPlatformManager.ProcessingResult;
import com.claytablet.wcs.CTObjectFactory;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.MessageChecker;
import com.claytablet.wcs.system.impl.ContextManagerImpl;
import com.claytablet.wcs.system.impl.MessageCheckerImpl;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;

/**
 * @author Mike Field
 *
 */
public class UICTMessageChecker implements Action {

	private Log LOG = LogFactory.getLog(getClass().getName() + ":" + Thread.currentThread().getId());
	
	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactory factory;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		if (CTObjectFactory.hasInitError()) {
			LOG.warn("Skipping message checking as ClayTablet connector fails to start...");
			return;
		}
		
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession adminSession = factory.newGlobalAdminSession(ctx);
		try {
			LOG.debug("Checking messages using user: " + ics.GetSSVar("username"));
			TranslationEntityManager entityManager = adminSession.getTranslationEntityManager();
			MessageChecker messageChecker = new MessageCheckerImpl(ics, entityManager, adminSession);
			ProcessingResult pr = messageChecker.checkMessagesAndUpdateContent();
			model.add("result", pr);
		} finally {
			adminSession.close();
		}
	}
	
}
