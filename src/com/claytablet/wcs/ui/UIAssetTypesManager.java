package com.claytablet.wcs.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.wcs.system.AssetListManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

public class UIAssetTypesManager implements Action {
	protected static final Log LOG = LogFactory.getLog(UIAssetTypesManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest public Model model;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private AssetListManager assetListManager;
	@InjectForRequest private SiteManager siteManager;

	public void handleRequest(ICS ics) {
		boolean requiresDimensionEnabled = "true".equals(ics.GetVar("requireDimension"));
		String site = ics.GetVar("site");
		if (StringUtils.isEmpty(site)) {
			site = siteManager.getSiteNameById(ics.GetSSVar("pubid"));
		}
		List<String> assetTypes = new ArrayList<String>();
		
		Map<String, String> assetTypeMap = assetListManager.getEnabledAssetTypesInSite(site);

		for (String assetType : assetTypeMap.values()) {
			if (!requiresDimensionEnabled || localeManager.isDimsEnabledForType(assetType)) {
				assetTypes.add(assetType);
			}
		}
		Collections.sort(assetTypes);
		model.add("types", assetTypes);
	}
}
