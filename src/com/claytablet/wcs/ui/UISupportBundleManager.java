/**
 * 
 */
package com.claytablet.wcs.ui;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.gst.foundation.facade.assetapi.asset.TemplateAssetAccess;

/**
 * @author Mike Field
 *
 */
public class UISupportBundleManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UISupportBundleManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ContextManager contextManager;
	@InjectForRequest private TranslationEntityManager entityManager;
	@InjectForRequest private WorkflowManager workflowManager;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest private AssetManager assetManager;
	@InjectForRequest private TemplateAssetAccess assetDao;
	@InjectForRequest private ConfigManager configManager;
	@InjectForRequest private TranslationRequestManager translationRequestManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest private ManagerFactorySession session;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		String pagename = ics.GetVar("pagename");
		model.add("mypagename", pagename);
		
		if ("CustomElements/CT/WEM/CreateSupportBundlePost".equals(pagename)) {
			ManagerFactorySession adminSession = session.sudo();
			try {
				/* Post, create support bundle */
				String name = ics.GetVar("name");
				String note = ics.GetVar("note");
				Date date = new Date(); 
				DateFormat format = new SimpleDateFormat("_yyyyMMdd-hhmmss");
/*				
				ics.StreamHeader("Content-Disposition", "attachment; filename=" + name+format.format(date)+ ".ZIP");
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ZipOutputStream zos = new ZipOutputStream(bos);
				zos.putNextEntry(new ZipEntry("info.xml"));
				PrintWriter pw = new PrintWriter(zos);
				pw.println("Collected at =" + date);
				pw.println("Note=" + note);
				pw.flush();
				zos.closeEntry();
				zos.close();
				byte[] bytes = bos.toByteArray();
*/
				ics.StreamHeader("Content-Disposition", "attachment; filename=CT_OWCS_ConfigExport" +format.format(date)+ ".xml");
				String exportXml = adminSession.getConfigurationManager().exportConfiguration(false);
				ics.StreamText(exportXml);
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				adminSession.close();
			}
		}

	}

}
