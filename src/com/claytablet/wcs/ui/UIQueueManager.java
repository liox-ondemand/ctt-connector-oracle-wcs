/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntity;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationJob;
import com.claytablet.client.producer.TranslationJobFilter;
import com.claytablet.client.producer.TranslationRequest;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.impl.jpa.JpaTranslationEntityManager;
import com.claytablet.wcs.system.ConfigManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.UserManager;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.UserManagerImpl;
import com.claytablet.wcs.ui.UIUtils.SiteInfo;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * @author Mike Field
 *
 */
public class UIQueueManager implements Action {

	protected static final Log LOG = LogFactory.getLog(UIQueueManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactorySession session;
	
	private TranslationEntityManager entityManager;
	private ConfigurationManager configManager;
	@InjectForRequest private SiteManager siteManager;
	@InjectForRequest public Model model;
	@InjectForRequest private ConfigManager cm;

	private List<TranslationRequest> allQueued;

	public void handleRequest(ICS ics) {
		
		String pagename = ics.GetVar("pagename");
		String app = ics.GetVar("app");
		ManagerFactorySession sudoSession = null;
		String siteId = ics.GetVar("selectSite");
		
		try {
			if ("WEM".equals(app)) {
				UserContextImpl ctx = new UserContextImpl(ics);
	
				if (StringUtils.isEmpty(siteId)) {
					siteId = ctx.getSiteId();
				}
				model.add("cursiteid", siteId);
				
				if (!StringUtils.isEmpty(siteId)) {
					session = session.sudo(siteId);
				} else {
					session = session.sudoGlobalAdminSession();
				}
				sudoSession = session;
				
				Map<String, String> allSites = siteManager.getAllSites();
				List<SiteInfo> sites = new ArrayList<SiteInfo>();
				
				for (Map.Entry<String, String> entry : allSites.entrySet()) {
					if (ctx.getUserProfile().isAdmin(entry.getKey())) {
						sites.add(new SiteInfo(entry.getKey(), entry.getValue()));
					}
				}
				Collections.sort(sites);
				model.add("sites", sites);
			}
			
			entityManager = (TranslationEntityManager)session.getTranslationEntityManager();
			configManager = session.getConfigurationManager();
			allQueued = entityManager.getAllQueuedTranslationRequests();
			
			if ("CustomElements/CT/QueuePost".equals(pagename)) {
				doQueuePost();
			} else {
				doQueueFront();
			}
		} finally {
			if (sudoSession!=null) {
				sudoSession.close();
			}
		}
	}

	private void doQueueFront() {
		LOG.info("Loading QueueFront");
		model.add("translationRequests", allQueued);
		model.add("jobs", findOwnJob());
		model.add("teams", entityManager.getAllTeamProfiles());
		model.add("message", ics.GetVar("message"));
		model.add("showheaderfooter", ics.GetVar("showheaderfooter"));
		model.add("showteam", configManager.get(ProducerCommonConfigs.CT_ENABLE_TEAM_MANAGEMENT));
		model.add("pagesize", cm.getMaxItemsPerPage());
	}

	private List<TranslationJob> findOwnJob() {
		List<TranslationJob> jobs = entityManager.findTranslationJobs(
				TranslationJobFilter.factory.filter("status='CREATED'"));
		if (session.isSudoSession()) {
			List<TranslationJob> ownJobs = new ArrayList<TranslationJob>();
			for (TranslationJob job : jobs) {
				if (job.getCreatedUser().equals(session.getUserProfile().getId())) {
					ownJobs.add(job);
				}
			}
			return ownJobs;
		} else {
			return jobs;
		}
	}
	
	// If jobid was specified, set job id
	private TranslationJob _getJobIfAny(String jobid) {
		TranslationJob job = null;
		if (Utilities.goodString(jobid) && !"queue".equalsIgnoreCase(jobid)) {
			if ("newjob".equalsIgnoreCase(jobid)) {
				job = entityManager.save(entityManager.createTranslationJob(ics.GetVar("newjobname")));
			} else {
				job = entityManager.getTranslationJob(jobid);
			}
		}	
		return job;
	}
	
	private void doQueuePost() {
		LOG.info("Loading QueuePost");
		String formpostval_trids = ics.GetVar("reqids");
		String formpostval_jobid = ics.GetVar("jobid");
		String removetrq = ics.GetVar("removetrq");
		String[] trids = null;
		if(formpostval_trids!= null)
		{
			trids = formpostval_trids.split(";");
		}
		else
		{
			model.add("message", "There were no request IDs specified to perform an action");
		}
		
		String jobname = null;
		FTValList args = new FTValList();
		
		if(trids != null)
		{
	      	args.removeAll();
	    	args.setValString("showheaderfooter", "true");
	    	args.setValString("app", ics.GetVar("app"));
			if (trids.length>0) {
				int addedCount = 0;
				int skippedCount = 0;
				
				TranslationJob job = _getJobIfAny(formpostval_jobid);
		    	args.setValString("jobid", job.getId());
		    	args.setValString("siteId", job.getSiteId());
				for (String trid : trids) {
					try {
						TranslationRequest tr = entityManager.getTranslationRequest(trid);
						if (tr.getSiteId().equals(job.getSiteId())) {
							tr.setTranslationJob(job);
							tr = entityManager.takeOwnerShipAndSave(tr);
							addedCount++;
							jobname = job.getName();
						} else {
							skippedCount++;
						}
					} catch (Exception e) {
						LOG.warn("Cannot add translation request '" + trid + "' into " + job, e);
						skippedCount++;
					}
				}	
				if (skippedCount==0) {
			      	String msg = "Successfully added " + addedCount + 
			      			" translation requests to the job " + jobname;
					args.setValString("message", msg);
			      	model.add("message", msg);
				} else {
			      	String msg = "Added " + addedCount + " translation requests to the job " + 
			      			jobname + ", skipped " + skippedCount + " due to site mismatch";
			      	args.setValString("message", msg);
			      	model.add("message", msg);
				}
			}
			
	      	ics.CallElement("CustomElements/CT/JobFront", args);	
		}
	}
	
}
