package com.claytablet.wcs.ui;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.claytablet.client.producer.ManagerFactory.DbType;

public interface UIFilter extends UIFilterCondition {
	List<? extends FieldCondition> getFieldConditions(); 
	List<? extends Field> getFields();
	List<? extends Field> getAvailableFields();
	
	public interface Field {
		String getDisplayName();
		String getColName();
		List<Operator<?>> getOperators();
	}

	public static interface FieldCondition extends UIFilterCondition {
		Field getField();
		List<? extends SingleCondition<?>> getSingleConditions();
	}
	
	public static interface SingleCondition<T> {
		boolean isNot();
		Operator<T> getOperator();
		List<? extends OperatorModifier> getOperatorModifiers();
		T getOperand();
		List<T> getOperands();
		List<String> getOperandStrings();
		String getWhereClause(String colName);

		public static class NOT {
			public static <T> SingleCondition<T> not(final SingleCondition<T> condition) {
				return new SingleCondition<T>() {
					@Override public String getWhereClause(String colName) { return "NOT (" + condition.getWhereClause(colName) + ")"; }
					@Override public boolean isNot() { return !condition.isNot(); }
					@Override public Operator<T> getOperator() { return condition.getOperator(); };
					@Override public List<? extends OperatorModifier> getOperatorModifiers() { return condition.getOperatorModifiers(); }
					@Override public T getOperand() { return condition.getOperand(); }
					@Override public List<T> getOperands() { return condition.getOperands(); }
					@Override public List<String> getOperandStrings() { return condition.getOperandStrings(); }
				} ;
			}
		}
	}

	public static interface OperatorValidation {
		String getValidationType();
		String getValidationErrorMessage();
		String getValueFormat();
	}
	
	public static enum OperatorValidator implements OperatorValidation {
		string(null, null),
		number("The value is not a number: ", null),
		date("The value is not a date: ", "yyyy-mm-dd");
		
		private String errorMessage;
		private String valueFormat;

		private OperatorValidator(String errorMessage, String valueFormat) {
			this.errorMessage = errorMessage;
			this.valueFormat = valueFormat;
		}

		@Override
		public String getValidationType() {
			return toString();
		}

		@Override
		public String getValidationErrorMessage() {
			return errorMessage;
		}

		@Override
		public String getValueFormat() {
			return valueFormat;
		}
	}
	
	public static interface OperatorMetadata extends OperatorValidation {
		String getDisplayName();
		List<String> getDisplayNameList();
		List<? extends OperatorModifier> getModifiers();
		boolean isOperandsCountLimited();
		int getOperandsCount();
		
		public static class Impl implements OperatorMetadata {
			public static Impl createMetadata() {
				return new Impl();
			}
			
			private static int UNLIMIT_OPERANDS_COUNT = -1;
			
			private List<String> displayNames = Collections.emptyList();
			private List<? extends OperatorModifier> modifiers = Collections.emptyList();
			private int operandsCount = UNLIMIT_OPERANDS_COUNT;

			private String validationType = null;
			private String validationErrorMessage = null;
			private String valueFormat = null;
			
			
			public Impl() {
			}

			@Override
			public String getDisplayName() { return displayNames.get(0); }

			@Override
			public List<String> getDisplayNameList() { return displayNames; }

			@Override
			public List<? extends OperatorModifier> getModifiers() { return modifiers; }

			@Override
			public boolean isOperandsCountLimited() { return operandsCount != UNLIMIT_OPERANDS_COUNT; }

			@Override
			public int getOperandsCount() { return operandsCount; }

			@Override
			public String getValidationType() { return validationType; }

			@Override
			public String getValidationErrorMessage() {  return validationErrorMessage; }

			@Override
			public String getValueFormat() { return valueFormat; }
			
			public Impl setDisplayName(String displayName) {
				this.displayNames = Collections.singletonList(displayName);
				return this;
			}

			public Impl setDisplayNameList(List<String> displayNames) {
				this.displayNames = new ArrayList<String>(displayNames);
				this.operandsCount = displayNames.size();
				return this;
			}

			public Impl setDisplayNameList(String[] displayNames) {
				return setDisplayNameList(Arrays.asList(displayNames));
			}

			public Impl setModifiers(List<OperatorModifier> modifiers) {
				this.modifiers = new ArrayList<OperatorModifier>(modifiers);
				return this;
			}
			
			public Impl setOperandsCount(int operandsCount) {
				this.operandsCount = operandsCount;
				return this;
			}

			public Impl setUnlimitedOperandsCount() {
				this.operandsCount = UNLIMIT_OPERANDS_COUNT;
				return this;
			}
			
			public Impl setValidator(OperatorValidator validator) {
				validationType = validator.getValidationType();
				validationErrorMessage = validator.getValidationErrorMessage();
				valueFormat = validator.getValueFormat();
				return this;
			}
		}
	}
	
	public static interface Operator<W> {
		OperatorMetadata getMetadata();
		String getWhereClause(String colName, List<W> operands, 
				List<? extends OperatorModifier> modifiers);
		W parseOperand(String str);
		String operandToString(W operand);
		List<W> getAcceptedOperands();

		public static class Util {
			@SuppressWarnings("unchecked")
			public static <T> String operandToString(Operator<T> operator, Object operand) {
				return operator.operandToString((T)operand);
			}
			
			public static <X> Operator<X> addModifier(final Operator<X> operator, 
					final OperatorModifier modifier) {
				return new Operator<X>() {
					@Override public OperatorMetadata getMetadata() {  
						return new OperatorMetadata() {
								@Override public String getValidationType() {
									return operator.getMetadata().getValidationType();
								}
								@Override public String getValidationErrorMessage() {
									return operator.getMetadata().getValidationErrorMessage();
								}
								@Override public String getValueFormat() {
									return operator.getMetadata().getValueFormat();
								}
								@Override public String getDisplayName() {
									return operator.getMetadata().getDisplayName();
								}
								@Override public List<String> getDisplayNameList() {
									return operator.getMetadata().getDisplayNameList();
								}
								@Override public List<? extends OperatorModifier> getModifiers() {
									List<OperatorModifier> modifiers = new ArrayList<OperatorModifier>( 
											operator.getMetadata().getModifiers());
									modifiers.add(modifier);
									return modifiers;
								}
								@Override public boolean isOperandsCountLimited() {
									return operator.getMetadata().isOperandsCountLimited();
								}
								@Override public int getOperandsCount() {
									return operator.getMetadata().getOperandsCount();
								}
							};
					}
					@Override public String getWhereClause(String colName,
							List<X> operands, List<? extends OperatorModifier> modifiers) {
						return operator.getWhereClause(colName, operands, modifiers);
					}
					@Override public X parseOperand(String str) { return operator.parseOperand(str); }
					@Override public String operandToString(X operand) { return operator.operandToString(operand); }
					@Override public List<X> getAcceptedOperands() { return operator.getAcceptedOperands(); }
				};
			}
			public static <X> Operator<X> limitOperands(
					final Operator<X> operator, final List<X> operands) {
				return new Operator<X>() {
					@Override public OperatorMetadata getMetadata() { return operator.getMetadata(); };
					@Override public String getWhereClause(String colName,
							List<X> operands, List<? extends OperatorModifier> modifiers) {
						return operator.getWhereClause(colName, operands, modifiers);
					}
					@Override public X parseOperand(String str) { return operator.parseOperand(str); }
					@Override public String operandToString(X operand) { return operator.operandToString(operand); }
					@Override public List<X> getAcceptedOperands() { return operands; }
				};
			}
		}
	}
	
	public static interface OperatorModifier {
		String getDisplayName();
	}
	
	public enum StringOperators implements Operator<String> {
		contains("contains", 1),
		matches("matches", 1),
		likes("likes", 1),
		equals("="),
		;

		private OperatorMetadata.Impl metadata;

		private StringOperators(String displayName, int operandsCount) {
			this(displayName);
			metadata.setOperandsCount(operandsCount);
		}

		private StringOperators(String displayName) {
			metadata = OperatorMetadata.Impl.createMetadata();
			metadata.setDisplayName(displayName).setValidator(OperatorValidator.string).
					setModifiers(Arrays.<OperatorModifier>asList(StringOperationModifier.values()));
		}

		@Override
		public OperatorMetadata getMetadata() {
			return metadata;
		}
		
		@Override
		public String getWhereClause(String colName, List<String> operands,
				List<? extends OperatorModifier> modifier) {
			String operand;
			switch (this) {
			case matches:
				operand = escapeSingleQuote(operands.get(0)); 
				if (!modifier.contains(StringOperationModifier.matchcase)) {
					return "UPPER(" + colName + ")=UPPER('" + operand + "')";
				} else {
					return colName + "='" + operand + "'";
				}
			case contains:
				operand = escapeLikeOperand(operands.get(0));
				if (!modifier.contains(StringOperationModifier.matchcase)) {
					return "UPPER(" + colName + ") like UPPER('%" + operand + "%') ESCAPE '\\'";
				} else {
					return colName + " like '%" + operand + "%' ESCAPE '\\'";
				}
			case likes:
				operand = operands.get(0);
				if (!modifier.contains(StringOperationModifier.matchcase)) {
					return "UPPER(" + colName + ") like UPPER('" + operand + "') ESCAPE '\\'";
				} else {
					return colName + " like '" + operand + "' ESCAPE '\\'";
				}
			case equals:
				StringBuffer condition = new StringBuffer();
				for (String opd : operands) {
					String operandEscaped = escapeSingleQuote(opd);
					if (condition.length()>0) {
						condition.append(" OR ");
					}
					if (!modifier.contains(StringOperationModifier.matchcase)) {
						condition.append("UPPER(").append(colName).append(")=UPPER('").
								append(operandEscaped).append("')");
					} else {
						condition.append(colName + "='" +operandEscaped + "'");
					}
				}
				return condition.toString();
			default:
				throw new IllegalArgumentException("Unrecognized StringOperator: " + this);
			}
		}
		
		private String escapeSingleQuote(String str) {
			return str.replace("'", "''");
		}

		static public String escapeLikeOperand(String str) {
			return str.replace("\\", "\\\\").replace("%", "\\%").replace("?", "\\?").
					replace("_", "\\_").replace("'", "\\'");
		}
		
		@Override
		public String parseOperand(String str) {
			return str;
		}
		
		@Override
		public String operandToString(String operand) {
			return operand;
		}

		@Override
		public List<String> getAcceptedOperands() {
			return Collections.emptyList();
		}
	}

	public enum StringOperationModifier implements OperatorModifier {
		matchcase("match case"),
		;

		private String displayName;
		
		StringOperationModifier(String displayName) {
			this.displayName = displayName;
		}
		@Override
		public String getDisplayName() {
			return displayName;
		}
	}
	
	public enum NumberOperators implements Operator<Number> {
		eq("="),
		gt(">"),
		lt("<"),
		bt("between", "and"),
		;
		
		private OperatorMetadata metadata;

		private NumberOperators(String ... displayNames) {
			metadata = OperatorMetadata.Impl.createMetadata().setValidator(OperatorValidator.number).
					setDisplayNameList(displayNames); 
		}

		@Override
		public OperatorMetadata getMetadata() {
			return metadata;
		}
		
		@Override
		public String getWhereClause(String colName, List<Number> operands,
				List<? extends OperatorModifier> modifier) {
			switch (this) {
			case eq:
				return colName + "=" + operandToString(operands.get(0));
			
			case gt:
				return colName + ">" + operandToString(operands.get(0));
				
			case lt:
				return colName + "<" + operandToString(operands.get(0));
				
			case bt:
				return colName + ">=" + operandToString(operands.get(0)) + 
						" AND " + colName + "<=" + operandToString(operands.get(1));
			default:
				throw new IllegalArgumentException("Unrecognized NumberOperator: " + this);
			}
		}

		@Override
		public Number parseOperand(String str) {
			try {
				return NumberFormat.getInstance().parse(str);
			} catch (ParseException e) {
				throw new RuntimeException("Cannot parse '" + str + "' as a number");
			}
		}

		@Override
		public String operandToString(Number operand) {
			return NumberFormat.getInstance().format(operand);
		}
		
		@Override
		public List<Number> getAcceptedOperands() {
			return Collections.emptyList();
		}
	}

	public enum DateOperators implements Operator<Date> {
		on ("on"),
		before ("before"),
		after ("after"),
		bt ("between", "and"),
		;

		private static final String DATE_FORMAT = "yyyy-MM-dd";
		
		private OperatorMetadata metadata;

		private DateOperators(String ... displayNames) {
			metadata = OperatorMetadata.Impl.createMetadata().
					setDisplayNameList(displayNames).setValidator(OperatorValidator.date); 
		}

		@Override
		public OperatorMetadata getMetadata() {
			return metadata;
		}
		
		@Override
		public String getWhereClause(String colName, List<Date> operands,
				List<? extends OperatorModifier> modifier) {
			String colToChar = "to_char(" + colName + ", '" + DATE_FORMAT;
			switch (this) {
			case on:
				return colToChar + "')='" + operandToString(operands.get(0)) + "'";
			
			case before:
				return colToChar + "')<'" + operandToString(operands.get(0)) + "'";
				
			case after:
				return colToChar + "')>'" + operandToString(operands.get(0)) + "'";
				
			case bt:
				return colToChar + "')>='" + operandToString(operands.get(0)) + "'" +
					" AND " + colToChar + "')<='" + operandToString(operands.get(1)) + "'";
			default:
				throw new IllegalArgumentException("Unrecognized NumberOperator: " + this);
			}
		}

		@Override
		public Date parseOperand(String str) {
			try {
				return getDateFormat().parse(str);
			} catch (ParseException e) {
				try {
					return getAltDateFormat().parse(str);
				} catch (ParseException e2) {
					throw new RuntimeException("Cannot parse '" + str + "' as a date");
				}
			}
		}

		private SimpleDateFormat getDateFormat() {
			return new SimpleDateFormat(DATE_FORMAT);
		}

		private SimpleDateFormat getAltDateFormat() {
			return new SimpleDateFormat("MM/dd/yyyy");
		}

		@Override
		public String operandToString(Date operand) {
			return getDateFormat().format(operand);
		}
		
		@Override
		public List<Date> getAcceptedOperands() {
			return Collections.emptyList();
		}
	}
	
	public static class ChoiceOperator<T> implements Operator<T>{
		private Map<String, T> operands;
		private Operator<T> baseOperator;

		public ChoiceOperator(Map<String, T> operands, Operator<T> baseOperator) {
			this.operands = operands;
			this.baseOperator = baseOperator;
		}
		
		@Override
		public OperatorMetadata getMetadata() {
			return baseOperator.getMetadata();
		}

		@Override
		public String getWhereClause(String colName, List<T> operands,
				List<? extends OperatorModifier> modifiers) {
			return baseOperator.getWhereClause(colName, operands, modifiers);
		}

		@Override
		public T parseOperand(String str) {
			return operands.get(str);
		}

		@Override
		public String operandToString(T operand) {
			for (Map.Entry<String, T> entry : operands.entrySet()) {
				if (operand.equals(entry.getValue())) {
					return entry.getKey();
				}
			}
			return null;
		}

		@Override
		public List<T> getAcceptedOperands() {
			return new ArrayList<T>(operands.values());
		}
	}
}
