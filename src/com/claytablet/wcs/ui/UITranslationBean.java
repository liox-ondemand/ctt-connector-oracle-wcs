package com.claytablet.wcs.ui;

import com.fatwire.assetapi.data.AssetId;
import com.fatwire.services.ui.beans.UIAssetBean;


public class UITranslationBean extends UIAssetBean {
	
	private AssetId assetId;
	private boolean success = false;
	private String detail;
	
	public void setAsset(AssetId assetId){
		this.assetId = assetId;
	}
	
	public AssetId getAsset(){
		return this.assetId;
	}
	
	public boolean isSuccess(){
		return this.success;
	}
	
	public void setSuccess(boolean success){
		this.success = success;
	}
	
	public String getDetail(){
		return this.detail;
	}
	
	public void setDetail(String detail){
		this.detail = detail;
	}
	

}
