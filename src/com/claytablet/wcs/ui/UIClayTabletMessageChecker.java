/**
 * 
 */
package com.claytablet.wcs.ui;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.CTPlatformException;
import com.claytablet.client.producer.CTPlatformManager.ProcessingResult;
import com.claytablet.client.producer.CTPlatformManager;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProgressReporter;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.TranslationProvider;
import com.claytablet.wcs.CTObjectFactory;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.impl.ConfigManagerImpl;
import com.claytablet.wcs.system.impl.ContentManagerImpl;
import com.claytablet.wcs.system.impl.JobTimeoutProgressReporter;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.JobTimeoutProgressReporter.Breaker;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;

/**
 * @author Mike Field
 *
 */
public class UIClayTabletMessageChecker implements Action {

	protected static final Log LOG = LogFactory.getLog(UIClayTabletMessageChecker.class.getName());
	@InjectForRequest private ICS ics;
	@InjectForRequest private ManagerFactory managerFactory;
	private ManagerFactorySession session;
	@InjectForRequest public Model model;
	
	public void handleRequest(ICS ics) {
		if (CTObjectFactory.hasInitError()) {
			LOG.warn("Skipping message checking as ClayTablet connector fails to start...");
			return;
		}
		session = managerFactory.newSession(new UserContextImpl(ics));
		try {
			String pagename = ics.GetVar("pagename");
			if ("CustomElements/CT/CheckMessages".equals(pagename)) {
				ProcessingResult pr = _checkMessagesAndUpdateContent();
				model.add("result", pr);
			}
		} finally {
			session.close();
		}
	}
	
	private String _getBatchUser() {
        return ics.GetProperty("xcelerate.batchuser", "futuretense_xcel.ini", true);
 	}
	
	private String _getBatchPass() {
        String encryptedpass = ics.GetProperty("xcelerate.batchpass", "futuretense_xcel.ini", true);
        String batchpass = Utilities.decryptString(encryptedpass);
        return batchpass;
	}	
	
	private ProcessingResult _checkMessagesAndUpdateContent() {
		ManagerFactorySession adminSession = session.sudoGlobalAdminSession();
		
		try {
			List<TranslationProvider> vendors = 
					adminSession.getTranslationEntityManager().getAllTranslationProviders();
			int breakInterval = session.getConfigurationManager().get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_BREAK_INTERVAL);
			if (Breaker.CTMessagePoller.readyToContinue(1000L * breakInterval)) {
				int breakTimeOut = session.getConfigurationManager().get(ConfigManagerImpl.WCS_BACKGROUND_PROCESS_TIMEOUT);
				try {
					JobTimeoutProgressReporter progressReporter = new JobTimeoutProgressReporter(
							Breaker.CTMessagePoller, 1000L * breakTimeOut);
					CTPlatformManager pm = adminSession.getCTPlatformManager();
					pm.setProgressReporter(progressReporter);
					ProcessingResult pr = adminSession.getCTPlatformManager( ).checkMessages(vendors,
							new ContentManagerImpl(ics, ics.GetSSVar("username"), session, SessionFactory.getSession()));
					if (progressReporter.shouldContinue()) {
						LOG.debug("Resetting breaker " + Breaker.CTMessagePoller);
						Breaker.CTMessagePoller.reset();
					}
					return pr;
				} catch (CTPlatformException e) {
					LOG.error("Unable to check for messages from Clay Tablet platform with exception: " + e.toString());
					return null;
				}
			} else {
				LOG.info("Skipping this poll period as breaking period from previous poll is not over yet...");
				return ProcessingResult.empty;
			}
		} finally {
			adminSession.close();
		}
	}
}
