package com.claytablet.wcs.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Util.ftMessage;

import com.claytablet.client.producer.UserContext;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.queue.DatabaseJobQueue;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.facade.sql.table.TableColumn;
import com.fatwire.gst.foundation.facade.sql.table.TableCreator;
import com.fatwire.gst.foundation.facade.sql.table.TableDef;

public class UIDbQueueHelper implements Action {

	protected static final Log LOG = LogFactory.getLog(UIDbQueueHelper.class.getName());
	
	@Override
	public void handleRequest(ICS arg0) {
		UserContext ctx = new UserContextImpl(arg0);
		if (!ctx.getUserProfile().isGlobalAdmin()) {
			throw new UnsupportedOperationException("Only administrator can perform this operation.");
		} else {
			String pagename = arg0.GetVar("pagename");
			if ("CustomElements/CT/WEM/DeveloperToolPost".equals(pagename)) {
				//doDeveloperToolPost();
			} else {
				//doDeveloperToolFront();
			}
		}
	}
	
	private boolean dropCtQueueTable(ICS ics){
		// probably need better exception handling than this
		try{
			new com.fatwire.gst.foundation.facade.sql.table.TableCreator(ics).delteTable("CT_queue");	
		}
		catch(Exception e)
		{
			LOG.error(e.getMessage() + e.getStackTrace());
			return false;
		}
		
		return true;
	}
	
	private void createCTQueueTable(ICS ics) {
		LOG.info("Creating CT Queue Table...");
        final TableDef def = new TableDef("CT_queue", "", ftMessage.objecttbl);
	        
        def.addColumn(new TableColumn(DatabaseJobQueue.ID, TableColumn.Type.ccbigint, true).setNullable(false));
        def.addColumn(new TableColumn(DatabaseJobQueue.ACTION, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.Q_ENTRY, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.Q_ENTRY_CLASS, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.STATUS, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.INSERTION_DATE, TableColumn.Type.ccdatetime).setNullable(true));

        new com.fatwire.gst.foundation.facade.sql.table.TableCreator(ics).createTable(def);
        LOG.info("CT Queue table creation completed.");
    }

}
