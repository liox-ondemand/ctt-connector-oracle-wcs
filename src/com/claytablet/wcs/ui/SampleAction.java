package com.claytablet.wcs.ui;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.wcs.system.LocaleManager;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;

/**
 * Sample action that demonstrates how to work with the new AbstractAction class.
 * @author Tony Field
 */
public class SampleAction extends AbstractAction {

	@InjectForRequest private ICS ics;
	@InjectForRequest private LocaleManager localeManager;
	@InjectForRequest public Model model;
	
	public void handleRequest() {
	
		// do something basic
		String pagename = ics.GetVar("pagename");
		model.add("mypagename", pagename);
		
		// do something awesome
		localeManager.getAllWCSLocalesAndIds();
		
		// nothing more to do
	}
}
