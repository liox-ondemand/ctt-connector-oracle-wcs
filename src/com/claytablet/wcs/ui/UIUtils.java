/**
 * 
 */
package com.claytablet.wcs.ui;


import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringEscapeUtils;

import com.fatwire.gst.foundation.controller.action.Model;
/**
 * @author Mike Field
 *
 */
public class UIUtils {

	
	/**
	 * Iterates over map and returns the key for the given searchvalue. Returns the first key found
	 * for the given value (in case there is more than one entry with that value).
	 * @param map Map to search on. Must be Map<String, String>
	 * @param searchstring String to search for
	 * @return String key for given search value. Null if not found.
	 */
	public static String getKeyForThisValue(Map<String, String> map, String searchstring) {
		String mykey = null;
		Iterator<Entry<String, String>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<String,String> pairs = (Map.Entry<String,String>)iterator.next();
		    String value =  pairs.getValue();
		    String key = pairs.getKey();
		    if (searchstring.equalsIgnoreCase(value)) {
		    	mykey = key;
		    	break;
		    }
		}
		return mykey;
	}
	
	public static Object getValueForThisKey(Model map, String searchstring) {
		Object myvalue = null;
		Iterator<Entry<String, Object>> iterator = map.entries().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<String,Object> pairs = (Map.Entry<String,Object>)iterator.next();
		    Object value =  pairs.getValue();
		    String key = pairs.getKey();
		    if (searchstring.equalsIgnoreCase(key)) {
		    	myvalue = value;
		    	break;
		    }
		}
		return myvalue;
	}
	
	public static String escapeHtmlWithNewLine(String str) {
		if (str!=null) {
			String html = StringEscapeUtils.escapeHtml(str);
			
			html = html.replaceAll("\\r\\n", "<br/>").replaceAll("\\n", "<br/>").replaceAll("\\r", "<br/>");
			return html;
		} else {
			return null;
		}
	}

	public static class SiteInfo implements Comparable<SiteInfo>{
		private String name;
		private String id;
		
		public SiteInfo(String id, String name) {
			super();
			this.name = name;
			this.id = id;
		}
		
		public String getName() { return name; }
		public String getId() { return id; }

		@Override
		public int compareTo(SiteInfo s) {
			return name.compareTo(s.name);
		}
	}
}
