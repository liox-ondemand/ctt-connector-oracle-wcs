package com.claytablet.wcs.ui;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Interfaces.Utilities;

import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserProfile;
import com.claytablet.wcs.CTObjectFactory;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.impl.SiteManagerImpl;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.UserManagerImpl;
import com.claytablet.wcs.system.impl.UserProfileImpl;
import com.fatwire.gst.foundation.controller.action.Action;
import com.fatwire.gst.foundation.controller.action.Model;
import com.fatwire.gst.foundation.controller.annotation.InjectForRequest;
import com.fatwire.services.ServicesManager;
import com.fatwire.services.SiteService;
import com.fatwire.services.exception.ServiceException;
import com.fatwire.system.Session;
import com.fatwire.system.SessionFactory;
import com.fatwire.ui.util.GenericUtil;
import com.openmarket.xcelerate.site.Publication;

public class UIAdminAuthManager implements Action {
	protected static final Log LOG = LogFactory.getLog(UIAdminAuthManager.class.getName());

	@InjectForRequest private ICS ics;
	@InjectForRequest public Model model;
	


	public void handleRequest(ICS ics) {
		
		if (!Utilities.goodString( ics.GetSSVar("pubid") )) {
			try{
				String user = GenericUtil.getLoggedInUserName(ics);
			    Session ses = SessionFactory.getSession();
			    ServicesManager servicesManager = (ServicesManager) ses.getManager(ServicesManager.class.getName());
			    SiteService siteService = servicesManager.getSiteService();
			    Map<String, String> prefs = siteService.getUserPreferences(user, null);
			    String lastSite = prefs.get("WEMUI:lastSite");
			    if (StringUtils.isNotEmpty(lastSite)){
			        Publication publication = Publication.Load(ics, "name", lastSite);
			        String pubid = publication.Get("id");
			        FTValList args1 = new FTValList();
			        ics.SetSSVar("pubid", pubid);
			        LOG.info("Setting pubid: "+pubid);
			        ics.CallElement("OpenMarket/Xcelerate/Actions/Security/SetPublicationName", args1);
			    }
			} catch (Throwable t) {
				LOG.warn("Note: Error attempting to set pubid manually - currently only supported on 11.1.1.8.0");
				LOG.debug("Failed to set pubid manually", t);
			}
		}
		
		if (CTObjectFactory.hasInitError()) { 
			model.add("initError", true);
			model.add("initErrorDetail", CTObjectFactory.getInitError());
			model.add("authorized", "false");
			model.add("siteAuthorized", "false");
			if ("true".equalsIgnoreCase(String.valueOf(ics.GetVar("retry")))) {
				model.add("retry", "true");
				CTObjectFactory.resetInitError();
			}
		} else {
			UserProfileImpl user = new UserContextImpl(ics).getUserProfile();
			model.add("authorized", user.isGlobalAdmin());
			
			SiteManagerImpl siteManager = new SiteManagerImpl(ics);
			Collection<String> sites = siteManager.getAllSites().values();
			List<String> authorizedSite;
			if (user.isGlobalAdmin()) {
				model.add("siteAuthorized", true);
				authorizedSite = new ArrayList<String>(sites);
			} else {
				UserManagerImpl um = new UserManagerImpl(ics);
				authorizedSite = new ArrayList<String>();
				for (String site : sites) {
					if (um.hasCTSiteAdminRole(user.getName(), site)) {
						authorizedSite.add(site);
					}
				}
				model.add("siteAuthorized", !authorizedSite.isEmpty());
			}
			Collections.sort(authorizedSite);
			model.add("authorizedSites", authorizedSite);
		}
	}
}
