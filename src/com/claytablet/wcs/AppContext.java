/**
 * 
 */
package com.claytablet.wcs;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.fatwire.gst.foundation.controller.action.ActionLocator;
import com.fatwire.gst.foundation.controller.action.Factory;
import com.fatwire.gst.foundation.controller.action.Injector;
import com.fatwire.gst.foundation.controller.action.support.ClassActionLocator;
import com.fatwire.gst.foundation.controller.action.support.DefaultWebAppContext;
import com.fatwire.gst.foundation.controller.action.support.RenderPageActionLocator;

/**
 * @author Mike Field
 *
 */
public class AppContext extends DefaultWebAppContext {

	protected static final Log LOG = LogFactory.getLog(AppContext.class.getName());

	/**
	 * @param context
	 */
	public AppContext(final ServletContext context) {
		super(context);
	}

	public AppContext(final ServletContext context, final com.fatwire.gst.foundation.controller.AppContext parent) {
		super(context, parent);
	}
	
	public Factory getFactory(final ICS ics) {
		return (Factory)new CTObjectFactory(ics);
	}
	
	public ActionLocator createActionLocator() {
        final Injector injector = createInjector();
        final ActionLocator root = new RenderPageActionLocator(injector);
        final ClassActionLocator cal = new ClassActionLocator(root, injector);
        return cal;
    }
}
