package com.claytablet.wcs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.claytablet.client.producer.ManagerFactorySession;


public class CTSessionFilter implements Filter {

	protected static final Log LOG = LogFactory.getLog(CTSessionFilter.class.getName());
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		
		List<ManagerFactorySession> ctsessions = new ArrayList<ManagerFactorySession>();
		request.setAttribute("ctsessionholder", ctsessions);
		
		filterChain.doFilter(request, response);
		
		for (ManagerFactorySession session : ctsessions) {
			if (session.isOpen()) {
				session.close();
			}
		}
		
		
	}
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}


}
