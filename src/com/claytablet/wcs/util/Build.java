package com.claytablet.wcs.util;
import COM.FutureTense.Interfaces.ICS;
import org.apache.commons.logging.LogFactory;
public final class Build {
  private static boolean bPrinted = false;
  private static String name = "Clay Tablet WebCenter Sites Java Library";
  private static String version = "SNAPSHOT Build Date: Fri 12/01/2017 14:22:34";
  public static synchronized final void printBuildDate() {
      if (!bPrinted) {
          LogFactory.getLog(Build.class).info(name + " Initialized.  " + version);
          bPrinted = true;
      }
  }
  public static void main(String[] args) {
     System.out.println(name + " " + version);
  }
}
