package com.claytablet.wcs.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

public class FileUpload {
	static public String handleFile(HttpServletRequest request, HttpServletResponse response, 
			String[] supportedFileExts) throws FileUploadException, IOException {
		assert ServletFileUpload.isMultipartContent(request);
		
		try {
			ServletFileUpload servletFileUpload = new ServletFileUpload(
					new DiskFileItemFactory());
			@SuppressWarnings("unchecked")
			List<FileItem> fileItemsList = servletFileUpload.parseRequest(request);
	
			String optionalFileName = "";
			FileItem fileItem = null;
	
			Iterator<FileItem> it = fileItemsList.iterator();
			while (it.hasNext()) {
				FileItem fileItemTemp = (FileItem) it.next();
				if (fileItemTemp.isFormField()) {
					String fieldName = fileItemTemp.getFieldName();
					if (fieldName.equals("filename")) {
						optionalFileName = fileItemTemp.getString();
					}
				} else
					fileItem = fileItemTemp;
			}
	
			if (fileItem != null) {
				String fileName = fileItem.getName();
				if (fileItem.getSize() > 0) {
					if (optionalFileName.trim().equals(""))
						fileName = FilenameUtils.getName(fileName);
					else
						fileName = optionalFileName;
	
					String prefix = FilenameUtils.getBaseName(fileName);
					String ext = FilenameUtils.getExtension(fileName);
					if (isSupportedFileType(ext, supportedFileExts)) {
						if (StringUtils.isEmpty(prefix)) {
							prefix = "upload";
						}
						File tempFile = File.createTempFile(prefix, "."+ext);
						FileOutputStream fos = new FileOutputStream(tempFile);
						try {
							IOUtils.copy(fileItem.getInputStream(), fos);
							return tempFile.getAbsolutePath();
						} finally {
							IOUtils.closeQuietly(fos);
						}
					} else {
						throw new IOException("File type '" + ext + "' is not supported, " + 
								getSupportedTypeString(supportedFileExts));
					}
				}
			}
			// if we get here, we have an empty request
			throw new IOException("Cannot process empty upload request");
		} catch (FileUploadException e) {
			throw new IOException("Cannot process upload request", e);
		}
	}

	private static String getSupportedTypeString(String[] supportedFileExts) {
		StringBuffer buf = new StringBuffer();
		
		for (String supportedExt : supportedFileExts) {
			if (buf.length()!=0) {
				buf.append(", ");
			}
			buf.append('\'').append(supportedExt).append('\'');
		}
		if (supportedFileExts.length>1) {
			return "supported types are " + buf;
		} else {
			return "supported type is " + buf;
		}
	}

	private static boolean isSupportedFileType(String ext, String[] supportedFileExts) {
		if (supportedFileExts==null || supportedFileExts.length==0) {
			return true;
		}
		for (String supportedExt : supportedFileExts) {
			if (ext.compareToIgnoreCase(supportedExt)==0) {
				return true;
			}
		}
		return false;
	}
}
