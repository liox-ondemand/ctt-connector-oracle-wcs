/**
 * 
 */
package com.claytablet.wcs;

import static com.claytablet.client.util.NotNullUtils.notNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import COM.FutureTense.Interfaces.ICS;

import com.claytablet.client.producer.ConfigurationManager;
import com.claytablet.client.producer.CustomizableConfigurationKey;
import com.claytablet.client.producer.ManagerFactory;
import com.claytablet.client.producer.ManagerFactory.DbType;
import com.claytablet.client.producer.ManagerFactorySession;
import com.claytablet.client.producer.ProducerCommonConfigs;
import com.claytablet.client.producer.TranslationEntityManager;
import com.claytablet.client.producer.UserContext;
import com.claytablet.client.producer.UserProfile;
import com.claytablet.client.producer.impl.AbstractManagerFactorySessionImpl;
import com.claytablet.client.producer.impl.ConfigKeyImpl;
import com.claytablet.module.isolated.SQSS3Module;
import com.claytablet.wcs.system.AssetListManager;
import com.claytablet.wcs.system.AssetManager;
import com.claytablet.wcs.system.ContextManager;
import com.claytablet.wcs.system.JobManager;
import com.claytablet.wcs.system.LocaleManager;
import com.claytablet.wcs.system.SiteManager;
import com.claytablet.wcs.system.SubTypeManager;
import com.claytablet.wcs.system.TranslationRequestManager;
import com.claytablet.wcs.system.UserManager;
import com.claytablet.wcs.system.WorkflowManager;
import com.claytablet.wcs.system.impl.AssetListManagerImpl;
import com.claytablet.wcs.system.impl.AssetManagerImpl;
import com.claytablet.wcs.system.impl.ConfigManagerImpl;
import com.claytablet.wcs.system.impl.ContextManagerImpl;
import com.claytablet.wcs.system.impl.JobManagerImpl;
import com.claytablet.wcs.system.impl.LocaleManagerImpl;
import com.claytablet.wcs.system.impl.SiteManagerImpl;
import com.claytablet.wcs.system.impl.SubTypeManagerImpl;
import com.claytablet.wcs.system.impl.SystemEventUtils;
import com.claytablet.wcs.system.impl.TranslationRequestManagerImpl;
import com.claytablet.wcs.system.impl.UserContextImpl;
import com.claytablet.wcs.system.impl.UserManagerImpl;
import com.claytablet.wcs.system.impl.WorkflowManagerImpl;
import com.claytablet.wcs.system.queue.DatabaseJobQueue;
import com.fatwire.gst.foundation.controller.action.support.IcsBackedObjectFactoryTemplate;
import com.fatwire.gst.foundation.controller.annotation.ServiceProducer;
import com.fatwire.system.SessionFactory;

/**
 * @author Mike Field
 *
 */
public class CTObjectFactory extends IcsBackedObjectFactoryTemplate {

	private static String initError = null;
	private static Throwable initErrorCause = null;

	private static String csWebAppRoot = null;
	private static String sitesRoot = null;
	
	private ICS ics;
	private ContextManager contextManager;

	private Log LOG = notNull(LogFactory.getLog(getClass().getName() + ":" + Thread.currentThread().getId()));
	
	static final private AtomicReference<ManagerFactory> factoryRef = 
			new AtomicReference<ManagerFactory>();
	
	private static String dbInstanceId = null; 
	
	static {
		((CustomizableConfigurationKey<Boolean>)ProducerCommonConfigs.CT_ENABLE_TEAM_MANAGEMENT).setDefaultValue(false);
	}

	public static String getDbInstanceId() {
		return dbInstanceId;
	}
	
	/**
	 * @param ics
	 */
	public CTObjectFactory(ICS ics) {
		super(ics);
		
		if (initError==null) {
			try {
				this.ics = ics;
				UserContext ctx = new UserContextImpl(ics);
				
				if (StringUtils.isEmpty(ctx.getSiteId())) {
					// we need to fake a site for the context to initialize ManagerFactory, in case the user is not a global admin
					final UserContext originalCtx = ctx; 
					ctx = new UserContext() {
						@Override public UserProfile getUserProfile() { return originalCtx.getUserProfile(); }
						@Override public String getSiteId() { return "fake"; }
					};
				}
				
				synchronized(factoryRef) {
					if (getFactory()==null) {
						if (isNull(ctx)) {
							LOG.error("ctx is null when initializing CTObjectFactory");
						} else if (isNull(ctx.getUserProfile())) {
							LOG.error("userProfile is null when initializing CTObjectFactory ");
						} else {
							LOG.info("Initializing CTObjectFactory");
			
							String jspRoot = ics.GetProperty("cs.jsproot", "futuretense.ini", true);
							if (!StringUtils.isEmpty(jspRoot)) {
								csWebAppRoot = new File(jspRoot).getParentFile().getAbsolutePath();
								System.setProperty(SQSS3Module.CT3SQS_JAR_PATH_BASE_PROP, 
										new File(csWebAppRoot, "WEB-INF/lib").getAbsolutePath());
							}
							
							String sitesCacheRoot = ics.GetProperty("cs.pgcachefolder", "futuretense.ini", true);
							if (!StringUtils.isEmpty(sitesCacheRoot)) {
								sitesRoot = new File(sitesCacheRoot).getParentFile().getAbsolutePath();
							}
							
							String dbconnStr = ics.GetProperty("csct.dbconnpicture", "futuretense.ini", true);
							String dsn = null;
							if (StringUtils.isEmpty(dbconnStr)) {
								LOG.info("Cannot find csct.dbconnpicture in futuretense.ini, default to cs.dsn to initialize datasource");
								dbconnStr = ics.GetProperty("cs.dbconnpicture", "futuretense.ini", true);
								if (dbconnStr.contains("$dsn")) {
									dsn = ics.GetProperty("cs.dsn", "futuretense.ini", true);
									dbconnStr = dbconnStr.replaceAll("\\$dsn", dsn);
								}
							} else {
								LOG.info("Found csct.dbconnpicture in futuretense.ini, initialization data source");
							}
							
							if (StringUtils.isEmpty(dbconnStr)) {
								throw new RuntimeException("Cannot find cs.dbconnpicture in futuretense.ini, "
										+ "cannot initialize data source for ClayTablet connector");
							}
							
	                        Context initCtx = new InitialContext();
	                        String envCtxString = "";
	                        dsn = dbconnStr;
	                        
	                        int sep = dbconnStr.lastIndexOf('/');
	                        if (sep>0) {
	                        	envCtxString = dbconnStr.substring(0, sep) ;
	                        	dsn = dbconnStr.substring(sep);
	                        }
	                        		
							Context envCtx = (Context) initCtx.lookup(envCtxString);
				
							// Look up our data source
							DataSource ds = (DataSource) envCtx.lookup(dsn);
							if (ds==null) {
								throw new RuntimeException("Cannot find data source " + dsn + " in " + envCtxString +
										", fails initialize data source for ClayTablet connector");
							}
							DatabaseMetaData metaData = ds.getConnection().getMetaData();
							factoryRef.set(ManagerFactory.Init.newDatabaseManagerFactory(
									DbType.fromDriverName(metaData.getDriverName()), dbconnStr));
							LOG.debug("getting a session for: "+ctx.getUserProfile().getName());
							ManagerFactorySession session = getFactory().newSession(ctx);
							
							try {
								dbInstanceId = session.getDbInstanceId();
								// load ConfigManager to overwrite the configuration settings 
								ConfigManagerImpl cm = createConfigManager(ics);
								cm.initPoReferenceConfig();
								DatabaseJobQueue.createTables(ics);
								
								if (!SystemEventUtils.isAsyncOpsEventCreated(ics)) {
									LOG.info("Creating and enabling async operation system event ...");
									if (SystemEventUtils.createAsyncOpsEvent(ics, session) && 
											SystemEventUtils.enableAsyncOpsEvent(ics)) {
										LOG.info("Successfully created and enabled async operation system event ...");
									} else {
										LOG.warn("Cannot created and enabled async operation system event ...");
									}
								}
								if (!SystemEventUtils.isCheckMessageEventCreated(ics)) {
									LOG.info("Creating and enabling CT message checking system event ...");
									if (SystemEventUtils.createCheckMessageEvent(ics, session) &&
											SystemEventUtils.enableCheckMessageEvent(ics)) {
										LOG.info("Successfully created and enabled CT message checking system event ...");
									} else {
										LOG.warn("Cannot created and CT message checking system event ...");
									}
								}
								LOG.info("ManagerFactory successfully initialized as user " + ctx.getUserProfile().getName());
							} finally {
								session.close();
							}
						}
						if (getFactory()==null) {
							throw new RuntimeException("Cannot initialize ManagerFactory, insufficient privilege");
						}
					}
				}
				initError = null;
				initErrorCause = null;
			} catch (Throwable t) {
				LOG.error("Failed to initialize CTObjectFactory", t);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				PrintWriter pw = new PrintWriter(bos);
				t.printStackTrace(pw);
				IOUtils.closeQuietly(pw);
				initError = bos.toString();
				initErrorCause = t;
			}
		}
	}
	
	public static boolean hasInitError() {
		return initError!=null;
	}
	
	public static String getInitError() {
		return initError;
	}

	public static void resetInitError() {
		initError = null;
		initErrorCause = null;
	}
	
	private void checkError() {
		if (hasInitError()) {
			throw new RuntimeException("CTObjectFactory not initialized", initErrorCause);
		}
	}
	
	private ManagerFactory getFactory() {
		return factoryRef.get();
	}

	private boolean isNull(Object e)
	{
		if (e==null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	private void storeSessionInRequestFilter(ManagerFactorySession reqsession) {
		List<ManagerFactorySession> reqsessions = (ArrayList<ManagerFactorySession>) ics.getIServlet().getServletRequest().getAttribute("ctsessionholder");
		if (reqsessions != null) {
			reqsessions.add(reqsession);
		} else {
			LOG.error("The ctsessionholder object is null. It should have been created in the CTSessionFilter servlet filter. Make sure the CTSessionFilter filter is wired up correctly in the web.xml file.");
		}
	}
	
	@ServiceProducer(cache=false)
	public ManagerFactorySession createManagerFactorySession(final ICS ics) {
		if (hasInitError()) return null;
		UserContext ctx = new UserContextImpl(ics);
		ManagerFactorySession activeSession = getFactory().getActiveSession(ctx);
		if (activeSession==null || !activeSession.isOpen()) {
			if (StringUtils.isEmpty(ctx.getSiteId())) {
				LOG.debug("Getting a new global admin session");
				activeSession = getFactory().newGlobalAdminSession(ctx);
			} else {
				LOG.debug("Getting a new regular session");
				activeSession = getFactory().newSession(ctx);
			}
			storeSessionInRequestFilter(activeSession);
		}
		return activeSession;
	}

	@ServiceProducer(cache=true)
	public ManagerFactory createManagerFactory(final ICS ics) {
		if (hasInitError()) {
			return new DummyManagerFactory();
		};
		return getFactory();
	}
	
	@ServiceProducer(cache=false)
	public AssetManager createAssetManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		return new AssetManagerImpl(ics, session, SessionFactory.getSession());
	}

	@ServiceProducer(cache=false)
	public AssetListManager createAssetListManager(final ICS ics) {
		return new AssetListManagerImpl(ics);
	}
	
	@ServiceProducer(cache=false)
	public SiteManager createSiteManager(final ICS ics) {
		return new SiteManagerImpl(ics);
	}
	
	@ServiceProducer(cache=false)
	public WorkflowManager createWorkflowManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		return new WorkflowManagerImpl(ics, session);
	}
	
	@ServiceProducer(cache=false)
	public ConfigManagerImpl createConfigManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		return new ConfigManagerImpl(ics, session);
	}
	
	@ServiceProducer(cache=false)
	public JobManager createJobManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		return new JobManagerImpl(ics, session);
	}
	
	@ServiceProducer(cache=false)
	public LocaleManager createLocaleManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		return new LocaleManagerImpl(ics, session);
	}
	
	@ServiceProducer(cache=false)  /** TODO: This goes away. But it's still used in UIClayTabletMessageChecker **/
	public ContextManager createContextManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		this.contextManager = new ContextManagerImpl(ics, ics.GetSSVar("username"), session, sitesRoot, csWebAppRoot);
		return this.contextManager;
	}
	
	@ServiceProducer(cache=false)
	public TranslationEntityManager createTranslationEntityManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		TranslationEntityManager em = session.getTranslationEntityManager();
		return em;
	}
	
	@ServiceProducer(cache=false)
	public ConfigurationManager createCtConfigurationManager(final ICS ics) {
		ManagerFactorySession session = createManagerFactorySession(ics);
		return session.getConfigurationManager();
	}
	
	@ServiceProducer(cache=false)
	public TranslationRequestManager createTranslationRequestManager(final ICS ics) {
		return new TranslationRequestManagerImpl( this.createTranslationEntityManager(ics) );
	}
	
	@ServiceProducer(cache=false)
	public UserManager createUserManager(final ICS ics) {
		return new UserManagerImpl(ics);
	}
	
	// added by Rick Ptasznik - used to find asset definitions in the site config UI screen
	@ServiceProducer(cache=false)
	public SubTypeManager createSubTypeManager(final ICS ics){
		return new SubTypeManagerImpl(ics);
	}
	
	private class DummyManagerFactory implements ManagerFactory {
		@Override public ManagerFactorySession newSession(UserContext ctx) { 
			throw new RuntimeException("CTObjectFactory fails to initialize", initErrorCause); 
		}
		@Override public ManagerFactorySession newGlobalAdminSession(UserContext ctx) { 
			throw new RuntimeException("CTObjectFactory fails to initialize", initErrorCause);  
		}
		@Override public ManagerFactorySession getActiveSession(UserContext ctx) { 
			throw new RuntimeException("CTObjectFactory fails to initialize", initErrorCause);  
		}
		
		@Override public void close(){ }
		
		@Override
		public AbstractManagerFactorySessionImpl createSession(UserContext ctx) {
			throw new RuntimeException("CTObjectFactory fails to initialize", initErrorCause);  
		}
	}
}
