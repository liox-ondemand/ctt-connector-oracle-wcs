/**
 * 
 */
package com.claytablet.wcs.installer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.claytablet.wcs.system.queue.DatabaseJobQueue;
import com.fatwire.gst.foundation.facade.sql.table.TableColumn;
import com.fatwire.gst.foundation.facade.sql.table.TableDef;

import COM.FutureTense.Interfaces.FTValList;
import COM.FutureTense.Interfaces.ICS;
import COM.FutureTense.Util.ftErrors;
import COM.FutureTense.Util.ftMessage;

/**
 * Supports creating all neccessary CT tables in WCS
 * @author Mike Field
 *
 */
public class TableCreator {

    private final Log LOG = LogFactory.getLog(TableCreator.class.getPackage().getName());
    private ICS ics;
    
    TableCreator(ICS ics) {
    	this.ics = ics;
    }
    /**
     * Creates a table with the specified name and columns. Uses ftcmd=createtable,
     * sets tabletype=obj and uploadDir=/export/+tablename.
     * @param tablename Name of table to create
     * @param columns Map<String,String> of column names and data types. E.g. "assettype","varchar(255)"
     * @return True if success, false otherwise.
     */
    public boolean createObjectTable(String tablename, Map<String, String> columns) {

        ics.ClearErrno();
        
        FTValList vals = new FTValList();
        vals.put("ftcmd", "createtable");
        vals.put("tablename", tablename);
        vals.put("systable", "obj");
        vals.put("uploadDir", "/export/" + tablename);

		Iterator<Entry<String, String>> iterator = columns.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<String,String> pairs = (Map.Entry<String,String>)iterator.next();
		    vals.put(pairs.getKey(), pairs.getValue());
		}       


        boolean bResult = ics.CatalogManager(vals);
        if (!bResult) return false;
        int errno = ics.GetErrno();
        switch (errno) {
            case ftErrors.tableAreadyExists:
                LOG.info("Attempted to re-create table but it already existed: " + tablename);
                return true;
            case ftErrors.success:
            case 0: 
            	LOG.info("Successfully created table " + tablename);
                return true;
            default:
                LOG.warn("Table creation failed.  Errno: "+errno);
                return false;
        }
    }
    
    /**
     * Creates the queue table. We can't return a success/failure boolean because the GSF TableCreator
     * class doesn't return a success/failure status.
     *
     * @return true on success, false on failure
     */
    public void createCTQueueTable() {
    	LOG.info("Creating CT Queue Table...");
        final TableDef def = new TableDef("CT_queue", "", ftMessage.objecttbl);
        
        def.addColumn(new TableColumn(DatabaseJobQueue.ID, TableColumn.Type.ccbigint, true).setNullable(false));
        def.addColumn(new TableColumn(DatabaseJobQueue.ACTION, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.Q_ENTRY, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.Q_ENTRY_CLASS, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.STATUS, TableColumn.Type.ccvarchar).setLength(255).setNullable(true));
        def.addColumn(new TableColumn(DatabaseJobQueue.INSERTION_DATE, TableColumn.Type.ccdatetime).setNullable(true));

        new com.fatwire.gst.foundation.facade.sql.table.TableCreator(ics).createTable(def);
        LOG.info("CT Queue table creation completed.");
    }
	
}
