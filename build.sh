#!/bin/sh

BASE=/root/git/ctt-connector-oracle-wcs
JSKLIBFOLDER=/opt/apache-tomcat-7.0.54/webapps/cs/WEB-INF/lib

echo Using base $BASE 
echo Using jsk- $JSKLIBFOLDER

JARNAME=claytablet-sites-1.0.jar
VENDORNAME="Clay Tablet"

echo === Building $JARNAME ===
echo Removing Old jar File...
rm $BASE/target/$JARNAME




echo Setting Class Path...
CP=$BASE/lib/gator.jar
CP=$CP:$BASE/lib/assetapi-impl.jar
CP=$CP:$BASE/lib/assetapi.jar
CP=$CP:$BASE/lib/ics.jar
CP=$CP:$BASE/lib/basic.jar
CP=$CP:$BASE/lib/mail.jar
CP=$CP:$BASE/lib/commons-lang-2.4.jar
CP=$CP:$BASE/lib/commons-fileupload-1.2.1.jar
CP=$CP:$BASE/lib/httpcore-4.1.2.jar
CP=$CP:$BASE/lib/httpclient-4.1.2.jar
CP=$CP:$BASE/lib/commons-logging-1.1.1.jar
CP=$CP:$BASE/lib/cs-core.jar
CP=$CP:$BASE/lib/cs.jar
CP=$CP:$BASE/lib/framework.jar
CP=$CP:$BASE/lib/xcelerate.jar
CP=$CP:$BASE/lib/gst-foundation-all-11.6.1.jar
CP=$CP:$BASE/lib/jettison-1.0.1.jar
CP=$CP:$BASE/lib/servlet-api.jar
CP=$CP:$BASE/lib/services-api-11.1.1.8.0.jar
CP=$CP:$BASE/lib/services-impl-11.1.1.8.0.jar
CP=$CP:$BASE/lib/claytablet-client-producer.jar
CP=$CP:$BASE/lib/claytablet-custom-jobmetadata.jar
CP=$CP:$BASE/lib/claytablet-translation.jar
CP=$CP:$BASE/lib/claytablet-freeway-simpleclient-3.0.jar
CP=$CP:$BASE/lib/hibernate-jpa-2.0-api-1.0.1.Final.jar
CP=$CP:$BASE/lib/hibernate3.jar
CP=$CP:$BASE/lib/commons-io-1.4.jar

echo Building...
mkdir $BASE/target/tmp-compile
cd $BASE/src
DEBUG_FLAGS="-Xlint:deprecation -Xlint:unchecked"

NAME="$VENDORNAME WebCenter Sites Java Library"
VERSION="SNAPSHOT Build Date: $(date +"%a %m/%d/%Y %T")"

mkdir com/claytablet/wcs/util

BUILD=com/claytablet/wcs/util/Build.java
echo "package com.claytablet.wcs.util;" > $BUILD
echo "import COM.FutureTense.Interfaces.ICS;" >> $BUILD
echo "import org.apache.commons.logging.LogFactory;" >> $BUILD
echo "public final class Build {" >> $BUILD
echo "  private static boolean bPrinted = false;" >> $BUILD
echo "  private static String name = \"$NAME\";" >> $BUILD
echo "  private static String version = \"$VERSION\";" >> $BUILD
echo "  public static synchronized final void printBuildDate() {" >> $BUILD
echo "      if (!bPrinted) {" >> $BUILD
echo "          LogFactory.getLog(Build.class).info(name + \" Initialized.  \" + version);" >> $BUILD
echo "          bPrinted = true;" >> $BUILD
echo "      }" >> $BUILD
echo "  }" >> $BUILD
echo "  public static void main(String[] args) {" >> $BUILD
echo "     System.out.println(name + \" \" + version);" >> $BUILD
echo "  }" >> $BUILD
echo "}" >> $BUILD

export JAVA_HOME=/usr/java/jdk1.7.0_80
javac $DEBUG_FLAGS -cp $CP -d $BASE/target/tmp-compile com/claytablet/wcs/ui/*.java com/claytablet/wcs/*.java com/claytablet/wcs/util/*.java com/claytablet/wcs/system/*.java com/claytablet/wcs/system/queue/*.java com/claytablet/wcs/system/impl/*.java
cd $BASE

mkdir $BASE/target/tmp-compile/META-INF
MANIFEST=$BASE/target/Manifest.txt
echo "Implementation-Title: $NAME" > $MANIFEST
echo "Implementation-Vendor: $VENDORNAME" >> $MANIFEST
echo "Implementation-Version: $VERSION" >> $MANIFEST
echo "Main-Class: com.claytablet.owcs.util.Build" >> $MANIFEST


echo Jarring... 
jar cfm $BASE/target/$JARNAME $MANIFEST -C $BASE/target/tmp-compile .
rm -rf $BASE/target/tmp-compile
rm $MANIFEST

echo Deploying to JSK...
cp $BASE/target/$JARNAME $JSKLIBFOLDER
cp $BASE/WebResources/ct/css/ct.css* $JSKLIBFOLDER/../../ct/css
cp $BASE/WebResources/ct/doc/* $JSKLIBFOLDER/../../ct/doc
echo Done.
