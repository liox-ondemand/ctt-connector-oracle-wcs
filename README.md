# Clay Tablet Connector for Oracle WebCenter Sites #

* **Author** Clay Tablet Technologies
* **Current version** 1.1.6
* Currently supports OWCS 11.1.1.6.1 and 11.1.1.8. It does NOT support OWCS 12 yet.

## License ##

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available in license.txt


## Setting up your development environment ##

### Prerequisite ###
- Java 1.6 or 1.7 (1.8 or higher does not work)
- Eclipse
- Install Fatwire CSDT plugin for Eclipse (com.fatwire.EclipseCSDT_11.1.1.v8_0_r157925.jar)

### Install JSK (OWCS Jump Start Kit) ###
* Obtain JSK installer for 11.1.1.6.1 or 11.1.1.8 from Oracle
* Run the installer with Java (executable jar) and follow the instruction
* After installing JSK, start JSK and log into it using the standard user/password "fwadmin/xceladmin"

### Build your code ###
* Run build.bat. The first time you run it, it will generate a setenv.bat under %YourSourceCodeHome% with the following content


```
#!batch

   SET JAVAHOME=C:\devtools\Java\jdk1.7.0_75\bin
   SET BASE=%YourSourceCodeHome%\CTWCSConnector
   SET JSKLIBFOLDER=%YourJSKHome%\App_Server\apache-tomcat-6.0.32\webapps\cs\WEB-INF\lib
   SET JARNAME=claytablet-sites.jar
   SET VENDORNAME=Clay Tablet

```

* Edit setenv.bat to match your environment
* Run %YourSourceCodeHome%\build.bat to build. It will automatically copy all the jar files built into your JSK folder.

### Build release package ###
* Delete %YourSourceCodeHome%/CTWCSConnector/target/installer folder (if exists)
* Run %YourSourceCodeHome%/package.bat
* Zip up %YourSourceCodeHome%/CTWCSConnector/target/installer into a ZIP file, which becomes the release package

### Deploy release package on your JSK instance ###
* See install and config guide under WebResources\ct\doc

### Importing your projects into Eclipse ###
* Import existing Java project into Eclipse workspace

### Setup Eclipse to connect to your JSK ###
* Change your JSK's cs_workspace folder location
* * Shutdown your JSK
* * Edit your JSK's Sites/11.1.1.6.1/futuretense.ini file, change the cs.csdtfolder property to point to the "export" folder in your checked out connector source code, e.g.
from

```
#!batch

cs.csdtfolder=%YourJSKHome%/Sites/11.1.1.6.1/export

```
to: 

```
#!batch

#cs.csdtfolder=%YourJSKHome%/Sites/11.1.1.6.1/export
cs.csdtfolder=%YourSourceCodeHome%/export

```
* * Restart your JSK
* Importing existing ClayTablet-CSDT project under export/envision/cs_workspace into your Eclipse workspace
* Window > Open Perspective > Oracle WebCenter Sites, enter path to your JSK's Sites/11.1.1.6.1/ folder
* After setting up cs_workspace folder location and connecting Eclipse to JSK, when JSK is running, all changes made to the Element Catalog and Site Catalog enties (XML and JSP files) in your source folder will be automatically imported into JSK and reflect in the UI

### Logging ###
* By default you can find OWCS log file under %YourJSKHome%\Sites\11.1.1.6.1\logs\sites.log (for OWCS11.1.1.6.1).
* Edit %YourJSKHome%\App_Server\apache-tomcat-6.0.32\webapps\cs\WEB-INF\classes\log4j.properties to configure logging.

### How to debug ###
* To start JSK in debug mode, go to %YourJSKHome%\App_Server\apache-tomcat-6.0.32\bin, start JSK with:

```
#!batch

    startup debug

```
* Attach your debugger to JPDA listening port (8000 by default)