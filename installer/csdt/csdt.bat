@echo off

echo Importing site components from Datastore

::
:: Set the base path to your WCS installation directory and CS web application directory

set OWCS_ROOT=C:\Oracle\Sites
set CS_WEBAPP_ROOT=C:\Oracle\Middleware\user_projects\domains\csDomain\cs

:: Setting for JSK

:: set OWCS_ROOT=C:\Oracle\WebCenter\Sites\11gR1\Sites\11.1.1.6.1
:: set CS_WEBAPP_ROOT=%OWCS_ROOT%\..\..\App_Server\apache-tomcat-6.0.32\webapps\cs

set WEBINF_LIB=%CS_WEBAPP_ROOT%\WEB-INF\lib
set JAVAX_BASE=%CD%

:: Set the URL and login credential to use CSDT
set SVR=http://localhost:7001/cs/Satellite
set USR=fwadmin
set PW=xceladmin
set DS=cs_workspace
set FROM_SITES=
set TO_SITES=

:: Determine the version of WCS and set the classpath accordingly
::

if exist %WEBINF_LIB%\csdt-client-11.1.1.8.0.jar GOTO SET_CP_8

:: Set classpath for 11.1.1.6.1
set CP=%OWCS_ROOT%\csdt-client-1.2.2.jar
set CP=%CP%;%WEBINF_LIB%\wem-sso-api-1.2.2.jar
set CP=%CP%;%WEBINF_LIB%\wem-sso-api-cas-1.2.2.jar
set CP=%CP%;%WEBINF_LIB%\wem-sso-api-oam-1.2.2.jar
set CP=%CP%;%WEBINF_LIB%\cas-client-core-3.1.9.jar
set CP=%CP%;%WEBINF_LIB%\cs-core.jar
GOTO SET_CP_COMMON

:: Set classpath for 11.1.1.8.0
:SET_CP_8
set CP=%WEBINF_LIB%\csdt-client-11.1.1.8.0.jar
set CP=%CP%;%WEBINF_LIB%\wem-sso-api-11.1.1.8.0.jar
set CP=%CP%;%WEBINF_LIB%\wem-sso-api-cas-11.1.1.8.0.jar
set CP=%CP%;%WEBINF_LIB%\wem-sso-api-oam-11.1.1.8.0.jar
set CP=%CP%;%WEBINF_LIB%\cas-client-core-3.1.9.jar
set CP=%CP%;%WEBINF_LIB%\cs-core.jar

:SET_CP_COMMON
set CP=%CP%;%WEBINF_LIB%\commons-logging-1.1.1.jar
set CP=%CP%;%WEBINF_LIB%\httpclient-4.1.2.jar
set CP=%CP%;%WEBINF_LIB%\httpcore-4.1.2.jar
set CP=%CP%;%WEBINF_LIB%\httpmime-4.1.2.jar
set CP=%CP%;%WEBINF_LIB%\rest-api-1.2.2.jar
set CP=%CP%;%WEBINF_LIB%\spring-2.5.5.jar
set CP=%CP%;%JAVAX_BASE%\javax.servlet-3.0.jar

echo %CP%

:: TEST that WSDT commands work using "listds" command (lists what's in workspace, aka the "data source")
if ""%1"" == ""import"" goto:do_import
call:csdt listds @SITECATALOG
call:csdt listds @ELEMENTCATALOG
call:csdt listds @ALL_ASSETS
goto:eof

:do_import
call:csdt import @ALL_ASSETS
call:csdt import @SITECATALOG
call:csdt import @ELEMENTCATALOG

::call:csdt listds @ASSET_TYPE:*
:: if the above "listds" commands work, then comment-out the above 2 commands (or delete the lines), then uncomment all the below lines and re-run this script file
::csdt import @SITE:FirstSiteII
goto:eof

:csdt
	set CSDT_CMD=%1
	set RESOURCES=%2
	set FROM_SITES=%3
	set TO_SITES=%4

	echo. 
	echo %CSDT_CMD% %RESOURCES% %FROM_SITES% %TO_SITES%
	java -classpath %CP% com.fatwire.csdt.client.main.CSDT %SVR% username=%USR% password=%PW% resources=%RESOURCES% fromSites=%FROM_SITES% toSites=%TO_SITES% datastore=%DS% cmd=%CSDT_CMD%
goto:eof
