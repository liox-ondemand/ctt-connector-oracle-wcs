ECHO OFF 

SET BASE=%CD%

if exist %BASE%\target\installer goto :do_not_overwrite

MD %BASE%\target\installer
XCOPY /I /Y /E %BASE%\WebResources\ct %BASE%\target\installer\cs\ct
XCOPY /I /Y /E %BASE%\WebResources\js %BASE%\target\installer\cs\js
XCOPY /I /Y /E %BASE%\installer\WEB-INF %BASE%\target\installer\cs\WEB-INF\
XCOPY /I /Y /E %BASE%\installer\gsf %BASE%\target\installer\gsf

copy %BASE%\target\claytablet-sites*.jar %BASE%\target\installer\cs\WEB-INF\lib

XCOPY /I /Y /E %BASE%\installer\csdt %BASE%\target\installer\csdt
XCOPY /I /Y /E %BASE%\installer\BP-addon %BASE%\target\installer\BP-addon
xcopy /I /Y /E %BASE%\export\envision\cs_workspace %BASE%\target\installer\csdt\export\envision\cs_workspace

del %BASE%\target\installer\csdt\export\envision\cs_workspace\.classpath
del %BASE%\target\installer\csdt\export\envision\cs_workspace\.project
 
GOTO :EOF

:do_not_overwrite
ECHO %BASE%\target\installer already exists, cannot overwrite. Exiting...