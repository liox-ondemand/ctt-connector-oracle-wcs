ECHO OFF
if exist setenv.bat CALL setenv.bat 
if exist setenv.bat GOTO build

:env
echo Creating setenv.bat, please edit it to set up your own environment and run build.bat again to build
echo SET JAVAHOME=%JAVA_HOME%\bin>> setenv.bat
echo SET BASE=%CD%>> setenv.bat
echo SET JSKLIBFOLDER=C:\FatWire\JSK\11.1.1.6.1-ClayTablet-2\App_Server\apache-tomcat-6.0.32\webapps\cs\WEB-INF\lib>> setenv.bat
echo SET JARNAME=claytablet-sites.jar>> setenv.bat
echo SET VENDORNAME=Clay Tablet>> setenv.bat
EXIT /B

:build

echo === Checking environment ===
if not exist %JAVAHOME%\javac.exe echo JAVAHOME not set correctly, please edit setenv.bat to set it up, exiting...
if not exist %JAVAHOME%\javac.exe EXIT /B

echo === Building %JARNAME% ===
echo Removing Old jar File - %BASE%\target\%JARNAME%
if exist %BASE%\target\%JARNAME% del %BASE%\target\%JARNAME%

echo Setting Class Path...
SET CP=%BASE%\lib\gator.jar
SET CP=%CP%;%BASE%\lib\assetapi-impl.jar
SET CP=%CP%;%BASE%\lib\assetapi.jar
SET CP=%CP%;%BASE%\lib\ics.jar
SET CP=%CP%;%BASE%\lib\basic.jar
SET CP=%CP%;%BASE%\lib\mail.jar
SET CP=%CP%;%BASE%\lib\commons-lang-2.4.jar
SET CP=%CP%;%BASE%\lib\commons-fileupload-1.2.1.jar
SET CP=%CP%;%BASE%\lib\httpcore-4.1.2.jar
SET CP=%CP%;%BASE%\lib\httpclient-4.1.2.jar
SET CP=%CP%;%BASE%\lib\commons-logging-1.1.1.jar
SET CP=%CP%;%BASE%\lib\cs-core.jar
SET CP=%CP%;%BASE%\lib\cs.jar
SET CP=%CP%;%BASE%\lib\framework.jar
SET CP=%CP%;%BASE%\lib\xcelerate.jar
SET CP=%CP%;%BASE%\lib\gst-foundation-all-11.6.1.jar
SET CP=%CP%;%BASE%\lib\jettison-1.0.1.jar
SET CP=%CP%;%BASE%\lib\servlet-api.jar
SET CP=%CP%;%BASE%\lib\services-api-11.1.1.8.0.jar
SET CP=%CP%;%BASE%\lib\services-impl-11.1.1.8.0.jar
SET CP=%CP%;%BASE%\lib\claytablet-client-producer.jar
SET CP=%CP%;%BASE%\lib\claytablet-custom-jobmetadata.jar
SET CP=%CP%;%BASE%\lib\claytablet-translation.jar
SET CP=%CP%;%BASE%\lib\claytablet-freeway-simpleclient-3.0.jar
SET CP=%CP%;%BASE%\lib\hibernate-jpa-2.0-api-1.0.1.Final.jar
SET CP=%CP%;%BASE%\lib\hibernate3.jar
SET CP=%CP%;%BASE%\lib\commons-io-1.4.jar

echo classpath=%CP%

echo Building...
mkdir %BASE%\target\tmp-compile
cd %BASE%\src
SET DEBUG_FLAGS=-Xlint:deprecation -Xlint:unchecked -g

SET NAME=%VENDORNAME% WebCenter Sites Java Library
SET VERSION=1.0 Build date: %date% %time%

SET BUILD=com/claytablet/wcs/util/Build.java
echo package com.claytablet.wcs.util; > %BUILD%
echo import COM.FutureTense.Interfaces.ICS; >> %BUILD%
echo import org.apache.commons.logging.LogFactory; >> %BUILD%
echo public final class Build { >> %BUILD%
echo   private static boolean bPrinted = false; >> %BUILD%
echo   private static String name = "%NAME%"; >> %BUILD%
echo   private static String version = "%VERSION%"; >> %BUILD%
echo   public static synchronized final void printBuildDate() { >> %BUILD%
echo       if (!bPrinted) { >> %BUILD%
echo           LogFactory.getLog(Build.class).info(name + " Initialized.  " + version); >> %BUILD%
echo           bPrinted = true; >> %BUILD%
echo       } >> %BUILD%
echo   } >> %BUILD%
echo   public static void main(String[] args) { >> %BUILD%
echo      System.out.println(name + " " + version); >> %BUILD%
echo   } >> %BUILD%
echo } >> %BUILD%

%JAVAHOME%\javac -source 1.6 -target 1.6 %DEBUG_FLAGS% -cp %CP% -d %BASE%\target\tmp-compile com\claytablet\wcs\ui\*.java com\claytablet\wcs\*.java com\claytablet\wcs\util\*.java com\claytablet\wcs\system\*.java com\claytablet\wcs\system\queue\*.java com\claytablet\wcs\system\impl\*.java
cd %BASE%

mkdir %BASE%\target\tmp-compile\META-INF
SET MANIFEST=%BASE%\target\Manifest.txt
echo Implementation-Title: %NAME% > %MANIFEST%
echo Implementation-Vendor: %VENDORNAME% >> %MANIFEST%
echo Implementation-Version: %VERSION% >> %MANIFEST%
echo Main-Class: com.claytablet.owcs.util.Build >> %MANIFEST%


echo Jarring... 
%JAVAHOME%\jar cfm %BASE%\target\%JARNAME% %MANIFEST% -C %BASE%\target\tmp-compile .
rmdir /s /q %BASE%\target\tmp-compile
del %MANIFEST%

if not exist %JSKLIBFOLDER%\cs.jar echo JSKLIBFOLDER not set up correctly, finishing without deploying to JSK...
if not exist %JSKLIBFOLDER%\cs.jar EXIT /B

echo Deploying to JSK...
copy %BASE%\target\%JARNAME%* %JSKLIBFOLDER%
copy %BASE%\WebResources\ct\css\ct.css* %JSKLIBFOLDER%\..\..\ct\css
copy %BASE%\WebResources\ct\doc\* %JSKLIBFOLDER%\..\..\ct\doc
echo Done (%DATE% %TIME%)