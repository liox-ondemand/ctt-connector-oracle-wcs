<%@page import="com.fatwire.services.AuthorizationService"
%><%@page import="com.fatwire.services.exception.ServiceException"
%><%@page import="COM.FutureTense.Interfaces.ICS"
%><%@page import="com.fatwire.assetapi.data.AssetId"
%><%@page import="com.fatwire.services.util.AssetUtil"
%><%@page import="java.util.Arrays"
%><%@page import="com.openmarket.xcelerate.interfaces.IAsset"
%><%@page import="com.fatwire.assetapi.data.AssetData"
%><%@page import="java.util.ArrayList"
%><%@page import="com.claytablet.bulktranslation.UITranslationBean"
%><%@page import="com.fatwire.services.AssetService"
%><%@page import="com.fatwire.services.ServicesManager"
%><%@page import="com.fatwire.system.SessionFactory"
%><%@page import="com.fatwire.system.Session"
%><%@page import="com.fatwire.services.util.JsonUtil"
%><%@page import="com.fatwire.cs.ui.framework.UIException"
%><%@page import="java.util.List"
%><%@page import="com.fatwire.ui.util.GenericUtil"
%><%@page import="org.apache.commons.lang.StringUtils"
%><%@page import="com.fatwire.cs.ui.framework.LocalizedMessages"
%><%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><cs:ftcs><%
try {
	List<AssetId> assetsToTranslate = GenericUtil.retainDistinctElements(GenericUtil.emptyIfNull(JsonUtil.jsonToIdList(StringUtils.defaultString(request.getParameter("assetIds")))));

	Session ses = SessionFactory.getSession();
	ServicesManager servicesManager = (ServicesManager)ses.getManager( ServicesManager.class.getName() );
	final AssetService assetService = servicesManager.getAssetService();
	final ICS _ics = ics;
  
	List<UITranslationBean> result = GenericUtil.transformList(assetsToTranslate, new GenericUtil.Transformer<AssetId, UITranslationBean>() {
		public UITranslationBean transform(AssetId assetId) {
			UITranslationBean uiBean = null;
			try {
				AssetData assetData = assetService.read(assetId, Arrays.asList(IAsset.NAME));
				Object nameData = AssetUtil.getAttribute(assetData, IAsset.NAME);
				String name = nameData == null ? "" : String.valueOf(nameData);
				String type = AssetUtil.getAssetTypeDescription(assetData);
				
				// Encode and Clean for XSS 
				name =  GenericUtil.cleanString(name);				

				uiBean = new UITranslationBean();
				// This is where you would call your code to send for translation and set sucess or detail error message.
				boolean success = true;
				uiBean.setSuccess(success);
				if(!success) {
					uiBean.setDetail("Could not send asset for translation");
				} else {
					uiBean.setDetail("Asset sent for translation");
				}
				uiBean.setName(name);
				uiBean.setAsset(assetId);
				uiBean.setId(assetId.toString());
				uiBean.setType(type);
			} catch (ServiceException e) {
				throw new UIException(e);
			}
			return uiBean;
		}
	});
	request.setAttribute("result", result);
} catch(UIException e) {
	request.setAttribute(UIException._UI_EXCEPTION_, e);
	throw e;
} catch(Exception e) {
	UIException uie = new UIException(e);
	request.setAttribute(UIException._UI_EXCEPTION_, uie);
	throw uie;
}%></cs:ftcs>