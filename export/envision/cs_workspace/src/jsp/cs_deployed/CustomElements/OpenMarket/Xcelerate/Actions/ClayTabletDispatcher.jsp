<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="assetset" uri="futuretense_cs/assetset.tld"
%><%@ taglib prefix="commercecontext" uri="futuretense_cs/commercecontext.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="listobject" uri="futuretense_cs/listobject.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="searchstate" uri="futuretense_cs/searchstate.tld"
%><%@ taglib prefix="siteplan" uri="futuretense_cs/siteplan.tld"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors"
%><cs:ftcs><%--

INPUT

OUTPUT

--%>
<render:callelement elementname='<%="CustomElements/CT/" + ics.GetVar("displayelement")%>'>
	<render:argument name="c" value='<%=ics.GetVar("c")%>'/>
	<render:argument name="cid" value='<%=ics.GetVar("cid")%>'/>
    <% if (Utilities.goodString(ics.GetVar("assetids"))) { %><render:argument name="assetids" value='<%=ics.GetVar("assetids")%>'/><% } %>
    <% if (Utilities.goodString(ics.GetVar("allassetids"))) { %><render:argument name="allassetids" value='<%=ics.GetVar("allassetids")%>'/><% } %>
</render:callelement>
</cs:ftcs>
