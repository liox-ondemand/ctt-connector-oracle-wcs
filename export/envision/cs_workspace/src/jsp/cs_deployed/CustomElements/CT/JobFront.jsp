<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIJobManager"> 
<jsp:useBean id="now" class="java.util.Date"/>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
	
	<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
	
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<link type="text/javascript" href="js/fw/inspectasset.js"></link>

<script language="javascript"><!--
function inspectAsset(type, longid) {
	SitesApp.event(SitesApp.id('asset', type + ':' + longid), 'inspect'); // This works for .6.1
	<%--
	// SitesApp.event(fwutil.buildDocId(longid, type), 'inspect'); // This works for .8
	// Be sure to do ics.callement on OpenMarket/Xcelerate/UIFramework/LoadDojo at the top first
	--%>
}
-->
</script>

<script type="text/javascript">
	function getLocale(loc) {
		//convert from CS's locale notation format to the format Dojo NLS uses.
		//(e.g. en_US -> en-us, fr_FR -> fr-fr, etc.)
		return loc.toLowerCase().replace('_', '-');
	}
	var djConfig = {
		fw_csPath: '',
		pubName: 'FirstSiteII',
		parseOnLoad: true,
		locale: getLocale('en_US')
	};


	function toggleCheckboxes() {
		var hasChecked = false;
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].checked) {
				hasChecked = true;
				break;
			}
		}
		if (hasChecked) {
			uncheckAll();
		} else {
			checkAll();
		}
		document.getElementById("toggler").checked = false;
		var totalSelected = getNumberOfSelectedItems();	
		updateTotal(totalSelected);
	}

	function checkAll() {
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			boxes[i].checked = true;
			selectOrUnselectItem(boxes[i].value, boxes[i].checked);
		}
	}

	function uncheckAll() {
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			boxes[i].checked = false;
			selectOrUnselectItem(boxes[i].value, boxes[i].checked);
		}
	}

	function selectAndCheckAll() {
		selectAll();
		checkAll();
	}

	function unselectAndUncheckAll() {
		unselectAll();
		uncheckAll();
	}

	function selectAll() {
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			selectitems[i].selected = true;
		}
		updateTotal( selectitems.options.length );
	}

	function unselectAll() {
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			selectitems[i].selected = false;
		}
		updateTotal( "0" );
	}

	function updateDropdownAndTotal(thisboxvalue, ischecked) {
		// When the box is checked or unchecked, select or unselect the corresponding
		// item from the assetids drop-down menu.
		
		// Updated the drop-down menu
		selectOrUnselectItem(thisboxvalue, ischecked);
		
		var totalSelected = getNumberOfSelectedItems();	
		updateTotal(totalSelected);
	}

	function getNumberOfSelectedItems() {
		var selectitems = document.getElementById("reqids");
		var total = 0;
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].selected == true) {
				total++;
			}
		}
		return total;
	}

	function updateTotal(total) {
		document.getElementById("select_total").innerHTML = total;
	}

	function selectOrUnselectItem(itemvalue, selectval) {
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].value == itemvalue) {
				selectitems[i].selected = selectval;
				break;
			}
		}
	}

	function checkOrUncheckBox(itemvalue, checkedval) {
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].value == itemvalue) {
				boxes[i].checked = checkedval;
				break;
			}
		}	
	}

	function restoreCheckedState() {
		var boxList = document.getElementsByName("page_reqids");
		var boxes = [];
		
		for (var i=0; i<boxList.length; i++) {
			boxes.push(boxList[i]);
		}
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].selected) {
				for (var j=0; j < boxes.length; j++) {
					if (selectitems[i].value == boxes[j].value) {
						boxes[j].checked = true;
						boxes.splice(j, 1);
						break;
					}		
				}
			}
		}
	}

	function getCheckedRadio( radioname ) {
		var checkedval = "";
		var radios = document.getElementsByName( radioname );
		for (var i=0; i<radios.length; i++) {
			if (radios[i].checked) {
				checkedval = radios[i].value;
			}
		}
		return checkedval;
	}

	function clearMessage() {
		activeView.clearMessage();
	    alert();
	}

	function sortPage(orderbyclause) {
		document.getElementById("orderbyclause").value = orderbyclause;
		refreshPage(0);
	}

	function setFirstRecord(firstrec) {
		refreshPage(firstrec);
	}

	function showBlock(elementid) {
		show(document.getElementById(elementid), 'block');
	}

	function show(element, display) {
		element.style.visibility = 'visible';
		element.style.display = display;
	}
	
	function refreshPage(firstrec) {
		document.getElementById("page_firstrecord").value = firstrec;
		
		var reqPane = document.getElementById("jobrequests");
		if (reqPane) {
			document.getElementById("modal_blocker").style.top = "60px";
			showBlock("modal_blocker");
			var progress = document.getElementById("requestsloadingprogress");
			showBlock("requestsloadingprogress");
			var orderbyclause = document.getElementById("orderbyclause").value;
			var pagesize_input = document.getElementById("page_size");
			var pagesize = document.getElementById("default_page_size").value;
			if (pagesize_input) {
				pagesize = pagesize_input.value;
			} 
	
			var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestsList" +
				"&jobid=" + document.getElementById("jobid").value +
				"&where=" + encodeURIComponent(document.getElementById("whereclause").value) +
				"&firstrec=" + firstrec + "&pagesize=" + pagesize;  
			
			if (document.getElementById("app")) {
				posturl+="&app=" + 	document.getElementById("app").value + "&page=jobdetail";
			}
			if (document.getElementById("siteid")) {
				posturl+="&siteId=" + 	document.getElementById("siteid").value;
			}
			if (orderbyclause) {
				posturl+="&orderby=" + encodeURIComponent(orderbyclause);	
			}
	
	 		$.ajax({url:posturl,
	 			success:function(result){
	 				document.getElementById("jobrequests").innerHTML=result;
	 				document.getElementById("request_status_summary").innerHTML = 
	 					document.getElementById("h_request_status_summary").innerHTML;
	 				document.getElementById("updated_reqid_section").innerHTML = '';
					document.getElementById("request_list_summary").innerHTML =
						document.getElementById("h_request_list_summary").innerHTML;
					document.getElementById("request_list_pages").innerHTML =
						document.getElementById("h_request_list_pages").innerHTML;
					document.getElementById("requestsloadingprogress").style.display = "none";
					document.getElementById("modal_blocker").style.display = "none";
					restoreCheckedState();
				},
				error: function(xhr, textStatus, errorThrown){
					document.getElementById("requestsloadingprogress").style.display = "none";
					document.getElementById("modal_blocker").style.display = "none";
					alert("Failed to update the requests list: " + errorThrown);
				}
	 		});
		}
	}

	function RemoveSelectedRequests() {
		var totalSelected = getNumberOfSelectedItems();
		
		if (totalSelected<=0) {
			alert("Please select translation request(s) to remove");
		} else if (confirm("Are you sure you want to remove " + totalSelected + 
				" translation request from the job?")) {
			var selectedRequestIds = "";
			var selectitems = document.getElementById("reqids");
			var total = 0;
			for (var i=0; i < selectitems.options.length; i++) {
				if (selectitems[i].selected == true) {
					selectedRequestIds += selectitems[i].value + " ";
				}
			}
			updateRequestList("removeRequest", selectedRequestIds);			
		}
	}

	function ApproveSelectedRequests() {
		var totalSelected = getNumberOfSelectedItems();
		
		if (totalSelected<=0) {
			alert("Please select translation request(s) to approve");
		} else {
			var selectedRequestIds = "";
			var selectitems = document.getElementById("reqids");
			var total = 0;
			var selectedCount = 0;
			for (var i=0; i < selectitems.options.length; i++) {
				if (selectitems[i].selected == true && selectitems[i].innerHTML=="REVIEW_TRANSLATION") {
					selectedRequestIds += selectitems[i].value + " ";
					selectedCount++;
				}
			}
			if (selectedCount==0) {
				alert("None of the selected item(s) is in the 'REVIEW_TRANSLATION' state to get approved.");
			} else if (confirm("" + selectedCount + " of the " + totalSelected + " translation requests are in the 'REVIEW_TRANSLATION' state. " + 
					"Are you sure you want to approve the translation for those request?")) {			
				updateRequestList("approveRequest", selectedRequestIds);
			}
		}
	}
	
	function copySelectedToJob(allowMove) {
		var totalSelected = getNumberOfSelectedItems();
		
		if (totalSelected<=0) {
			alert("Please select translation request(s) to copy " + 
					(allowMove=='true'? "or move " : "") + " to another job.");
		} else {
			var selectedRequestIds = "";
			var selectedCount = 0;
			var selectitems = document.getElementById("reqids");
			var copyToJobElem = document.getElementById("copytojobid");
			var dest = copyToJobElem.value; 
			var total = 0;
			for (var i=0; i < selectitems.options.length; i++) {
				if (selectitems[i].selected == true) {
					selectedRequestIds += selectitems[i].value + " ";
					selectedCount++;
				}
			}
			var msg = "Do you wish to copy " + selectedCount + " translaiton requests " +
					" to ";
			if (dest=='newjob') {
				msg += "a new translation job?";
			} else {
				msg += "translation job '" + 
						copyToJobElem.options[copyToJobElem.selectedIndex].innerHTML + "'?";
			}
			if (!confirm(msg)) {
				return false;
			}
			var action = "copyRequest";
			if (action) {
				var name = '';
				if (dest=='newjob') {
					while (name == '') {
						name = prompt("Please enter the name of the new job:", "");
						if (name==null) {
							return false;
						}
						name = name.trim();
					}
					dest = '';
				}
				var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestCopyPost";
				
				var params = {};
				params['jobid'] = document.getElementById("jobid").value;
				params['action'] = action;
				params['targetJobId'] = dest;
				params['targetJobName'] = name;
				params['_authkey_'] = document.getElementsByName('_authkey_')[0].value;
				
				if (document.getElementById("app")) {
					params['app'] = document.getElementById("app").value;
				}
				if (document.getElementById("siteid")) {
					params['siteId'] = document.getElementById("siteid").value;
				}
				if (selectedRequestIds) {
					params['reqIds'] = selectedRequestIds;
				}
				
				$.ajax({url:posturl,
					method: 'POST',
					data: params,
					success:function(result){
						document.getElementById("jumpjobpane").innerHTML = result;
						document.getElementById("copytojobid").innerHTML = 
								document.getElementById("updated_copytojobid").innerHTML;
						var jumpJobName = document.getElementById("jumpjoblink").innerHTML;
						if (confirm("Successfully copied " + selectedCount + 
								" translation requests to translation job '" + jumpJobName +
								"'. Do you wish to view that job now?")) {
							document.getElementById("jumpjoblink").click();
						}
					},
					error: function(xhr, textStatus, errorThrown){
						alert("Failed to copy the requests: " + errorThrown);
					}
				});
			}
		}
	}
	
	function updateRequestList(actionParams, selectedids) {
		var pagesize_input = document.getElementById("page_size");
		var pagesize = document.getElementById("default_page_size").value;
		if (pagesize_input) {
			pagesize = pagesize_input.value;
		}
		
		var orderbyclause = document.getElementById("orderbyclause").value;
		var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestsList";
		
		var params = {};
		params['jobid'] = document.getElementById("jobid").value;
		params['where'] = document.getElementById("whereclause").value;
		params['firstrec'] = 0;
		params['pagesize'] = pagesize;
		params['_authkey_'] = document.getElementsByName('_authkey_')[0].value;
		
		
		var reqPane = document.getElementById("jobrequests");
		document.getElementById("modal_blocker").style.top = "60px";
		showBlock("modal_blocker");
		var progress = document.getElementById("requestsloadingprogress");
		showBlock("requestsloadingprogress");
		
		if (document.getElementById("app")) {
			params['app'] = document.getElementById("app").value;
			params['page'] = 'jobdetail';
		}
		if (document.getElementById("siteid")) {
			params['siteId'] = document.getElementById("siteid").value;
		}
		if (orderbyclause) {
			params['orderby'] = orderbyclause;
		}
		if (actionParams) {
			params['action'] = actionParams;
		}
		if (selectedids) {
			params['reqIds'] = selectedids;
		}
		
		$.ajax({url:posturl,
			method: 'POST',
			data: params,
			success:function(result){
				document.getElementById("jobrequests").innerHTML=result;
				document.getElementById("request_status_summary").innerHTML = 
					document.getElementById("h_request_status_summary").innerHTML;
				document.getElementById("requestsloadingprogress").style.display = "none";
				document.getElementById("modal_blocker").style.display = "none";
				document.getElementById("request_list_summary").innerHTML =
					document.getElementById("h_request_list_summary").innerHTML;
				document.getElementById("request_list_pages").innerHTML =
					document.getElementById("h_request_list_pages").innerHTML;
				document.getElementById("reqids_section").innerHTML = 
					document.getElementById("updated_reqid_section").innerHTML;
				document.getElementById("updated_reqid_section").innerHTML = '';
				var totalSelected = getNumberOfSelectedItems();	
				updateTotal(totalSelected);
			},
			error: function(xhr, textStatus, errorThrown){
				document.getElementById("requestsloadingprogress").style.display = "none";
				document.getElementById("modal_blocker").style.display = "none";
				alert("Failed to update the requests list: " + errorThrown);
			}
		});
	}
	function unselectItemsOnLoad() {

		var selectedassetids="${cs.assetids}";
		if (selectedassetids != "") {
			unselectAndUncheckAll();
			
			var saSelectedIds = selectedassetids.split(";");
			for (var i=0; i < saSelectedIds.length; i++) {
				selectOrUnselectItem( saSelectedIds[i], true );
				checkOrUncheckBox( saSelectedIds[i], true );
			}
			
			var totalSelected = getNumberOfSelectedItems();	
			updateTotal(totalSelected);
		}
		
	}
	
	function showFilter() {
		var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestFilter" +
			"&jobid=" + document.getElementById("jobid").value;  

		if (document.getElementById("app")) {
			posturl+="&app=" + 	document.getElementById("app").value + "&page=jobdetail";
		}
		if (document.getElementById("siteid")) {
			posturl+="&siteId=" + 	document.getElementById("siteid").value;
		}
		
		$.ajax({url:posturl,
			success:function(result){
				if (document.getElementById("jobrequestfilters")) {
					document.getElementById("jobrequestfilters").innerHTML=result;
				}
				restoreCheckedState();
			},
			error: function(xhr, textStatus, errorThrown){
				alert("Failed to load the request filters: " + errorThrown);
			}
		});
	}

	function showRow(divid) {
		document.getElementById(divid).style.visibility = 'visible';
		document.getElementById(divid).style.display = 'table-row';
	}

	function showTableCell(divid) {
		document.getElementById(divid).style.visibility = 'visible';
		document.getElementById(divid).style.display = 'table-cell';
	}

	function hideDiv(divid) {
		hide(document.getElementById(divid));
	}

	function showElement(elementid) {
		showInline(document.getElementById(elementid));
	}
	
	function hide(element) {
		element.style.visibility = 'hidden';
		element.style.display = 'none';
	}

	function showInline(element) {
		element.style.visibility = 'visible';
		element.style.display = 'inline';
	}

	function showNewFilterCondition(fieldIndex) { 
		showRow('newcondition_field' + fieldIndex + '_tr');
		showElement('hide_newcondition_field' + fieldIndex);
		hideDiv('show_newcondition_field' + fieldIndex);
	}

	function hideNewFilterCondition(fieldIndex) { 
		hideDiv('newcondition_field' + fieldIndex + '_tr');
		showElement('show_newcondition_field' + fieldIndex);
		hideDiv('hide_newcondition_field' + fieldIndex);

		var boxes = document.getElementsByName("field" + fieldIndex + "_operand_checkbox");
		for (var i=0; i < boxes.length; i++) {
			boxes[i].checked = false;
		}

		var inputs = document.getElementsByName("field" + fieldIndex + "_operand");
		for (var i=0; i < inputs.length; i++) {
			inputs[i].value = '';
		}
	}
	
	function switchfilteroperator(fieldIndex) {
		var select = document.getElementById("field" + fieldIndex + "_operatorselector");
		
		if (select) {
			for (var i=0; i<select.options.length; i++) {
				if (!select.options[i].selected) {
					operand0Input = document.getElementById("field" + fieldIndex + '_operand0');
					valueFormat = document.getElementById("field" + fieldIndex + '_operator' + i + "_valueformat");
					if (operand0Input && valueFormat) {
						operand0Input.placeholder = valueFormat.value;
					}
				}
				var rows = document.getElementsByName("field" + fieldIndex + "_operator" + i + "_extraoperands");
				for (var j=0; j < rows.length; j++) {
					if (!select.options[i].selected) {
						hide(rows[j]);
					} else {
						rows[j].style.visibility = 'visible';
						rows[j].style.display = 'table-row';
					}
				}
			}
		}
	}
	
	function applyFilter(numNewFilterFields) {
		var curFilter = JSON.parse(document.getElementById("current_filter").value);
		var newFilter = getNewConditionJson(numNewFilterFields);
		
		if (!newFilter) {
			return false;
		}

		if (removeFilter(curFilter) || (newFilter && newFilter.fields.length>0)) {
			var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestFilter" +
				"&jobid=" + document.getElementById("jobid").value +
				"&newFilter=" + encodeURIComponent(JSON.stringify(newFilter)) + 
				"&currentFilter=" + encodeURIComponent(JSON.stringify(curFilter));  
			
			if (document.getElementById("app")) {
				posturl+="&app=" + 	document.getElementById("app").value + "&page=jobdetail";
			}
			if (document.getElementById("siteid")) {
				posturl+="&siteId=" + 	document.getElementById("siteid").value;
			}
			
			$.ajax({url:posturl,
				success:function(result){
					document.getElementById("jobrequestfilters").innerHTML=result;
					document.getElementById("whereclause").value = document.getElementById("filter_whereclause").value;
					updateRequestList(null, null);
				},
				error: function(xhr, textStatus, errorThrown){
					alert("Failed to load the request filters: " + errorThrown);
				}
			});
		}
	}
	
	function removeFilter(filter) {
		var removed = false;
		var removedFilters=[];
		var boxes = document.getElementsByName("removed_condition_checkbox");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].checked) {
				var idx = parseConditionIndexes(boxes[i].value);
				if (idx.length>2) {
					filter.fields[idx[0]].operators[idx[1]].operands[idx[2]] = "";
					removed = true;
				} else {
					filter.fields[idx[0]].operators[idx[1]].removed = "true";
					removed = true;
				}
			}
		}
		return removed;
	}
	
	function parseDate(val) {
	    var year;
	    var month;
	    var day;
		var dateParts = val.split("-");
		
	    if (dateParts.length == 3) {
		    year = dateParts[0];
		    month = dateParts[1];
		    day = dateParts[2];
	    } else {
	    	dateParts = val.split("/");
		    if (dateParts.length == 3) {
			    year = dateParts[2];
			    month = dateParts[0];
			    day = dateParts[1];
		    } else {
		    	return null;
		    }
	    }
	    
	    if (isNaN(day) || isNaN(month) || isNaN(year))
	        return null;

	    var result = new Date(year, (month - 1), day);
	    if (result == null)
	        return null;
	    if (result.getDate() != day)
	        return null;
	    if (result.getMonth() != (month - 1))
	        return null;
	    if (result.getFullYear() != year)
	        return null;

	    return result;
	}
	
	function validateValue(val, validator) {
		if (validator=='date') {
			return parseDate(val)!=null;	
		} else if (validator=='number') {
			return !isNaN(Number(val));
		} else {
			return true;
		}
	}
	
	function getNewConditionJson(numNewFilterFields) {
		var fields = [];
		var errorElement = null;
		var error = "";
		var hasError = false;
		
		for (var i=0; i<numNewFilterFields; i++) {
			var tr = document.getElementById("newcondition_field"+i + "_tr");
			if (tr && tr.style.display != 'none') {
				var operators = [];
				var operands = [];
				var op;
				var opselector = document.getElementById("field" + i +"_operatorselector");
				
				if (opselector) {
					op = opselector.selectedIndex;
				} else {
					op = 0;
				}

				var validator = document.getElementById("field" + i + "_operator"+ op + "_validator");
				var validatorMessage = document.getElementById("field" + i + "_operator"+ op + "_validatormsg");
				var j = 0;
				var hasEmptyValue = false;
				var hasValue = false;
				do {
					var operand0Input = document.getElementById("field" + i + "_operand0");
					if (j==0 && operand0Input) { 
						operands.push(operand0Input.value);
						if (!operand0Input.value || operand0Input.value.lenght==0) {
							hasEmptyValue = true;
							errorElement = operand0Input; 
						} else if (validator && !validateValue(operand0Input.value, validator.value)) {
							hasError = true;
							errorElement = operand0Input; 
							if (validatorMessage) {
								error = validatorMessage.value + operand0Input.value;
							} else {
								error = "'" + operand0Input.value + "' is not a valid value";
							}
							break;
						} else {
							hasValue = true;
						}
					} else {
						var operandInput = document.getElementById(
								"field" + i + "_operator" + op + "_operand" + j);
						if (operandInput) {
							operands.push(operandInput.value);
							if (!operandInput.value || operandInput.value.lenght==0) {
								hasEmptyValue = true;
								errorElement = operandInput; 
							} else if (validator && !validateValue(operandInput.value, validator.value)) {
								hasError = true;
								errorElement = operandInput; 
								if (validatorMessage) {
									error = validatorMessage.value + operandInput.value;
								} else {
									error = "'" + operandInput.value + "' is not a valid value";
								}
								break;
							} else {
								hasValue = true;
							}
						} else {
							break;
						}
					}	
					j++;
				} while (true);
				
				if (hasEmptyValue && hasValue) {
					error = "Condition entry cannot be empty";
					hasError = true;
				} 
				if (hasError) {
					break;					
				}
				var options = document.getElementsByName("field" + i + "_operand_checkbox");
				for (var k=0; k < options.length; k++) {
					if (options[k].checked) {
						operands.push(options[k].value);
						hasValue=true;
					}
				}
				
				if (hasValue && operands.length>0) {
					operators.push({ operator : op,	operands : operands});
					fields.push({ name : document.getElementById("field" + i + "_displayName").value,
						operators : operators});
				}
			}
		}
		if (hasError) {
			alert(error);
			if (errorElement) errorElement.focus();
			return null;
		} else {
			return { fields : fields };
		}
	}
			
	function cancelFilter(numNewFilterFields) {
		for (var i=0; i<numNewFilterFields; i++) {
			if (document.getElementById("hide_newcondition_field"+i)) {
				hideNewFilterCondition(i);
			}
		}
		var boxes = document.getElementsByName("removed_condition_checkbox");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].checked) {
				var condition_index = parseConditionIndexes(boxes[i].value);
				var operandIndex = -1;
				if (condition_index.length>2) {
					operandIndex = condition_index[2];
				}
				restoreFilterCondition(condition_index[0], condition_index[1], operandIndex);
			}
		}
	}
	
	function parseConditionIndexes(val) {
		return val.split('_');
	}
	
	function clearFilter() {
		if (confirm("Are you sure you want to clear the current filter " + 
				"and display all translation requests in the translaion job?")) {
			showFilter();
			document.getElementById("whereclause").value = "";
			updateRequestList(null, null);
		}
	}
	
	function restoreFilterCondition(fieldIndex, singleConditionIndex, operandIndex) {
		if (operandIndex>=0) {
			showElement("remove_condition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			hideDiv("restore_condition_field"  + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			document.getElementById("current_filter_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).className="current_filter";
			document.getElementById("removedcondition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).checked=false;
		} else {
			showElement("remove_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			hideDiv("restore_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			document.getElementById("current_filter_field" + fieldIndex + "_sc" + singleConditionIndex).className="current_filter";
			document.getElementById("removedcondition_field" + fieldIndex +	"_sc" + singleConditionIndex).checked=false;
		}
	}

	function removeFilterCondition(fieldIndex, singleConditionIndex, operandIndex) {
		if (operandIndex>=0) {
			showElement("restore_condition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			hideDiv("remove_condition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			document.getElementById("current_filter_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).className="removed_filter";
			document.getElementById("removedcondition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).checked=true;
		} else {
			showElement("restore_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			hideDiv("remove_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			document.getElementById("current_filter_field" + fieldIndex + "_sc" + singleConditionIndex).className="removed_filter";
			document.getElementById("removedcondition_field" + fieldIndex +	"_sc" + singleConditionIndex).checked=true;
		}
	}
	
	function switchDescription(aclevel) {
		var descs = document.getElementsByName("analysiscode" + aclevel + "_valuedesc");
		var selectedIndex = document.getElementById("analysiscode" + aclevel + "_value").selectedIndex;
		for (var i=0; i < descs.length; i++) {
			if (descs[i].id=="analysiscode" + aclevel + "_value" + selectedIndex + "_desc") {
				showInline(descs[i]);
			} else {
				hide(descs[i]);
			}
		}
	}
	
	function showRequestDetail(reqId) {
		var rect = document.getElementById("job_detail_table").getBoundingClientRect();
		
		showBlock("modal_blocker");
		document.getElementById("requests_detail").style.top = (-1*rect.top + 60) + "px";
		document.getElementById("requests_detail").style.left = (-1*rect.left + 100) + "px";
		document.getElementById("requests_detail").style.minWidth = "60em"
		document.getElementById("requests_detail").style.minHeight = "33em";
		document.getElementById("requests_detail").style.maxWidth = "60em"
		document.getElementById("requests_detail").style.maxHeight = "33em";
		
		showBlock("requests_detail");
		document.getElementById("requests_detail_content").innerHTML = "Loading translation request detail...";
		
		var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestDetail&requestid="+ reqId;
		
		if (document.getElementById("app")) {
			posturl+="&app=" + 	document.getElementById("app").value;
		}
		if (document.getElementById("siteid")) {
			posturl+="&siteId=" + 	document.getElementById("siteid").value;
		}
		
		$.ajax({url:posturl,
			success:function(result){
				document.getElementById("requests_detail_content").innerHTML = result;
			}});
	}

	function hideRequestDetail() {
		document.getElementById("requests_detail").style.display = "none";
		document.getElementById("modal_blocker").style.display = "none";
	}
</script>
	
</script>

	<script type="text/javascript" src="wemresources/js/WemContext.js"></script>
	<script type="text/javascript">
		//initialize wemcontext before loading dojo layers (see AppBar FIXME)
		//Unfortunately it needs to be AFTER dojo because WemContext relies on xhrGet!
		WemContext.initialize('','http://localhost:8080/cas/logout');
		var wemcontext = WemContext.getInstance();
		// If a new browser window is opened, as a pop up or new tab or new window, and if the Wem top bar is not there.
		// Then global sls js object would not avilable. In that scenario we load the object in two ways based on how the new window opened.
		// If as a pop-up
		if(window.opener) window.fw_sls_obj = window.opener.top.fw_sls_obj;
		// If as a browser tab or new brower window		
		if(!window.opener && window.top === window.self) document.write('<script type="text/javascript" src="ContentServer?user_locale=en-us&pagename=fatwire%2Fui%2Futil%2FGetSLSObj"><' + '/script>');
	</script>




<script type="text/javascript" src='js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: '0'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='wemresources/js/dojopatches.js'></script>

<link href="Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>

		
</head>
<body class="AdvForms">
	
	<satellite:form action="ContentServer" method="post" name="AppForm">
<!-- 
	<INPUT TYPE='HIDDEN' NAME='_authkey_' VALUE='554446B0FA2A97A5208D6158954E06ED9AE51AAFBE5ECAC8511A06205DDB49975CFA03961A21C220A83CC500F7FE9845'>
 -->		
		<input type="hidden" name="_charset_" value="UTF-8" />
		<input type="hidden" name="cs_environment" value="ucform" />
		<input type="hidden" name="cs_formmode" value="WCM" />
		<input type="hidden" name="PubName" value='${cs.pubid}' />
		<input type="hidden" name="siteId" id="siteid" value='${siteid}' />
		




<ics:callelement element="CustomElements/CT/HeaderTab"/>



<script language="javascript">
function getActiveView() {
	// Clear error message, if any
	var activeView, docId;
	SitesApp = parent.SitesApp;
	if (SitesApp) {
	//	if (!parent.dojo || !SitesApp) alert("No dojo or sitesapp");;
		activeDoc = docId !== undefined ?
		SitesApp.getDocument(docId):
		SitesApp.getActiveDocument();
		activeView = activeDoc.get('activeView');
		return activeView;
	} else {
		return null;
	}
}

<c:if test="${not empty message}">
	var activeView = getActiveView();
	activeView.message("${message}");
</c:if>
<c:if test="${not empty error}">
	activeView.error("${error}");
</c:if>

function validateCopyBack() {
	return confirm("Are sure you want to directly copy all assets in job '" + document.getElementById("jobname").value + 
			"' to the target locale(s)? If you proceed, the assets will not be sent to translation provider and " +
					"will not be translated. Existing target assets will be overwritten.");
}

function validateDelete() {
	return confirm("Are sure you want to delete job '" + document.getElementById("jobname").value + "'?");
}

function validateArchiveActive() {
	return confirm("Job '" + document.getElementById("jobname").value + 
			"' is still active. Are you sure you want to archive it?" +
			"If you archive it now, you will no longer see it in your job list, but your translation " +
			"provider may still be working on it. Translation content sent back for archived job will NOT be processed.");
}

function validateArchive() {
	return confirm("Are you sure you want to archive job '" + document.getElementById("jobname").value + "'?");
}

function validateUnarchive() {
	return confirm("Are you sure you want to unarchive job '" + document.getElementById("jobname").value + "'?");
}

function validateSend() {
	var error = false;
	var msg = "";
	
	if (document.getElementById("jobname").value == "") {
		msg += "Job name is a required field. ";
		document.getElementById("jobname").focus();
		error = true;
	}
	
	var vendorSelect = document.getElementById("vendor");
	
	if (vendorSelect == null || vendorSelect.value == "") {
		if (vendorSelect) { 
			document.getElementById("vendor").focus();
		}
		msg += "Translation Provider is a required field. ";
		error = true;
	}
	
	var poNumberSelect = document.getElementById("ponumber");
	if (document.getElementById("porequired").value == "true" && 
			poNumberSelect && poNumberSelect.options[poNumberSelect.selectedIndex].value== "") {
		msg += "PO Number is a required field. ";
		document.getElementById("ponumber").focus();
		error = true;
	}
	
	if (error) {
		alert(msg);
		return false;
	} else {
		var metadataValidators = document.getElementsByName("metadata_validator");
		for (var i=0; i < metadataValidators.length; i++) {
			var result = eval(metadataValidators[i].value);
			if (result) {
				alert(result);
				return false;
			}
		}
		return confirm("Are you sure you want to send out job '" + document.getElementById("jobname").value + 
				"' to the translation provider '" + vendorSelect[vendorSelect.selectedIndex].innerHTML + "' now?'")
	}
}

function validateSave() {
	if (document.getElementById("jobname").value == "") {
		document.getElementById("jobname").focus();
		alert( "Job name is a required field. ");
		return false;
	} else {
		return true;
	}
}

function closePopup() {
	hideRequestDetail();
}

function showDiv(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'block';
}

function hideDiv(divid) {
	document.getElementById(divid).style.visibility = 'hidden';
	document.getElementById(divid).style.display = 'none';
}

function toggleDiv(vendorsig, divname) {
	var divobj = document.getElementById(divname);
	if (vendorsig.lastIndexOf("LIONBRIDGE_FREEWAY:", 0) === 0) {
		showDiv(divname);
	} else {
		hideDiv(divname);
	}
}

function init(vendorid, startingid) {
	toggleProviderMetadata(vendorid);
	setFirstRecord(startingid);
	showFilter();
    
    $( "#copytojobid" ).on( "change", function() {
    	var thisval = document.getElementById("copytojobid").value;
      	if (thisval == '') {
      		document.getElementById("copyToJob").disabled=true;
      	} else {
      		document.getElementById("copyToJob").disabled=false;
      	}
    });
}

function toggleProviderMetadata(vendorid) {
	if (vendorid==null || vendorid=='') {
		document.getElementById("providermetadata").innerHTML='';
	} else {
		document.getElementById("providermetadata").innerHTML='Loading provider metadata...';
		var iscurrentprovider = 'false';
		
		if (vendorid == encodeURI(document.getElementById("providerid").value)) {
			iscurrentprovider = 'true';
		}

		var posturl = 'ContentServer?pagename=CustomElements/CT/ProviderMetadata&providerid=' + vendorid +
			'&editable=' + document.getElementById("editable").value + 
			'&providermetadata=' + encodeURI(document.getElementById("curmetadata").value) +
			'&iscurrentprovider=' + iscurrentprovider;
		
 		$.ajax({url:posturl,success:function(result){
			document.getElementById("providermetadata").innerHTML=result;
		}});
	}
}
</script>

<% if ("WEM".equals(ics.GetVar("app"))) { %>
	<c:set var="app" value="WEM"/>
	<input type="hidden" name="app" id="app" value="WEM"/>
<% } %>

	<input type="hidden" name="docId" value="0"/>
	<input type="hidden" name="pagename" value="CustomElements/CT/JobPost"/>
	<input type="hidden" name="jobid" id="jobid" value="${job.id}"/>
	<input type="hidden" name="providerid" id="providerid" value="${job.translationProvider.id}"/>
	<input type="hidden" name="curmetadata" id="curmetadata" value="${providerMetadata}"/>
	<input type="hidden" name="page_firstrecord" id="page_firstrecord" value="${page_firstrecord}"/>
	<input type="hidden" name="orderbyclause" id="orderbyclause" value="nativeSourceId ASC"/>
	<input type="hidden" name="whereclause" id="whereclause" value=""/>
	<input type="hidden" name="default_page_size" id="default_page_size" value="${pagesize}"/>
	<input type="hidden" id="porequired" value="${poRequired}"/>
		
	<c:choose>
		<c:when test="${job.status ne 'CREATED'}"><input type="hidden" name="editable" id='editable' value="false"/></c:when>
		<c:otherwise><input type="hidden" name="editable" id='editable' value="true"/></c:otherwise>
	</c:choose>
	
	<fmt:formatDate value="${job.dueDate}" var="dueDateFormatted" type="date" pattern="yyyy-MM-dd" />

	<p id="reqids_section" style="visibility:hidden;display:none;">
	<select multiple name="reqids" id="reqids" width="15" size="10">
	<c:forEach var="req" items="${translationRequests}">
		<option value="${req.id}">${req.status}</option>
	</c:forEach>
	</select>
	</p>
	
<div id="requestsloadingprogress" class="popup_panel_center">
	<img src="js/fw/images/ui/wem/loading.gif" style="h-align:center;"></img>
	<p>Loading translation requests for job ...</p></div>
<div id="requests_detail" class="popup_panel" 
		style="min-width:60em;min-height:20em;padding:1em">
<table style="width:100%;height:18em">
<tr><td>
<div id="requests_detail_content">Loading translation request detail ...</div></td></tr>
<tr><td style="width:100%;text-align:center;">
	<input type="button" value="Close" onClick="hideRequestDetail();" class="f1image-medium"/></td></tr>
</table>
</div>
<div class="blocking_overlay" id="modal_blocker" onclick="closePopup()"></div>
<TABLE style="margin: 1em; width=100%" BORDER="0" CELLPADDING="0" CELLSPACING="0" >
<c:choose><c:when test="${'WEM' eq app}">
<render:getpageurl outstr="listurl" pagename="CustomElements/CT/JobListFront">
		<render:argument name="app" value="WEM"/>
		<render:argument name="selectSite" value="${siteid}"/>
</render:getpageurl> 
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/JobFront">
		<render:argument name="jobid" value='<%=ics.GetVar("jobid")%>'/>
		<render:argument name="app" value="WEM"/>
		<render:argument name="siteId" value="${siteid}"/>
</render:getpageurl> 
</c:when><c:otherwise>
<render:getpageurl outstr="listurl" pagename="CustomElements/CT/JobListFront"/>
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/JobFront">
		<render:argument name="jobid" value='<%=ics.GetVar("jobid")%>'/>
</render:getpageurl> 
</c:otherwise></c:choose>
<TR><TD><TABLE width="100%" id="job_detail_table">
	<TR><TD width="50%"><SPAN CLASS="title-text">Job Details</SPAN>&nbsp;&nbsp;
	<a href='<ics:getvar name="refreshurl"/>'><img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a></TD>
	<TD align="right" width="50%"><a href='<ics:getvar name="listurl"/>'>All Translation Jobs</a></TD></TR>
</TABLE></TD></TR>
<tr><td><img height="5" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>
	<TABLE id="jobinfotable" CLASS="width-inner-100" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	<cols>
		<col width="20%"/>
		<col width="2%"/>
		<col width="70%"/>
		<col>
	</cols>
	<c:if test="${app eq 'WEM'}">
		<tr><td colspan="4"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		<TR><TD CLASS="form-label-text">Site:</TD>
		<TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset">${jobsitename}</TD></TR>
	</c:if>
	<tr><td colspan="4"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Job ID:</TD>
		<TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset">${job.id}</TD>
		<TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset filter-sidebar" align="right"> 	
		<c:choose><c:when test="${job.status eq 'CREATED'}">
			<input type="submit" name="Submit" value="Save Job" class="f1image-medium" onclick='document.getElementById("action").value="save"; return validateSave();'/> 
		</c:when><c:otherwise><c:if test="${not job.archived}">
			<c:choose><c:when test="${not job.status.active}">
				<input type="submit" name="Submit" value="Archive Job" class="f1image-medium" onclick='document.getElementById("action").value="archive"; return validateArchive();'/>
			</c:when><c:otherwise><c:if test="${'WEM' eq app}"> 
				<input type="submit" name="Submit" value="Archive Job" class="f1image-medium" onclick='document.getElementById("action").value="archive"; return validateArchiveActive();'/>
			</c:if>
			</c:otherwise></c:choose></c:if>
			<c:if test="${job.archived and 'WEM' eq app}">
				<input type="submit" name="Submit" value="Unarchive Job" class="f1image-medium" onclick='document.getElementById("action").value="unarchive"; return validateUnarchive();'/>
			</c:if>
		</c:otherwise></c:choose></TD>
	</TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Job Name:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset">
			<c:choose>
				<c:when test="${job.status ne 'CREATED'}"><c:out value="${job.name}" escapeXml="true"/><input type="hidden" name="jobname" id="jobname" value="${job.name}"/></c:when>
				<c:otherwise><input type="text" name="jobname" id="jobname" value="${job.name}" size="40"/></c:otherwise>
			</c:choose>
		</TD>
		<TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif">
		<TD class="form-inset filter-sidebar" align="right" >	<c:if test="${job.status eq 'CREATED'}">
			<input type="submit" name="Submit" value="Send Now" class="f1image-medium" <c:if test="${empty translationRequests}">disabled</c:if> 
				onclick='document.getElementById("action").value="send"; return validateSend();'/>
		</c:if></TD>
	</TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR>
		<TD CLASS="form-label-text">Description:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset">	<c:choose>
			<c:when test="${job.status ne 'CREATED'}">
				${jobdescription}
			</c:when>
			<c:otherwise><textarea rows="4" cols="50" name="jobdesc" id="jobdesc" >${job.description}</textarea></c:otherwise>
		</c:choose></TD>
		<TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset filter-sidebar" align="right" style="width:100%"> <c:if test="${job.status eq 'CREATED'}">
			<input type="submit" name="Submit" value="Copy Back" class="f1image-medium" 
					onclick='document.getElementById("action").value="copyback"; return validateCopyBack();'/> 
		</c:if></TD>
	</TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR>
		<TD CLASS="form-label-text">Status:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset" <c:if test="${job.inError}">style="color:red"</c:if>> ${job.status.displayName}<c:if test="${job.archived}">&nbsp;(Archived)</c:if>
			<c:if test="${job.inError}">(error)</c:if></TD>
		<TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset filter-sidebar" align="right" style="width:100%"> <c:if test="${job.status eq 'CREATED'}">
			<input type="submit" name="Submit" value="Delete Job" class="f1image-medium" onclick='document.getElementById("action").value="delete"; return validateDelete();'/> 
		</c:if></TD>
	</TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<c:if test="${showteam eq 'true'}">	
	<TR><TD CLASS="form-label-text">Team:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset">${job.team.name}</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	</c:if>
	<TR><TD CLASS="form-label-text">Created:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset">${job.createdUser} (<fmt:formatDate value="${job.createdDate}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />)
	</TD></TR>
	<c:if test="${job.status ne 'CREATED' && not empty job.submittedUser}">
		<TR><TD CLASS="form-label-text">Submitted by:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
			<TD class="form-inset">${job.submittedUser} (<fmt:formatDate value="${job.submittedDate}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />)
		</TD></TR>
	</c:if>
	<TR><TD CLASS="form-label-text">Last updated:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset"><fmt:formatDate value="${job.updatedDate}" type="date" pattern="yyyy-MM-dd HH:mm:ss" />
	</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	
	<c:if test="${not empty poNumbers}"><TR><TD CLASS="form-label-text">PO Number:</TD>
		<TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset">
		<c:choose>
			<c:when test="${job.status ne 'CREATED'}">${job.poReference}
				<input type="hidden" name="ponumber" id="ponumber" value="${job.poReference}"/>
			</c:when><c:otherwise><select name="ponumber" id="ponumber">
				<option value=""></option>
				<c:forEach var="ponumber" items="${poNumbers}" varStatus="itPoNumbers">
					<option value="${ponumber.name}"
						<c:if test="${ponumber.name eq job.poReference}">selected</c:if>
					><c:out value="${ponumber.name} : ${ponumber.desc}" escapeXml="true"/></option>
				</c:forEach>
			</select> <c:if test="${poRequired eq 'true'}">&nbsp;*</c:if> </c:otherwise>
		</c:choose>
	</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr></c:if>
	
	<TR><TD CLASS="form-label-text">Due Date (yyyy-mm-dd):</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset">
		<c:choose>
			<c:when test="${job.status ne 'CREATED'}"><c:choose>
				<c:when test="${job.dueDate.time < now.time}"><b><font color="red">${dueDateFormatted}</font></b></c:when>
				<c:otherwise>${dueDateFormatted}</c:otherwise></c:choose>
				<input type="hidden" name="duedate" id="duedate" value="${dueDateFormatted}"/></c:when>
			<c:otherwise><input type="text" name="duedate" id="duedate" value="${dueDateFormatted}" placeholder="yyyy-mm-dd"/></c:otherwise>
		</c:choose>
	</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Translation Provider:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset">
		<c:if test="${not empty translationVendors}">
		<c:choose>
			<c:when test="${job.status ne 'CREATED'}">
				${job.translationProvider.name}<input type="hidden" name="vendor" id="vendor" value="${job.translationProvider.id}"/>
			</c:when> <c:otherwise>
				<select name="vendor" id="vendor" 
						onChange="toggleProviderMetadata(this.value);">
					<option value=""></option>
					<c:forEach var="vendor" items="${translationVendors}">
						<c:if test="${vendor.valid or job.translationProvider.name eq vendor.name}">
							<option value="${vendor.id}"
								<c:if test="${job.translationProvider.name eq vendor.name or 
										empty job.translationProvider and fn:length(translationVendors) eq 1}">selected</c:if>
							>${vendor.name} <c:if test="${not vendor.valid}">(invalid)</c:if></option>
						</c:if>
					</c:forEach>
					<c:forEach var="vendor" items="${translationVendors}">
						<c:if test="${not vendor.valid and not (job.translationProvider.name eq vendor.name)}">
							<option value="${vendor.id}" disabled>${vendor.name} (invalid)</option>
						</c:if>
					</c:forEach>
				</select>
			</c:otherwise>
		</c:choose>
		<br/>
		</c:if>
		<div id="providermetadata"></div>
		<c:if test="${empty translationVendors}">
			<font style="color:red;">An administrator has not yet configured any providers.</font>
		</c:if>
	</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Send Modified Fields Only: </TD><TD></TD><TD class="form-inset">
			<c:choose><c:when test="${job.status eq 'CREATED'}">
				<input type="checkbox" name="sendonlymodified" id="sendonlymodified"
					<c:if test="${job.translateModifiedOnly}">checked</c:if> />
			</c:when><c:otherwise>${job.translateModifiedOnly}</c:otherwise></c:choose>
					
	</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Assets in this Job:</TD><TD></TD>
	<TD colspan="3" class="form-inset" id="request_status_summary">
		<c:choose> <c:when test="${empty translationRequests}">None</c:when></c:choose>
	</td></TR><TR><TD/><TD/><TD>
		<c:if test="${not empty translationRequests}">
			<SPAN id="request_list_summary"></SPAN><SPAN id="select_total">0</SPAN> selected.</td>
<td/>
</tr>
<tr> <td colspan="3">
	<table style="width: 100%"><tr><td>
	<SPAN id="request_list_pages"></SPAN></td><td align="right">				
		<input type="button" name="selectall" id="selectall" value="Select All" onClick="selectAndCheckAll();" class="f1image-medium"/>
		<input type="button" name="unselectall" id="unselectall" value="Unselect All" onClick="unselectAndUncheckAll();" class="f1image-medium"/>
		<c:if test="${job.status eq 'CREATED'}">
			<input type="button" name="removerequests" id="removerequests" value="Remove Selected" onClick="RemoveSelectedRequests();" class="f1image-medium"/>
		</c:if>
		<c:if test="${not job.archived and job.status.active and job.status ne 'CREATED'}">
		<input type="button" name="approverequests" id="approverequests" value="Approve Selected" onClick="ApproveSelectedRequests();" class="f1image-medium"/>
		</c:if>
	</table>
</td><td/><td>
	<c:choose> <c:when test="${job.status eq 'CREATED'}">
		<input type="button" name="copyToJob" id="copyToJob" value="Copy to" class="bt-image-small" 
				onclick="return copySelectedToJob(event, 'true');" disabled/>
	</c:when><c:otherwise>
		<input type="button" name="copyToJob" id="copyToJob" value="Copy to" class="bt-image-small" 
				onclick="return copySelectedToJob(event, 'false');" disabled/>
	</c:otherwise> </c:choose>			
	<select name="copytojobid" id="copytojobid" width="12em" style="max-width:55%;">
        <option value=""></option>
		<option value="newjob">Create New Job</option>
		<option disabled>----------------------------</option>
		<c:if test="${fn:length(jobs) eq 0}">
			<option disabled>-- No existing job --</option>
		</c:if>
		<c:forEach var="otherjob" items="${jobs}">
			<c:if test="${otherjob.status eq 'CREATED' && otherjob.id ne job.id}">
	    		<option value="${otherjob.id}">${otherjob.name}</option>
	    	</c:if>
       	</c:forEach>
	</select>
	<p style="display:none" id="jumpjobpane"></p>
</td>
</tr>
<tr><td colspan="3" id="jobrequests" valign="top" class="width-inner-100"><a name="requests"/></td>
<td/><td class="filter-sidebar" valign="top"> 
<div id="filter-div">	
<input type="hidden" id="filtersjson"/>  
<DIV CLASS="title-text" align="right">Filter</DIV><hr style="font-size: 0px;"/>
<SPAN id="jobrequestfilters" valign="top" align="right"></SPAN>
</div></tr>
</table>
</td></tr>

</c:if>
		
		
	</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	</TABLE>
</TD></TR>
</TABLE>
<input type="hidden" name="action" id="action" value=""/>
<input type="hidden" name="archive" id="archive" value="0"/>




</satellite:form>

<ics:callelement element="CustomElements/CT/FooterTab"/>

		
	<script>
		var num = document.AppForm.length, i;
		document.AppForm.encoding="application/x-www-form-urlencoded";
		for (i=0; i < num; i++) {
			if (document.AppForm.elements[i].type=="file") document.AppForm.encoding="multipart/form-data";
		}
		(function() {
			var _alert = window.alert;
			if (SitesApp) {
				window.alert = function() {
					var out = SitesApp.getActiveView() || SitesApp;
					out.clearFeedback();
					out.clearMessage();
					out.message(arguments[0], arguments[1] || "error");
					//fw/ui/controller/AdvancedController subscribes to this event
					parent.dojo.publish('/fw/ui/form/alert',[]);
					return true;
				};
			}
		})();

		var vendorElem = document.getElementById("vendor");
		var vendor = null;
		if (vendorElem) {
			vendor = vendorElem.value;
		}
		window.onload = init(vendor, document.getElementById("page_firstrecord").value);

	</script>
	
</body>
</html>
</gsf:root>
</cs:ftcs>