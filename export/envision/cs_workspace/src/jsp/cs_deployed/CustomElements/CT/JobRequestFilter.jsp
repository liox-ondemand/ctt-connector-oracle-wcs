<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs> <gsf:root action="class:com.claytablet.wcs.ui.UITranslationRequestFilterManager">
	<input type="hidden" id="current_filter" value="${currentFilterJson}"/>
	<input type="hidden" name="filter_whereclause" id="filter_whereclause" value="${filter.whereClause}"/>
	<c:choose> <c:when test="${filter.totalRequestCount lt 11 and empty filter.whereClause}">
		Filter only applies when there are more than 10 assets.
	</c:when> <c:otherwise>
	<table class="filter-sidebar">
		<tr><td><b>Current conditions:</b></td>
			<c:choose><c:when test="${fn:length(filter.fieldConditions) eq 0}"><td>&nbsp;&nbsp;none</td>
			</c:when><c:otherwise>
				<td align="right"><input type="button" name="clearfilter" id="clearfilter" value="Clear" 
						onClick="clearFilter();" class="bt-image-small"/></td>
			</c:otherwise></c:choose>
		</tr>
	</table>
	<table class="filter-sidebar">
		<input type="hidden" value="${fn:length(allFields)}"/>
		<c:forEach var="fieldInfo" items="${allFields}" varStatus="fld">
			<c:set var="field" value="${fieldInfo.field}"/>
			<c:set var="condition" value="${fieldInfo.condition}"/>
			<c:set var="isOptions" value="false"/>
			<tr><td style="text-align:right">
			<c:choose><c:when test="${fn:length(field.operators) eq 0}">
					<span class="disable_filter_field">${field.displayName}</span></c:when>
			<c:otherwise>
				<span><b>${field.displayName}</b></span>&nbsp;
				<input type="hidden" id="field${fld.index}_displayName" 
						value="${field.displayName}"/>
				<input type="hidden" id="field${fld.index}_operatorCount" 
						value="${fn:length(field.operators)}"/>
				<span id="show_newcondition_field${fld.index}" class="add_filter_ctrl"
					onclick="showNewFilterCondition(${fld.index})" title="Add new condition">+</span>
				<span id="hide_newcondition_field${fld.index}" class="remove_filter_ctrl" style="display:none"
					onclick="hideNewFilterCondition(${fld.index})" title="Cancel new condition">x</span></td></tr>
			<tr id="newcondition_field${fld.index}_tr" style="display:none;">
				<td style="text-align:right" align="right"><table style="width: 100%">
				<c:set var="operator0ValueFormat" value="${field.operators[0].metadata.valueFormat}"/>
				<c:forEach var="operator" items="${field.operators}" varStatus="op">
					<c:if test="${not empty operator.metadata.validationType}">
						<input type="hidden" id="field${fld.index}_operator${op.index}_validator" 
								value="${operator.metadata.validationType}"/>
					</c:if>
					<c:if test="${not empty operator.metadata.validationErrorMessage}">
						<input type="hidden" id="field${fld.index}_operator${op.index}_validatormsg" 
								value="${operator.metadata.validationErrorMessage}"/>
					</c:if>
					<c:if test="${not empty operator.metadata.valueFormat}">
						<input type="hidden" id="field${fld.index}_operator${op.index}_valueformat" 
								value="${operator.metadata.valueFormat}"/>
					</c:if>
				</c:forEach>
				<c:choose> <c:when test="${fn:length(field.operators) lt 2}"> 
					<c:forEach var="operator" items="${field.operators}" varStatus="op">
					<tr><c:choose> <c:when test="${fn:length(operator.acceptedOperands) eq 0}">
						<td align="right" style="text-align:right;width: 100%;"><table>						
							<input type="hidden" id="field${fld.index}_operator${op.index}_operandsCount" 
									value="${operator.metadata.operandsCount}"/>
							<c:forEach var="dispName" items="${operator.metadata.displayNameList}" varStatus="opd">
								<tr><td style="text-align:right;">${dispName}</td><td>&nbsp;
								<input id="field${fld.index}_operator${op.index}_operand${opd.index}"
									<c:if test="${not empty operator.metadata.valueFormat}"> placeholder='${operator.metadata.valueFormat}'</c:if>
									name="field${fld.index}_operand" type="text" size="15"></input></td></tr>
							</c:forEach>
						</table></td>
					</c:when><c:otherwise>
						<td align="right" style="width: 100%;">
							<table style="align: right;">
							<c:forEach var="option" items="${fieldInfo.availableOperands}" varStatus="opdoption"><tr><td>
								<input id="field${fld.index}_operator${op.index}_option${opdoption.index}" 
										name="field${fld.index}_operand_checkbox" type="checkbox" value="${option}"/></td>
								<td style="text-align: right;">&nbsp;${option}
							</td></tr></c:forEach>
							<c:set var="isOptions" value="true"/>
							</table></td>
						</c:otherwise></c:choose>										
					<td></tr>
					</c:forEach>
				</c:when><c:otherwise>
					<tr><td align="right"><table><tr><td style="text-align:right;">
					<select id="field${fld.index}_operatorselector" style="text-align: right" onchange="switchfilteroperator(${fld.index})">
						<c:forEach var="operator" items="${field.operators}"  varStatus="op">
							<option value="${operator.metadata.displayName}" 
									<c:if test="${op.index eq 0}">selected</c:if>
							>${operator.metadata.displayName}</option>
						</c:forEach>
					</select></td><td>
					&nbsp;<input id="field${fld.index}_operand0" name="field${fld.index}_operand" type="text" size="15"
							placeholder="${operator0ValueFormat}"></input> </td></tr>
					<c:forEach var="operator" items="${field.operators}" varStatus="op">
						<c:if test="${operator.metadata.operandsCountLimited and fn:length(operator.metadata.displayNameList) gt 1}">
							<c:forEach var="dispName" items="${operator.metadata.displayNameList}"  varStatus="opd">
								<c:if test="${opd.index ne 0}">
									<tr id="field${fld.index}_operator${op.index}_extraoperands${opd.index}" 
											name="field${fld.index}_operator${op.index}_extraoperands" style="display:none;">
									<td style="text-align:right;">${dispName}</td><td>&nbsp; 
									<input id="field${fld.index}_operator${op.index}_operand${opd.index}" 
											<c:if test="${not empty operator.metadata.valueFormat}">
													placeholder='${operator.metadata.valueFormat}'</c:if>
											name="field${fld.index}_operand" type="text" size="15"></input>
									</td></tr>
								</c:if>
							</c:forEach>
						</c:if>
					</c:forEach>
					</table></c:otherwise></c:choose></td></tr>
			</table></c:otherwise></c:choose></td></tr>
			<c:if test="${not empty condition}">
				<c:set var="hasCondition" value="true"/>
				<c:forEach var="sc" items="${condition.singleConditions}" varStatus="sc_loop">
					<tr><td style="text-align:right">
					<c:choose>
					<c:when test="${isOptions}">
						<c:forEach var="operand" items="${sc.operandStrings}" varStatus="operands">
							<input id="removedcondition_field${fld.index}_sc${sc_loop.index}_op${operands.index}" 
									name="removed_condition_checkbox" type="checkbox" style="display:none" 
									value="${fld.index}_${sc_loop.index}_${operands.index}"/>
							<SPAN id="current_filter_field${fld.index}_sc${sc_loop.index}_op${operands.index}" class="current_filter">
								&nbsp;&nbsp;${operand}
							</SPAN>
							<span id="restore_condition_field${fld.index}_sc${sc_loop.index}_op${operands.index}" 
									class="add_filter_ctrl" style="display:none" 
									onclick="restoreFilterCondition(${fld.index}, ${sc_loop.index}, ${operands.index})">+</span>
							<span id="remove_condition_field${fld.index}_sc${sc_loop.index}_op${operands.index}" 
									class="remove_filter_ctrl"
									onclick="removeFilterCondition(${fld.index}, ${sc_loop.index}, ${operands.index})">x</span>
						</c:forEach>
					</c:when><c:otherwise>
						<SPAN id="current_filter_field${fld.index}_sc${sc_loop.index}" class="current_filter">
							<input id="removedcondition_field${fld.index}_sc${sc_loop.index}" 
									name="removed_condition_checkbox" type="checkbox" style="display:none" 
									value="${fld.index}_${sc_loop.index}"/>
							<c:forEach var="operand" items="${sc.operandStrings}" varStatus="operands">
								<c:if test="${fn:length(sc.operator.metadata.displayNameList) gt operands.index}">
									<SPAN class="current_filter_operator">
										&nbsp;&nbsp;${sc.operator.metadata.displayNameList[operands.index]}
									</SPAN>
								</c:if>
								&nbsp;&nbsp;${operand}
							</c:forEach>
						</SPAN>
						<span id="restore_condition_field${fld.index}_sc${sc_loop.index}" 
								class="add_filter_ctrl" style="display:none" title="Restore existing condition" 
								onclick="restoreFilterCondition(${fld.index}, ${sc_loop.index}, -1)">+</span>
						<span id="remove_condition_field${fld.index}_sc${sc_loop.index}" class="remove_filter_ctrl"
								title="Remove existing condition" 
								onclick="removeFilterCondition(${fld.index}, ${sc_loop.index}, -1)">x</span>
					</c:otherwise></c:choose>
				</td></tr></c:forEach>
			</c:if>
		</c:forEach>
		<tr><td align="right">
			<input type="button" name="applyfilter" id="applyfilter" value="Apply" 
					onClick="applyFilter(${fn:length(allFields)});" class="bt-image-small"/>
			<input type="button" name="cancelfilter" id="cancelfilter" value="Cancel" 
					onClick="cancelFilter(${fn:length(allFields)});" class="bt-image-small"/>
		</td></tr>
	</table>
	</c:otherwise></c:choose>
</gsf:root></cs:ftcs>
