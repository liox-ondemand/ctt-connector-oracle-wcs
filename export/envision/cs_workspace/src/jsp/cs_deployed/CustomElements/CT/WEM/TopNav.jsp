<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="assetset" uri="futuretense_cs/assetset.tld"
%><%@ taglib prefix="commercecontext" uri="futuretense_cs/commercecontext.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="listobject" uri="futuretense_cs/listobject.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="searchstate" uri="futuretense_cs/searchstate.tld"
%><%@ taglib prefix="siteplan" uri="futuretense_cs/siteplan.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIAdminAuthManager">



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<HTML><HEAD><META http-equiv="Pragma" content="No-cache">

<script type="text/javascript">
	function getLocale(loc) {
		//convert from CS's locale notation format to the format Dojo NLS uses.
		//(e.g. en_US -> en-us, fr_FR -> fr-fr, etc.)
		return loc.toLowerCase().replace('_', '-');
	}
	var djConfig = {
		fw_csPath: '<%=ics.GetProperty("ft.cgipath")%>',
		pubName: 'FirstSiteII',
		parseOnLoad: true,
		locale: getLocale('en_US')
	};
</script>


	



	<script type="text/javascript" src="<%=ics.GetProperty("ft.cgipath")%>wemresources/js/WemContext.js"></script>
	<script type="text/javascript">
		//initialize wemcontext before loading dojo layers (see AppBar FIXME)
		//Unfortunately it needs to be AFTER dojo because WemContext relies on xhrGet!
		WemContext.initialize('<%=ics.GetProperty("ft.cgipath")%>','http://localhost:8080/cas/logout');
		var wemcontext = WemContext.getInstance();
		// If a new browser window is opened, as a pop up or new tab or new window, and if the Wem top bar is not there.
		// Then global sls js object would not avilable. In that scenario we load the object in two ways based on how the new window opened.
		// If as a pop-up
		if(window.opener) window.fw_sls_obj = window.opener.top.fw_sls_obj;
		// If as a browser tab or new brower window		
		if(!window.opener && window.top === window.self) document.write('<script type="text/javascript" src="<%=ics.GetProperty("ft.cgipath")%>ContentServer?user_locale=en-us&pagename=fatwire%2Fui%2Futil%2FGetSLSObj"><' + '/script>');
	</script>




<script type="text/javascript" src='<%=ics.GetProperty("ft.cgipath")%>js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: 'null'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='<%=ics.GetProperty("ft.cgipath")%>wemresources/js/dojopatches.js'></script>












<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/toolbar.css" rel="styleSheet" type="text/css">
<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript"> var sst = "true"; var enableAdminTab = "false"; 
function popNewWindow(winURL)
{
    editwindow=window.open(winURL+"&showSiteTree="+parent.frames['XcelWorkFrames'].showSiteTree,"_blank");
}

function showRoles()
{
    alert("The user fwadmin is assigned the\nfollowing roles in the site FirstSite II:\n" +  "* ArtworkEditor\n" +  "* GeneralAdmin\n" +  "* CTAdmin\n" +  "* Approver\n" +  "* ContentEditor\n" +  "* WorkflowAdmin\n" +  "* AdvancedUser\n" +  "* SiteAdmin\n" +  "* MarketingAuthor\n" +  "* CTUser\n" +  "* MarketingEditor\n" +  "* ContentAuthor\n" +  "* ProductAuthor\n" +  "* ProductEditor\n" +  "* SitesUser\n" +  "* DocumentAuthor\n" +  "* DocumentEditor\n" +  "* Designer\n" +  "* ArtworkAuthor\n" +  ""); } </SCRIPT><SCRIPT Language="JavaScript">
	var isNew = 0;
	var isNS4 = 0;
	var isIE4 = 0;
	var isIE5 = 0;
	var isNS5 = 0;
	var isID  = 0;
	var docObj = "";
	var styleObj = "";

	// Determine browser versions
	var brow = ((navigator.appName) + (parseInt(navigator.appVersion)));
	var isat = navigator.userAgent.indexOf("MSIE");
	var ieVer = -1;
	if (isat > -1)
	{
		// IE
		isat += 5;
		var end = navigator.userAgent.indexOf(";", isat);
		if (end > isat)
			ieVer = parseInt(navigator.userAgent.substring(isat, end));
	}

	if (ieVer == 4)
	{isIE4 = 1;}
	if (ieVer == 5)
	{isIE5 = 1;}
	if (ieVer == 6)
	{isIE5 = 1;}
	if (ieVer > 6)
	{isNew = 1;}

	if (parseInt(navigator.appVersion) >= 5) { isNew=1; }
	else if (brow=="Netscape4") {isNS4=1;}

	if (brow=="Netscape5") {isNS5=1;}

	// Set docObj and styleObj
	if (isNS4 || isIE4 || isIE5 ||isNew) {
		docObj = (isNS4) ? 'document' : 'document.all';
		styleObj = (isNS4) ? '' : '.style';
		if (isIE5 || isNS5 || isNew) {isID=1;}
	}
 function toggleTree() { 	
		var pURL = "<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=OpenMarket%2FXcelerate%2FUIFramework%2FShowWorkFrames";
		var thisPage = "&ThisPage=ShowStartMenuItems";
		var origBase = pURL + thisPage ;
    
    ReverseContentDisplay("tab7");
    if (parent.frames['XcelWorkFrames'].showSiteTree=="true")
    {
        parent.frames['XcelWorkFrames'].showSiteTree="false";
    }
    else
    {
        parent.frames['XcelWorkFrames'].showSiteTree="true";
        clearToolbarHistory();
    }
    parent.frames['XcelWorkFrames'].location = origBase + "&showSiteTree=" + parent.frames['XcelWorkFrames'].showSiteTree;
     } 
var hist = new Array();


function Asset(type,id,name)
{
    this.type = type;
    this.id = id;
    this.name = name;
}

function addToHistory(atype,aid,aname)
{
    var len = hist.length;
    var found = false;
    for (i=0; i < len; i++)
    {
        if (hist[i].id == aid)
        {
            for (j=i; j < len-1; j++)
            {
                hist[j].id = hist[j+1].id;
                hist[j].type = hist[j+1].type;
                hist[j].name = hist[j+1].name;
            }
            hist[len-1].type = atype;
            hist[len-1].id = aid;
            hist[len-1].name = aname;
            found = true;
        }
    }
    if (found !=true)
    {
        if (len == 100)
        {
            for (j=0; j < len-1; j++)
            {
                hist[j].id = hist[j+1].id;
                hist[j].type = hist[j+1].type;
                hist[j].name = hist[j+1].name;
            }
            hist[len-1].type = atype;
            hist[len-1].id = aid;
            hist[len-1].name = aname;
        }
        else
        {
            hist[len] = new Asset(atype,aid,aname);
        }
    }
}

function removeFromHistory(aid,atype)
{
    var len = hist.length;
    for (i=0; i < len; i++)
    {
        if (hist[i].id == aid && hist[i].type==atype)
        {
            for (j=i; j < len-1; j++)
            {
                hist[j].id = hist[j+1].id;
                hist[j].type = hist[j+1].type;
                hist[j].name = hist[j+1].name;
            }
            hist.length = len-1;
        }
    }
}

function clearToolbarHistory()
{
    hist.length = 0;
}

function displayInsiteImage( imgObj )
{
    //[2008-03-05 KGF] insite button is always clickable now.
     swap(imgObj,'<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/toolbar/insitehover.gif'); 
}

function showHistory()
{
    var len = hist.length;
 var pURL = "<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=OpenMarket%2FXcelerate%2FActions%2FShowRecentFront"; var origBase = pURL; 
    var typearray = "";
    var historyarray = "";
    for (i=len-1; i >= 0; i--) {
    if (historyarray == "")
    {
        historyarray = ""+hist[i].id+"";
        typearray = "" + hist[i].type + "";
    }
    else
    {
        historyarray =  historyarray + "," +  hist[i].id +"";
        typearray = typearray + "," + hist[i].type +"";}
    }
    origBase = origBase + "&historyarray=" + historyarray+"&typearray="+typearray;
    parent.XcelWorkFrames.XcelAction.location = origBase;
}
</SCRIPT>
<SCRIPT Language="JavaScript">
function ReverseContentDisplay(d)
{
	if(document.getElementById(d).style.display == "none") { document.getElementById(d).style.display = "inline"; }
	else { document.getElementById(d).style.display = "none"; }
}

function switchToggleIcon(iconDivFrom,iconDivTo)
{       
    if(document.getElementById(iconDivFrom).style.display != "none")
    {
        document.getElementById(iconDivFrom).style.display = "none";
        document.getElementById(iconDivTo).style.display = "inline";
		if(iconDivFrom == 'FullWindowToggleDiv') {
			document.getElementById(iconDivTo).style.left = 0;
		} else {
			document.getElementById(iconDivTo).style.left = "220px";
		}
		toggleTree();
    }

}
</SCRIPT>
<style>
.logoWebCenterSites { background-image:none !important; margin-top:-5px !important; }
</style>

</HEAD><BODY leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div class="topMenuBar">
<div class="topMenuBarLeft">
<div class="logoWebCenterSites"><img src="ct/wem/app/images/CTLogo-BW-Trans.png" alt="Clay Tablet" onclick="parent.frames['bodyframe'].location.href='<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=CustomElements/CT/WEM/Dashboard';" onMouseOver="" onMouseOut=""/></div>
<div class="topMenuBarMenu">
<c:if test="${'true' eq siteAuthorized}">
<div class="menuItem" onclick="parent.frames['bodyframe'].location.href='<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=CustomElements/CT/WEM/Dashboard';" onMouseOver="" onMouseOut=""><span id="newWordsDiv">Dashboard</span></div>
</c:if>
<c:if test="${'true' eq authorized}">
<div class="menuItem" onclick="parent.frames['bodyframe'].location.href='<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=CustomElements/CT/WEM/CTConfigFront';" onMouseOver="" onMouseOut=""><span id="newWordsDiv">Configuration</span></div>
</c:if>
<c:if test="${'true' eq siteAuthorized}">
<div class="menuItem" onclick="parent.frames['bodyframe'].location.href='<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=CustomElements/CT/JobListFront&app=WEM';" onMouseOver="" onMouseOut=""><span id="searchWordsDiv">Jobs</span></div>
<div class="menuItem" onclick="parent.frames['bodyframe'].location.href='<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=CustomElements/CT/QueueFront&app=WEM';" onMouseOver="" onMouseOut=""><span id="searchWordsDiv">Queues</span></div>
</c:if>
<c:if test="${'true' eq authorized}">
<div class="menuItem" onclick="parent.frames['bodyframe'].location.href='<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=CustomElements/CT/WEM/EnableLocalesFront'" onMouseOver="" onMouseOut=""><span id="siteplanWordsDiv">Enable Locales</span></div>
<div class="menuItem" onclick="parent.frames['bodyframe'].location.href='<%=ics.GetProperty("ft.cgipath")%>ContentServer?pagename=CustomElements/CT/WEM/AssignLocalesFront'" onMouseOver="" onMouseOut=""><span id="workflowWordsDiv">Assign Locales</span></div>
</c:if>
</div>
<div>

</div>
</div>
<div class="topMenuBarRight">
<div class="toggleNav"></div>
</div>
</div>
</BODY></HTML>
</gsf:root>
</cs:ftcs>
