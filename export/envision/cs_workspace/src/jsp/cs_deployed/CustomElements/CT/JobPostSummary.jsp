<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
	

<script type="text/javascript">
	function getLocale(loc) {
		//convert from CS's locale notation format to the format Dojo NLS uses.
		//(e.g. en_US -> en-us, fr_FR -> fr-fr, etc.)
		return loc.toLowerCase().replace('_', '-');
	}
	var djConfig = {
		fw_csPath: '',
		pubName: 'FirstSiteII',
		parseOnLoad: true,
		locale: getLocale('en_US')
	};
</script>


	



	<script type="text/javascript" src="wemresources/js/WemContext.js"></script>
	<script type="text/javascript">
		//initialize wemcontext before loading dojo layers (see AppBar FIXME)
		//Unfortunately it needs to be AFTER dojo because WemContext relies on xhrGet!
		WemContext.initialize('','http://localhost:8080/cas/logout');
		var wemcontext = WemContext.getInstance();
		// If a new browser window is opened, as a pop up or new tab or new window, and if the Wem top bar is not there.
		// Then global sls js object would not avilable. In that scenario we load the object in two ways based on how the new window opened.
		// If as a pop-up
		if(window.opener) window.fw_sls_obj = window.opener.top.fw_sls_obj;
		// If as a browser tab or new brower window		
		if(!window.opener && window.top === window.self) document.write('<script type="text/javascript" src="ContentServer?user_locale=en-us&pagename=fatwire%2Fui%2Futil%2FGetSLSObj"><' + '/script>');
	</script>




<script type="text/javascript" src='js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: '0'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='wemresources/js/dojopatches.js'></script>

	<link href="Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
	<link href="Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
	<link href="Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
	<link href="Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>

		
</head>
<body class="AdvForms">
	
	<satellite:form action="ContentServer" method="post" name="AppForm"><INPUT TYPE='HIDDEN' NAME='_authkey_' VALUE='554446B0FA2A97A5208D6158954E06ED9AE51AAFBE5ECAC8511A06205DDB49975CFA03961A21C220A83CC500F7FE9845'>
		
		
		
		
		<input type="hidden" name="_charset_" value="UTF-8" />
		<input type="hidden" name="cs_environment" value="ucform" />
		<input type="hidden" name="cs_formmode" value="WCM" />
		<input type="hidden" name="PubName" value='${cs.pubid}' />
		<input type="hidden" name="siteId" value='<%=ics.GetVar("siteId")%>'/>

<ics:callelement element="CustomElements/CT/HeaderTab"/>



<script language="javascript">

function getActiveView() {
	// Clear error message, if any
	var activeView, docId;
	SitesApp = parent.SitesApp;
	if (SitesApp) {
	//	if (!parent.dojo || !SitesApp) alert("No dojo or sitesapp");;
		activeDoc = docId !== undefined ?
		SitesApp.getDocument(docId):
		SitesApp.getActiveDocument();
		activeView = activeDoc.get('activeView');
		return activeView;
	} else {
		return null;
	}
}

	var activeView = getActiveView();
	
	<%if (ics.GetVar("errorMessage")!=null && ics.GetVar("errorMessage").length()>0) {%>
		var error = '<%=ics.GetVar("errorMessage")%>';
		activeView.error(error);
	<% } else { %>
		activeView.message('<%=ics.GetVar("message")%>');
	<% } %>
</script>

<% if ("WEM".equals(ics.GetVar("app"))) { %>
	<c:set var="app" value="WEM"/>
	<input type="hidden" name="app" id="app" value="WEM"/>
<% } %>

	<input type="hidden" name="docId" value="0"/>

<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0">

<c:choose><c:when test="${'WEM' eq app}">
<render:getpageurl outstr="listurl" pagename="CustomElements/CT/JobListFront">
	<render:argument name="app" value="WEM"/>
	<render:argument name="selectSite" value='<%=ics.GetVar("siteId")%>'/>
</render:getpageurl> 
<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/JobFront">
	<render:argument name="jobid" value='<%=ics.GetVar("jobid")%>'/>
	<render:argument name="app" value="WEM"/>
	<render:argument name="siteId" value='<%=ics.GetVar("siteId")%>'/>
</render:getpageurl> 
</c:when><c:otherwise>
<render:getpageurl outstr="listurl" pagename="CustomElements/CT/JobListFront"/>
<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/JobFront">
	<render:argument name="jobid" value='<%=ics.GetVar("jobid")%>'/>
</render:getpageurl> 
</c:otherwise></c:choose>

<TR><TD><TABLE width="100%">
	<TR><TD width="50%"><SPAN CLASS="title-text">Job Details</SPAN></TD>
	<TD align="right" width="50%"><a href='<ics:getvar name="listurl"/>'>All Translation Jobs</a></TD></TR>
</TABLE></TD></TR>
<tr><td><img height="5" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>
	<TABLE CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Job ID:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset"><ics:getvar name="jobid"/></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Name:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset"><ics:getvar name="jobname"/></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Status:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD>
		<TD class="form-inset"><ics:getvar name="jobstatus"/></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Translation Vendor:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset"><ics:getvar name="vendorname"/></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<% String jobid = ics.GetVar("jobid"); if (jobid==null || "".equals(jobid)) { %>
	<TR><TD CLASS="form-label-text">&nbsp;</TD><TD></TD><TD class="form-inset"><input type="button" name="Submit" value="Return to List" class="f1image-medium" onclick="location.href='<ics:getvar name="listurl"/>';"></TD></TR>
	<% } else { %>
	<TR><TD CLASS="form-label-text">&nbsp;</TD><TD></TD><TD class="form-inset"><input type="button" name="Submit" value="View Details" class="f1image-medium" onclick="location.href='<ics:getvar name="detailsurl"/>';"></TD></TR>
	<% } %>
	<tr><td colspan="3"><img height="20" width="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	</TABLE>
</TD></TR>
</TABLE>





</satellite:form>
<ics:callelement element="CustomElements/CT/FooterTab"/>

		
	<script>
		var num = document.AppForm.length, i;
		document.AppForm.encoding="application/x-www-form-urlencoded";
		for (i=0; i < num; i++) {
			if (document.AppForm.elements[i].type=="file") document.AppForm.encoding="multipart/form-data";
		}
		(function() {
			var _alert = window.alert;
			if (SitesApp) {
				window.alert = function() {
					var out = SitesApp.getActiveView() || SitesApp;
					out.clearFeedback();
					out.clearMessage();
					out.message(arguments[0], arguments[1] || "error");
					//fw/ui/controller/AdvancedController subscribes to this event
					parent.dojo.publish('/fw/ui/form/alert',[]);
					return true;
				};
			}
		})();
	</script>
	
</body>
</html>



</cs:ftcs>