<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIJobRequestCopyManager">
<render:getpageurl outstr="joburl" pagename="CustomElements/CT/JobFront">
		<render:argument name="jobid" value='${targetJob.id}'/>
		<render:argument name="app" value='<%=ics.GetVar("app")%>'/>
		<render:argument name="siteId" value='${siteid}'/>
</render:getpageurl> 

<a href='<ics:getvar name="joburl"/>' id="jumpjoblink">${targetJob.name}</a>

<select name="updated_copytojobid" id="updated_copytojobid" width="12em" style="max-width:55%;">
    <option value=""></option>
	<option value="newjob">Create New Job</option>
	<option disabled>----------------------------</option>
	<c:if test="${fn:length(jobs) le 1}">
		<option disabled>-- No existing job --</option>
	</c:if>
	<c:forEach var="otherjob" items="${jobs}">
		<c:if test="${otherjob.status eq 'CREATED' && otherjob.id ne jobid}">
    		<option value="${otherjob.id}">${otherjob.name}</option>
    	</c:if>
      	</c:forEach>
</select>
</gsf:root>
</cs:ftcs>
