<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIEncryptionConfigManager">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script> 
	
	<link href="ct/css/ct.css" rel="stylesheet" type="text/css"/>
	<script language="Javascript"><!--
	function configChanged() {
		document.getElementById("updatebutton").disabled =false;
		document.getElementById("cancelbutton").disabled =false;
		document.getElementById("generatebutton").disabled =false;
		document.getElementById("change_warning").style.display = "inline";
	}
	
	function cancelEdit() {
		if (confirm("Do you wish to discard your changes to the encryption key configuration and revert to existing configuration?")) { 
			document.getElementById("keystore_type").value = document.getElementById("original_keystore_type").value;
			document.getElementById("keystore_path").value = document.getElementById("original_keystore_path").value;
			document.getElementById("keystore_password").value = document.getElementById("original_keystore_password").value;
			document.getElementById("key_alias").value = document.getElementById("original_key_alias").value;
			document.getElementById("key_password").value = document.getElementById("original_key_password").value;
			document.getElementById("updatebutton").disabled =true;
			document.getElementById("cancelbutton").disabled =true;
			document.getElementById("generatebutton").disabled =true;
			document.getElementById("keystore_type_label").removeAttribute("style");
			document.getElementById("keystore_path_label").removeAttribute("style");
			document.getElementById("keystore_password_label").removeAttribute("style");
			document.getElementById("key_alias_label").removeAttribute("style");
			document.getElementById("key_password_label").removeAttribute("style");
			document.getElementById("change_warning").style.display = "none";
		}
	}
	
	function validateField(fieldname) {
		if (!document.getElementById(fieldname).value ||
				document.getElementById(fieldname).value=="") {
			document.getElementById(fieldname + "_label").style.color = "red";
			document.getElementById(fieldname).focus();
			return false;
		} else {
			document.getElementById(fieldname + "_label").removeAttribute("style");
			return true;
		}
	}
	
	function validateParam() {
		var action = document.getElementById('action').value;
		if (action == 'update' || action=='generate') {
			var validated = true;
			
			validated = validateField("key_alias") && validated; 
			validated = validateField("keystore_password") && validated; 
			validated = validateField("keystore_path") && validated; 
			validated = validateField("keystore_type") && validated;

			if (validated && (!document.getElementById("key_password").value || 
					document.getElementById("key_password").value=="")) {
				if (!confirm("Password for encryption key is not specified, is it the same as the keystore's password?")) {
					validated = validateField("key_password") && validated;
				}
			}
			if (validated) {
				if (action=='update') {
					return confirm("Do you wish to change encryption key settings? " + 
							"If you proceed, sensitive information will be re-encrypted using the new key.");
				} else {
					return confirm("Do you wish to generate a new encryption key? " + 
							"If you proceed, a new key will be created and insert into the specified keystore. " +
							"If the keystore does not exist, a new keystore file will be created at the given location. " + 
							"Sensitive information will be re-encrypted using the new key.");
				}
			} else {
				return false;
			}
		} else if (action == 'clear') {
			return confirm("Do you wish to clear encrytion key setting? If you proceed, sensitive information " + 
					"will be re-encrypted with default encryption.");
		} else {
			return false;
		}
	}
	
	function showAlertMessage() {
		var msg = document.getElementById("alertmessage").value;
		if (msg!=null && msg!='') {
			alert(msg);
		}
		setTimeout(resetMessage, 10000);
	}

	function resetMessage() {
		document.getElementById('regularmessage').innerHTML='';
	}

	function confirmRefresh() {
		if (confirm("You are about to refresh this page. Unsaved changes made on the page will be lost. Do you wish to proceed?")) {
			document.getElementById("refreshlink").click();
		}
	}
	-->
	</script>
</head>

<body>
<ics:callelement element="CustomElements/CT/WEM/Header"/>
<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/WEM/EncryptionConfig">
</render:getpageurl>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Configure Encryption Key</SPAN>&nbsp;&nbsp;
	<SPAN style="text-align:right;"><a id='refreshlink' href='<ics:getvar name="refreshurl"/>' style="display:none"/>
		<a href="javascript:confirmRefresh()">
		<img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a>
	</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>

<input type="hidden" id="alertmessage" value='<c:out value="${error_message}" escapeXml="true"/>' />
<div id="regularmessage" class="regular_message"><c:out value="${message}" escapeXml="true"/></div>
<input type="hidden" id="original_keystore_type" value="${original_keystoreType}"/>
<input type="hidden" id="original_keystore_path" value="${original_keystorePath}"/>
<input type="hidden" id="original_keystore_password" value="${original_keystorePassword}"/>
<input type="hidden" id="original_key_alias" value="${original_keyAlias}"/>
<input type="hidden" id="original_key_password" value="${original_keyPassword}"/>
<c:if test="${not empty load_error}">
	<div id="loaderror_msg" style="color:red;"><c:out value="${load_error}" escapeXml="true"/></div> </c:if>
<c:if test="${empty original_keystorePath}">
	<div id="noconfig_msg">You have not configured your own encryption key yet, sensitive information is encrypted with default encryption.</div> </c:if>
<form id="EncryptionConfigForm" onSubmit="return validateParam();">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/EncryptionConfig"/>
	<input type="hidden" name="action" id="action" value=""/>
	<table>
	<tr><td id="keystore_type_label">Keystore type:&nbsp;</td><td>&nbsp;</td>
		<td><select id="keystore_type" name="keystoreType" onchange="configChanged()">
			<c:if test="${fn:length(keystoreTypes)>1}">
				<option name="keystore_none" value="">&nbsp;&nbsp;&nbsp;&nbsp;</option>
			</c:if>
			<c:forEach var="type" items="${keystoreTypes}">
				<option name="keystore_${type}" value="${type}"
					<c:if test="${type eq keystoreType}"> selected </c:if> 
				> ${type}&nbsp;&nbsp;&nbsp;&nbsp;</option>
			</c:forEach>
		</select></td></tr>
	<tr><td id="keystore_path_label">Keystore path:&nbsp;</td><td>&nbsp;</td>
		<td colspan="3"><input type="text" name="keystorePath" id="keystore_path" size="80" 
				value="${keystorePath}" onchange="configChanged()"></td></tr>
	<tr><td id="keystore_password_label">Keystore password:&nbsp;</td><td>&nbsp;</td>
		<td colspan="3"><input type="password" name="keystorePassword" size="30"  id="keystore_password" 
				value="${keystorePassword}" onchange="configChanged()"></td></tr>
	<tr><td id="key_alias_label">Key alias:&nbsp;</td><td>&nbsp;</td>
		<td colspan="3"><input type="text" name="keyAlias" id="key_alias" size="30"  value="${keyAlias}" 
				onchange="configChanged()"></td></tr>
	<tr><td id="key_password_label">Key password:&nbsp;</td><td>&nbsp;</td>
		<td colspan="3"><input type="password" name="keyPassword" size="30"  id="key_password" value="${keyPassword}" 
				onchange="configChanged()"></td></tr>
	<tr> <td colspan="5"> Supported key type(s) are: 
		<c:forEach var="type" items="${keyTypes}" varStatus="status">
			AES&nbsp;${type}bits<c:if test="${!status.last}">,</c:if>&nbsp;</c:forEach>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<SPAN id="change_warning" style="color:red;<c:if test='${not hasUnsavedChanges}'>display:none</c:if>">
					Change not saved</SPAN> </td></tr> 
<!-- 		
-->		
	<tr><td style="text-align:left" colspan="5"><input class="bt-image-medium" type="submit" name="submit" id="updatebutton" 
		<c:if test="${not hasUnsavedChanges}">disabled="true" </c:if> 
		value="Update" onclick="document.getElementById('action').value='update';"/>&nbsp;&nbsp;
	<input class="bt-image-medium" type="submit" name="submit" id="clearbutton" value="Clear" 
		onclick="document.getElementById('action').value='clear';"/>&nbsp;&nbsp;
	<input class="bt-image-medium" type="submit" name="submit" id="generatebutton" value="Generate Key"
		<c:if test="${not hasUnsavedChanges}">disabled="true" </c:if> 
		onclick="document.getElementById('action').value='generate';"/>&nbsp;&nbsp;
	<select id="key_type" name="keyType" onchange="configChanged()">
		<c:forEach var="type" items="${keyTypes}">
			<option name="key_${type}" value="${type}" <c:if test="${type eq keyType}">selected</c:if>> 
				AES&nbsp;${type}bits&nbsp;</option>
		</c:forEach>
	</select>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input class="bt-image-medium" type="button" id="cancelbutton" value="Discard Changes" 
		<c:if test="${not hasUnsavedChanges}">disabled="true" </c:if> 
		onclick="return cancelEdit();"/>
	</tr></table>
</form>
</td></tr></TABLE>
<ics:callelement element="CustomElements/CT/WEM/Footer"/>

<script>
	window.onload = showAlertMessage();
</script>

</body></html>
</gsf:root>
</cs:ftcs>
