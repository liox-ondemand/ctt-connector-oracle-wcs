<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs><gsf:root action="class:com.claytablet.wcs.ui.UIAuthorizationManager"> 

<c:if test="${isSiteEnabled && isUserAuthorized}">

dojo.declare('fw.ui.view.advanced.ClayTabletAssetView', [fw.ui.controller.BaseDocController, fw.ui.view.AdvancedView, fw.ui.view.TabbedViewMixin], {
        // summary:
        // A view class for Custom Advanced UI screen
        // overrides the getter for the 'viewArgs' attribute
        // the base class (AdvancedView) relies on the args property to build the correct
        // URL to the Advanced UI framework.
        // Customize as required.
         
        getAdvancedURLParams: function () {
                var asset = this.model.get('asset'), viewArgs;
                var view = SitesApp.getActiveView();
                var defaultThisPage = "ClayTabletDispatcher";
                var overrideThisPage = this.model.get('ThisPageOverride');
                if (overrideThisPage != null) {
                	defaultThisPage = overrideThisPage;
                }
                var elementname = view.name;
                if (elementname == "new") {
                	elementname = this.model.get('nextctelement');
                }
                 if (view && asset && elementname) {
                 	    
                        viewArgs = {
                                ThisPage: defaultThisPage,
                                c: asset.type,
                                cid: asset.id,
                                displayelement: elementname
                        };

                }
                else {
                        throw new Error(fw.util.getString("UI/UC1/JS/CannotRenderView"));
                } 

                return viewArgs;
        },

        show: function () {
                // set the title to name for the tab to open with the title
                var title = this.model.get('name');
                if (title == "") {
                	var doc = SitesApp.getActiveDocument();
                	title = doc.asset.name;
                	var tbc = doc.getToolbarConfig("versioning");
                }
                var overrideThisPage = this.model.get('ThisPageOverride');
                if (title != "Translation Jobs" && title != "Translation Queue" && overrideThisPage == null) {
                	title = "Translate " + title;
                }
                this.set('title', title);
                return this.inherited(arguments);
        }       
}); 

<!--  -->

  
  
webcenter.sites['${param.namespace}'] = function (config) {	

	config.clayTabletActions = {
	    loadJobListFront: function (args) {
		     SitesApp.event("document", "new", {
				   "document-type": "claytabletdoc",
				   "asset": {
					   "type": "",
					   "subtype": "",
					   "id": ""
				   },
				   "name": "Translation Jobs",
				   "nextctelement": "JobListFront"    
			});
	    },
	    
	    loadQueueFront: function (args) {
		     SitesApp.event("document", "new", {
				   "document-type": "claytabletdoc",
				   "asset": {
					   "type": "",
					   "subtype": "",
					   "id": ""
				   },
				   "name": "Translation Queue",
				   "nextctelement": "QueueFront"    
			});
	    },
	    
	    loadTranslateFront: function (args) {
			var doc = SitesApp.getActiveDocument();
			if (doc) {
				    var currentasset = doc.asset;
				    if (currentasset.name) {
		 		    	SitesApp.event("document", "new", {
		 				   "document-type": "claytabletdoc",
						   "asset": {
							   "type": currentasset.type,
							   "subtype": currentasset.subtype,
							   "id": currentasset.id
						   },
						   "name": currentasset.name,
						   "nextctelement": "TranslateFront"    
						});
					}
			} else {
				SitesApp.getActiveView().error("You must open an asset before translating it.");
			}
	    },
	    
	    loadClayTabletSite: function (args) {
	    	window.open("http://www.clay-tablet.com");
	    },
	    
	    sayHello: function (args) {
	    	alert("hello world!");
	    }
	};

	config.defaultView = {
		"default": "form",
		"claytabletdoc": "claytabletdocview"
	};
		
	config.menubar['default'].push({
	    "id": "clayTabletMenu",
	    "label": "Translation",
	    "children": [
		{label: "Translation Queue", action: config.clayTabletActions.loadQueueFront},
		{label: "Job Status", action: config.clayTabletActions.loadJobListFront},
		{separator: true},
		{label: "Translate Asset", action: config.clayTabletActions.loadTranslateFront },
		{label: "Asset Translation Status", action: 'TranslationStatusFront'},
		{label: "View Translations", action: 'ViewTranslationsFront'},
		{separator: true},
		{label: "About Clay Tablet", action: config.clayTabletActions.loadClayTabletSite }
	    ]
	}
	);

   	   	config.views['claytabletview'] = {
			'viewClass': "fw.ui.view.advanced.ClayTabletAssetView"
        }
   	   	config.views['TranslateFront'] = {
			"viewClass": "fw.ui.view.advanced.ClayTabletAssetView",
			"controllerClass": "fw.ui.controller.BulkAssetController"
        }
  		config.documents["claytabletdoc"] = "fw.ui.document.ClayTabletAssetDocument";		
		config.views["claytabletdocview"] = { 
			"viewClass": "fw.ui.view.advanced.ClayTabletAssetView"
		}
        
        <!--  --> 	
        
        config.views["bulktranslation"] = {
			'viewClass': 'extensions.fw.ui.view.BulkTranslationView'
		};		
		
		
		
		config.controllers["fw.ui.document.AssetDocument"].bulktranslate = {
				"next-view": "bulktranslation"
		};
			
		config.controllers["fw.ui.controller.BulkAssetController"].bulktranslate = {
		"next-view": "bulktranslation"
		};
		

		if (fw.ui.ActionLabels) {
			fw.ui.ActionLabels.keys["bulktranslate"] = "Bulk Translate";
			config.contextMenus.asset.push("bulktranslate");
   			config.contextMenus["asset/Page"].push("bulktranslate");
   		}		


 		<!--  -->
 		
 		
        config.controllers["fw.ui.document.AssetDocument"].QueueFront = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    } 
        config.controllers["fw.ui.document.AssetDocument"].JobListFront = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    } 	
        config.controllers["fw.ui.document.AssetDocument"].BulkTranslationFront = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    } 
       	config.controllers["fw.ui.document.AssetDocument"].TranslateFront = {
	          "next-view": "claytabletview",
	          "model-update-required": false,
	          "goback": "goBack",
	          "refresh": "refresh"
	    } 	
	    config.controllers["fw.ui.document.AssetDocument"].RequestCorrectionFront = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    } 
        config.controllers["fw.ui.document.AssetDocument"].TranslationStatusFront = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    } 
        config.controllers["fw.ui.document.AssetDocument"].ViewTranslationsFront = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    } 	    
        config.controllers["fw.ui.document.AssetDocument"].TranslateChildren = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    } 
        config.controllers["fw.ui.document.AssetDocument"].TranslateBranch = {
	          "next-view": "claytabletview",
	          "model-update-required": false
	    }   	     	   	    
	    	    	    	    
		config.toolbars["claytabletview"] = {
			"default1": ["goback", "refresh"],
	 	}
	 			
		config.controllers["fw.ui.view.advanced.ClayTabletAssetView"] = {
			"goback1": "goBack",
			"refresh1": "default"
		}

		config.controllers["fw.ui.controller.ClayTabletBulkAssetController"] = {
			"bookmark": "default",
			"unbookmark": "default",
			"delete": {
				"next-view": "delete"
			},
			"approve": {
				"next-view": "approval"
			},
			"TranslateFront": {
				"next-view": "claytabletview"
			}
		}
	    
	    config.toolbarButtons.customBack = {
      		src: 'js/fw/images/ui/ui/toolbarButton/goback.png',
      		onClick: function() { location.reload(); }
      	}
 
		config.controllers["fw.ui.controller.BulkAssetController"].TranslateFront = {
			"next-view": "claytabletview",
	        "model-update-required": false	
		}

	    config.searchContextMenus = {
	        "default":["edit", "copy", "preview", "share", "delete", "bookmark", "tagasset", "TranslateFront"],
	        "asset":["edit", "copy", "preview", "share", "delete", "bookmark", "tagasset", "TranslateFront"],
	        "asset/Page":["edit", "copy", "preview", "share", "delete", "bookmark", "tagasset", "TranslateFront"]
	    }
	    
	    
      
}
fw.ui.view.TreeContextMenuView.prototype._sls.TranslateChildren = "Translate Children";
fw.ui.view.TreeContextMenuView.prototype._sls.TranslateBranch = "Translate Branch";
fw.ui.view.TreeContextMenuView.prototype._sls.TranslateFront = "Translate";
</c:if>
</gsf:root>
</cs:ftcs>