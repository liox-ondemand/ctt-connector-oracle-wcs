<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ page import="com.claytablet.wcs.util.FileUpload"
%><cs:ftcs>
 
<% try { %>
<input type='hidden' id='uploadResult' 
	value="<%= FileUpload.handleFile(request, response, new String[]{"XML", "ZIP"}) %>
"/>
<% } catch (Throwable t) { %>
"/>
<input type='hidden' id='uploadError' 
	value="<%= t.toString() %>"/>
<% } %> 

</cs:ftcs>
