<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="assetset" uri="futuretense_cs/assetset.tld"
%><%@ taglib prefix="commercecontext" uri="futuretense_cs/commercecontext.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="listobject" uri="futuretense_cs/listobject.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="searchstate" uri="futuretense_cs/searchstate.tld"
%><%@ taglib prefix="siteplan" uri="futuretense_cs/siteplan.tld"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors"
%><cs:ftcs>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <title>Oracle WebCenter Sites 11gR1</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="<%=ics.GetProperty("ft.cgipath")%>wemresources/images/icons/sitesFavicon.ico" type="image/x-icon" />

  </head>
    <frameset rows="40,*" FRAMEBORDER="NO" FRAMESPACING="0" BORDER="0">
  		<frame src="Satellite?pagename=CustomElements/CT/WEM/TopNav" frameborder="0" name="topnav" SCROLLING="NO" NORESIZE>
  		<frame src="Satellite?pagename=CustomElements/CT/WEM/Dashboard" frameborder="0" name="bodyframe">
	</frameset>
</html>

</cs:ftcs>
