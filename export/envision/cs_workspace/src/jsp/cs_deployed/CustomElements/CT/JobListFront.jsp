<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIAdminJobListManager">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
<link href="Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<script language="javascript">
function checkAll(type) {
	var boxes = document.getElementsByName(type + "_togglers");
	for (var i=0; i < boxes.length; i++) {
		boxes[i].checked = true;
		selectOrUnselectItem(type, boxes[i].value, boxes[i].checked);
	}
	var totalSelected = getNumberOfSelectedItems(type);	
	updateTotal(type, totalSelected);
}

function uncheckAll(type) {
	var boxes = document.getElementsByName(type + "_togglers");
	for (var i=0; i < boxes.length; i++) {
		boxes[i].checked = false;
		selectOrUnselectItem(type, boxes[i].value, boxes[i].checked);
	}
	var totalSelected = getNumberOfSelectedItems(type);	
	updateTotal(type, totalSelected);
}

function selectAndCheckAll(type) {
	selectAll(type);
	checkAll(type);
}

function unselectAndUncheckAll(type) {
	unselectAll(type);
	uncheckAll(type);
}

function selectAll(type) {
	var selectitems = document.getElementById(type + "_jobids");
	for (var i=0; i < selectitems.options.length; i++) {
		selectitems[i].selected = true;
	}
	updateTotal( type, selectitems.options.length );
}

function unselectAll(type) {
	var selectitems = document.getElementById(type + "_jobids");
	for (var i=0; i < selectitems.options.length; i++) {
		selectitems[i].selected = false;
	}
	updateTotal( type, "0" );
}

function updateDropdownAndTotal(type, thisboxvalue, ischecked) {
	selectOrUnselectItem(type, thisboxvalue, ischecked);
	
	var totalSelected = getNumberOfSelectedItems(type);	
	updateTotal(type, totalSelected);
}

function getNumberOfSelectedItems(type) {
	var selectitems = document.getElementById(type + "_jobids");
	var total = 0;
	for (var i=0; i < selectitems.options.length; i++) {
		if (selectitems[i].selected == true) {
			total++;
		}
	}
	return total;
}

function updateTotal(type, total) {
	if (document.getElementById(type + "_select_total")) {
		document.getElementById(type + "_select_total").innerHTML = total;
	}
}

function selectOrUnselectItem(type, itemvalue, selectval) {
	var selectitems = document.getElementById(type + "_jobids");
	for (var i=0; i < selectitems.options.length; i++) {
		if (selectitems[i].value == itemvalue) {
			selectitems[i].selected = selectval;
			break;
		}
	}
}

function restoreCheckedState(type) {
	var selectedCount = 0;
	var boxList = document.getElementsByName(type + "_togglers");
	if (boxList) {
		var boxes = [];
		
		for (var i=0; i<boxList.length; i++) {
			boxes.push(boxList[i]);
		}
		var selectitems = document.getElementById(type + "_jobids");
		if (selectitems) {
			for (var i=0; i < selectitems.options.length; i++) {
				if (selectitems[i].selected) {
					selectedCount++;
					for (var j=0; j < boxes.length; j++) {
						if (selectitems[i].value == boxes[j].value) {
							boxes[j].checked = true;
							boxes.splice(j, 1);
							break;
						}		
					}
				}
			}
		}
		updateTotal(type, selectedCount);
	}
}

function getCheckedRadio( radioname ) {
	var checkedval = "";
	var radios = document.getElementsByName( radioname );
	for (var i=0; i<radios.length; i++) {
		if (radios[i].checked) {
			checkedval = radios[i].value;
		}
	}
	return checkedval;
}

function getCheckedBoxes( boxname ) {

	var boxes = document.getElementsByName( boxname );
	var checked = "";
	for (var i=0; i<boxes.length; i++) {
		if (boxes[i].checked) {
			checked += ", " + boxes[i].value;
		}
	}
	return checked;
}

function clearMessage() {
	activeView.clearMessage();
    alert();
}

function toggleCheckboxes(type) {
	var hasChecked = false;
	var boxes = document.getElementsByName(type + "_togglers");
	for (var i=0; i < boxes.length; i++) {
		if (boxes[i].checked) {
			hasChecked = true;
			break;
		}
	}
	if (hasChecked) {
		uncheckAll(type);
	} else {
		checkAll(type);
	}
	document.getElementById(type + "_toggler").checked = false;
}

function archiveSelectedJobs() {
	var totalSelected = getNumberOfSelectedItems("inactive");
	
	if (totalSelected<=0) {
		alert("Please select translation job(s) to archive");
		return false;
	} else if (confirm("Are you sure you want to archive " + totalSelected + " translation job(s)?")) {
		var selectedJobIds = "";
		var selectitems = document.getElementById("inactive_jobids");
		var total = 0;
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].selected) {
				total++;
				selectedJobIds += selectitems[i].value + " ";
			}
		}
		updateJobList("inactive", "&action=archive&jobIds="+ encodeURIComponent(selectedJobIds));
	}
}

function unarchiveSelectedJobs() {
	var totalSelected = getNumberOfSelectedItems("archived");
	
	if (totalSelected<=0) {
		alert("Please select translation job(s) to unarchive");
		return false;
	} else if (confirm("Are you sure you want to unarchive " + totalSelected + " translation job(s)?")) {
		var selectedJobIds = "";
		var selectitems = document.getElementById("archived_jobids");
		var total = 0;
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].selected == true) {
				selectedJobIds += selectitems[i].value + " ";
			}
		}
		updateJobList("archived", "&action=unarchive&jobIds="+ encodeURIComponent(selectedJobIds));
	}
}

function showAlertMessage() {
	var msg = document.getElementById("alertmessage").value;
	if (msg!=null && msg!='') {
		alert(msg);
	}
}

function resetMessage() {
	if (document.getElementById('archivemessage')) {
		document.getElementById('archivemessage').innerHTML='';
	}
	if (document.getElementById('unarchivemessage')) {
		document.getElementById('unarchivemessage').innerHTML='';
	}
}

function toggleJobCounts(jobid) {
	var shownJobIdElem = document.getElementById("shownjobcountsid");
	var shownJobId = shownJobIdElem.value;
	
	var shownJobDetail = document.getElementById("jobdetail_"+shownJobId);
	var shownJobCount = document.getElementById("jobdetail_"+shownJobId);
	
	if (jobid==shownJobId) {
		// hide
		document.getElementById("jobdetail_"+jobid).className='tile-row-normal';
		hideDiv("jobcounts_"+jobid);
		shownJobIdElem.value = "";
	} else {
		if (shownJobId && shownJobId!="") {
			if (shownJobDetail) {
				document.getElementById("jobdetail_"+shownJobId).className='tile-row-normal';
			}
			if (shownJobCount) {
				hideDiv("jobcounts_"+shownJobId);
			}
		}
		document.getElementById("jobdetail_"+jobid).className="tile-row-highlight"
		showRow("jobcounts_"+jobid);
		shownJobIdElem.value = jobid;
	}
}

function showRow(elementid) {
	show(document.getElementById(elementid), 'table-row');
}

function showBlock(elementid) {
	show(document.getElementById(elementid), 'block');
}

function show(element, display) {
	element.style.visibility = 'visible';
	element.style.display = display;
}

function hideDiv(divid) {
	hideElement(document.getElementById(divid));
}

function hideElement(element) {
	element.style.visibility = 'hidden';
	element.style.display = 'none';
}

function sortPage(type, orderbyclause) {
	document.getElementById("orderbyclause_" + type).value = orderbyclause;
	refreshJobList(type, 0);
}

function refreshJobList(type, firstrec) {
	getJobList(type, firstrec, null, false);
}

function updateJobList(type, actionParam) {
	if (document.getElementById("showarchived").value=='true' || type!='archived') {
		getJobList(type, 0, actionParam, true);
	}
}

function changeSite() {
	document.forms["AppForm"].submit();
}

function getJobList(type, firstrec, actionParam, isUpdate) {
	document.getElementById("page_firstrecord_" + type).value = firstrec;
	
	var jobPane = document.getElementById("job_list_" + type);
	if (jobPane) {
		document.getElementById("modal_blocker").style.top = "60px";
		showBlock("modal_blocker");
		showBlock("jobsloadingprogress_" + type);
		var pagesize_input = document.getElementById("page_size_" + type);
		var pagesize = document.getElementById("default_page_size").value;
		if (pagesize_input) {
			pagesize = pagesize_input.value;
		} 
		var orderbyclause = document.getElementById("orderbyclause_" + type).value;

		var posturl = "ContentServer?pagename=CustomElements/CT/JobListAjax" + "&type=" + type +
			"&where=" + encodeURIComponent(document.getElementById("whereclause_" + type).value) +
			"&firstrec=" + firstrec + "&pagesize=" + pagesize;  

		var siteSelect = document.getElementById("selectSite");
		if (siteSelect) {
			posturl += "&siteId=" + siteSelect.value;
		}
		if (document.getElementById("app")) {
			posturl+="&app=" + 	document.getElementById("app").value;
		}
		if (orderbyclause) {
			posturl+="&orderby=" + encodeURIComponent(orderbyclause);	
		}

		if (actionParam) {
			posturl+=actionParam;
		}
		
 		$.ajax({url:posturl,
 			success:function(result){
 				jobPane.innerHTML=result;
 				
 				if (isUpdate) {
					if (document.getElementById("jobids_section_" + type)) {
						document.getElementById("jobids_section_" + type).innerHTML = 
							document.getElementById("updated_jobids_section_" + type).innerHTML;
					}
					var filteredElement = document.getElementById("filtered_job_count_"+type);
					var operationsElement = document.getElementById("operations_"+type);
					if (operationsElement){ 
						if (filteredElement && filteredElement.value>0) {
							show(operationsElement, "inline");
						} else {
							hideElement(operationsElement);
						}
					}
					if (document.getElementById("updated_jobids_section_" + type)) {
		 				document.getElementById("updated_jobids_section_" + type).innerHTML = '';
					}
 				}
				
				if (document.getElementById("h_job_list_summary_" + type)) {
					document.getElementById("job_list_summary_" + type).innerHTML =
						document.getElementById("h_job_list_summary_" + type).innerHTML;
				}
				if (document.getElementById("h_job_list_pages_" + type)) {
					document.getElementById("job_list_pages_" + type).innerHTML =
						document.getElementById("h_job_list_pages_" + type).innerHTML;
				}
				if (document.getElementById("showarchived").value=='false' &&
						document.getElementById("h_archived_job_summary")) {
					document.getElementById("job_list_summary_archived").innerHTML =
						document.getElementById("h_archived_job_summary").innerHTML;
				}
				if (document.getElementById("h_jobcount_archived")) {
					document.getElementById("jobcount_archived").value =
						document.getElementById("h_jobcount_archived").value;
					if (document.getElementById("jobcount_archived").value>0) {
						show(document.getElementById("show_hide_archived_link"), "inline");
					} else {
						hideElement(document.getElementById("show_hide_archived_link"));
					}
				}
				document.getElementById("jobsloadingprogress_" + type).style.display = "none";
				document.getElementById("modal_blocker").style.display = "none";
				restoreCheckedState(type);
				
				if (type=='inactive' && actionParam!='' && isUpdate ) {
					updateJobList("archived", "");
				} else if (type=='archived' && actionParam!='' && isUpdate ) {
					updateJobList("inactive", "");
					updateJobList("active", "");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				document.getElementById("jobsloadingprogress_" + type).style.display = "none";
				document.getElementById("modal_blocker").style.display = "none";
				alert("Failed to update the job list: " + errorThrown);
			}
 		});
	}
}

function init() {
	showAlertMessage();
	updateJobList("active", "");
	updateJobList("inactive", "");
	updateJobList("archived", "");
}
</script>
</head>
<body>
<ics:callelement element="CustomElements/CT/HeaderTab"/>

<% if ("WEM".equals(ics.GetVar("app"))) { %>
	<input type="hidden" name="app" id="app" value="WEM"/>
	<c:set var="app" value="WEM"/>
<% } %>

<render:getpageurl outstr="listurl_noarchive" pagename="CustomElements/CT/JobListFront">
	<render:argument name="showarchived" value='false'/>
	<render:argument name="app" value="${app}"/>
	<render:argument name="selectSite" value="${cursiteid}"/>
</render:getpageurl>

<render:getpageurl outstr="listurl_witharchive" pagename="CustomElements/CT/JobListFront">
	<render:argument name="showarchived" value='true'/>
	<render:argument name="app" value="${app}"/>
	<render:argument name="selectSite" value="${cursiteid}"/>
</render:getpageurl>

<input type="hidden" id="alertmessage" value='<c:out value="${errorMessage}" escapeXml="true"/>' />
<input type="hidden" id="shownjobcountsid" value=''/>
<div class="blocking_overlay" id="modal_blocker"></div>

<satellite:form action="ContentServer" method="post" name="AppForm">
<input type="hidden" name="pagename" id="pagename" value="CustomElements/CT/JobListFront"/>
<input type="hidden" name="doAction" id="doAction" value=""/>
<input type="hidden" name="default_page_size" id="default_page_size" value="${pagesize}"/>
<input type="hidden" name="jobId" id="jobId" value=""/>
<input type="hidden" name="showarchived" id="showarchived" value="${'true' eq showArchived}"/>
<input type="hidden" name="app" value="${app}"/>

<jsp:useBean id="now" class="java.util.Date"/>

<usermanager:getuserfromname username='<%=ics.GetSSVar("username")%>' objvarname="u"/>
<ccuser:getsiteroles name="u" site='<%=ics.GetSSVar("pubid")%>' objvarname="roleobject"/>
<rolelist:getall name="roleobject" varname="roles"/>

<input type="hidden" name="docId" value="0"/>
		
<TABLE BORDER="0" style="margin: 1em;" CELLPADDING="0" CELLSPACING="0" style="min-width:40em">
<render:getpageurl outstr="listurl" pagename="CustomElements/CT/JobListFront">
	<render:argument name="showarchived" value='${showArchived}'/>
	<render:argument name="app" value="${app}"/>
	<render:argument name="selectSite" value="${cursiteid}"/>
</render:getpageurl>
<TR><TD style="width:50%"><SPAN CLASS="title-text">Translation Jobs</SPAN>&nbsp;&nbsp;<SPAN style="text-align:right;">
	<a href='<ics:getvar name="listurl"/>'><img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a></SPAN></TD>
	<TD align="right" style="width:50%">
		<c:if test="${app eq 'WEM'}">Site:&nbsp;&nbsp;<select name="selectSite" id="selectSite" onchange="changeSite()">
			<c:forEach var="siteinfo" items="${sites}">
				<option name="${siteinfo.name}" value="${siteinfo.id}"
					<c:if test="${siteinfo.id eq cursiteid}"> selected </c:if> 
				> ${siteinfo.name}&nbsp;&nbsp;&nbsp;&nbsp;</option>
			</c:forEach>
		</select></c:if>
	</TD></TR>
<tr><td colspan="2"><img height="5" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color" colspan="2"><img height="2" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td colspan="2"><img height="10" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR> <TD style="text-align:left;" colspan="2"><SPAN CLASS="form-label-text">Active Jobs:</SPAN>
		<SPAN id="job_list_summary_active"></SPAN></TD></TR>
<TR><TD style="text-align:left;padding-left:1em"><SPAN id="job_list_pages_active"></SPAN></TD></TR>
<TR><TD class="form-inset" style="width: 95%"; colspan="2">
<div id="jobsloadingprogress_active" class="popup_panel_center">
	<img src="js/fw/images/ui/wem/loading.gif" style="h-align:center;"></img>
	<p>Loading active jobs ...</p></div>
<input type="hidden" name="page_firstrecord_active" id="page_firstrecord_active" value="${page_firstrecord_active}"/>
<input type="hidden" name="orderbyclause_active" id="orderbyclause_active" value="updatedDate DESC"/>
<input type="hidden" name="whereclause_active" id="whereclause_active" value=""/>
<DIV id="job_list_active"></DIV>
</TD><TD>&nbsp;&nbsp;</TD></TR>

<TR>
<TD style="text-align:left;padding-left=20px"  colspan="2"><SPAN CLASS="form-label-text">Inactive Jobs:</SPAN>		
		<SPAN id="job_list_summary_inactive"></SPAN>
<p id="jobids_section_inactive" style="visibility:hidden;display:none;">
	<select multiple name="inactive_jobids" id="inactive_jobids" width="15" size="10">
	<c:forEach var="job" items="${inactive_jobs}">
		<option value="${job.id}">${job.status}</option>
	</c:forEach>
	</select>
</p>
</TD>
<TD style="text-align:right"><SPAN id="archivemessage" class="regular_message"><c:out value="${archiveMessage}" escapeXml="true"/></SPAN></TD></TR>
<TR><TD style="text-align:left;padding-left:1em"><SPAN id="job_list_pages_inactive"></SPAN></TD>
<TD style="text-align:right"><SPAN id="operations_inactive" style="display:none">
	<input type="button" name="inactive_selectall" id="inactive_selectall" value="Select All" onClick="selectAndCheckAll('inactive');" class="f1image-medium"/>
	<input type="button" name="inactive_unselectall" id="inactive_unselectall" value="Unselect All" onClick="unselectAndUncheckAll('inactive');" class="f1image-medium"/>
	<input type="button" name="Archive" value="Archive Selected" class="bt-image-long" 
		onclick='archiveSelectedJobs();'>
	</SPAN></TD></TR>
<TR><TD class="form-inset" style="width: 95%"; colspan="2">
<div id="jobsloadingprogress_inactive" class="popup_panel_center">
	<img src="js/fw/images/ui/wem/loading.gif" style="h-align:center;"></img>
	<p>Loading inactive jobs ...</p></div>
<input type="hidden" name="page_firstrecord_inactive" id="page_firstrecord_inactive" value="${page_firstrecord_inactive}"/>
<input type="hidden" name="orderbyclause_inactive" id="orderbyclause_inactive" value="updatedDate DESC"/>
<input type="hidden" name="whereclause_inactive" id="whereclause_inactive" value=""/>
<DIV id="job_list_inactive"></DIV>
</TD><TD>&nbsp;&nbsp;</TD></TR>

<TR><TD style="text-align:left;" colspan="2"><SPAN CLASS="form-label-text">Archived Jobs:</SPAN>		
		<SPAN id="job_list_summary_archived"></SPAN>
<c:choose><c:when test="${showArchived}">
	<a href='<ics:getvar name="listurl_noarchive"/>' id="show_hide_archived_link" style="display:none"><b>Hide archived jobs</b></a>
</c:when><c:otherwise>
	<a href='<ics:getvar name="listurl_witharchive"/>' id="show_hide_archived_link" style="display:none"><b>Show archived jobs</b></a>
</c:otherwise></c:choose>
<p id="jobids_section_archived" style="visibility:hidden;display:none;">
	<select multiple name="archived_jobids" id="archived_jobids" width="15" size="10">
	<c:forEach var="job" items="${archived_jobs}">
		<option value="${job.id}">${job.status}</option>
	</c:forEach>
	</select>
</p>

</TD><TD style="text-align:right"><SPAN id="unarchivemessage" class="regular_message"><c:out value="${unarchiveMessage}" escapeXml="true"/></SPAN></TD></TR>
<TR><TD style="text-align:left;padding-left:1em"> <SPAN id="job_list_pages_archived"></SPAN></TD>
<TD style="text-align:right">	<SPAN id="operations_archived" style="display:none">
	<input type="button" name="archived_selectall" id="archived_selectall" value="Select All" onClick="selectAndCheckAll('archived');" class="f1image-medium"/>
	<input type="button" name="archived_unselectall" id="archived_unselectall" value="Unselect All" onClick="unselectAndUncheckAll('archived');" class="f1image-medium"/>
	<input type="button" name="Unarchive" value="Unarchive Selected" class="bt-image-long" 
		onclick='unarchiveSelectedJobs();'>
	</SPAN></TD></TR>
<TR><TD class="form-inset" style="width: 95%"; colspan="2">
<div id="jobsloadingprogress_archived" class="popup_panel_center">
	<img src="js/fw/images/ui/wem/loading.gif" style="h-align:center;"></img>
	<p>Loading archived jobs ...</p></div>
<input type="hidden" name="page_firstrecord_archived" id="page_firstrecord_archived" value="${page_firstrecord_archived}"/>
<input type="hidden" name="orderbyclause_archived" id="orderbyclause_archived" value="updatedDate DESC"/>
<input type="hidden" name="whereclause_archived" id="whereclause_archived" value=""/>
<input type="hidden" name="jobcount_archived" id="jobcount_archived" value="0"/>
<DIV id="job_list_archived"></DIV>
</TD><TD>&nbsp;&nbsp;</TD></TR>

<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD CLASS="form-label-text">&nbsp;</TD><TD></TD><TD class="form-inset">&nbsp;</TD></TR>
<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE>

</satellite:form>
<ics:callelement element="CustomElements/CT/FooterTab"/>
<script>
	window.onload = init();
</script>
</body>
</html>
</gsf:root>
</cs:ftcs>
