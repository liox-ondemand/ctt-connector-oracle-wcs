<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<ics:callelement element="CustomElements/CT/WEM/Header"/>

<script>
function validateFile() {
	var file = document.getElementById("configurationfile").value;

	if (file && file.trim()!='') {
		var msg;
		
		if (document.getElementById("mergeChecked").checked) {
			msg = "Do you wish to import the configuration file and merge with existing configuration?";
		} else {
			msg = "Do you wish to import the configuration file and overwrite existing configuration?";
		}
		if (confirm(msg)) {
			document.getElementById("submitbutton").disabled = true;
			document.getElementById("progress").style.display = 'inline';
			setTimeout(importUploadedFile, 2000);
			var form = document.getElementById("AppForm");
			form.submit();
			return true;
		} else {
			return false;
		}
	} else {
		alert("Please select a file to submit.")
		return false;
	}
}

function importUploadedFile() {
	var iframe = document.getElementById('resultPane');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
	if (innerDoc) {
		var postedResult = innerDoc.getElementById("uploadResult");
		var postedError = innerDoc.getElementById("uploadError");
		if (postedError) {
			var errorMsg = postedError.value;
			if (errorMsg && errorMsg.trim()!='') {
				alert("Failed to upload configuration file: " + errorMsg);
			} else {
				alert("Failed to upload configuration file: unknown error");
			}
			document.getElementById("submitbutton").disabled = false;
			document.getElementById("progress").style.display = 'none';
			return;
		} else if (postedResult) {
			var postedFile = postedResult.value; 
			if (postedFile && postedFile.trim()!='') {
				document.getElementById("submitbutton").disabled = false;
				document.getElementById("progress").style.display = 'none';
				document.getElementById("uploadedfile").value = postedFile.trim();
				document.getElementById("originalfile").value = document.getElementById("configurationfile").value;
				document.getElementById("mergeOption").value = document.getElementById("mergeChecked").checked;
				postedResult.value = "";
				document.getElementById("importbutton").click();
				return;
			}
		} 
	}
	setTimeout(importUploadedFile, 2000);
}
</script>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Import Configuration
</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>

<satellite:form id="AppForm" enctype="multipart/form-data" method="POST" target="resultPane">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ImportConfigPost"/>
	<table>
	<tr><td>Please select a configuration XML file to import.</td></tr>
	<tr><td>
Typical exported configuration file includes system configuration, site configuration, PO number configuraion, and locale mapping.
Existing configuration will be overwritten. Only license id is included in the export; configured translation providers are not 
exported and will not be overwritten. For security reason, passwords are not usable if the export is imported in a WCS instance
other than the original instance will it comes from. please re-enter the passwords after importing the configuration.</td></tr>
	<tr><td><BR/></td></tr>
	<tr><td><strong>Configuration file:&nbsp;</strong>
		<input name="configurationfile" id="configurationfile" type="file" accept="application/xml,application/zip" style="width:80em"/></td></tr>
	<tr><td><input id="mergeChecked" type="checkbox" checked>Merge with existing configuration</input></td></tr>
	<tr><td><input class="bt-image-long" type="button" name="upload" id="submitbutton" value="Submit" onclick="return validateFile();"/>
	<SPAN id="progress" style="display:none">&nbsp;&nbsp;&nbsp;Uploading...</SPAN></td></tr>
	</table>
</satellite:form>
<form id="importFileForm" style="display:none">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ImportConfigurationImport"/>
	<input type="hidden" name="originalfile" id="originalfile" value=""/>
	<input type="hidden" name="uploadedfile" id="uploadedfile" value=""/>
	<input type="hidden" name="merge" id="mergeOption" value="false"/>
	<input class="bt-image-long" type="submit" name="submit" id="importbutton" value="Submit"/>
</form>

</td></tr></TABLE>
<iframe id="resultPane" width="100%" height="100%" frameborder="0" style="display: block;" name="resultPane"/>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</cs:ftcs>
