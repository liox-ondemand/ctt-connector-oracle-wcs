<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UITranslationStatusManager">
<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
	
	<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
	
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<script type="text/javascript" src='js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: '0'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='wemresources/js/dojopatches.js'></script>

<script language="javascript"><!--
function inspectAsset(type, longid) {
	SitesApp.event(SitesApp.id('asset', type + ':' + longid), 'inspect'); // This works for .6.1
	<%--
	// SitesApp.event(fwutil.buildDocId(longid, type), 'inspect'); // This works for .8
	// Be sure to do ics.callement on OpenMarket/Xcelerate/UIFramework/LoadDojo at the top first
	--%>
}
-->

</script>

	<script language="Javascript"><!--
	
		function check(source, name) {
  			checkboxes = document.getElementsByName(name);
			for(var i=0, n=checkboxes.length;i<n;i++) {
			  checkboxes[i].checked = source.checked;
			}
		}
		
		function initReqIds(checkboxName)
		{
			document.getElementById("reqIds").value = fetchRequestIdString(checkboxName);
			if(document.getElementById("reqIds").value=="")
			{
				alert("No translation requests have been selected.");
				return false;
			}
			return true;
		}
		
		function fetchRequestIdString(checkboxName)
		{
			return getAllCheckboxIds(checkboxName);
		}
		
		function getAllCheckboxIds(n)
		{
			var concat = "";
			checkboxes = document.getElementsByName(n);
			if (checkboxes != null) {
				for(var k=0; k<checkboxes.length; k++)
				{
					if(checkboxes[k].checked)
					{
						concat = concat+checkboxes[k].value+"|";	
					}
				}
			}
			return concat;
		}
	//-->
	</script>

<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>
</head>
<body>		
<ics:callelement element="CustomElements/CT/HeaderHtml"/>
<ics:callelement element="CustomElements/CT/HeaderTab"/>
	
<c:set var="csroot" value='${csroot}'/>

	<satellite:form action="${csroot}ContentServer" method="post" name="AppForm">
	<input type="hidden" name="pagename" id="pagename" value="CustomElements/CT/TranslationStatusFront"/>
	<input type="hidden" name="doAction" id="doAction" value=""/>
	<input type="hidden" name="reqIds" id="reqIds" value=""/>
	<input type="hidden" name="c" id="reqIds" value="${c}"/>
	<input type="hidden" name="cid" id="reqIds" value="${cid}"/>
	<gsf:asset-load name="asset" c="${c}" cid="${cid}" attributes="name"/>



<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD>
<TABLE><TR><TD width="100%">
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/TranslationStatusFront" cid="${cid}" c="${c}"/>
<SPAN CLASS="title-text">Translation Status:</SPAN>&nbsp;<SPAN CLASS="title-value-text">${asset.name}</SPAN> 
&nbsp;&nbsp;<a href='<ics:getvar name="refreshurl"/>'><img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a>
</TD><TD align="right"><a href="javascript:inspectAsset('${c}','${cid}');">Back</a>
</TR></TABLE></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td>
	This screen displays the status of all translation requests from or to the current asset.
</td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<c:forEach var="segment" items="${segments}" varStatus="segs">
<TR><TD>
	<c:if test="${segment.type eq 'WaitingApproval'}">
		<c:choose><c:when test="${not empty segment.fromList or not empty segment.toList}">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td valign="top"><strong>Waiting approval:</strong></td>
				<td>&nbsp;</td>
				<td align="right">
					<input type="submit" name="Approve" class="f1image-medium" value="Approve Selected" 
							onclick='document.getElementById("doAction").value="accept"; return initReqIds("trids${segs.index}");'/>
				</td></tr></table>
		</c:when><c:otherwise>
			<strong>Waiting approval :</strong> &nbsp;there is no translation request waiting approval.
		</c:otherwise></c:choose>
	</c:if>
	<c:if test="${segment.type eq 'Active'}">
		<strong>Active :</strong>
		<c:if test="${empty segment.fromList and empty segment.toList}">
			&nbsp;there is no active translation request.
		</c:if>
	</c:if>
	<c:if test="${segment.type eq 'Inactive'}">
		<strong>Inactive :</strong>
		<c:if test="${empty segment.fromList and empty segment.toList}">
			&nbsp;there is no inactive translation request.
		</c:if>
	</c:if>
</td></tr>
<c:if test="${not empty segment.fromList or not empty segment.toList}">
<tr><td>
	<table BORDER="0" CELLSPACING="0" CELLPADDING="0" width="100%" align="right">
	<tr><td></td><td class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
	<tr><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td>
	<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
	<tr><td colspan="24" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

		<tr>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				<input type="checkbox" name="activecheck" onClick="check(this, 'trids${segs.index}')"/></td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Job Name
			</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Job Status
			</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Job Submitted	
			</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Provider
			</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Source
			</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Target
			</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Updated	
			</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				Status
			</td>
			<td class="tile-c" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
		
		
		<c:forEach var="entry" items="${segment.fromList}">
		<tr> 
			<td>
				<input type="checkbox" name="trids${segs.index}" value="${entry.requestid}"/>
			</td>
			<td></td>
			<c:choose><c:when test="${not empty entry.jobid}"> 
				<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/JobFront">
						<render:argument name="jobid" value="${entry.jobid}"/>
				</render:getpageurl>			
				<td nowrap="nowrap"><a href="${cs.detailsurl}">${entry.jobname}</a></td>
			</c:when><c:otherwise>
				<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/QueueFront">
				</render:getpageurl>			
				<td nowrap="nowrap"><a href="${cs.detailsurl}">- In Translation Queue -</a></td>
			</c:otherwise></c:choose>
			<td></td>
			<td nowrap="nowrap">${entry.jobstatusdisplayname}</td>
			<td></td>
			<td nowrap="nowrap" title='<fmt:formatDate value="${entry.submitteddate}" pattern="yyyy-MM-dd HH:mm:ss" />'>
				<fmt:formatDate value="${entry.submitteddate}" pattern="yyyy-MM-dd" />
			</td>
			<td></td>
			<td nowrap="nowrap">${entry.provider}</td>
			<td></td>
			<td class="thisasset">${entry.srclocale}</td>
			<td></td>
			<td> <c:choose><c:when test="${not empty entry.targetassetid}">
				<a href="javascript:inspectAsset('${entry.targetassetid.type}', '${entry.targetassetid.id}');">${entry.targetlocale}</a>
				</c:when><c:otherwise>${entry.targetlocale}</c:otherwise></c:choose></td>
			<td></td>
			<td nowrap="nowrap" title='<fmt:formatDate value="${entry.updateddate}" pattern="yyyy-MM-dd HH:mm:ss"/>'>	
				<fmt:formatDate value="${entry.updateddate}" pattern="yyyy-MM-dd" />
			</td>
			<td></td>
			<td nowrap="nowrap">${entry.txstatusdisplayname}</td>
		</tr>		
		</c:forEach>	
		
		<c:forEach var="entry" items="${segment.toList}">
		<tr> 
			<td>
				<input type="checkbox" name="trids${segs.index}" value="${entry.requestid}"/>
			</td>
			<td></td>
			<c:choose><c:when test="${not empty entry.jobid}"> 
				<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/JobFront">
						<render:argument name="jobid" value="${entry.jobid}"/>
				</render:getpageurl>			
				<td nowrap="nowrap"><a href="${cs.detailsurl}">${entry.jobname}</a></td>
			</c:when><c:otherwise>
				<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/QueueFront">
				</render:getpageurl>			
				<td nowrap="nowrap"><a href="${cs.detailsurl}">- In Translation Queue -</a></td>
			</c:otherwise></c:choose>
			<td></td>
			<td nowrap="nowrap">${entry.jobstatusdisplayname}</td>
			<td></td>
			<td nowrap="nowrap" title='<fmt:formatDate value="${entry.submitteddate}" pattern="yyyy-MM-dd HH:mm:ss" />'>
				<fmt:formatDate value="${entry.submitteddate}" pattern="yyyy-MM-dd" />
			</td>
			<td></td>
			<td nowrap="nowrap">${entry.provider}</td>
			<td></td>
			<td> <c:choose><c:when test="${not empty entry.sourceassetid}">
				<a href="javascript:inspectAsset('${entry.sourceassetid.type}', '${entry.sourceassetid.id}');">${entry.srclocale}</a>
				</c:when><c:otherwise>${entry.srclocale}</c:otherwise></c:choose></td>
			<td></td>
			<td class="thisasset">${entry.targetlocale}</td>
			<td></td>
			<td nowrap="nowrap" title='<fmt:formatDate value="${entry.updateddate}" pattern="yyyy-MM-dd HH:mm:ss"/>'>	
				<fmt:formatDate value="${entry.updateddate}" pattern="yyyy-MM-dd" />
			</td>
			<td></td>
			<td nowrap="nowrap">${entry.txstatusdisplayname}</td>
		</tr>		
		</c:forEach>	
		
		</table>
		
		
		</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr><tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<tr><td></td><td background="${csroot}Xcelerate/graphics/common/screen/shadow.gif"><IMG WIDTH="1" HEIGHT="5" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td>
	</tr>
	</table>
</td></tr>
</c:if>
<tr><td><br/></td></tr>
</c:forEach>
</TABLE>	
	</satellite:form>

<ics:callelement element="CustomElements/CT/FooterTab"/>
<ics:callelement element="CustomElements/CT/FooterHtml"/>
</body>

</gsf:root>
</cs:ftcs>