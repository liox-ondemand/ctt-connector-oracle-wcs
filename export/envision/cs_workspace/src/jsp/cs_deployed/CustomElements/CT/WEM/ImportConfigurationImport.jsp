<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIImportConfigurationManager">

<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<ics:callelement element="CustomElements/CT/WEM/Header"/>

<script>
</script>

<!-- Get the roles of the user for this site -->
<usermanager:getuserfromname username='<%=ics.GetSSVar("username")%>' objvarname="u"/>
<ccuser:getsiteroles name="u" site='<%=ics.GetSSVar("pubid")%>' objvarname="roleobject"/>
<rolelist:getall name="roleobject" varname="roles"/>
<input type="hidden" name="docId" value="0"/>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Import Configuration

</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>
<c:if test="${not empty errorMessage}">
<span style="color:red;">${errorMessage}</span>
</c:if>
<c:if test="${result.success}">
<p>Successfully imported configuration file '<%=ics.GetVar("originalfile")%>' into WCS</p>
<p>Updated the configuration for the following site(s):</p> 
<c:forEach var="site" items="${sites}"><li>${site}</li></c:forEach>
</c:if>
<render:getpageurl outstr="backurl" pagename="CustomElements/CT/WEM/ImportConfigurationFront"/>
<table>
<tr><td><a href='<ics:getvar name="backurl"/>'>Back</a></td></tr>
</table>

</td></tr></TABLE>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</gsf:root>
</cs:ftcs>
