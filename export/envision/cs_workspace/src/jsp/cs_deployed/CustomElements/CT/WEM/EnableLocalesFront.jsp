<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors;"

%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIEnableLocalesManager">
<ics:callelement element="CustomElements/CT/WEM/Header"/>
<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>

<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script language="Javascript"><!--
function hideDiv(divid) {
	hide(document.getElementById(divid));
}

function hide(element) {
	element.style.visibility = 'hidden';
	element.style.display = 'none';
}

function showBlock(elementid) {
	show(document.getElementById(elementid), 'block');
}

function show(element, display) {
	element.style.visibility = 'visible';
	element.style.display = display;
}

function popup(event, locale) {
	var IE = document.all ? true : false; // check to see if you're using IE
	var cursorX, cursorY;
	
	if (IE) //do if internet explorer 
    {
        cursorX = event.clientX + document.body.scrollLeft;
        cursorY = event.clientY + document.body.scrollTop;
    }
    else  //do for all other browsers
    {
        cursorX = (window.Event) ? event.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        cursorY = (window.Event) ? event.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    }
    
	document.getElementById("available_sites_"+locale).style.top = (cursorY + 2) + "px"; 
	document.getElementById("available_sites_"+locale).style.left = (cursorX + 2) + "px"; 
	showBlock("available_sites_"+locale);	
	showBlock("modal_blocker");
}

function hidePopup(locale) {
	var addedSites = document.getElementById("added_sites_" + locale).value;
	
	var boxes = document.getElementsByName("additional_sitename_" + locale);
	for (var i=0; i < boxes.length; i++) {
		if (addedSites.indexOf("," + boxes[i].value + ",")>=0) {
			boxes[i].checked = true;
		} else {
			boxes[i].checked = false;
		}
	}
	
	hideDiv("available_sites_"+locale);
	hideDiv("modal_blocker");
}

function addToSites(locale) {
	var addedSites = "";
	var boxes = document.getElementsByName("additional_sitename_" + locale);
	for (var i=0; i < boxes.length; i++) {
		if (boxes[i].checked) {
			addedSites += boxes[i].value + ",";
		}
	}
	if (addedSites.length>0) {
		setIsChanged("show_added_sites_" + locale, locale);
	}
	document.getElementById("show_added_sites_" + locale).innerHTML = addedSites;
	document.getElementById("added_sites_" + locale).value = "," + addedSites;
	hideDiv("available_sites_"+locale);
	hideDiv("modal_blocker");
}

function processForm(action) {
	
	document.getElementById("action").value = action;
	var hasError = false;
	var msg = "The following error(s) occurred:";
	if (action == "update") {
		if (document.getElementById("ischanged").value != "true" || 
				(document.getElementById("show_hidden") &&
				document.getElementById("show_hidden").value == "false" &&
				document.getElementById("hidden_changed").value == "true" && 
				!confirm("There are hidden locales and you have made changes to those locales. " + 
						"If you wish to save those changes, please cancel the operation and show " +
						"those locales first before saving. If you proceed now those changes will be discarded. " +
						"Do you wish to continue?"))) {
			return;
		}
		var inputs = document.getElementsByTagName("input");
		for (var i=0; i < inputs.length; i++) {
			if (inputs[i].id.indexOf("mapped_value_") > -1) {
				if (inputs[i].value == "") {
					hasError = true;
					msg += "\n- All fields are required";
					break;
				}
			}
		}
	} else if (action == "reset") {
		if (document.getElementById("ischanged").value != "true" ||
				!confirm("Are you sure you wish to discard unsaved changes?")) {
			return;
		}
	} else {
		if (document.getElementById("dimension_name").value == '' ||
				document.getElementById("dimension_descr").value == '' ) {
			hasError = true;
			msg += "\n- 'Locale name in WCS' and 'Description' fields are required";
		} 
		
		var sites = document.getElementsByName("sitename");
		hasSites = false;
		for (var i=0; i < sites.length; i++) {
			if (sites[i].checked) {
				hasSites = true;
				break;
			}
		}
		if (!hasSites) {
			hasError = true;
			msg += "\n- The new locale needs to be assigned to at least one site";
		}
	}
	
	if (hasError) {
		alert(msg);
	} else {
		document.forms["AppForm"].submit();
	}
	
}

function setIsChanged(elementid, locale) {
	document.getElementById('edited_' + locale).value='true';
	document.getElementById("ischanged").value = true;
	if (document.getElementById("is_hidden_"+locale)) {
		document.getElementById("hidden_changed").value = true;
	}
	var highlightElem = document.getElementById(elementid);
	if (highlightElem) {
		highlightElem.style.color = "red";
	}
}

function setIsChangedBg(elementid, locale) {
	document.getElementById('edited_' + locale).value='true';
	document.getElementById("ischanged").value = true;
	if (document.getElementById("is_hidden_"+locale)) {
		document.getElementById("hidden_changed").value = true;
	}
	var highlightElem = document.getElementById(elementid);
	if (highlightElem) {
		highlightElem.style.background = "red";
	}
}

function showOrHideLocales(show) {
	var showRows = document.getElementsByName(show? 'fold_row' : 'unfold_row');
	for (var i=0; i < showRows.length; i++) {
		showRows[i].style.visibility = 'visible';
		showRows[i].style.display='table-row';
	}	
	var hideRows = document.getElementsByName(show? 'unfold_row' : 'fold_row');
	for (var i=0; i < hideRows.length; i++) {
		hideRows[i].style.visibility = 'hidden';
		hideRows[i].style.display='none';
	}
	document.getElementById("show_hidden").value = show? 'true' : 'false';
}

function promptNewName(oldname) {
	var hiddenfield = document.getElementById("newname_" + oldname);
	var editedfield = document.getElementById("edited_" + oldname);
	if (document.getElementById("ischanged").value == "true") {
		alert("The following error occurred:\n-You have unsaved changes. Click \"Save Changes\" before changing the locale name.");
	} else {
		var newname = prompt("Enter new locale name:", oldname);
		if (newname != null) {
			var confirmed = confirm("Are you sure you want to change the locale name?");
			if (confirmed) {
				hiddenfield.value = newname;
				editedfield.value = 'true';
				document.getElementById("action").value = "update";
				document.forms["AppForm"].submit();				
			}
		}
	}
}

function checkIfChanged() {
	if (document.getElementById("ischanged").value == "true") {
		hasError = true;
		msg += "\n- You have unsaved changes. Click \"Save Changes\" before changing the locale name.";
	}
}
//-->
</script>

<div class="blocking_overlay" id="modal_blocker"></div>

<satellite:form name="AppForm" id="AppForm" method="POST">
<input type="hidden" id="action" name="action" value=""/>
<input type="hidden" id="ischanged" name="ischanged" value="false"/>
<input type="hidden" name="submitted" value="true"/>
<input type="hidden" name="pagename" value="CustomElements/CT/WEM/EnableLocalesFront"/>
	
	
	
<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important;">
<TR><TD colspan="3"><SPAN CLASS="title-text">Locales and Locale Mapping</SPAN></SPAN></TD></TR>
<tr><td colspan="3"><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td colspan="3" class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td colspan="3"><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD colspan="3">This screen allows you to configure new and existing locales, including their mapping to Clay Tablet language codes.<br/><br/></td></tr>
<tr><TD>
<input type="button" name="button" value="Save Changes" onClick="javascript:processForm('update');" class="f1image-medium"/>
<input type="button" name="button" value="Reset" onClick="javascript:processForm('reset');" class="f1image-medium"/></TD>
<TD>&nbsp;&nbsp;&nbsp;&nbsp;</TD>
<TD width="40%"><input type="button" name="button" value="Add New Locale" class="f1image-medium" 
	onclick="javascript:showBlock('add-locale-panel')"/></TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD VALIGN="TOP">




<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
<tr><td></td><td class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
<tr><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td>
<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<tr><td colspan="12" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr>
<td class="tile-a" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Locale Name in WCS</DIV></td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Description</DIV></td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">CT Language Code</DIV></td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Hidden</DIV></td>
<td class="tile-c" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Shared to Sites</DIV></td>
<td class="tile-c" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
</tr>
<tr><td colspan="12" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>




<c:forEach var="locale" items="${locales}">
	<asset:list type="Dimension" list="dimList">
		<asset:argument name="name" value="${locale}"/>
	</asset:list>
			 
<tr class="tile-row-normal">
<td valign="middle" NOWRAP="NOWRAP"></td>
<td><BR></td><td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset"><div style="float:left;">${locale}</div>
	<input type="hidden" id="edited_${locale}" name="edited_${locale}" value=""/>
	<div style="float:right;"><a href="#" onclick="promptNewName('${locale}');">edit</a><input type="hidden" id="newname_${locale}" name="newname_${locale}" value="${locale}"/></div><BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<ics:listloop listname="dimList">
 		<input type="text" id="newdesc_${locale}" name="newdesc_${locale}" value="<ics:listget listname="dimList" fieldname="description"/>"  
 			onChange="setIsChanged('newdesc_${locale}', '${locale}')"/>   
	</ics:listloop>
	<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<input type="text" id="mapped_value_${locale}" name="mapped_value_${locale}" 
			value="${languagemappings[locale]}"	onChange="setIsChanged('mapped_value_${locale}', '${locale}')"/>
<BR></DIV></td>
<td><BR></td><td ALIGN="CENTER"><SPAN id="hidden_frame_${locale}" name="hidden_frame">
	<input type="checkbox" name="hidden_${locale}" onChange="setIsChangedBg('hidden_frame_${locale}', '${locale}')"/></SPAN></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" style="text-align:right;">
	<SPAN id="show_added_sites_${locale}" style="color:red;"></SPAN>
	<input type="hidden" id="added_sites_${locale}" name="added_sites_${locale}" value=""></input>
	<c:forEach var="sitename" items="${sharedsites[locale]}" varStatus="loop">
    	${sitename}<c:if test="${!loop.last}">,</c:if>
    </c:forEach>&nbsp;						
    <c:if test="${fn:length(availablesites[locale]) gt 0}">
	    <span id="addSitesToLocale_${locale}" class="add_filter_ctrl" 
	    		title="Add more sites" onclick="javascript:popup(event, '${locale}');">+</span>&nbsp;
	</c:if>
</td>
<td><BR></td>
</tr>

</c:forEach>
<c:if test="${fn:length(hiddenlocales) gt 0}">
<tr name="fold_row" style="display:none;"><td colspan="11">&nbsp;<a href="javascript:showOrHideLocales(false)"
	><img id="hidden_locale_fold" src="js/fw/images/ui/ui/search/upArrow.png" style="cursor:pointer;" border="0"/>&nbsp;Hide ${fn:length(hiddenlocales)} Locales</a>&nbsp;
	<input type="hidden" id="show_hidden" name="show_hidden" value=""/>
	<input type="hidden" id="hidden_changed" value=""/></td><tr>
<tr name="unfold_row"><td colspan="11">&nbsp;<a href="javascript:showOrHideLocales(true)"
	><img id="hidden_locale_unfold" src="js/fw/images/ui/ui/search/downArrow.png" style="cursor:pointer;" border="0"/>&nbsp;Show ${fn:length(hiddenlocales)} Hidden Locales</a>&nbsp;</td><tr>

<c:forEach var="locale" items="${hiddenlocales}">
	<asset:list type="Dimension" list="dimList">
		<asset:argument name="name" value="${locale}"/>
	</asset:list>
			 
<tr class="tile-row-normal"  name="fold_row" style="display:none;">
<td valign="middle" NOWRAP="NOWRAP"></td>
<td><BR></td><td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset"><div style="float:left;">${locale}</div>
	<input type="hidden" id="edited_${locale}" name="edited_${locale}" value=""/>
	<input type="hidden" id="is_hidden_${locale}" name="is_hidden_${locale}" value="true"/>
	<div style="float:right;"><a href="#" onclick="promptNewName('${locale}');">edit</a><input type="hidden" id="newname_${locale}" name="newname_${locale}" value="${locale}"/></div><BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<ics:listloop listname="dimList">
 		<input type="text" id="newdesc_${locale}" name="newdesc_${locale}" value="<ics:listget listname="dimList" fieldname="description"/>"  
 			onChange="setIsChanged('newdesc_${locale}', '${locale}');"/>   
	</ics:listloop>
	<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<input type="text" id="mapped_value_${locale}" name="mapped_value_${locale}" 
			value="${languagemappings[locale]}"	onChange="setIsChanged('mapped_value_${locale}', '${locale}')"/>
<BR></DIV></td>
<td><BR></td><td ALIGN="CENTER"><SPAN id="hidden_frame_${locale}" name="hidden_frame">
	<input type="checkbox" name="hidden_${locale}" checked onChange="setIsChangedBg('hidden_frame_${locale}', '${locale}')"/></SPAN></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" style="text-align:right;">
	<SPAN id="show_added_sites_${locale}" style="color:red;"></SPAN>
	<input type="hidden" id="added_sites_${locale}" name="added_sites_${locale}" value=""></input>
	<c:forEach var="sitename" items="${sharedsites[locale]}" varStatus="loop">
    	${sitename}<c:if test="${!loop.last}">,</c:if>
    </c:forEach>&nbsp;						
    <c:if test="${fn:length(availablesites[locale]) gt 0}">
	    <span id="addSitesToLocale_${locale}" class="add_filter_ctrl" 
	    		title="Add more sites" onclick="javascript:popup(event, '${locale}');">+</span>&nbsp;
	</c:if>
</td>
<td><BR></td>
</tr>

</c:forEach>
</c:if>
</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr><tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td></td><td background="${csroot}Xcelerate/graphics/common/screen/shadow.gif"><IMG WIDTH="1" HEIGHT="5" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td>
</tr>
</table>

<c:forEach var="locale" items="${locales}">
	<div id="available_sites_${locale}" class="popup_panel" style="padding-top:0px">
	<asset:list type="Dimension" list="dimList">
		<asset:argument name="name" value="${locale}"/>
	</asset:list>
			 
	<table>
	<tr><td class="remove_filter_ctrl" style="width:100%"/>
		<td class="remove_filter_ctrl" style="text-align:right;"><SPAN onclick="hidePopup('${locale}');">x</SPAN></td></tr>	
	<c:forEach var="sitename" items="${availablesites[locale]}" varStatus="loop">
		<tr><td><input type="checkbox" name="additional_sitename_${locale}" value="${sitename}"> ${sitename}<td></td></tr>
		<c:if test="${loop.last}"><tr><td>
			<input type="button" name="button" value="Apply" class="f1image-small" onClick="javascript:addToSites('${locale}');"/></TD></TR>
		</c:if>	
	</c:forEach>
	</table>
	</div>
</c:forEach>

<c:forEach var="locale" items="${hiddenlocales}">
	<div id="available_sites_${locale}" class="popup_panel" style="padding-top:0px">
	<asset:list type="Dimension" list="dimList">
		<asset:argument name="name" value="${locale}"/>
	</asset:list>
			 
	<table>
	<tr><td class="remove_filter_ctrl" style="width:100%"/>
		<td class="remove_filter_ctrl" style="text-align:right;"><SPAN onclick="hidePopup('${locale}');">x</SPAN></td></tr>	
	<c:forEach var="sitename" items="${availablesites[locale]}" varStatus="loop">
		<tr><td><input type="checkbox" name="additional_sitename_${locale}" value="${sitename}"> ${sitename}<td></td></tr>
		<c:if test="${loop.last}"><tr><td>
			<input type="button" name="button" value="Apply" class="f1image-small" onClick="javascript:addToSites('${locale}');"/></TD></TR>
		</c:if>	
	</c:forEach>
	</table>
	</div>
</c:forEach>

</TD>
<TD>&nbsp;&nbsp;&nbsp;&nbsp;</TD><TD width="40%" VALIGN="TOP">
<TABLE id="add-locale-panel" BORDER="0" CELLPADDING="0"	CELLSPACING="0" style="display: none;">
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>
	<TABLE CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Locale Name in WCS:</TD>
	<TD><IMG HEIGHT="1" WIDTH="5" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></TD>
	<TD class="form-inset"><input type="text" name="dimension_name" id="dimension_name" value='${dimname}'/> * &nbsp; 
		<input type="button" name="button" value="Add Locale" class="f1image-small" onClick="javascript:processForm('enable');"/></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Description:</TD><TD></TD><TD class="form-inset">
		<input type="text" name="dimension_descr" id="dimension_descr" value='${dimdescr}'/> * &nbsp; 
		<input type="button" name="button" value="Cancel" class="f1image-small" onClick="javascript:hideDiv('add-locale-panel');"/></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">CT Language Code:</TD><TD></TD><TD class="form-inset">
		<input type="text" name="mapped_value" id="mapped_value" value='${mapped_value}'/></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Shared to Sites:</TD><TD></TD><TD class="form-inset">
	<c:forEach var="curr" items="${siteslist}">
			<input type="checkbox" name="sitename" value="${curr}"> ${curr}<br/>
	</c:forEach></TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

	</TABLE>
</TD></TR>
</TABLE>

</TABLE>
</TD></TR></TABLE>






</satellite:form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
<script>
	showOrHideLocales(${showHidden});
</script>
</gsf:root>
</cs:ftcs>
