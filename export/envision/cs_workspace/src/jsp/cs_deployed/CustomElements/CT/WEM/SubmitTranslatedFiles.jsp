<%@ page import="com.claytablet.wcs.ui.UISubmitTranslatedFilesManager" %>

<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<ics:callelement element="CustomElements/CT/WEM/Header"/>

<script>
function validateFile() {
	var file = document.getElementById("translatedfile").value;

	if (file && file.trim()!='') {
		document.getElementById("submitbutton").disabled = true;
		document.getElementById("progress").style.display = 'inline';
		setTimeout(importUploadedFile, 2000);

		document.getElementById("submit").click();
		return true;
	} else {
		return false;
	}
}

function importUploadedFile() {
	var iframe = document.getElementById('resultPane');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
	if (innerDoc) {
		var postedResult = innerDoc.getElementById("uploadResult");
		var postedError = innerDoc.getElementById("uploadError");
		if (postedError) {
			var errorMsg = postedError.value;
			if (errorMsg && errorMsg.trim()!='') {
				alert("Failed to upload translation file: " + errorMsg);
			} else {
				alert("Failed to upload translation file: unknown error");
			}
			document.getElementById("submitbutton").disabled = false;
			document.getElementById("progress").style.display = 'none';
			return;
		} else if (postedResult) {
			var postedFile = postedResult.value; 
			if (postedFile && postedFile.trim()!='') {
				document.getElementById("submitbutton").disabled = false;
				document.getElementById("progress").style.display = 'none';
				document.getElementById("uploadedfile").value = postedFile.trim();
				document.getElementById("originalfile").value = document.getElementById("translatedfile").value;
				postedResult.value = "";
				document.getElementById("importbutton").click();
				return;
			}
		}
	}
	setTimeout(importUploadedFile, 2000);
}
</script>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Submit Translated Files
</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>

<satellite:form id="uploadFileForm" enctype="multipart/form-data" method="POST" target="resultPane">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/SubmitTranslatedFilesPost"/>
	<table>
	<tr><td colspan="2">Please select a translate file or a ZIP file that contains translated files to update the target assets.</td></tr>
	<tr><td><BR/></td></tr>
	<tr><td><Strong>Translated file(s):&nbsp;</Strong></td><td>
		<input name="translatedfile" id="translatedfile" type="file" accept="application/xml,application/zip"/></td></tr>
	<tr><td colspan="2">
	<input class="bt-image-long" type="button" name="submitbutton" id="submitbutton" value="Submit" onclick="return validateFile();"/>
	<input class="bt-image-long" type="submit" name="submit" id="submit" value="Submit" style="display: none"/>
	<SPAN id="progress" style="display:none">&nbsp;&nbsp;&nbsp;Uploading...</SPAN></td></tr>
	</table>
</satellite:form>
<form id="importFileForm" style="display:none">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/SubmitTranslatedFilesImport"/>
	<input type="hidden" name="originalfile" id="originalfile" value=""/>
	<input type="hidden" name="uploadedfile" id="uploadedfile" value=""/>
	<input class="bt-image-long" type="submit" name="import" id="importbutton" value="Import"/>
</form>

</td></tr></TABLE>
<iframe id="resultPane" width="100%" height="100%" frameborder="0" style="display: block;" name="resultPane"/>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</cs:ftcs>
