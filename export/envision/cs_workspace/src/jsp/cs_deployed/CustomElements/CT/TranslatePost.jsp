<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
	
<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
<script type="text/javascript">
	function getLocale(loc) {
		//convert from CS's locale notation format to the format Dojo NLS uses.
		//(e.g. en_US -> en-us, fr_FR -> fr-fr, etc.)
		return loc.toLowerCase().replace('_', '-');
	}
	var djConfig = {
		fw_csPath: '<%=ics.GetProperty("ft.cgipath")%>',
		pubName: 'FirstSiteII',
		parseOnLoad: true,
		locale: getLocale('en_US')
	};
</script>


	



	<script type="text/javascript" src="${csroot}wemresources/js/WemContext.js"></script>
	<script type="text/javascript">
		//initialize wemcontext before loading dojo layers (see AppBar FIXME)
		//Unfortunately it needs to be AFTER dojo because WemContext relies on xhrGet!
		WemContext.initialize('${csroot}','http://localhost:8080/cas/logout');
		var wemcontext = WemContext.getInstance();
		// If a new browser window is opened, as a pop up or new tab or new window, and if the Wem top bar is not there.
		// Then global sls js object would not avilable. In that scenario we load the object in two ways based on how the new window opened.
		// If as a pop-up
		if(window.opener) window.fw_sls_obj = window.opener.top.fw_sls_obj;
		// If as a browser tab or new brower window		
		if(!window.opener && window.top === window.self) document.write('<script type="text/javascript" src="${csroot}ContentServer?user_locale=en-us&pagename=fatwire%2Fui%2Futil%2FGetSLSObj"><' + '/script>');
	</script>




<script type="text/javascript" src='${csroot}js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: '0'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='${csroot}wemresources/js/dojopatches.js'></script>

	<link href="${csroot}Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
	<link href="${csroot}Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
	<link href="${csroot}Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
	<link href="${csroot}Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>

		
</head>
<body class="AdvForms">
	
	<satellite:form action="ContentServer" method="post" name="AppForm">
	<INPUT TYPE='HIDDEN' NAME='_authkey_' VALUE='554446B0FA2A97A5208D6158954E06ED9AE51AAFBE5ECAC8511A06205DDB49975CFA03961A21C220A83CC500F7FE9845'>
		
		
		
		
		<input type="hidden" name="_charset_" value="UTF-8" />
		<input type="hidden" name="cs_environment" value="ucform" />
		<input type="hidden" name="cs_formmode" value="WCM" />
		<input type="hidden" name="PubName" value='${cs.pubid}' />
		

<ics:callelement element="CustomElements/CT/HeaderTab"/>
<gsf:root action="class:com.claytablet.wcs.ui.UITranslationWizard">

</gsf:root>

</satellite:form>
<ics:callelement element="CustomElements/CT/FooterTab"/>

		
	<script>
		var num = document.AppForm.length, i;
		document.AppForm.encoding="application/x-www-form-urlencoded";
		for (i=0; i < num; i++) {
			if (document.AppForm.elements[i].type=="file") document.AppForm.encoding="multipart/form-data";
		}
		(function() {
			var _alert = window.alert;
			window.alert = function() {
				var out = SitesApp.getActiveView() || SitesApp;
				out.clearFeedback();
				out.clearMessage();
				out.message(arguments[0], arguments[1] || "error");
				//fw/ui/controller/AdvancedController subscribes to this event
				parent.dojo.publish('/fw/ui/form/alert',[]);
				return true;
			};
		})();
	</script>
	
</body>
</html>
</cs:ftcs>
