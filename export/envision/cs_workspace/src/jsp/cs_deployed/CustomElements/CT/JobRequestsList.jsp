<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>

<% if ("WEM".equals(ics.GetVar("app"))) { %>
	<c:set var="app" value="WEM"/>
<% } %>
<% if ("jobdetail".equals(ics.GetVar("page"))) { %>
	<c:set var="page" value="jobdetail"/>
<% } %>

	<gsf:root action="class:com.claytablet.wcs.ui.UIJobRequestsManager">
		<input type="hidden" id="total_unfiltered_requests" value="${job_totalrecords}"/>
		<div id="updated_reqid_section" style="visibility:hidden;display:none;">
			<select multiple name="reqids" id="reqids" width="15" size="10" style="display:none;">
			<c:forEach var="req" items="${translationRequests}">
				<option value="${req.id}">${req.status}</option>
			</c:forEach>
			</select>
		</div>	
		<SPAN id="h_request_status_summary" style="visibility:hidden;display:none">
			<c:choose><c:when test="${job.requestErrorCount gt 0}">
				Total (<SPAN style="color:red">Error</SPAN>)&nbsp;${job_totalrecords}&nbsp;(<SPAN style="color:red">${job.requestErrorCount}</SPAN>)
			</c:when><c:otherwise>Total ${job_totalrecords}</c:otherwise></c:choose> 
			&nbsp;&nbsp;&nbsp;&nbsp;
			<c:forEach var="status" items="${job.requestStatuses}">
				${status.displayName} ${job.requestCountsByStatusMap[status]}
				<c:if test="${job.requestErrorCountsByStatusMap[status] gt 0}">
					(<SPAN style="color:red">${job.requestErrorCountsByStatusMap[status]}</SPAN>)
				</c:if>
				&nbsp;&nbsp;&nbsp; 
			</c:forEach></SPAN>
		<SPAN id="h_request_list_summary" style="visibility:hidden;display:none">
		Showing	<c:if test="${fn:length(page_startrecords)>1}">${page_firstrec+1} to ${page_lastrec+1} of </c:if>${page_totalrecords}
		<c:choose> <c:when test="${page_totalrecords eq job_totalrecords}">
			 total record(s),
		</c:when><c:otherwise>
			filtered record(s)<c:if test="${empty job}"> out of ${job_totalrecords} in total</c:if>, 
		</c:otherwise></c:choose></SPAN>
		<SPAN id="h_request_list_pages" style="visibility:hidden;display:none">
			<c:if test="${fn:length(page_startrecords)<=1 and page_totalrecords>10}">
			&nbsp;&nbsp;Pages:&nbsp;1&nbsp;
			</c:if>
			<c:if test="${fn:length(page_startrecords) > 1}">
			
				&nbsp;&nbsp;Pages:&nbsp;
				<c:forEach var="page_startids" items="${page_startrecords}" varStatus="status">
					<c:choose> <c:when test="${page_startids lt 0}" >
						<a href="#requests" onClick="setFirstRecord(${page_startids * -1});">...</a>
					</c:when> <c:otherwise>
						<fmt:formatNumber value="${page_startids div pagesize + 1}" var="pagenum" pattern="0" />
						<c:if test="${page_firstrec ne page_startids}">
							<a href="#requests" onClick="setFirstRecord(${page_startids});">${pagenum}</a></c:if>
						<c:if test="${page_firstrec eq page_startids}">${pagenum}</c:if>
					</c:otherwise></c:choose>
					<c:if test="${!status.last}"> | </c:if> 
				</c:forEach>
			</c:if>&nbsp;
			
			<c:if test="${fn:length(translationRequests) > 10}">
				(<select id="page_size" name="page_size" value="${pagesize}" onchange="setFirstRecord(0);">
					<c:forEach var="size" items="${page_size_options}">
						<option value="${size}" <c:if test="${size eq pagesize}"> selected</c:if>>${size}</option>
					</c:forEach>
				</select>&nbsp;per page)&nbsp;&nbsp;
			</c:if>
			<c:if test="${fn:length(translationRequests) <= 10}">
				<input type="hidden" name="page_size" id="page_size" value="${pagesize}"/>
			</c:if>
		</SPAN>			
		<table class="width-inner-100" BORDER="0" CELLSPACING="0" CELLPADDING="0" style="width:100%;">
		<tr><td></td><td class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
		<tr><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td>
		<table class="width-inner-100 fixed_width_table" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
		<col width="1%">
		<col width="1%">
		<col width="3%">
		<col width="1%">
		<col width="20%">
		<col width="2%">
		<col width="20%">
		<col width="2%">
		<col width="10%">
		<col width="2%">
		<col width="15%">
		<col width="2%">
		<col width="10%">
		<col width="2%">
		<c:if test="${not empty job and job.status ne 'CREATED' and job.status ne 'COMPLETED'}"> 
		<col width="12%">
		<col width="2%">
		</c:if>
		<c:if test="${'WEM' eq app}">
		<col width="10%">
		<col width="2%">
		</c:if>
		<col width="18%">
		<col width="2%">
		<tr><td colspan="18" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		<tr>
		<td class="tile-a" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
		
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">
			<input type="checkbox" id="toggler" name="toggler" onClick="toggleCheckboxes();"/></DIV></td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "nativeSourceId"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("nativeSourceId DESC")'>Asset Type : ID 
						<img src="ct/images/upArrow.png" border="0"/></SPAN></DIV></td>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("nativeSourceId ASC")'>Asset Type : ID 
						<img src="ct/images/downArrow.png" border="0"/></SPAN></DIV></td>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("nativeSourceId ASC")'>Asset Type : ID</SPAN></DIV></td>
			</c:otherwise>	</c:choose>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "name"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("name DESC")'>Asset Name
						<img src="ct/images/upArrow.png" border="0"/></SPAN></DIV></td>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("name ASC")'>Asset Name
						<img src="ct/images/downArrow.png" border="0"/></SPAN></DIV></td>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("name ASC")'>Asset Name</SPAN></DIV></td>
			</c:otherwise>	</c:choose>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "nativeSourceLanguage"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("nativeSourceLanguage DESC")'>Source
						<img src="ct/images/upArrow.png" border="0"/></SPAN></DIV></td>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("nativeSourceLanguage ASC")'>Source
						<img src="ct/images/downArrow.png" border="0"/></SPAN></DIV></td>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("nativeSourceLanguage ASC")'>Source</SPAN></DIV></td>
			</c:otherwise>	</c:choose>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
				<DIV class="non_sortable_table_title">Source Updated</SPAN></DIV></td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "nativeTargetLanguage"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("nativeTargetLanguage DESC")'>Target
						<img src="ct/images/upArrow.png" border="0"/></SPAN></DIV></td>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("nativeTargetLanguage ASC")'>Target
						<img src="ct/images/downArrow.png" border="0"/></SPAN></DIV></td>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onclick='sortPage("nativeTargetLanguage ASC")'>Target</SPAN></DIV></td>
			</c:otherwise>	</c:choose>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<c:if test="${not empty job and job.status ne 'CREATED' and job.status ne 'COMPLETED'}"> 
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "inError ASC, status" or sort_by eq "inError DESC, status"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("inError ASC, status DESC")'>Status
						<img src="ct/images/upArrow.png" border="0"/></SPAN></DIV></td>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("inError DESC, status ASC")'>Status
						<img src="ct/images/downArrow.png" border="0"/></SPAN></DIV></td>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onclick='sortPage("inError DESC, status ASC")'>Status</SPAN></DIV></td>
			</c:otherwise>	</c:choose>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</c:if>
		<c:if test="${'WEM' eq app}">
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "createdUser"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("createdUser DESC")'>Creator
						<img src="ct/images/upArrow.png" border="0"/></SPAN></DIV></td>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("createdUser ASC")'>Creator
						<img src="ct/images/downArrow.png" border="0"/></SPAN></DIV></td>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("createdUser ASC")'>Creator</SPAN></DIV></td>
			</c:otherwise>	</c:choose>
		<td class="tile-c" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
		</c:if>
		<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "updatedDate"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("updatedDate DESC")'>Last Updated
						<img src="ct/images/upArrow.png" border="0"/></SPAN></DIV></td>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("updatedDate ASC")'>Last Updated
						<img src="ct/images/downArrow.png" border="0"/></SPAN></DIV></td>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("updatedDate ASC")'>Last Updated</SPAN></DIV></td>
			</c:otherwise>	</c:choose>
		<td class="tile-c" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
		</tr>
		<tr><td colspan="18" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		
		<c:if test="${fn:length(translationRequests) gt 0 }">
			<c:forEach var="tr" items="${translationRequests}" begin="${page_firstrec}" end="${page_lastrec}">
				<%
					com.claytablet.client.producer.TranslationRequest tr = 
							(com.claytablet.client.producer.TranslationRequest)pageContext.getAttribute("tr");
				%>			
			<c:set var="assetidparts" value="${fn:split(tr.nativeSourceId, ':')}" />
			<c:if test="${not empty tr.nativeTargetId}">
				<c:set var="targetidparts" value="${fn:split(tr.nativeTargetId, ':')}" />
			</c:if>
			<tr class="tile-row-normal">
			<td valign="middle" NOWRAP="NOWRAP"></td>
			<td><BR></td>
			<td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset">
				<input type="checkbox" name="page_reqids" id="page_reqids_${tr.id}" value="${tr.id}" onClick="updateDropdownAndTotal(this.value, this.checked)"/><BR>
			</DIV></td><td><BR></td>
			<td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" 
					title='<c:out value="${assetidparts[0]} : ${assetidparts[1]}" escapeXml="true"/>'>
				<c:out value="${assetidparts[0]} : ${assetidparts[1]}" escapeXml="true"/></td>
			<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" 
					title='<c:out value="${tr.name}" escapeXml="true"/>'>
				<c:out value="${tr.name}" escapeXml="true"/></td>
			<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">
			<c:choose><c:when test="${'WEM' ne app}">
			<a href="javascript:inspectAsset('${assetidparts[0]}', '${assetidparts[1]}');">
					${tr.nativeSourceLanguage}</a></c:when>
			<c:otherwise>${tr.nativeSourceLanguage}</c:otherwise></c:choose></td>
			<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">
				<%= (new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(com.claytablet.wcs.system.Utils.stringToExtendedAssetId(ics, tr.getNativeSourceId()).getUpdatedDate()) %>
			</td>
			<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">
			<c:choose><c:when test="${(not empty tr.nativeTargetId) and 'WEM' ne app}">
				<a href="javascript:inspectAsset('${assetidparts[0]}', '${targetidparts[1]}');">
						${tr.nativeTargetLanguage}</a></c:when>
			<c:otherwise>${tr.nativeTargetLanguage}</c:otherwise></c:choose></td>					
			<c:if test="${not empty job and job.status ne 'CREATED'}"> 
			<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset"><a 
				<c:choose><c:when test="${tr.inError}"> style="color:red" 
				</c:when>
				<c:otherwise>title="${tr.extStatus}" </c:otherwise></c:choose>
				title="${tr.extStatus}" href="javascript:showRequestDetail('${tr.id}')"> ${tr.extStatus} </a></td>
			</c:if>
			<c:if test="${'WEM' eq app}">
			<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${tr.createdUser}">${tr.createdUser}</td>
			</c:if>
			<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">
			<fmt:formatDate value="${tr.updatedDate}" pattern="yyyy-MM-dd HH:mm:ss" />
			</td>
			<td><BR></td>
			</tr>
			</c:forEach>
		</c:if>
		</table></td>
		<td class="tile-dark" VALIGN="top" WIDTH="1">
			<IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif">
		</td>
		<tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		<tr><td></td><td background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/shadow.gif"><IMG WIDTH="1" HEIGHT="5" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td>
		</td></tr></table>	
	</gsf:root>
</cs:ftcs>
