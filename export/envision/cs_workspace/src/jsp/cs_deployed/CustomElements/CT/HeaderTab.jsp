<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="assetset" uri="futuretense_cs/assetset.tld"
%><%@ taglib prefix="commercecontext" uri="futuretense_cs/commercecontext.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="listobject" uri="futuretense_cs/listobject.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="searchstate" uri="futuretense_cs/searchstate.tld"
%><%@ taglib prefix="siteplan" uri="futuretense_cs/siteplan.tld"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors"
%><cs:ftcs><%--

INPUT

OUTPUT

--%>

<script language="Javascript">
var activeView, docId;
SitesApp = parent.SitesApp;
if (SitesApp) {
	// if (!parent.dojo || !SitesApp) alert("No dojo or sitesapp");;
	activeDoc = docId !== undefined ?
	SitesApp.getDocument(docId):
	SitesApp.getActiveDocument();
	activeView = activeDoc.get('activeView');		
	if (!activeView.hideLoadingScreen) alert ("No hide loading screen");
	activeView.hideLoadingScreen();
}
</script>
<style>
.message { width:100%;background-color:#F4F7BC;border:1px solid #000000;padding:8px;margin-bottom:10px; }
.warning { width:100%;background-color:#FF0000;border:1px solid #000000;padding:8px;margin-bottom:10px; }
.f1image-small { background-image: url('<%=ics.GetProperty("ft.cgipath")%>ct/images/buttons/button-small.png'); width:83px; height:26px; border:none; color:#FFFFFF; font-family: Tahoma,Verdana,Geneva,sans-serif; font-size: 10px; line-height: 1; font-weight:bold; }
.f1image-medium { background-image: url('<%=ics.GetProperty("ft.cgipath")%>ct/images/buttons/button-medium.png'); width:103px; height:26px; border:none; color:#FFFFFF; font-family: Tahoma,Verdana,Geneva,sans-serif; font-size: 10px; line-height: 1; font-weight:bold; }
</style>
<div dojoType="dijit.layout.BorderContainer_0" class="bordercontainer">

</cs:ftcs>
