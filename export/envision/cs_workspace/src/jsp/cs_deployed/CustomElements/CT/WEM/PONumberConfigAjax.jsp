<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIPONumberAjaxManager">

<table CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0" width="70%">
<tr HEIGHT="1"><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td>
<td>	
<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<col width="2%">
<col width="30%">
<col width="2%">
<col width="64%">
<col width="2%">
<tr><td colspan="2" class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif"
		style="text-align:right;">PO Number&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif"/>
	<td colspan="2" class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">
	Description</td></tr>
	<c:if test="${fn:length(poNumbers) eq 0}"><tr><td/><td colspan="4">No PO Number configured yet.</td></tr></c:if>
<c:forEach var="poNumber" items="${poNumbers}" varStatus="loop">
	<c:choose>
		<c:when test="${rowstyle eq 'tile-row-normal'}"><c:set var="rowstyle" value="tile-row-highlight"/></c:when>
		<c:otherwise><c:set var="rowstyle" value="tile-row-normal"/></c:otherwise>
	</c:choose>
	<form id="fm_ponumberedit_${loop.index}">
		<input type="hidden" name="pagename" value="CustomElements/CT/WEM/PONumberConfigAjax"/>
		<input type="hidden" name="original_ponumber" id="originalponame_${loop.index}" 
			value="<c:out value='${poNumber.name}' escapeXml='true'/>"/>
		<input type="hidden" name="original_podesc" id="originalpodesc_${loop.index}" 
			value="<c:out value='${poNumber.desc}' escapeXml='true'/>"/>
		<input type="hidden" name="site" value="${siteid}"/>
		<tr class="${rowstyle}" id="ponumberdisplay_${loop.index}"><td>&nbsp;&nbsp;</td>
			<td NOWRAP="NOWRAP" ALIGN="RIGHT">
			<a name="uneditableLink" style="display:none; visible:false;cursor:no-drop;">
				<c:out value='${poNumber.name}' escapeXml='true'/></a>
			<a name="editableLink" href='javascript:editPONumber("${loop.index}")'>
				<c:out value='${poNumber.name}' escapeXml='true'/>
			</a>&nbsp;:&nbsp;&nbsp;
			</td><td/>
			<td><c:out value="${poNumber.desc}" escapeXml="true"/>&nbsp;&nbsp;
				<c:if test="${poNumber.name eq savedPoNumber}">
					<SPAN name="message" class="regular_message" style="text-align:right">
						<c:out value='${savemessage}' escapeXml='true'/></SPAN></c:if>
			</td><td/>
		</tr>
		<tr class="${rowstyle}" id="ponumberedit_${loop.index}" style="display: none;"><td><BR/></td>
			<td NOWRAP="NOWRAP" ALIGN="left">
			<input type="text" name="value" id="PONumberName_${loop.index}"
				style="padding: 2px;" size="25" value='<c:out value="${poNumber.name}" escapeXml="true"/>'/>
				&nbsp;:&nbsp;&nbsp;
			</td><td/>
			<td NOWRAP="NOWRAP" ALIGN="LEFT" VALIGN="MIDDLE" class="small-text-inset">
				<input type="text" name="value" id="PONumberDesc_${loop.index}" 
					style="padding: 2px;" size="65" value="<c:out value='${poNumber.desc}' escapeXml='true'/>"/>
			</td>
			<td>&nbsp;&nbsp;</td></tr>
		<tr class="${rowstyle}" valign="middle" id="ponumberaction_${loop.index}" style="display: none;"><td></td>
			<td colspan="3" class="small-text-inset" style="padding-top:5px;padding-bottom:5px">
			<div><input type="button" name="action" value="Update" class="bt-image-small"
						onclick='updatePONumber("${loop.index}");'/> 
				<input type="button" name="action" value="Delete" class="bt-image-small"
						onclick='removePONumber("${loop.index}");'/> 
				<input type="button" name="Cancel" value="Cancel" class="bt-image-small" 
						onclick='hidePONumberEditor("${loop.index}");'/> 
			</div></td><td/></tr>
	</form>
</c:forEach>
<c:choose>
	<c:when test="${rowstyle eq 'tile-row-normal'}"><c:set var="rowstyle" value="tile-row-highlight"/></c:when>
	<c:otherwise><c:set var="rowstyle" value="tile-row-normal"/></c:otherwise>
</c:choose>
<tr class="${rowstyle}" id="ponumberedit_new" style="display: none;"><td>&nbsp;&nbsp;</td>
	<td NOWRAP="NOWRAP" ALIGN="RIGHT">
	<input type="text" name="value" id="PONumberName_new" style="padding: 2px;" size="25"/>
		&nbsp;:&nbsp;&nbsp;
	</td><td/><td class="small-text-inset">
		<input type="text" name="value" id="PONumberDesc_new" style="padding: 2px;" size="65"/>
	</td><td>&nbsp;&nbsp;</td></tr>
<tr class="${rowstyle}" valign="middle" id="ponumberaction_new" 
		style="display: none;padding-top:5px;padding-bottom:5px"><td><BR/></td>
	<td colspan="3" class="small-text-inset" style="padding-top:5px;padding-bottom:5px"><DIV>
		<input type="button" name="action" value="Save" class="bt-image-small"
				onclick='addPONumber();'/> 
		<input type="button" name="Cancel" value="Cancel" class="bt-image-small" 
				onclick='hidePONumberEditor("new");'/></DIV> 
		</td><td>&nbsp;&nbsp;</td></tr>
</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr HEIGHT="1"><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</table>
<SPAN name="message" class="regular_message" style="text-align:right">
	<c:out value='${deletemessage}' escapeXml='true'/></SPAN>
<input type="hidden" id="defaultChangeMessage" 
		value="<c:out value='${defaultchangemessage}' escapeXml='true'/>"/>
<input type="hidden" id="requireponumber" value="${requireponumber}"/>
<br/>
<input type="button" id="btAddNew" value="Add New PO Number" class="bt-image-long" onclick='editPONumber("new");'/> 

</gsf:root></cs:ftcs>
