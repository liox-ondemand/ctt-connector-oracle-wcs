<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%>



<cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIAdminAuthManager">

<html xmlns="http://www.w3.org/1999/xhtml">    
  <head>      
	<c:if test="${'true' eq retry}">  
		<render:getpageurl outstr="url" pagename="CustomElements/CT/WEM/Dashboard">
		</render:getpageurl> 
	    <meta http-equiv="refresh" content="3;URL='<ics:getvar name="url"/>'" />    
	</c:if>
<script language="javascript"><!--
	function showOrHide(elementId) {
		var element = document.getElementById(elementId);
		
		if (element) {
			if (element.style.display=="none") {
				element.style.visibility = 'visible';
				element.style.display = "block";
			} else {
				element.style.visibility = 'invisible';
				element.style.display = "none";
			}
		}
	}

	
	
--></script>


</head>

<body class="AdvForms">

<form><input type="hidden" id="hiddenValue"/ value="2"></form>

<ics:callelement element="CustomElements/CT/WEM/Header"/>

<c:if test="${'true' eq retry}">
Restarting ClayTablet connector, please wait...
</c:if>
<c:if test="${'true' ne retry}"> 
<render:getpageurl outstr="retryurl" pagename="CustomElements/CT/WEM/Dashboard">
		<render:argument name="retry" value="true"/>
		<render:argument name="app" value="WEM"/>
</render:getpageurl> 

<c:set var="dashboardversion" value="1.1.7"/>
<c:set var="binaryversion" value="<%=com.claytablet.wcs.system.Version.CURRENT.toString()%>"/>

<c:choose>
<c:when test="${'true' eq initError}">
<H3 style="color:red;margin:1em;"><p>The ClayTablet connector fails to start properly</p></H3>
<table CLASS="width-outer-90" border="0" cellpadding="0" cellspacing="0" width="800">
<tr><td>
Please check you setup procedure, fix the issue indicated in the <a href="javascript:showOrHide('InitErrorDetail');">error detail</a>, 
and <a href='<ics:getvar name="retryurl"/>'>restart the connector</a>; 
or <a href="http://www.clay-tablet.com/support" target="_blank">contact ClayTablet</a> for support.
</td></tr>
<tr><td>
<textarea rows="10" cols="120" id="InitErrorDetail" style="display:none">${initErrorDetail}</textarea>
</td></tr>
</table> 

 
</c:when>
<c:otherwise>
<c:if test="${dashboardversion ne binaryversion}">
<c:set var="authorized" value='false'/>
<H3 style="color:red;margin:1em;"><p>The ClayTablet connector does not appear to be installed or upgraded properly. 
Please double check your installation/upgrade procedure, or <a href="http://www.clay-tablet.com/support" target="_blank">contact ClayTablet</a> for support.</p> 
(Dashboard version: ${dashboardversion} vs. binary version: ${binaryversion})</H3>
</c:if>
</c:otherwise>
</c:choose>

<br/>

<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
<table CLASS="width-outer-90" border="0" cellpadding="0" cellspacing="0" width="800">
<c:if test="${'true' eq siteAuthorized}">
<tr><td valign="top">

<TABLE CLASS="width-outer-30" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Configuration</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

 <p>This component provides the main control center for deployment-specific configuration of the integration.</p>
 <p>
<c:if test="${'true' eq authorized}">
 	<a href="ContentServer?pagename=CustomElements/CT/WEM/CTConfigFront">System Configuration</a><br/>
    <a href="ContentServer?pagename=CustomElements/CT/WEM/WCSConfigFront">Site Configuration</a><br/>
    <a href="ContentServer?pagename=CustomElements/CT/WEM/ProvidersConfigFront">Translation Providers</a><br/>
</c:if>
    <a href="ContentServer?pagename=CustomElements/CT/WEM/PONumberConfigFront">PO Numbers Configuration</a><br/>
<c:if test="${'true' eq authorized}">
    <a href="ContentServer?pagename=CustomElements/CT/WEM/EncryptionConfig">Encryption Configuration</a><br/>
</c:if>
 </p>

</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE>
</td><td valign="top">

<TABLE CLASS="width-outer-30" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Management</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

          <p>These management tools allow you to view the details about all jobs, translation requests, and exceptions.</p>
          <p>
          <a href="ContentServer?pagename=CustomElements/CT/JobListFront&app=WEM">View All Jobs</a><br/>
          <a href="ContentServer?pagename=CustomElements/CT/QueueFront&app=WEM">View All Queues</a><br/>
 
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE>

</td>
<c:if test="${'true' eq authorized}">
<td valign="top">

<TABLE CLASS="width-outer-30" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Locale Tools</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

          <p>These locale tools allow users to enable globalization directly in WCS without having to go through multiple configuration screens.</p>
          <p>
          <a href="ContentServer?pagename=CustomElements/CT/WEM/EnableLocalesFront">Locales and Locale Mapping</a><br/>
          <a href="ContentServer?pagename=CustomElements/CT/WEM/AssignLocalesFront">Assign Locales to Assets</a>
          </p>
 
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE>
</td></tr>
</c:if>
</c:if>
<tr>
<c:if test="${'true' eq authorized}">

<td valign="top">
<TABLE CLASS="width-outer-30" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Utilities</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

          <p>These utilities allow you to manually upload translation files or poll CT platform for updates of outstanding translation jobs.</p>  

          <a href="ContentServer?pagename=CustomElements/CT/WEM/SubmitTranslatedFiles">Submit Translated Files</a><br/>
          <a href="ContentServer?pagename=CustomElements/CT/WEM/CheckMessages">Check Messages Now</a><br/>
          <a href="ContentServer?pagename=CustomElements/CT/WEM/ExportConfigurationFront">Export Configurations</a><br/>
          <a href="ContentServer?pagename=CustomElements/CT/WEM/ImportConfigurationFront">Import Configurations</a><br/>
<!--           
          <a href="ContentServer?pagename=CustomElements/CT/WEM/CreateSupportBundleFront">Create Support Bundle</a><br/>
 -->
           </p>
 
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE></td>
</c:if>

<td valign="top">
<TABLE CLASS="width-outer-30" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">About this Integration (Version ${binaryversion})</SPAN></TD></TR>
<c:if test="${'true' eq authorized}">
</c:if>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

          <p>This integration provides seamless user interface and back-end connectivity between WebCenter Sites and Clay Tablet for the purpose of enabling automated translation support in WCS via Clay Tablet's translation routing platform.</p>
          <p>
<c:if test="${'true' eq authorized or 'true' eq siteAuthorized or 'true' eq initError or dashboardversion ne binaryversion}">
          <a href="ct/doc/Clay Tablet Connector OWCS Install and Config.pdf" target="_blank">Installation and Configuration Guide</a><br/>
</c:if>          
          <a href="ct/doc/Clay Tablet Connector OWCS User Guide.pdf" target="_blank">User Guide</a></p>

 
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE>
</td><td valign="top">



<TABLE CLASS="width-outer-30" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">About Clay Tablet</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

          <p>Clay Tablet's connectivity platform and CMS connectors are the most widely used and proven solutions available. Used worldwide for over 5 years, the system is stable, proven, tested and endorsed by all leading CMS vendors, relied on by scores of enterprise clients for fast reliable movement of content for translation.</p>
          <p>For more information about Clay Tablet, <a href="http://www.clay-tablet.com" target="_blank">visit the Clay Tablet website</a>.</p>

 
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE>
</td></tr>
</table>
</c:if>	        
<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</body>
</html>
</gsf:root>	        
</cs:ftcs>
