<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>

<% if ("WEM".equals(ics.GetVar("app"))) { %>
	<c:set var="app" value="WEM"/>
	<c:set var="col_total" value="20"/>
	<c:set var="col_total_counts" value="14"/>
<% } else { %>
	<c:set var="col_total" value="18"/>
	<c:set var="col_total_counts" value="12"/>
<% } %>

	<gsf:root action="class:com.claytablet.wcs.ui.UIJobListAjax">
		<jsp:useBean id="now" class="java.util.Date"/>
		<div id="updated_jobids_section_${type}" style="visibility:hidden;display:none;">
			<select multiple name="${type}_jobids" id="${type}_jobids" width="15" size="10" style="display:none;">
			<c:forEach var="job" items="${jobs}">
				<option value="${job.id}">${job.status}</option>
			</c:forEach>
			</select>
		</div>	
		<SPAN id="h_job_status_summary_${type}" style="visibility:hidden;display:none">
			Total ${total_jobs} &nbsp;&nbsp;&nbsp;&nbsp;
			<c:forEach var="status" items="${job_statuses}">
				${status.displayName} ${job_statusesMap[status]}&nbsp;&nbsp;&nbsp;&nbsp; 
			</c:forEach></SPAN>
		<c:if test="${type eq 'inactive'}">
			<SPAN id="h_archived_job_summary" style="visibility:hidden;display:none">
				<c:if test="${archivedjobscount eq 0}">
					there is no archived job.
				</c:if>
				<c:if test="${archivedjobscount eq 1}">
					there is 1 archived job.
				</c:if>
				<c:if test="${archivedjobscount gt 1}">
					there are ${archivedjobscount} archived jobs.
				</c:if>
			</SPAN>
		</c:if>
		<c:choose><c:when test="${not empty archivedjobscount}">
			<input type="hidden" id="h_jobcount_archived" value="${archivedjobscount}"/>
		</c:when><c:otherwise>
			<input type="hidden" id="h_jobcount_${type}" value="${total_jobs}"/>
		</c:otherwise></c:choose>
		<input type="hidden" id="filtered_job_count_${type}" value="${page_totalrecords}"/>
		<SPAN id="h_job_list_summary_${type}" style="visibility:hidden;display:none">
		<c:choose><c:when test="${total_jobs eq 0}">
			there is no ${type} job.
		</c:when><c:otherwise>
			Showing	<c:if test="${fn:length(page_startrecords)>1}">${page_firstrec+1} to ${page_lastrec+1} of </c:if>${page_totalrecords}
			<c:choose> <c:when test="${page_totalrecords eq total_jobs}">
				 total record(s)
			</c:when><c:otherwise>
				filtered record(s)out of ${total_jobs} total  
		</c:otherwise></c:choose>
		<c:if test="${type ne 'active' and page_totalrecords>0}">, <SPAN id="${type}_select_total"></SPAN> selected</c:if>
		</c:otherwise></c:choose>
		</SPAN>
		<SPAN id="h_job_list_pages_${type}" style="visibility:hidden;display:none">
			<c:if test="${fn:length(page_startrecords)<=1 and page_totalrecords>10}">
			&nbsp;&nbsp;Pages:&nbsp;1&nbsp;
			</c:if>
			<c:if test="${fn:length(page_startrecords) > 1}">
			
				&nbsp;&nbsp;Pages:&nbsp;
				<c:forEach var="page_startids" items="${page_startrecords}" varStatus="status">
					<c:choose> <c:when test="${page_startids lt 0}" >
						<a href="#jobs" onClick="refreshJobList('${type}', ${page_startids * -1});">...</a>
					</c:when> <c:otherwise>
						<fmt:formatNumber value="${page_startids div pagesize + 1}" var="pagenum" pattern="0" />
						<c:if test="${page_firstrec ne page_startids}">
							<a href="#jobs" onClick="refreshJobList('${type}', ${page_startids});">${pagenum}</a></c:if>
						<c:if test="${page_firstrec eq page_startids}">${pagenum}</c:if>
					</c:otherwise></c:choose>
					<c:if test="${!status.last}"> | </c:if> 
				</c:forEach>
			</c:if>&nbsp;
			
			<c:if test="${fn:length(jobs) > 10}">
				(<select id="page_size_${type}" name="page_size" value="${pagesize}" onchange="refreshJobList('${type}', 0);">
					<c:forEach var="size" items="${page_size_options}">
						<option value="${size}" <c:if test="${size eq pagesize}"> selected</c:if>>${size}</option>
					</c:forEach>
				</select>&nbsp;per page)&nbsp;&nbsp;
			</c:if>
			<c:if test="${fn:length(translationRequests) <= 10}">
				<input type="hidden" name="page_size" id="page_size_${type}" value="${pagesize}"/>
			</c:if>
		</SPAN>
<c:if test="${page_totalrecords gt 0}">					
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" width="100%" style="margin: 1em;" >
<tr><td></td><td class="tile-dark"  HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
<tr><td class="tile-dark"  WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td>
<td>
<table class="width-inner-100 fixed_width_table" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<col width="2%">
<col width="1%">
<col width="15%">
<col width="1%">
<col width="20%">
<col width="1%">
<col width="10%">
<col width="1%">
<col width="9%">
<col width="1%">
<col width="7%">
<col width="1%">
<c:if test="${'WEM' eq app}">
<col width="7%">
<col width="1%">
</c:if>
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<tr><td colspan="${col_total}" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr>
<c:choose><c:when test="${type eq 'active'}">
	<td class="tile-a" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
</c:when><c:otherwise>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">
	&nbsp;<input type="checkbox" id="${type}_toggler" name="${type}_toggler" onClick='toggleCheckboxes("${type}");'/></DIV></td>
</c:otherwise></c:choose>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "name"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "name DESC")'>Name 
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "name ASC")'>Name 
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "name ASC")'>Name</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Description</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "status"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "status DESC")'>Status 
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "status ASC")'>Status 
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "status ASC")'>Status</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "SIZE(translationRequests)"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "SIZE(translationRequests) DESC")'># of Assets
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "SIZE(translationRequests) ASC")'># of Assets 
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "SIZE(translationRequests) DESC")'># of Assets</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "dueDate"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "dueDate DESC")'>Due Date 
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "dueDate ASC")'>Due Date
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "dueDate DESC")'>Due Date</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "provider"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "provider DESC")'>Provider
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "provider ASC")'>Provider
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "provider ASC")'>Provider</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<c:if test="${'WEM' eq app}">
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "createdUser"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "createdUser DESC")'>Created
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "createdUser ASC")'>Created
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "createdUser ASC")'>Created</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
</c:if>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "updatedDate"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "updatedDate DESC")'>Updated
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "updatedDate ASC")'>Updated
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "updatedDate DESC")'>Updated</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP">
			<c:choose>	<c:when test='${sort_by eq "submittedDate"}'>
				<c:choose> <c:when test="${sort_dir eq 'ASC'}">
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "submittedDate DESC")'>Submitted
						<img src="ct/images/upArrow.png" border="0"/></DIV>
				</c:when> <c:otherwise>
					<DIV class="sortable_table_title" onClick='sortPage("${type}", "submittedDate ASC")'>Submitted
						<img src="ct/images/downArrow.png" border="0"/></DIV>
				</c:otherwise> </c:choose>
			</c:when> <c:otherwise> 
				<DIV class="sortable_table_title" onClick='sortPage("${type}", "submittedDate DESC")'>Submitted</DIV>
			</c:otherwise>	</c:choose> </td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;</td>
</tr>
<tr><td colspan="${col_total}" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

<c:if test="${fn:length(jobs) gt 0 }">
<c:forEach var="job" items="${jobs}" begin="${page_firstrec}" end="${page_lastrec}">
	<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/JobFront">
		<render:argument name="jobid" value="${job.id}"/>
		<render:argument name="app" value="${app}"/>
		<c:if test="${not empty siteid}">
			<render:argument name="siteId" value="${siteid}"/>
		</c:if>
	</render:getpageurl> 
	<fmt:formatDate value="${job.dueDate}" var="dueDateFormatted" type="date" pattern="yyyy-MM-dd" />
	<fmt:formatDate value="${job.updatedDate}" var="updatedDateFormatted" type="date" pattern="yyyy-MM-dd" />
	<fmt:formatDate value="${job.updatedDate}" var="updatedDateFormatted2" type="date" pattern="yyyy-MM-dd hh:mm:ss" />
	<fmt:formatDate value="${job.submittedDate}" var="submittedDateFormatted" type="date" pattern="yyyy-MM-dd" />
	<fmt:formatDate value="${job.submittedDate}" var="submittedDateFormatted2" type="date" pattern="yyyy-MM-dd hh:mm:ss" />

<tr class="tile-row-normal" id="jobdetail_${job.id}">
<c:choose><c:when test="${type eq 'active'}">
	<td valign="middle" NOWRAP="NOWRAP"></td>
</c:when><c:otherwise>
	<td valign="middle" NOWRAP="NOWRAP"><DIV class="small-text-inset">
		&nbsp;<input type="checkbox" name="${type}_togglers" id="toggler_${job.id}" value="${job.id}"
			onClick="updateDropdownAndTotal('${type}', this.value, this.checked)"/><BR>
	</DIV></td>
</c:otherwise></c:choose>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title='<c:out value="${job.name}" escapeXml="true"/>'>
	<a href='<ics:getvar name="detailsurl"/>'><c:out value="${job.name}" escapeXml="true"/></a></td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title='<c:out value="${job.description}" escapeXml="true"/>'>
	<c:out value="${job.description}" escapeXml="true"/></td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${job.status.displayName}">
	<a href='javascript:toggleJobCounts("${job.id}")' <c:if test="${job.inError}">style="color:red"</c:if>>
		${job.status.displayName}<c:if test="${job.inError}"> (error)</c:if></a></td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">
	${job.requestCount}<c:if test="${job.requestErrorCount gt 0}">&nbsp;(<a href='javascript:toggleJobCounts("${job.id}")' style="color:red">${job.requestErrorCount}</a>)</c:if></td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">
	<c:choose>
		<c:when test="${job.dueDate.time < now.time}"><b><font color="red">${dueDateFormatted}</font></b></c:when>
		<c:otherwise>${dueDateFormatted}</c:otherwise>
	</c:choose>
</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" 
		title='<c:out value="${job.translationProvider.name}" escapeXml="true"/>'>${job.translationProvider.name}</td>
<c:if test="${'WEM' eq app}">
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${job.createdUser}">${job.createdUser}</td>
</c:if>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${updatedDateFormatted2}">${updatedDateFormatted}</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${submittedDateFormatted2}">${submittedDateFormatted}</td>
<td><BR></td>
</tr>
<tr class="tile-row-highlight" name="jobcounts_row" id="jobcounts_${job.id}" style="display:none"><td colspan="6"/>
<td colspan="${col_total_counts}"># of assets<c:if test="${job.requestErrorCount gt 0}">&nbsp;(<SPAN style="color:red">Error</SPAN>)</c:if>:&nbsp;&nbsp;
	<c:forEach var="status" items="${job.requestStatuses}">
		${status.displayName} ${job.requestCountsByStatusMap[status]}
		<c:if test="${job.requestErrorCountsByStatusMap[status] gt 0}">
			(<SPAN style="color:red">${job.requestErrorCountsByStatusMap[status]}</SPAN>)
		</c:if>&nbsp;&nbsp;&nbsp; 
	</c:forEach>
</td></tr>
</c:forEach>
</c:if>
</table>
</td><td class="tile-dark"  WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr><tr><td colspan="3" class="tile-dark"  HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td></td><td background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/shadow.gif"><IMG WIDTH="1" HEIGHT="5" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td>
</tr>
</table>
</c:if>
	</gsf:root>
</cs:ftcs>
