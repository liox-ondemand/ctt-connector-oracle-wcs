<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>	<gsf:root action="class:com.claytablet.wcs.ui.UIJobRequestDetailManager">

<c:set var="assetidparts" value="${fn:split(tr.nativeSourceId, ':')}" />
<%try { %>
<gsf:asset-load name="thisasset" c="${assetidparts[0]}" cid="${assetidparts[1]}" attributes="name"/>
<%} catch (Throwable t) { %>
<%}%>
<c:if test="${not empty tr.nativeTargetId}">
	<c:set var="targetassetidparts" value="${fn:split(tr.nativeTargetId, ':')}" />
	<%try { %>
		<gsf:asset-load name="targetasset" c="${targetassetidparts[0]}" cid="${targetassetidparts[1]}" attributes="name"/>
	<%} catch (Throwable t) { %>
	<%}%>
</c:if>

<table width="100%" style="table-layout:fixed;word-wrap:break-word"">
<thead><col width="20%"/><col width="2%"/><col width="78%"/></thead>
<tr><td colspan="3"><b>Translation request detail</b></td></tr>
<tr><td><b>Source Asset:</b></td><td/><td>
	<c:choose><c:when test="${not empty thisasset}">${thisasset.name}</c:when>
		<c:otherwise><SPAN style="color:red">Cannot load source asset</SPAN></c:otherwise>
	</c:choose> 
	(${assetidparts[0]} : ${assetidparts[1]})</td></tr>
<tr><td><b>Target Asset:</b></td><td/><td><c:choose><c:when test="${not empty tr.nativeTargetId}">
	<c:choose><c:when test="${not empty targetasset}">${targetasset.name}</c:when>
		<c:otherwise><SPAN style="color:red">Cannot load target asset</SPAN></c:otherwise></c:choose>
	(${targetassetidparts[0]} : ${targetassetidparts[1]})</c:when><c:otherwise> - </c:otherwise></c:choose></td></tr>
<tr><td><b>Source Locale:</b></td><td/><td>${tr.nativeSourceLanguage}</td></tr>
<tr><td><b>Target Locale:</b></td><td/><td>${tr.nativeTargetLanguage}</td></tr>
<tr><td><b>Status:</b></td><td/><td <c:if test="${tr.inError}">style="color:red"</c:if>>${tr.extStatus}</td></tr>
<c:if test="${tr.inError}"><tr><td valign="top"><b>Error Detail:</b></td><td/><td>${tr.error}</td></tr> </c:if>
<tr><td>&nbsp;</td></tr>
<tr><td colspan="3"><b>CT Support information</b></td></tr>
<tr><td><b>Job Id:</b></td><td/><td>${tr.translationJob.id}</td></tr>
<tr><td><b>Asset Id:</b></td><td/><td>${tr.translationAsset.id}</td></tr>
<tr><td><b>Asset Task Id:</b></td><td/><td>${tr.assetTaskId}</td></tr>
<tr><td><b>Job Provider Ref :</b></td><td/><td>${tr.translationAsset.providerJobReference}</td></tr>
<tr><td><b>Asset Provider Ref :</b></td><td/><td>${tr.translationAsset.providerAssetReference}</td></tr>
</tbody>
</table>
						
</gsf:root></cs:ftcs>
