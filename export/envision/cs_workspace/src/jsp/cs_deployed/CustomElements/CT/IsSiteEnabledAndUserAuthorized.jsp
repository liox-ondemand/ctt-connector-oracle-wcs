<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs><gsf:root action="class:com.claytablet.wcs.ui.UIAuthorizationManager"> 

<ics:setvar name="allowctintegration" value="false"/>
<ics:setvar name="sitetreeokfunctions" value=""/>
<ics:setvar name="contenttreeokfunctions" value=""/>

<c:if test="${isSiteEnabled && isUserAuthorized}">

	<ics:setvar name="allowctintegration" value="true"/>
	<ics:setvar name="sitetreeokfunctions" value=";TranslateFront;TranslateBranch"/>
	<ics:setvar name="contenttreeokfunctions" value=";TranslateFront;TranslateChildren"/>

</c:if>
</gsf:root>
</cs:ftcs>
