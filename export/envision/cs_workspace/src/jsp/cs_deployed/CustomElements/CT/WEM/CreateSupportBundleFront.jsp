<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UISupportBundleManager">

<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<ics:callelement element="CustomElements/CT/WEM/Header"/>

<script>
function validateName() {
	var name = document.getElementById("nameinput").value;
	
	if (name && name.trim()!='') {
		return true;
	} else if (confirm("Support bundle name cannot be empty. Use default name 'WCS_CT_SupportBundle'?")) {
		document.getElementById("nameinput").value = 'WCS_CT_SupportBundle';
		return true;
	} else {
		return false;
	}
}
</script>
<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Create Support Bundle
</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>
<p>Click the Submit button to create a bundle of logs and diagnostic files for sending to Clay Tablet Support team.</p>

<satellite:form>
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/CreateSupportBundlePost"/>
	<table>
	<tr><td>Name:&nbsp;&nbsp;</td><td>
		<input type="text" name="name" id="nameinput" size="65" value="WCS_CT_SupportBundle"/></td></tr>
	<tr><td>Note:&nbsp;&nbsp;</td><td>
		<input type="text" name="note" id="noteinput" size="65"/></td></tr>
	<tr><td colspan="2">
		<input class="bt-image-long" type="submit" name="submit" value="Create Support Bundle" 
				onclick="return validateName();"/></td></tr>
	</table>
</satellite:form>
</td></tr></TABLE>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</gsf:root>
</cs:ftcs>
