<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%>
<cs:ftcs>
	<gsf:root action="class:com.claytablet.wcs.ui.UIProviderMetadataManager">
		<input type="hidden" name="providertype" value="${providertype}"/>
		
		<c:if test="${newElementName ne ''}">
			<ics:callelement element="CustomElements/CT/${newElementName}">
				<ics:argument name="providerid" value="${providerid}"/>
				<ics:argument name="providermetadata" value="${providermetadata}"/>
				<ics:argument name="editable" value="${editable}"/>
			</ics:callelement>
		</c:if>
	</gsf:root>
</cs:ftcs>
