<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%>
<cs:ftcs>
	<gsf:root action="class:com.claytablet.wcs.ui.UIFreewayMetadataManager">
		Analysis code:&nbsp;&nbsp;
			<c:choose>
				<c:when test="${empty error or error eq ''}">
					 ${acsummary} 
				</c:when>
				<c:otherwise>
					<font color="red">${error}</font>
				</c:otherwise>
			</c:choose>
		<table>
			<c:choose>
				<c:when test="${editable eq 'true'}">
					<c:forEach var="ac" items="${AnalysisCodes}"> <tr>
						<td>${ac.name}<input type="hidden" name="analysiscode${ac.level}_name" value="${ac.name}"/></td>
						<td><select name="analysiscode${ac.level}_value" id="analysiscode${ac.level}_value" 
									style="width:100%;" onchange='
		var aclevel = "${ac.level}";
		var descs = document.getElementsByName("analysiscode" + aclevel + "_valuedesc");
		var selectedIndex = document.getElementById("analysiscode" + aclevel + "_value").selectedIndex;
		for (var i=0; i < descs.length; i++) {
			if (descs[i].id=="analysiscode" + aclevel + "_value" + selectedIndex + "_desc") {
				showInline(descs[i]);
			} else {
				hide(descs[i]);
			}
		}
									'>
								<option value=""></option>
								<c:forEach var="code" items="${ac.codeNames}" >
									<c:choose>
										<c:when test="${ac.value eq code}">
											<option value="${code}" selected>${code}</option>
										</c:when>
										<c:otherwise>
											<option value="${code}">${code}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select>
						</td><td>
							<c:if test="${ac.required}">*
								<input type="hidden" name="metadata_validator" id="analysiscode${ac.level}_validator"
									value='
										var value = document.getElementById("analysiscode${ac.level}_value").value;
										if (!value || 0 == value.length) {
											document.getElementById("analysiscode${ac.level}_value").focus();
											"Analysis code ${ac.name} is required.";
										} else {
											null;
										}
									'/>
							</c:if>
						&nbsp;&nbsp;</td><td>
							<c:set var="desc_set" value="false"/>
							<c:forEach var="code" items="${ac.codeNames}"  varStatus="code_index">
								<c:choose>
									<c:when test="${ac.value eq code}">
										<c:set var="desc_set" value="true"/>
										<span name="analysiscode${ac.level}_valuedesc" 
												id="analysiscode${ac.level}_value${code_index.index+1}_desc">
											<c:out value="${ac.codeDescriptions[code_index.index]}" escapeXml="true"/>
										</span>										
									</c:when>
									<c:otherwise>
										<span name="analysiscode${ac.level}_valuedesc" 
												id="analysiscode${ac.level}_value${code_index.index+1}_desc" style="display:none;">
											<c:out value="${ac.codeDescriptions[code_index.index]}" escapeXml="true"/>
										</span>										
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<span name="analysiscode${ac.level}_valuedesc" 
									id="analysiscode${ac.level}_value0_desc" 
									<c:if test="${desc_set eq 'true'}"> style="display:none;"</c:if> >
								<c:out value="${ac.description}" escapeXml="true"/>
							</span>										
						</td>
					</tr> </c:forEach>
				</c:when>
				<c:otherwise>
					<c:forEach var="nameval" items="${AnalysisCodes}"> 
						<c:if test="${nameval.name ne ''}"> <tr>
							<td>${nameval.name}:</td>
							<td>${nameval.value}</td>
							<td><c:if test="${not empty nameval.valueDescription}"> 
								&nbsp;(<c:out value="${nameval.valueDescription}" escapeXml="true"/>)
							</c:if>
							</td>
						</tr></c:if> 
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</table>
		
	</gsf:root>
</cs:ftcs>
