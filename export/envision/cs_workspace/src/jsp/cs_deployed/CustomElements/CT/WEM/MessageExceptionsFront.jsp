<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIMessageExceptionsManager">
<ics:callelement element="CustomElements/CT/WEM/Header"/>

<h3>Message Exceptions</h3>

<p>Below are the current message exceptions. Select "Retry" or "Clear" beside each one to retry or delete it.</p>

<satellite:form>
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/MessageExceptionsPost"/>
	<input type="text" name="formfield" id="formfield" value="enter sample text here"/>
	<input type="submit" name="submit" value="Submit"/>
</satellite:form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</gsf:root>
</cs:ftcs>
