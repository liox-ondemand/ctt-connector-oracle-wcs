<%@ page contentType="text/xml" %>
<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs><gsf:root action="class:com.claytablet.wcs.ui.UIProviderValidationPostManager">

<?xml version="1.0" encoding="UTF-8"?>

<result>
	<error>${error}</error>
	<message><c:out value='${message}' escapeXml="true"/></message>
</result>
</gsf:root></cs:ftcs>