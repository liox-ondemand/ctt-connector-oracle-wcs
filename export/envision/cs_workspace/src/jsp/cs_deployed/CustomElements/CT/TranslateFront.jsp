<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%><cs:ftcs>

<gsf:root action="class:com.claytablet.wcs.ui.UITranslationWizard"> 

<c:if test="${numberOfAssets > 0}"><ics:callelement element="CustomElements/CT/HeaderHtml"/></c:if>
<satellite:form>
<ics:callelement element="CustomElements/CT/HeaderTab"/>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script language="javascript">

function showDiv(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'block';
}

function hideDiv(divid) {
	document.getElementById(divid).style.visibility = 'hidden';
	document.getElementById(divid).style.display = 'none';
}

function getActiveView() {
	// Clear error message, if any
	var activeView, docId;
	SitesApp = parent.SitesApp;
	if (!parent.dojo || !SitesApp) alert("No dojo or sitesapp");;
	activeDoc = docId !== undefined ?
	SitesApp.getDocument(docId):
	SitesApp.getActiveDocument();
	activeView = activeDoc.get('activeView');
	return activeView;
}

<c:if test="${not empty message}">
	var activeView = getActiveView();
	activeView.message("${message}");
</c:if>
<c:if test="${not empty error}">
	var activeView = getActiveView();
	activeView.message("${error}");
</c:if>

function processForm() {
	var hasError = false;
	var msg = "";
	
	var boxes = document.getElementsByName("selectedlocales");
	hasError = true;
	msg = "You must specify at least one target locale(s). ";
	for (var i=0; i < boxes.length; i++) {
		if (boxes[i].checked == true) {
			hasError = false;
			msg = "";
			break;
		}
	}	

	if (hasError) {
		var locales = document.getElementsByName("hidden_selectedlocales");
		for (var i=0; i < locales.length; i++) {
			if (locales[i].checked == true) {
				hasError = false;
				msg = "";
				break;
			}
		}
	}
	if (hasError) {
		alert(msg);
		return false;
	} else {
		var dest = document.getElementById("jobid");
		if (dest.value=='newjob') {
			var name = '';
			
			while (name == '') {
				name = prompt("Please enter the name of the new job:", "");
				if (name==null) {
					return false;
				}
				name = name.trim();
			}
			document.getElementById("newjobname").value = name;
		}	
		return true;
	}
}

function checkAllLocales(check) {
	var locales = document.getElementsByName("selectedlocales");
	for (var i=0; i < locales.length; i++) {
		locales[i].checked = check;
	}
	if ('true' == document.getElementById("showHidden").value) {
		var locales = document.getElementsByName("hidden_selectedlocales");
		for (var i=0; i < locales.length; i++) {
			locales[i].checked = check;
		}
	}
}

function showOrHideLocales(show) {
	var showRows = document.getElementsByName(show? 'fold_row' : 'unfold_row');
	for (var i=0; i < showRows.length; i++) {
		showRows[i].style.visibility = 'visible';
		showRows[i].style.display='table-row';
	}	
	var hideRows = document.getElementsByName(show? 'unfold_row' : 'fold_row');
	for (var i=0; i < hideRows.length; i++) {
		hideRows[i].style.visibility = 'hidden';
		hideRows[i].style.display='none';
	}
	if (show) {
		document.getElementById("showHidden").value="true";
	} else {
		var locales = document.getElementsByName("hidden_selectedlocales");
		for (var i=0; i < locales.length; i++) {
			locales[i].checked = false;
		}
		document.getElementById("showHidden").value="false";
	}
}

</script>

<input type="hidden" name="pagename" value="CustomElements/CT/TranslatePost"/>
<input type="hidden" name="siteid" id="siteid" value="${pubid}"/>		
<input type="hidden" name="assetids" id="assetids" value="<%=ics.GetVar("assetids")%>"/>		
	
<table border="0" cellpadding="0" cellspacing="0" width="100%"> <!-- start 3 column table -->
	<tr>
		<td valign="top" width="80%">
	
	
	
<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important;">
<c:if test="${numberOfAssets == 1}"><TR><TD><SPAN CLASS="title-text">Translate ${currentAssetType}:</SPAN>&nbsp;<SPAN CLASS="title-value-text">${asset.name}</SPAN></TD></TR></c:if>
<c:if test="${numberOfAssets > 1}"><TR><TD><SPAN CLASS="title-text">Translate Multiple Assets of Type: ${currentAssetType}</SPAN></SPAN></TD></TR></c:if>
<tr><td><img height="5" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>
	<TABLE CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	
	<c:if test="${numberOfAssets == 1}">
	<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Name:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset">${asset.name}</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Description:</TD><TD></TD><TD class="form-inset">${asset.description}</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">ID:</TD><TD></TD><TD class="form-inset">${asset.id}</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	
	<c:choose> <c:when test='${not empty currentLocale}'>	
		<TR><TD CLASS="form-label-text">Locale:</TD><TD></TD><TD class="form-inset">${currentLocale}</TD></TR>
	</c:when> <c:otherwise>
		<TR><TD CLASS="form-label-text">Locale:</TD><TD></TD><TD class="form-inset" style="color: red;">The asset has no locale, cannot translate</TD></TR>
	</c:otherwise></c:choose>
	<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Existing Translations:</TD><TD></TD><TD class="form-inset"><c:if test='${empty translatedLocales}'>None</c:if><c:if test='${not empty translatedLocales}'>${translatedLocales}</c:if></TD></TR>
	</c:if>

	<c:if test="${numberOfAssets > 1}">
	<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<TR><TD CLASS="form-label-text">Number of Assets:</TD><TD><IMG HEIGHT="1" WIDTH="5" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></TD><TD class="form-inset">${numberOfAssets}</TD></TR>
	<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<c:choose> <c:when test="${currentLocale ne ''}">	
		<TR><TD CLASS="form-label-text">Source Locale:</TD><TD></TD><TD class="form-inset">${currentLocale}</TD></TR>
	</c:when> <c:otherwise>
		<TR><TD CLASS="form-label-text">Source Locale:</TD><TD></TD><TD class="form-inset" style="color: red;">One or more of the assets has no locale, cannot translate</TD></TR>
	</c:otherwise></c:choose>
	</c:if>

	<c:if test="${not empty currentLocale}">
		<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		<TR><TD CLASS="form-label-text">Select Target Workflow:</TD><TD></TD><TD class="form-inset">
			<select name="workflowprocid" id="workflowprocid">
				<option value="">Do Not Add to Workflow</option>
				<c:forEach var="wf" items="${workflow}">
	          		<option value="${wf.key}">${wf.value}</option>
	        	</c:forEach>
			</select></TD></TR>
		<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		<TR><TD CLASS="form-label-text">Add To Job/Queue:</TD><TD></TD><TD class="form-inset">
			<select name="jobid" id="jobid">
				<option value="queue">Add To Queue</option>
				<option disabled>------------------------------</option>
				<option value="newjob">Create New Job</option>
				<option disabled>------------------------------</option>
				<c:if test="${fn:length(jobs) eq 0}">
				<option disabled>-- No existing job --</option>
				</c:if>
				<c:forEach var="job" items="${jobs}">
					<c:if test="${job.status eq 'CREATED'}">
	          		<option value="${job.id}">${job.name}</option>
	          		</c:if>
	        	</c:forEach>
			</select>
			<input type="hidden" name="newjobname" id="newjobname"/>
			</TD></TR>
		<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		
		<TR><TD CLASS="form-label-text">Send with Dependencies: </TD><TD></TD><TD class="form-inset"><input type="checkbox" name="sendwithdependencies" id="sendwithdependencies" value="1"/><BR></TD></TR>
		<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		<TR><TD CLASS="form-label-text">&nbsp;</TD><TD></TD><TD class="form-inset"><input type="submit" name="submit" id="submit" value="Next" class="f1image-small" onClick="return processForm();"/><BR></TD></TR>
		<tr><td colspan="3"><img height="20" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
		</c:if>
	</TABLE>
</TD></TR>
</TABLE>

		</td>
		<td width="3%">&nbsp; &nbsp; &nbsp;</td>
		<td valign="top" width="20%">
		
<c:if test="${not empty currentLocale}">		
	
<TABLE CBORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important; margin-top:15px;">
<TR><TD><SPAN CLASS="title-text">Target Locales</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td>		
		
	<p>
	<table border="0" cellpadding="0" cellspacing="0">
   	<tr><td><input type="button" id="selectAllLocales" value="Select All" class="f1image-small" onClick="checkAllLocales(1);"/>
   		&nbsp;&nbsp;<input type="button" id="selectNoneLocales" value="Select None" class="f1image-small" onClick="checkAllLocales(0);"/>
   	</td></tr>
   	<tr><td><BR/></td></tr>
	<c:forEach var="kid" items="${eligibleLocales}" varStatus="status">
	<tr><td nowrap style="line-height:1;"><input type="checkbox" name="selectedlocales" id="selectedlocales_${kid}" value="${kid}" />${localeDetails[kid]} (${kid})</td></tr>
   	</c:forEach>
	<c:if test="${fn:length(hiddenLocales) gt 0}">
	   	<tr><td><BR/></td></tr>
		<tr name="fold_row" style="display:none;"><td colspan="11">&nbsp;<a href="javascript:showOrHideLocales(false)"
			><img id="hidden_locale_fold" src="js/fw/images/ui/ui/search/upArrow.png" style="cursor:pointer;" border="0"/>
					&nbsp;Hide ${fn:length(hiddenLocales)} Locale(s)</a>&nbsp;
			<input type="hidden" id="showHidden" name="showHidden" value="false"/>
		<tr name="unfold_row"><td colspan="11">&nbsp;<a href="javascript:showOrHideLocales(true)"
			><img id="hidden_locale_unfold" src="js/fw/images/ui/ui/search/downArrow.png" style="cursor:pointer;" border="0"/>
					&nbsp;Show ${fn:length(hiddenLocales)} Hidden Locale(s)</a>&nbsp;</td><tr>

		<c:forEach var="kid" items="${hiddenLocales}">
		<tr name="fold_row" style="display:none;">
      		<td nowrap style="line-height:1;"><input type="checkbox" name="hidden_selectedlocales" id="selectedlocales_${kid}" value="${kid}"/>${localeDetails[kid]} (${kid})</td>
       	</tr>
       	</c:forEach>
    </c:if>
   	</table>
	</p>	
		
		
</td></tr>
</TABLE>
</c:if>
		
		</td>
	</tr>
	</table>				
		
		
<ics:callelement element="CustomElements/CT/FooterTab"/>
</satellite:form>
<c:if test="${numberOfAssets > 0}"><ics:callelement element="CustomElements/CT/FooterHtml"/></c:if>

</gsf:root>
</cs:ftcs>