<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UICTConfigManager">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
<link href="${csroot}Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="${csroot}Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="${csroot}Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="${csroot}Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script> 

<script language="javascript">
function showRow(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'table-row';
}

function showTableCell(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'table-cell';
}

function hideDiv(divid) {
	hideElement(document.getElementById(divid));
}

function hideElement(element) {
	element.style.visibility = 'hidden';
	element.style.display = 'none';
}

function showLink(element) {
	element.style.visibility = 'visible';
	element.style.display = 'inline';
}

function editConfig(keyName, hidden) {
	if (hidden && !confirm("You are about to edit a hidden system configuration: '" + keyName + 
			"'. Change may cause the ClayTablet connector to malfunction. Are you sure you want to proceed?")) {
		return;
	}
	hideDiv("configdisplay_" + keyName);
	showRow("configedit_" + keyName);
	showRow("configdesc_" + keyName);
	document.getElementById("editor_shown").value='true';
	var editableLinks = document.getElementsByName("editableLink");
	for (i=0; i<editableLinks.length; i++) {
		hideElement(editableLinks[i]);
	}
	var uneditableLinks = document.getElementsByName("uneditableLink");
	for (i=0; i<uneditableLinks.length; i++) {
		showLink(uneditableLinks[i]);
	}
}

function hideConfigEditor(keyName) {
	hideDiv("configedit_" + keyName);
	hideDiv("configdesc_" + keyName);
	showRow("configdisplay_" + keyName);
	document.getElementById("value_" + keyName).value = document.getElementById("configvalue_" + keyName).value;
	document.getElementById("editor_shown").value='false';
	var uneditableLinks = document.getElementsByName("uneditableLink");
	for (i=0; i<uneditableLinks.length; i++) {
		hideElement(uneditableLinks[i]);
	}
	var editableLinks = document.getElementsByName("editableLink");
	for (i=0; i<editableLinks.length; i++) {
		showLink(editableLinks[i]);
	}
}

function showAlertMessage(updatedKey, message, alertMessage) {
	if (alertMessage!=null && alertMessage!='' && alertMessage!='null') {
		alert(alertMessage);
	}
	
	if (updatedKey!=null && updatedKey!='' && updatedKey!='null' &&
			message!=null && message!='' && message!='null') {
		document.getElementById("message_" + updatedKey).innerHTML = message;
		setTimeout(function() {
			document.getElementById("message_" + updatedKey).innerHTML='';
		}, 5000);
	}
}

function confirmRefresh() {
	if (confirm("You are about to refresh this page. Unsaved changes made on the page will be lost. Do you wish to proceed?")) {
		document.getElementById("refreshlink").click();
	}
}
</script>
</head>

<body>
<ics:callelement element="CustomElements/CT/WEM/Header"/>
<input type="hidden" id="editor_shown" value='false'/>
<input type="hidden" id="error_message" value='<%=ics.GetVar("errormessage")%>'/>
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/WEM/CTConfigFront">
	<render:argument name="showHidden" value='<%=ics.GetVar("showHidden")%>'/>
	<render:argument name="showProtected" value='<%=ics.GetVar("showProtected")%>'/>
</render:getpageurl>

<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">System Configuration</SPAN>&nbsp;&nbsp;
	<SPAN style="text-align:right;"><a id='refreshlink' href='<ics:getvar name="refreshurl"/>' style="display:none;"/>
		<a href="javascript:confirmRefresh()">
		<img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a>
	</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>
<table CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0" width="100%">
<tr HEIGHT="1"><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td>
<td>	
<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<col width="2%">
<col width="25%">
<col width="2%">
<col width="50%">
<col width="2%">
<col width="17%">
<col width="2%">
<c:forEach var="section" items="${sections}">
<tr><td colspan="7" class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">
	&nbsp;&nbsp;<c:out value="${section.name}" escapeXml="true"/>&nbsp;&nbsp;</td></tr>
<tr><td colspan="7" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<c:forEach var="entry" items="${section.entries}">
	<c:choose>
		<c:when test="${rowstyle eq 'tile-row-normal'}"><c:set var="rowstyle" value="tile-row-highlight"/></c:when>
		<c:otherwise><c:set var="rowstyle" value="tile-row-normal"/></c:otherwise>
	</c:choose>
	<satellite:form  id="fm_configedit_${entry.key.name}" method="POST">
		<input type="hidden" name="pagename" value="CustomElements/CT/WEM/CTConfigPost"/>
		<input type="hidden" name="keyname" value="${entry.key.name}"/>
		<input type="hidden" name="showHidden" value='<%=ics.GetVar("showHidden")%>'/>
		<input type="hidden" name="showProtected" value='<%=ics.GetVar("showProtected")%>'/>
		<input type="hidden" id="configvalue_${entry.key.name}" value="<c:out value='${entry.value}' escapeXml='true'/>"/>
		<tr class="${rowstyle}" id="configdisplay_${entry.key.name}"><td>&nbsp;&nbsp;</td>
			<td NOWRAP="NOWRAP" ALIGN="RIGHT">
			<DIV title='<c:out value="${entry.key.description} (${entry.key.name})" escapeXml="true"/>'>
			<a name="uneditableLink" style="display:none; visible:false;cursor:no-drop;">
				<c:out value="${entry.key.label}" escapeXml="true"/></a>
			<a name="editableLink" href='javascript:editConfig("${entry.key.name}", ${entry.key.hidden})'
					<c:if test="${entry.key.hidden}">style="color:red;"</c:if>>
				<c:out value="${entry.key.label}" escapeXml="true"/>
			</a>&nbsp;:&nbsp;&nbsp;</DIV>
			</td><td/>
			<td colspan="4">
				<c:choose><c:when test="${entry.key.encrypted}">**********</c:when>
						<c:otherwise><c:out value="${entry.value}" escapeXml="true"/></c:otherwise>
				</c:choose>
				&nbsp;&nbsp;
			<c:out value="${entry.key.unit}" escapeXml="true"/>&nbsp;&nbsp;&nbsp;&nbsp;
			
			<SPAN id="message_${entry.key.name}" class="regular_message">
			</SPAN></td>
		</tr>
		<tr class="${rowstyle}" id="configedit_${entry.key.name}" style="display: none;"><td><BR/></td>
			<td NOWRAP="NOWRAP" ALIGN="RIGHT">
			<DIV title='<c:out value="${entry.key.description} (${entry.key.name})" escapeXml="true"/>'>
			<c:out value="${entry.key.label}" escapeXml="true"/>&nbsp;:&nbsp;&nbsp;</DIV>
			</td><td/>
			<td NOWRAP="NOWRAP" ALIGN="LEFT" VALIGN="MIDDLE"><DIV class="small-text-inset">
				<c:choose>
					<c:when test="${entry.key.valueRestricted}">
						<select name="value" id="value_${entry.key.name}">
							<c:forEach var="valueoption" items="${entry.key.valueOptions}">
								<option value="${valueoption}"
									<c:if test="${valueoption eq entry.value}">selected</c:if>
								><c:out value="${valueoption}" escapeXml="true"/></option>
							</c:forEach>
						</select>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test='${entry.key.valueClass.simpleName eq "Integer"}'>
								<c:set var='size' value='15'/></c:when>
							<c:otherwise><c:set var='size' value='65'/></c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test='${entry.key.multiLine}'>
								<textarea name="value" id="value_${entry.key.name}" style="padding: 2px;" 
										cols="65" rows="5"><c:out value='${entry.value}' escapeXml='true'/></textarea>
							</c:when><c:otherwise><c:choose>
								<c:when test='${not entry.key.encrypted}'>
									<input type="text" name="value" id="value_${entry.key.name}" 
										style="padding: 2px;" size="${size}" value="<c:out value='${entry.value}' escapeXml='true'/>"/>
								</c:when><c:otherwise>
									<input type="password" name="value" id="value_${entry.key.name}" 
										style="padding: 2px;" size="${size}" value=""/>
								</c:otherwise></c:choose>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>&nbsp;&nbsp;<c:out value="${entry.key.unit}" escapeXml="true"/>
			</DIV></td><td/><td><DIV class="small-text-inset">
				<input type="submit" name="action" value="Update" class="bt-image-small"/> 
				<input type="button" name="Cancel" value="Cancel" class="bt-image-small" 
						onclick='hideConfigEditor("${entry.key.name}");'/> 
				<BR/></DIV></td><td/>
		</tr>
		<tr class="${rowstyle}" id="configdesc_${entry.key.name}" style="display: none;"><td colspan="3"><BR/></td>
		<td colspan="4"><c:out value="${entry.key.description} (${entry.key.name})" escapeXml="true"/></td></tr> 
	</satellite:form>
	</c:forEach>
</c:forEach>
</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr HEIGHT="1"><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr></table>
</td></tr>
<tr><td><satellite:form>
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/CTConfigPost"/>
	<input type="submit" name="action" value="Export" style="visibility: invisble; display: none;"/>
	<input type="submit" name="action" value="Import" style="visibility: invisble; display: none;"/>
</satellite:form></td></tr></TABLE>
<p/>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>

<script>
	window.onload = showAlertMessage('<%=ics.GetVar("updatedkey")%>', 
			'<%=ics.GetVar("message")%>', document.getElementById('error_message').value);
</script>

</body>
</html>
</gsf:root>
</cs:ftcs>
