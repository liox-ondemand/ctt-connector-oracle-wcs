<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ page import="java.util.*" 
%><%@ page import="java.text.*" 
%><%@ page import="java.io.*" 
%><%@ page import="com.claytablet.wcs.CTObjectFactory" 
%><%@ page import="com.claytablet.client.producer.ManagerFactorySession" 
%><%@ page import="org.apache.commons.lang.StringEscapeUtils" 
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><cs:ftcs><%
ManagerFactorySession mfSession = null;
ManagerFactorySession adminSession = null;

try {
	mfSession = new CTObjectFactory(ics).createManagerFactorySession(ics);
	adminSession = mfSession.sudoGlobalAdminSession();
	Date date = new Date(); 
	DateFormat format = new SimpleDateFormat("_yyyyMMdd-hhmmss");
	
	ics.StreamHeader("Content-Disposition", "attachment; filename=CT_OWCS_ConfigExport" +format.format(date)+ ".xml");
	String exportXml = adminSession.getConfigurationManager().exportConfiguration(false);
	ics.StreamText(exportXml);
} finally {
	if (adminSession!=null) adminSession.close();
	if (mfSession!=null) mfSession.close();
}
%></cs:ftcs>

