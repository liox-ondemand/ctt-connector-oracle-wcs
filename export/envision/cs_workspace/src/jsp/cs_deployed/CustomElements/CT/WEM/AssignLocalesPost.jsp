<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIAssignLocalesManager">
<ics:callelement element="CustomElements/CT/WEM/Header"/>

<h3>Assign Locales</h3>

<p>The following assets have been updated with Locale:${locale} for Site:${site}</p>

<table width="600" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<tr><td>
<TABLE width="100%" CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	<tr><td>ASSET ID</td>
		<td>ASSET TYPE</td>
		<td>NAME</td>
		<td>DESCRIPTION</td>
		<td>STATUS</td>
	</tr>
	<c:forEach var="asset" items="${assetlist}">  	
		<tr><td>${asset.id}</td>
			<td>${asset.type}</td>
			<td>${asset.name}</td>
			<td>${asset.description}</td>
			<td>updated</td>
		</tr>
	</c:forEach>
	
	</table>
	</td></tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</gsf:root>
</cs:ftcs>
