<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIQueueManager"> 
<ics:callelement element="CustomElements/CT/HeaderTab"/>

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
	
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script language="javascript"><!--
function inspectAsset(type, longid) {

	SitesApp.event(SitesApp.id('asset', type + ':' + longid), 'inspect'); // This works for .6.1
	<%--
	// SitesApp.event(fwutil.buildDocId(longid, type), 'inspect'); // This works for .8
	// Be sure to do ics.callement on OpenMarket/Xcelerate/UIFramework/LoadDojo at the top first
	--%>
}
-->
</script>

<script type="text/javascript">
	function getLocale(loc) {
		//convert from CS's locale notation format to the format Dojo NLS uses.
		//(e.g. en_US -> en-us, fr_FR -> fr-fr, etc.)
		return loc.toLowerCase().replace('_', '-');
	}
	var djConfig = {
		fw_csPath: '',
		pubName: 'FirstSiteII',
		parseOnLoad: true,
		locale: getLocale('en_US')
	};


	function toggleCheckboxes() {
		var hasChecked = false;
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].checked) {
				hasChecked = true;
				break;
			}
		}
		if (hasChecked) {
			uncheckAll();
		} else {
			checkAll();
		}
		document.getElementById("toggler").checked = false;
		var totalSelected = getNumberOfSelectedItems();	
		updateTotal(totalSelected);
	}

	function checkAll() {
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			boxes[i].checked = true;
			selectOrUnselectItem(boxes[i].value, boxes[i].checked);
		}
	}

	function uncheckAll() {
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			boxes[i].checked = false;
			selectOrUnselectItem(boxes[i].value, boxes[i].checked);
		}
	}

	function selectAndCheckAll() {
		selectAll();
		checkAll();
	}

	function unselectAndUncheckAll() {
		unselectAll();
		uncheckAll();
	}

	function selectAll() {
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			selectitems[i].selected = true;
		}
		updateTotal( selectitems.options.length );
	}

	function unselectAll() {
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			selectitems[i].selected = false;
		}
		updateTotal( "0" );
	}

	function updateDropdownAndTotal(thisboxvalue, ischecked) {
		// When the box is checked or unchecked, select or unselect the corresponding
		// item from the assetids drop-down menu.
		
		// Updated the drop-down menu
		selectOrUnselectItem(thisboxvalue, ischecked);
		
		var totalSelected = getNumberOfSelectedItems();
		
		updateTotal(totalSelected);
	}

	function getNumberOfSelectedItems() {
		var selectitems = document.getElementById("reqids");
		var total = 0;
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].selected == true) {
				total++;
			}
		}
		return total;
	}

	function updateTotal(total) {
		document.getElementById("select_total").innerHTML = total;
	}

	function selectOrUnselectItem(itemvalue, selectval) {
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].value == itemvalue) {
				selectitems[i].selected = selectval;
				break;
			}
		}
	}

	function checkOrUncheckBox(itemvalue, checkedval) {
		var boxes = document.getElementsByName("page_reqids");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].value == itemvalue) {
				boxes[i].checked = checkedval;
				break;
			}
		}	
	}

	function restoreCheckedState() {
		var boxList = document.getElementsByName("page_reqids");
		var boxes = [];
		
		for (var i=0; i<boxList.length; i++) {
			boxes.push(boxList[i]);
		}
		var selectitems = document.getElementById("reqids");
		for (var i=0; i < selectitems.options.length; i++) {
			if (selectitems[i].selected) {
				for (var j=0; j < boxes.length; j++) {
					if (selectitems[i].value == boxes[j].value) {
						boxes[j].checked = true;
						boxes.splice(j, 1);
						break;
					}		
				}
			}
		}
	}

	function getCheckedRadio( radioname ) {
		var checkedval = "";
		var radios = document.getElementsByName( radioname );
		for (var i=0; i<radios.length; i++) {
			if (radios[i].checked) {
				checkedval = radios[i].value;
			}
		}
		return checkedval;
	}

	function getCheckedBoxes( boxname ) {

		var boxes = document.getElementsByName( boxname );
		var checked = "";
		for (var i=0; i<boxes.length; i++) {
			if (boxes[i].checked) {
				checked += ", " + boxes[i].value;
			}
		}
	}

	function clearMessage() {
		activeView.clearMessage();
	    alert();
	}

	function sortPage(orderbyclause) {
		document.getElementById("orderbyclause").value = orderbyclause;
		refreshPage(0);
	}

	function setFirstRecord(firstrec) {
		refreshPage(firstrec);
	}

	function refreshPage(firstrec) {
		document.getElementById("page_firstrecord").value = firstrec;
		document.getElementById("modal_blocker").style.top = "60px";
		showBlock("modal_blocker");
		var progress = document.getElementById("requestsloadingprogress");
		showBlock("requestsloadingprogress");
		var orderbyclause = document.getElementById("orderbyclause").value;
		var pagesize_input = document.getElementById("page_size");
		var pagesize = document.getElementById("default_page_size").value;
		if (pagesize_input) {
			pagesize = pagesize_input.value;
		} 

		var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestsList" +
			"&where=" + encodeURIComponent(document.getElementById("whereclause").value) +
			"&firstrec=" + firstrec + "&pagesize=" + pagesize;  
		
		var siteSelect = document.getElementById("selectSite");
		if (siteSelect) {
			posturl += "&siteId=" + siteSelect.value;
		}
		if (document.getElementById("app")) {
			posturl+="&app=" + 	document.getElementById("app").value;
		}
		if (orderbyclause) {
			posturl+="&orderby=" + encodeURIComponent(orderbyclause);	
		}

 		$.ajax({url:posturl,
 			success:function(result){
				document.getElementById("modal_blocker").style.display = "none";
				document.getElementById("requestsloadingprogress").style.display = "none";
				
				if (document.getElementById("jobrequests")) {
 					document.getElementById("jobrequests").innerHTML=result;
				}

				showHideBasedOnRequestCount();
				if (document.getElementById("updated_reqid_section")) {
					document.getElementById("updated_reqid_section").innerHTML = '';
				}
				if (document.getElementById("h_request_list_summary")) {
					document.getElementById("request_list_summary").innerHTML =
						document.getElementById("h_request_list_summary").innerHTML;
				}
				if (document.getElementById("h_request_list_pages")) {
					document.getElementById("request_list_pages").innerHTML =
						document.getElementById("h_request_list_pages").innerHTML;
				}
				restoreCheckedState();
			},
			error: function(xhr, textStatus, errorThrown){
				document.getElementById("modal_blocker").style.display = "none";
				document.getElementById("requestsloadingprogress").style.display = "none";
				alert("Failed to update the requests list: " + errorThrown);
			}
 		});
	}

	function showHideBasedOnRequestCount() {
		if (document.getElementById("total_unfiltered_requests") &&
				document.getElementById("total_unfiltered_requests").value=='0') {
			var toHide = document.getElementsByName("has_translation_request");
			var toHideRow = document.getElementsByName("has_translation_request_tr");
			var toShow = document.getElementsByName("no_translation_request");
			for (var i=0; i<toHide.length; i++) {
				hide(toHide[i]);
			}
			for (var i=0; i<toHideRow.length; i++) {
				hide(toHideRow[i]);
			}
			for (var i=0; i<toShow.length; i++) {
				show(toShow[i], "inline");
			}
		} else {
			var toHide = document.getElementsByName("no_translation_request");
			var toShow = document.getElementsByName("has_translation_request");
			var rowsToShow = document.getElementsByName("has_translation_request_tr");
			
			for (var i=0; i<toHide.length; i++) {
				hide(toHide[i]);
			}
			for (var i=0; i<toShow.length; i++) {
				show(toShow[i], "inline");
			}
			for (var i=0; i<rowsToShow.length; i++) {
				show(rowsToShow[i], "table-row");
			}
		}
	}
	
	function RemoveSelectedRequests() {
		var totalSelected = getNumberOfSelectedItems();
		
		if (totalSelected<=0) {
			alert("Please select translation request(s) to remove");
		} else if (confirm("Are you sure you want to remove " + totalSelected + 
				" translation request from the job?")) {
			var selectedRequestIds = "";
			var selectitems = document.getElementById("reqids");
			var total = 0;
			for (var i=0; i < selectitems.options.length; i++) {
				if (selectitems[i].selected == true) {
					selectedRequestIds += selectitems[i].value + " ";
				}
			}
			updateRequestList("removeRequest", selectedRequestIds);			
		}
	}

	function updateRequestList(actionParams, selectedids) {
		var pagesize_input = document.getElementById("page_size");
		var pagesize = document.getElementById("default_page_size").value;
		if (pagesize_input) {
			pagesize = pagesize_input.value;
		}
		
		var orderbyclause = document.getElementById("orderbyclause").value;
		var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestsList";
		
		var params = {};
		params['where'] = document.getElementById("whereclause").value;
		params['firstrec'] = 0;
		params['pagesize'] = pagesize;
		params['_authkey_'] = document.getElementsByName('_authkey_')[0].value;
		
		document.getElementById("modal_blocker").style.top = "60px";
		showBlock("modal_blocker");
		var progress = document.getElementById("requestsloadingprogress");
		showBlock("requestsloadingprogress");
		var orderbyclause = document.getElementById("orderbyclause").value;

		if (document.getElementById("app")) {
			params['app'] = document.getElementById("app").value;
		}
		
		var siteSelect = document.getElementById("selectSite");
		if (siteSelect) {
			params['siteId'] = document.getElementById("siteid").value;
		}
		if (orderbyclause) {
			params['orderby'] = orderbyclause;
		}
		if (actionParams) {
			params['action'] = actionParams;
		}
		if (selectedids) {
			params['reqIds'] = selectedids;
		}
		
		$.ajax({url:posturl,
			method: 'POST',
			data: params,
 			success:function(result){
				document.getElementById("modal_blocker").style.display = "none";
				document.getElementById("requestsloadingprogress").style.display = "none";
				if (document.getElementById("jobrequests")) {
 					document.getElementById("jobrequests").innerHTML=result;
				}
				showHideBasedOnRequestCount(); 
				if (document.getElementById("updated_reqid_section")) {
					document.getElementById("reqids_section").innerHTML = 
						document.getElementById("updated_reqid_section").innerHTML;
					document.getElementById("updated_reqid_section").innerHTML = '';
				}
				if (document.getElementById("h_request_list_summary")) {
					document.getElementById("request_list_summary").innerHTML =
						document.getElementById("h_request_list_summary").innerHTML;
				}
				if (document.getElementById("h_request_list_pages")) {
					document.getElementById("request_list_pages").innerHTML =
						document.getElementById("h_request_list_pages").innerHTML;
				}
				var totalSelected = getNumberOfSelectedItems();	
				updateTotal(totalSelected);
			},
			error: function(xhr, textStatus, errorThrown){
				document.getElementById("modal_blocker").style.display = "none";
				document.getElementById("requestsloadingprogress").style.display = "none";
				alert("Failed to update the requests list: " + errorThrown);
			}
		});
	}
	function unselectItemsOnLoad() {

		var selectedassetids="${cs.assetids}";
		if (selectedassetids != "") {
			unselectAndUncheckAll();
			
			var saSelectedIds = selectedassetids.split(";");
			for (var i=0; i < saSelectedIds.length; i++) {
				selectOrUnselectItem( saSelectedIds[i], true );
				checkOrUncheckBox( saSelectedIds[i], true );
			}
			
			var totalSelected = getNumberOfSelectedItems();	
			updateTotal(totalSelected);
		}
		
	}
	
	function showFilter() {
		var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestFilter";
		
		var siteSelect = document.getElementById("selectSite");
		if (siteSelect) {
			posturl += "&siteId=" + siteSelect.value;
		}
		if (document.getElementById("app")) {
			posturl+="&app=" + 	document.getElementById("app").value;
		}
		
		$.ajax({url:posturl,
			success:function(result){
				if (document.getElementById("jobrequestfilters")) {
					document.getElementById("jobrequestfilters").innerHTML=result;
				}
				restoreCheckedState();
			},
			error: function(xhr, textStatus, errorThrown){
				alert("Failed to load the request filters: " + errorThrown);
			}
		});
	}

	function showRow(divid) {
		document.getElementById(divid).style.visibility = 'visible';
		document.getElementById(divid).style.display = 'table-row';
	}

	function showTableCell(divid) {
		document.getElementById(divid).style.visibility = 'visible';
		document.getElementById(divid).style.display = 'table-cell';
	}

	function hideDiv(divid) {
		hide(document.getElementById(divid));
	}

	function showElement(elementid) {
		showInline(document.getElementById(elementid));
	}
	
	function hide(element) {
		element.style.visibility = 'hidden';
		element.style.display = 'none';
	}

	function showInline(element) {
		element.style.visibility = 'visible';
		element.style.display = 'inline';
	}

	function show(element, display) {
		element.style.visibility = 'visible';
		element.style.display = display;
	}

	function showNewFilterCondition(fieldIndex) { 
		showRow('newcondition_field' + fieldIndex + '_tr');
		showElement('hide_newcondition_field' + fieldIndex);
		hideDiv('show_newcondition_field' + fieldIndex);
	}

	function hideNewFilterCondition(fieldIndex) { 
		hideDiv('newcondition_field' + fieldIndex + '_tr');
		showElement('show_newcondition_field' + fieldIndex);
		hideDiv('hide_newcondition_field' + fieldIndex);

		var boxes = document.getElementsByName("field" + fieldIndex + "_operand_checkbox");
		for (var i=0; i < boxes.length; i++) {
			boxes[i].checked = false;
		}

		var inputs = document.getElementsByName("field" + fieldIndex + "_operand");
		for (var i=0; i < inputs.length; i++) {
			inputs[i].value = '';
		}
	}
	
	function switchfilteroperator(fieldIndex) {
		var select = document.getElementById("field" + fieldIndex + "_operatorselector");
		
		if (select) {
			for (var i=0; i<select.options.length; i++) {
				if (!select.options[i].selected) {
					operand0Input = document.getElementById("field" + fieldIndex + '_operand0');
					valueFormat = document.getElementById("field" + fieldIndex + '_operator' + i + "_valueformat");
					if (operand0Input && valueFormat) {
						operand0Input.placeholder = valueFormat.value;
					}
				}
				var rows = document.getElementsByName("field" + fieldIndex + "_operator" + i + "_extraoperands");
				for (var j=0; j < rows.length; j++) {
					if (!select.options[i].selected) {
						hide(rows[j]);
					} else {
						rows[j].style.visibility = 'visible';
						rows[j].style.display = 'table-row';
					}
				}
			}
		}
	}
	
	function applyFilter(numNewFilterFields) {
		var curFilter = JSON.parse(document.getElementById("current_filter").value);
		var newFilter = getNewConditionJson(numNewFilterFields);
		
		if (!newFilter) {
			return false;
		}
		if (removeFilter(curFilter) || (newFilter && newFilter.fields.length>0)) {
			var posturl = "ContentServer?pagename=CustomElements/CT/JobRequestFilter" +
				"&newFilter=" + encodeURIComponent(JSON.stringify(newFilter)) + 
				"&currentFilter=" + encodeURIComponent(JSON.stringify(curFilter));  
			
			var siteSelect = document.getElementById("selectSite");
			if (siteSelect) {
				posturl += "&siteId=" + siteSelect.value;
			}
			if (document.getElementById("app")) {
				posturl+="&app=" + 	document.getElementById("app").value;
			}
			
			$.ajax({url:posturl,
				success:function(result){
					document.getElementById("jobrequestfilters").innerHTML=result;
					document.getElementById("whereclause").value = document.getElementById("filter_whereclause").value;
					updateRequestList(null, null);
				},
				error: function(xhr, textStatus, errorThrown){
					alert("Failed to load the request filters: " + errorThrown);
				}
			});
		}
	}
	
	function removeFilter(filter) {
		var removed = false;
		var removedFilters=[];
		var boxes = document.getElementsByName("removed_condition_checkbox");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].checked) {
				var idx = parseConditionIndexes(boxes[i].value);
				if (idx.length>2) {
					filter.fields[idx[0]].operators[idx[1]].operands[idx[2]] = "";
					removed = true;
				} else {
					filter.fields[idx[0]].operators[idx[1]].removed = "true";
					removed = true;
				}
			}
		}
		return removed;
	}
	
	function parseDate(val) {
	    var year;
	    var month;
	    var day;
		var dateParts = val.split("-");
		
	    if (dateParts.length == 3) {
		    year = dateParts[0];
		    month = dateParts[1];
		    day = dateParts[2];
	    } else {
	    	dateParts = val.split("/");
		    if (dateParts.length == 3) {
			    year = dateParts[2];
			    month = dateParts[0];
			    day = dateParts[1];
		    } else {
		    	return null;
		    }
	    }
	    
	    if (isNaN(day) || isNaN(month) || isNaN(year))
	        return null;

	    var result = new Date(year, (month - 1), day);
	    if (result == null)
	        return null;
	    if (result.getDate() != day)
	        return null;
	    if (result.getMonth() != (month - 1))
	        return null;
	    if (result.getFullYear() != year)
	        return null;

	    return result;
	}
	
	function validateValue(val, validator) {
		if (validator=='date') {
			return parseDate(val)!=null;	
		} else if (validator=='number') {
			return !isNaN(Number(val));
		} else {
			return true;
		}
	}
	
	function getNewConditionJson(numNewFilterFields) {
		var fields = [];
		var errorElement = null;
		var error = "";
		var hasError = false;
		
		for (var i=0; i<numNewFilterFields; i++) {
			var tr = document.getElementById("newcondition_field"+i + "_tr");
			if (tr && tr.style.display != 'none') {
				var operators = [];
				var operands = [];
				var op;
				var opselector = document.getElementById("field" + i +"_operatorselector");
				
				if (opselector) {
					op = opselector.selectedIndex;
				} else {
					op = 0;
				}

				var validator = document.getElementById("field" + i + "_operator"+ op + "_validator");
				var validatorMessage = document.getElementById("field" + i + "_operator"+ op + "_validatormsg");
				var j = 0;
				var hasEmptyValue = false;
				var hasValue = false;
				do {
					var operand0Input = document.getElementById("field" + i + "_operand0");
					if (j==0 && operand0Input) { 
						operands.push(operand0Input.value);
						if (!operand0Input.value || operand0Input.value.lenght==0) {
							hasEmptyValue = true;
							errorElement = operand0Input; 
						} else if (validator && !validateValue(operand0Input.value, validator.value)) {
							hasError = true;
							errorElement = operand0Input; 
							if (validatorMessage) {
								error = validatorMessage.value + operand0Input.value;
							} else {
								error = "'" + operand0Input.value + "' is not a valid value";
							}
							break;
						} else {
							hasValue = true;
						}
					} else {
						var operandInput = document.getElementById(
								"field" + i + "_operator" + op + "_operand" + j);
						if (operandInput) {
							operands.push(operandInput.value);
							if (!operandInput.value || operandInput.value.lenght==0) {
								hasEmptyValue = true;
								errorElement = operandInput; 
							} else if (validator && !validateValue(operandInput.value, validator.value)) {
								hasError = true;
								errorElement = operandInput; 
								if (validatorMessage) {
									error = validatorMessage.value + operandInput.value;
								} else {
									error = "'" + operandInput.value + "' is not a valid value";
								}
								break;
							} else {
								hasValue = true;
							}
						} else {
							break;
						}
					}	
					j++;
				} while (true);
				
				if (hasEmptyValue && hasValue) {
					error = "Condition entry cannot be empty";
					hasError = true;
				} 
				if (hasError) {
					break;					
				}
				var options = document.getElementsByName("field" + i + "_operand_checkbox");
				for (var k=0; k < options.length; k++) {
					if (options[k].checked) {
						operands.push(options[k].value);
						hasValue=true;
					}
				}
				
				if (hasValue && operands.length>0) {
					operators.push({ operator : op,	operands : operands});
					fields.push({ name : document.getElementById("field" + i + "_displayName").value,
						operators : operators});
				}
			}
		}
		if (hasError) {
			alert(error);
			if (errorElement) errorElement.focus();
			return null;
		} else {
			return { fields : fields };
		}
	}
			
	function cancelFilter(numNewFilterFields) {
		for (var i=0; i<numNewFilterFields; i++) {
			if (document.getElementById("hide_newcondition_field"+i)) {
				hideNewFilterCondition(i);
			}
		}
		var boxes = document.getElementsByName("removed_condition_checkbox");
		for (var i=0; i < boxes.length; i++) {
			if (boxes[i].checked) {
				var condition_index = parseConditionIndexes(boxes[i].value);
				var operandIndex = -1;
				if (condition_index.length>2) {
					operandIndex = condition_index[2];
				}
				restoreFilterCondition(condition_index[0], condition_index[1], operandIndex);
			}
		}
	}
	
	function parseConditionIndexes(val) {
		return val.split('_');
	}
	
	function clearFilter() {
		if (confirm("Are you sure you want to clear the current filter " + 
				"and display all translation requests in the translaion job?")) {
			showFilter();
			document.getElementById("whereclause").value = "";
			updateRequestList(null, null);
		}
	}
	
	function restoreFilterCondition(fieldIndex, singleConditionIndex, operandIndex) {
		if (operandIndex>=0) {
			showElement("remove_condition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			hideDiv("restore_condition_field"  + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			document.getElementById("current_filter_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).className="current_filter";
			document.getElementById("removedcondition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).checked=false;
		} else {
			showElement("remove_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			hideDiv("restore_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			document.getElementById("current_filter_field" + fieldIndex + "_sc" + singleConditionIndex).className="current_filter";
			document.getElementById("removedcondition_field" + fieldIndex +	"_sc" + singleConditionIndex).checked=false;
		}
	}

	function removeFilterCondition(fieldIndex, singleConditionIndex, operandIndex) {
		if (operandIndex>=0) {
			showElement("restore_condition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			hideDiv("remove_condition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex);
			document.getElementById("current_filter_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).className="removed_filter";
			document.getElementById("removedcondition_field" + fieldIndex +
					"_sc" + singleConditionIndex + "_op" + operandIndex).checked=true;
		} else {
			showElement("restore_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			hideDiv("remove_condition_field" + fieldIndex + "_sc" + singleConditionIndex);
			document.getElementById("current_filter_field" + fieldIndex + "_sc" + singleConditionIndex).className="removed_filter";
			document.getElementById("removedcondition_field" + fieldIndex +	"_sc" + singleConditionIndex).checked=true;
		}
	}
	
	$(function() {
	    
	    $( "#jobid" ).on( "change", function() {
	    	var thisval = document.getElementById("jobid").value;
	      	if (thisval == '') {
	      		document.getElementById("addToJob").disabled=true;
	      	} else {
	      		document.getElementById("addToJob").disabled=false;
	      	}
	    });
	        
	});  

	function getActiveView() {
		// Clear error message, if any
		var activeView, docId;
		SitesApp = parent.SitesApp;
		if (SitesApp) {
	//		if (!parent.dojo || !SitesApp) alert("No dojo or sitesapp");;
			activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
			activeView = activeDoc.get('activeView');
			return activeView;
		} else {
			return null;
		}
	}

	<c:if test="${not empty message}">
		var activeView = getActiveView();
		activeView.message("${message}");
	</c:if>
	<c:if test="${not empty error}">
		activeView.error("${error}");
	</c:if>

	function showBlock(elementid) {
		show(document.getElementById(elementid), 'block');
	}

	function showDiv(divid) {
		document.getElementById(divid).style.visibility = 'visible';
		document.getElementById(divid).style.display = 'block';
	}

	function show(element, display) {
		element.style.visibility = 'visible';
		element.style.display = display;
	}
	
	function hideDiv(divid) {
		document.getElementById(divid).style.visibility = 'hidden';
		document.getElementById(divid).style.display = 'none';
	}

	function toggleDiv(vendorsig, divname) {
		var divobj = document.getElementById(divname);
		if (vendorsig.lastIndexOf("LIONBRIDGE_FREEWAY:", 0) === 0) {
			showDiv(divname);
		} else {
			hideDiv(divname);
		}
	}

	function changeSite() {
		document.getElementById("pagename").value = "CustomElements/CT/QueueFront";
		var form = document.getElementById("AppForm");
		form.submit();
	}

	function init(startingid) {
		setFirstRecord(startingid);
		showFilter();
	}

	function addSelectedToJob() {
		var totalSelected = getNumberOfSelectedItems();
		
		if (totalSelected<=0) {
			alert("Please select translation request(s) to add to job");
			return false;
		}		
		dest = document.getElementById("jobid").value; 
		if (!dest || dest=='') {
			alert("Please select translation job to add to");
			return false;
		} else {
			if (dest=='newjob') {
				var name = '';
				
				while (name == '') {
					name = prompt("Please enter the name of the new job:", "");
					if (name==null) {
						return false;
					}
					name = name.trim();
				}
				document.getElementById("newjobname").value = name;
			}	
			return true;
		}
	}
</script>

	<script type="text/javascript" src="wemresources/js/WemContext.js"></script>
	<script type="text/javascript">
		//initialize wemcontext before loading dojo layers (see AppBar FIXME)
		//Unfortunately it needs to be AFTER dojo because WemContext relies on xhrGet!
		WemContext.initialize('','http://localhost:8080/cas/logout');
		var wemcontext = WemContext.getInstance();
		// If a new browser window is opened, as a pop up or new tab or new window, and if the Wem top bar is not there.
		// Then global sls js object would not avilable. In that scenario we load the object in two ways based on how the new window opened.
		// If as a pop-up
		if(window.opener) window.fw_sls_obj = window.opener.top.fw_sls_obj;
		// If as a browser tab or new brower window		
		if(!window.opener && window.top === window.self) document.write('<script type="text/javascript" src="ContentServer?user_locale=en-us&pagename=fatwire%2Fui%2Futil%2FGetSLSObj"><' + '/script>');
	</script>




<script type="text/javascript" src='js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: '0'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='wemresources/js/dojopatches.js'></script>

<link href="Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>
</head>

<body>
<satellite:form name="AppForm" id="AppForm" method="POST">

<% if ("WEM".equals(ics.GetVar("app"))) { %>
	<c:set var="app" value="WEM"/>
	<input type="hidden" name="app" id="app" value="WEM"/>
<% } %>
	
	<input type="hidden" name="docId" value="0"/>
	<input type="hidden" name="removetrq" value="false"/>
	<input type="hidden" name="pagename" id="pagename" value="CustomElements/CT/QueuePost"/>
	<input type="hidden" name="page_firstrecord" id="page_firstrecord" value="0"/>
	<input type="hidden" name="orderbyclause" id="orderbyclause" value="nativeSourceId ASC"/>
	<input type="hidden" name="whereclause" id="whereclause" value=""/>
	<input type="hidden" name="default_page_size" id="default_page_size" value="${pagesize}"/>
	
	<p id="reqids_section" style="visibility:hidden;display:none;">
	<select multiple name="reqids" id="reqids" width="15" size="10">
	<c:forEach var="req" items="${translationRequests}">
		<option value="${req.id}">${req.id}</option>
	</c:forEach>
	</select>
	</p>

<c:choose><c:when test="${'WEM' eq app}">
	<render:getpageurl outstr="listurl" pagename="CustomElements/CT/QueueFront">
		<render:argument name="app" value="WEM"/>
		<render:argument name="selectSite" value="${cursiteid}"/>
	</render:getpageurl> 
</c:when><c:otherwise>
	<render:getpageurl outstr="listurl" pagename="CustomElements/CT/QueueFront"/>
</c:otherwise></c:choose>

<div id="requestsloadingprogress" class="popup_panel_center">
	<img src="js/fw/images/ui/wem/loading.gif" style="h-align:center;"></img>
	<p>Loading translation requests in queue ...</p></div>
<div class="blocking_overlay" id="modal_blocker"></div>
<TABLE CLASS="width-outer-90" style="width:95%" "BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text" style="width:50%">Translation Queue</SPAN>&nbsp;&nbsp;<SPAN style="text-align:right;">
	<a href='<ics:getvar name="listurl"/>'><img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a></SPAN></TD>
	<c:if test="${app eq 'WEM'}">
		<TD align="right" style="width:50%">Site:&nbsp;&nbsp;<select id="selectSite" name="selectSite" onchange="changeSite()">
			<c:forEach var="siteinfo" items="${sites}">
				<option name="${siteinfo.name}" value="${siteinfo.id}"
					<c:if test="${siteinfo.id eq cursiteid}"> selected </c:if> 
				> ${siteinfo.name}&nbsp;&nbsp;&nbsp;&nbsp;</option>
			</c:forEach>
		</select></TD>
	</c:if>
</TR>
	
<tr><td colspan="2"><img height="5" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td colspan="2" class="light-line-color"><img height="2" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td colspan="2"><img height="10" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD colspan="2">
	<TABLE CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	<TR><TD style="text-align:left;"><SPAN class="form-label-text" style="padding:0">Queued Requests:</SPAN>

<SPAN class="form-inset" name="no_translation_request" <c:if test="${not empty translationRequests}">style="display:none"</c:if>>None</SPAN>
<SPAN class="form-inset" name="has_translation_request" <c:if test="${empty translationRequests}">style="display:none"</c:if>>
	<SPAN id="request_list_summary"></SPAN><SPAN id="select_total">0</SPAN> selected.</SPAN>
</td>
<td>&nbsp;&nbsp;</td><td >
	<input type="hidden" id="filtersjson"/>  
</td>
</tr>
<tr name="has_translation_request_tr" <c:if test="${empty translationRequests}">style="display:none"</c:if>> <td>
	<table style="width: 100%"><tr><td>
	<SPAN id="request_list_pages"></SPAN></td><td align="right">				
		<input type="button" name="selectall" id="selectall" value="Select All" onClick="selectAndCheckAll();" class="f1image-medium"/>
		<input type="button" name="unselectall" id="unselectall" value="Unselect All" onClick="unselectAndUncheckAll();" class="f1image-medium"/>
		<input type="button" name="removerequests" id="removerequests" value="Delete Selected" onClick="RemoveSelectedRequests();" class="f1image-medium"/>
	</table>
</td><td>&nbsp;&nbsp;</td><td class="filter-sidebar" align="right"> 
	<input type="submit" name="addToJob" id="addToJob" value="Add to Job" class="bt-image-small" 
			onclick="return addSelectedToJob();" disabled/>
	<select name="jobid" id="jobid" style="max-width:55%;">
         		<option value=""></option>
		<option value="newjob">Create New Job</option>
		<option disabled>---------------------</option>
		<c:if test="${fn:length(jobs) eq 0}">
			<option disabled>-- No existing job --</option>
		</c:if>
		<c:forEach var="job" items="${jobs}">
			<c:if test="${job.status eq 'CREATED'}">
	    		<option value="${job.id}">${job.name}</option>
	    	</c:if>
       	</c:forEach>
	</select>
	<input type="hidden" name="newjobname" id="newjobname">
</tr>
<tr name="has_translation_request_tr" <c:if test="${empty translationRequests}">style="display:none"</c:if>>
<td id="jobrequests" valign="top" style="width:100%;"><a name="requests"/></td>
<td>&nbsp;&nbsp;</td><td class="filter-sidebar" valign="top"> 
<DIV id="filter-div" CLASS="title-text" align="right">Filter</DIV><hr style="font-size: 0px;"/>
<SPAN id="jobrequestfilters" valign="top" align="right"></SPAN>
</div></td></tr>
</table>
</td></tr>
		
	</TD></TR>
	</TABLE>
</TD></TR>
</TABLE>
</satellite:form>

<ics:callelement element="CustomElements/CT/FooterTab"/>

	<script>
		window.onload = init(document.getElementById("page_firstrecord").value);
	</script>
	
</body>
</html>
</gsf:root>
</cs:ftcs>
