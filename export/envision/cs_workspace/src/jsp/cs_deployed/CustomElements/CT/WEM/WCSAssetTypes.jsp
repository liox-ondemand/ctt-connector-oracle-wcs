<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%>
<cs:ftcs><gsf:root action="class:com.claytablet.wcs.ui.UIWCSAssetTypesManager"><td> 
	<input type="hidden" id="translateall" value="${translateall}"/>
	<select id="selectAssetType" size="20" style="height: 100%; width: 100%;" onchange="changeAssetType()">
		<c:if test="${fn:length(assettypes) eq 0 and fn:length(assettypes_without_attr) eq 0}">
			<option disabled>------- No asset types -------</option></c:if>
		<c:forEach var="assettype" items="${assettypes}">
			<option name="${assettype.name}" value="${assettype.name}" 
					<c:if test="${assettype.translateAll}">style="color:green;"</c:if>
					<c:if test="${not empty selectedtype and selectedtype.name eq assettype.name}">selected</c:if>>
				<c:set var="selectcount" value="${fn:length(assettype.selectedAttributes)}"/>
				<c:choose> <c:when test="${not translateall}"> <c:choose>
					<c:when test="${selectcount gt 0}">
						<b>${assettype.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(${selectcount}/${fn:length(assettype.translatableAttributes)} selected)</b> 
					</c:when><c:otherwise>${assettype.name}</c:otherwise></c:choose>
				</c:when><c:otherwise><c:choose>
					<c:when test="${assettype.translateAll}">${assettype.name}&nbsp;*</c:when><c:otherwise>
						<b>${assettype.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(${selectcount}/${fn:length(assettype.translatableAttributes)} selected)</b>
					</c:otherwise></c:choose>
				</c:otherwise></c:choose>					
			</option>
		</c:forEach>
		<c:if test="${fn:length(assettypes_without_attr) gt 0}">
			<option disabled>------- Not translatable -------</option>
			<c:forEach var="assettype" items="${assettypes_without_attr}">
				<option disabled>${assettype.name}</option>
			</c:forEach>
		</c:if>
	</select></td><td>&nbsp;&nbsp;</td><td>
	<select id="selectAssetDefinition" size="20" style="height: 100%; width: 100%;" onchange="changeDefinition()">
		<c:if test="${empty selectedtype}">	<option disabled>----- Select an asset type first -----</option></c:if>
		<option value="Any" <c:if test='${selectedDefinition eq "Any"}'> selected</c:if> >Any / All Definitions</option>
		<c:forEach var="subTypeDefinition" items="${subTypeNames}">
			<option name="${subTypeDefinition}" value="${subTypeDefinition}" 
				<c:if test="${not empty selectedDefinition and subTypeDefinition eq selectedDefinition }">selected</c:if>
				>${subTypeDefinition}				
			</option>
		</c:forEach>
	</select>
	</td><td>&nbsp;&nbsp;</td><td>
	<select id="selectAvailableAttributes" size="20" multiple style="height: 100%; width: 100%;">
		<c:if test="${empty selectedtype}">	<option disabled>----- Select an asset type first -----</option></c:if>
		<c:if test="${not empty selectedtype and fn:length(selectedtype.unselectedAttributes) eq 0}">
			<option disabled>----- No attribute -----</option> </c:if>
		<c:forEach var="attr" items="${selectedtype.unselectedAttributes}">
			<option name="${attr}" value="${attr}">${attr}</option>
		</c:forEach>
	</select></td><td>&nbsp;&nbsp;</td><td valign="middle" align="middle">
	<input type="button" class="bt-image-long" value=">> Add >>" onclick="addTranslatedAttrs()"
		<c:if test="${selectedtype.translateAll}">disabled</c:if> />
	</p></p>
	<input type="button" class="bt-image-long" value="&nbsp;<< Remove <<&nbsp;" onclick="removeTranslatedAttrs()"/>
	<c:if test="${translateall and not empty selectedtype}">
	</p></p>
	<input type="checkbox" id="cbTypeDefault" style="align:middle" onchange="selectTypeDefault(${not selectedtype.translateAll})"
		<c:if test="${selectedtype.translateAll}">checked</c:if> >&nbsp;Use Default*
	</c:if>
	</td><td>&nbsp;&nbsp;</td><td>
	<select id="selectTranslatableAttributes" size="20" multiple style="height: 100%; width: 100%;">
		<c:if test="${empty selectedtype}">	<option disabled>----- Select an asset type first -----</option></c:if>
		<c:if test="${not empty selectedtype and fn:length(selectedtype.selectedAttributes) eq 0}">
			<option disabled>--------- No attribute ---------</option> </c:if>
		<c:forEach var="attr" items="${selectedtype.selectedAttributes}">
			<option name="${attr}" value="${attr}">${attr}</option>
		</c:forEach>
	</select>
</td>
 	</gsf:root>
</cs:ftcs>
