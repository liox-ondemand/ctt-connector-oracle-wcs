<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="assetset" uri="futuretense_cs/assetset.tld"
%><%@ taglib prefix="commercecontext" uri="futuretense_cs/commercecontext.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="listobject" uri="futuretense_cs/listobject.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="searchstate" uri="futuretense_cs/searchstate.tld"
%><%@ taglib prefix="siteplan" uri="futuretense_cs/siteplan.tld"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors"
%><cs:ftcs><%--

INPUT

OUTPUT

--%>

		
	</form>
	<script>
		var num = document.AppForm.length, i;
		document.AppForm.encoding="application/x-www-form-urlencoded";
		for (i=0; i < num; i++) {
			if (document.AppForm.elements[i].type=="file") document.AppForm.encoding="multipart/form-data";
		}
		(function() {
			var _alert = window.alert;
			window.alert = function() {
				var out = SitesApp.getActiveView() || SitesApp;
				out.clearFeedback();
				out.clearMessage();
				out.message(arguments[0], arguments[1] || "error");
				//fw/ui/controller/AdvancedController subscribes to this event
				parent.dojo.publish('/fw/ui/form/alert',[]);
				return true;
			};
		})();
	</script>
	
</body>
</html>
</cs:ftcs>
