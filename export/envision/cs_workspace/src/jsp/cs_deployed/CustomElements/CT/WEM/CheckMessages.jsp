<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UICTMessageChecker">
<jsp:useBean id="date" class="java.util.Date" />
<ics:callelement element="CustomElements/CT/WEM/Header"/>
<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>

<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Checked Messages</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

<p>The Clay Tablet platform was checked successfully at <fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss" />. 
<c:if test="${not empty result}">
<ul>
<li>Messages processed successfully: ${result.processedMessagesCount}</li>
<li>Messages processed with error: ${result.errorMessagesCount}</li>
<li>Translation files processed successfully: ${result.processedFilesCount}</li>
<li>Translation files processed with error: ${result.errorFilesCount}</li>
<li>Translation jobs updated: ${result.updatedJobsCount}</li>
<li>Translation requests updated: ${result.updatedTranslationRequestsCount}</li>
<li>Target asset updated: ${result.updatedTargetItemsCount}</li>
<li>Target asset attributes updated: ${result.updatedFieldsCount}</li>
</ul>
</c:if>
<p>View the sites.log file for more detail of the processing results.</p>

<p><a href="ContentServer?pagename=CustomElements/CT/WEM/CheckMessages">Check again</a></p>
 
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
</TABLE>




<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</gsf:root>
</cs:ftcs>
