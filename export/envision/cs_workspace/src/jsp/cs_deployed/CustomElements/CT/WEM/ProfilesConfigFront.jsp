<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIProfilesConfigManager">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">


<script language="javascript">
function alertAction() {
	return confirm(document.getElementById("alertmsg").value);
}
</script>
</head>

<body>
<ics:callelement element="CustomElements/CT/WEM/Header"/>
<h3>Team Profiles Configuration</h3>
<form onSubmit="return alertAction()">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ProfilesConfigPost"/>
<c:choose>
	<c:when test="${teamenabled eq 'true'}">
		<input type="hidden" id="alertmsg" value="Do you wish to disable team management?"/>
		Team management is currently enabled.&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="action" value="Disable"/>
	</c:when>
	<c:otherwise>
		<input type="hidden" id="alertmsg" value="Do you wish to enable team management?"/>
		Team management is currently disabled.&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="action" value="Enable"/>
	</c:otherwise>
</c:choose>

</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</body>
</html>
</gsf:root>
</cs:ftcs>
