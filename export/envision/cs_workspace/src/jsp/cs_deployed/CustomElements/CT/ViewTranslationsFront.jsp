<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIViewTranslationsManager">

<head>
	<META http-equiv="Pragma" content="No-cache">
	
	<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
	
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<script type="text/javascript" src='js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: '0'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='wemresources/js/dojopatches.js'></script>

<script language="javascript"><!--
function inspectAsset(type, longid) {
	SitesApp.event(SitesApp.id('asset', type + ':' + longid), 'inspect'); // This works for .6.1
	<%--
	// SitesApp.event(fwutil.buildDocId(longid, type), 'inspect'); // This works for .8
	// Be sure to do ics.callement on OpenMarket/Xcelerate/UIFramework/LoadDojo at the top first
	--%>
}
-->

</script>

<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>
</head>

<body>		
<ics:callelement element="CustomElements/CT/HeaderHtml"/>
<ics:callelement element="CustomElements/CT/HeaderTab"/>

<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><TABLE><TR><TD width="100%">
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/ViewTranslationsFront" cid="${cid}" c="${c}"/>
<SPAN CLASS="title-text">Translation for:</SPAN>&nbsp;<SPAN CLASS="title-value-text">${asset.name}</SPAN> 
&nbsp;&nbsp;<a href='<ics:getvar name="refreshurl"/>'><img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a>
</TD><TD align="right"><a href="javascript:inspectAsset('${c}','${cid}');">Back</a>
</TR></TABLE></TD></TR>
<tr><td><img height="5" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>


<c:if test="${not empty translatedassets}">
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" width="100%">
<tr><td></td><td class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
<tr><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td>
<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<tr><td colspan="9" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
				<tr>
					<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">Type</td>  
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">Id</td>
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">Locale</td>
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">Asset Name</td>
          			<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          		</tr>
 <c:forEach var="asset" items="${translatedassets}">
 				<render:getpageurl outstr="translatedAssetURL" pagename="CustomElements/CT/TranslationStatusFront"  cid="${asset.id}"  c="${asset.type}"/>
          		<tr>
          			<td class="tile-b">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td>${asset.type}</td>  
          			<td class="tile-b">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td>${asset.id}</td>
          			<td class="tile-b">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td>${asset.locale}</td>
          			<td class="tile-b">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          			<td><a href="javascript:inspectAsset('${asset.type}', '${asset.id}');">${asset.name}</a></td>
          			<td class="tile-b">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          		</tr>
 </c:forEach>

</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr><tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td></td><td background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/shadow.gif"><IMG WIDTH="1" HEIGHT="5" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td>
</tr>
</table>
</c:if>

<c:if test="${empty translatedassets}">
<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/TranslateFront">
		<render:argument name="c" value="${c}"/>
		<render:argument name="cid" value="${cid}"/>
</render:getpageurl> 

This asset currently has no translations. <a href="${cs.detailsurl}">Translate this asset now</a>
</c:if>


</td></tr>
</table>

<ics:callelement element="CustomElements/CT/FooterTab"/>
</body>

</gsf:root>
</cs:ftcs>