<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIProvidersConfigManager">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">

<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script> 

<script language="javascript">
function showRow(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'table-row';
}

function showTableCell(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'table-cell';
}

function hideDiv(divid) {
	hideElement(document.getElementById(divid));
}

function hideElement(element) {
	element.style.visibility = 'hidden';
	element.style.display = 'none';
}

function showElement(element) {
	element.style.visibility = 'visible';
	element.style.display = 'inline';
}

function toggleLicenseDiv(jobCount) {
	var divid = "div_license_id";
	var div = document.getElementById(divid);
	if (div.style.display=='none') {
		showRow(divid);
		document.getElementById('change_licenseid_bt').value = 'Hide';
		if (jobCount==1) {
			alert("Cannot change ClayTablet license id while there is 1 active translation job. " +
					"Please complete or cancel the job before changing ClayTablet license id."); 
		} else if (jobCount>1) {
			alert("Cannot change ClayTablet license id while there are " + jobCount + " active translation jobs. " +
					"Please complete or cancel the jobs before changing ClayTablet license id."); 
		}
	} else {
		hideDiv(divid);
		document.getElementById('change_licenseid_bt').value = 'Change';
		document.getElementById('licenseid_text').value = document.getElementById('original_licenseid').value;
	}
}

function validateUpdateLicenseId() {
	var cont = false;
	if (document.getElementById("licenseid_text").value=="") {
		cont = confirm("You entered an empty license Id, do you wish to remove the existing ClayTablet license id from" + 
					" this connector? If you proceed, all existing provider will be disabled. " + 
						"You will need to reconfigure license id and account keys for them before sending out new jobs.");
	} else if (document.getElementById("licenseid_text").value==document.getElementById('original_licenseid').value) {
		alert("The enterred license Id is the same as the currently configured license Id");
		toggleLicenseDiv();
	} else if (document.getElementById("providers_count").value=="0" ||
			confirm("Update ClayTablet license Id will invalidate the account keys in all currently-configured " +
					"providers, you need to re-configure the account keys for them. Do you wish to continue")) {
		cont = true;
	}
	return cont;
}

function showProviderEditor(providerid) {
	if (document.getElementById("editor_shown").value=='true') {
		alert("Please finish editing the provider you are working with right now before editing another provider");
	} else {
		if (providerid!='new') {
			hideDiv("providerdisplay_" + providerid);
		}
		showRow("provideredit_" + providerid);
		showRow("providereditaction_" + providerid);
		if (providerid!='new' && document.getElementById("providercfgdisplay_"+ providerid)){
			showRow("providercfgedit_" + providerid);
			hideDiv("providercfgdisplay_" + providerid);
		}
		document.getElementById("editor_shown").value='true';
		var editableLinks = document.getElementsByName("editableLink");
		for (i=0; i<editableLinks.length; i++) {
			hideElement(editableLinks[i]);
		}
		var uneditableLinks = document.getElementsByName("uneditableLink");
		for (i=0; i<uneditableLinks.length; i++) {
			showElement(uneditableLinks[i]);
		}
		document.getElementById("btAddNew").disabled = true;
		document.getElementById("btAddNew").style.cursor = "no-drop";
	}
}

function hideProviderEditor(providerid) {
	if (providerid!='new') {
		document.getElementById("providername_" + providerid).value = 
				document.getElementById('original_name_' + providerid).value;
		document.getElementById("providertype_" + providerid).value = 
				document.getElementById('original_type_' + providerid).value;
		if (document.getElementById("provideraccount_" + providerid)) {
			document.getElementById("provideraccount_" + providerid).value = 
					document.getElementById('original_account_' + providerid).value;
		}
		document.getElementById("providerhost_" + providerid).value = 
				document.getElementById('original_host_' + providerid).value;
		document.getElementById("provideruser_" + providerid).value = 
				document.getElementById('original_user_' + providerid).value;
		document.getElementById("providerpassword_" + providerid).value = 
				document.getElementById('original_password_' + providerid).value;
		document.getElementById("providervalidateresult_" + providerid).innerHTML='';
	}
	
	hideDiv("providereditaction_" + providerid);
	hideDiv("providercfgedit_" + providerid);
	
	hideDiv("provideredit_" + providerid);
	
	if (providerid!='new') {
		showRow("providerdisplay_" + providerid);
	}
	if (providerid!='new' && document.getElementById("providercfgdisplay_"+ providerid)){
		hideProviderConfig(providerid);
	}
	document.getElementById("editor_shown").value='false';
	var uneditableLinks = document.getElementsByName("uneditableLink");
	for (i=0; i<uneditableLinks.length; i++) {
		hideElement(uneditableLinks[i]);
	}
	var editableLinks = document.getElementsByName("editableLink");
	for (i=0; i<editableLinks.length; i++) {
		showElement(editableLinks[i]);
	}
	document.getElementById("btAddNew").disabled = false;
	document.getElementById("btAddNew").style.cursor = "pointer";
}

function clearNewProviderForm() {
	document.getElementById("providername_new").value = ''; 
	document.getElementById("providertype_new").value = 'GENERIC';
	document.getElementById("provideraccount_new").value = '';
	document.getElementById("providerhost_new").value = '';
	document.getElementById("provideruser_new").value = '';
	document.getElementById("providerpassword_new").value = ''; 
}

function toggleProviderConfig(type, providerid) {
	if (document.getElementById("type_" + type + "_configurable").value == 'true') {
		showRow("providercfgedit_" + providerid);
	} else {
		hideDiv("providercfgedit_" + providerid);
	}
}

function showAlertMessage() {
	var msg = document.getElementById("alertmessage").value;
	if (msg!=null && msg!='') {
		alert(msg);
	}
	setTimeout(resetMessage, 10000);
}

function resetMessage() {
	document.getElementById('regularmessage').innerHTML='';
}

function validateProviderConnection(providerid) {
	var alertMsg = '';
	var server = document.getElementById("providerhost_" + providerid).value;
	var user = document.getElementById("provideruser_" + providerid).value;
	var password = document.getElementById("providerpassword_" + providerid).value;
	if (server=='') {
		alertMsg += "Server field cannot be empty\n";
	} else if (user=='') {
		alertMsg += "User field cannot be empty\n";
	} else if (password=='') {
		alertMsg += "Password field cannot be empty\n";
	} 
	if (alertMsg!='') {
		alert(alertMsg);
	} else {
		document.getElementById("providervalidateresult_" + providerid).style.color="black";
		document.getElementById("providervalidateresult_" + providerid).innerHTML='Validating...';
		var posturl = "ContentServer?pagename=CustomElements/CT/WEM/ProviderValidationPost" +
			"&action=ValidateProviderConnection" +
			"&providertype=" + document.getElementById("providertype_" + providerid).value +
			"&host=" + server + "&user=" + user + "&password=" + password;  
	
		$.ajax({url:posturl,success:function(result){
			$(result).find("error").each(function() {
				if ($(this).text()!='') {
			    	alert($(this).text());
					document.getElementById("providervalidateresult_" + providerid).style.color="red";
				} else {
					document.getElementById("providervalidateresult_" + providerid).style.color="green";
				}
			});
			$(result).find("message").each(function() {
				document.getElementById("providervalidateresult_" + providerid).innerHTML = $(this).text();
				setTimeout(function() {
					resetProviderValidateResult(providerid);
				}, 5000);
			});
		}});
	}
}

function resetProviderValidateResult(providerid) {
	document.getElementById("providervalidateresult_" + providerid).innerHTML='';
}

function validateProvider(providerid) {
	if (document.getElementById("action_to_validate").value=='Delete') {
		return confirm("Do you wish to delete provider '" + 
				document.getElementById("original_name_" + providerid).value + "'?");
	} else {
		if (document.getElementById("providername_" + providerid).value=='') {
			alert("Provider name is required");
			return false;
		} else if (document.getElementById("provideraccount_"+providerid).value=='') {
			return confirm("Provider account key is not configured. The new account will remain "
					+ "invalid until it is configure properly. Do you wish to continue?");
		} else {
			return true;
		}
	}
}

function validateNewProvider() {
	if (document.getElementById("providername_new").value=='') {
		alert("Provider name is required");
		return false;
	} else if (document.getElementById("provideraccount_new").value=='') {
		return confirm("Provider account key is not configured. The new account will remain "
				+ "invalid until it is configure properly. Do you wish to continue?");
	} else {
		return true;
	}
}

function hideProviderConfig(providerid) {
	hideDiv("providercfgdisplay_" + providerid);
	showElement(document.getElementById("providercfgunfold_"+providerid));
	hideDiv("providercfgfold_"+providerid);
}

function showProviderConfig(providerid) {
	showRow("providercfgdisplay_" + providerid);
	hideDiv("providercfgunfold_"+providerid);
	showElement(document.getElementById("providercfgfold_"+providerid));
}

function switchProviderConfig(providerid) {
	if (document.getElementById("providercfgdisplay_" + providerid).style.display=='none') {
		showProviderConfig(providerid);
	} else {
		hideProviderConfig(providerid);
	}
}

function showpassword(clearPassword, hiddenPassword) {
	showTableCell(clearPassword);
	hideDiv(hiddenPassword);
	setTimeout(function() {
		showTableCell(hiddenPassword); 
		hideDiv(clearPassword);}, 5000);
}

function confirmRefresh() {
	if (confirm("You are about to refresh this page. Unsaved changes made on the page will be lost. Do you wish to proceed?")) {
		document.getElementById("refreshlink").click();
	}
}
</script>

</head>

<body>
<ics:callelement element="CustomElements/CT/WEM/Header"/>

<c:forEach var="providertype" items="${providertypes}">
	<input type="hidden" id="type_${providertype}_configurable" value="${providertype.extendedConfig}"/>
</c:forEach>

<c:set var="alertmessage" value='<%=ics.GetVar("errormessage")%>'/>
<c:set var="regularmessage" value='<%=ics.GetVar("message")%>'/>
<input type="hidden" id="providers_count" value="${fn:length(providers)}"/>
<input type="hidden" id="alertmessage" value='<c:out value="${alertmessage}" escapeXml="true"/>' />
<input type="hidden" id="action_to_validate" value=''/>
<input type="hidden" id="editor_shown" value='false'/>

<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/WEM/ProvidersConfigFront">
</render:getpageurl>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Translation Providers Configuration</SPAN>&nbsp;&nbsp;
	<SPAN style="text-align:right;"><a id='refreshlink' href='<ics:getvar name="refreshurl"/>' style="display:none"/>
		<a href="javascript:confirmRefresh()">
		<img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a>
	</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>
<c:choose>
	<c:when test="${empty validatedLicenseId}">
		<form id="configure_license_id" width="%100" onSubmit="return validateUpdateLicenseId()">
		<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ProvidersConfigPost"/>
		<table class="width-inner-100" BORDER="0" CELLPADDING="0" CELLSPACING="0">
		<col width="%80"><col width="20%">
		<tr><td>Valid ClayTablet License Id has not been configured. Please configure it now:</td>
		<tr id='div_license_id'> <td>
			License Id : <input type="text" style="padding: 2px;" size='100' id='licenseid_text' name="licenseid" value="${licenseid}"/></td>
			<td ALIGN="right"><input type="Submit" name="action" class="bt-image-medium" value="Update"/></td>
		</tr>
		</form>
	</c:when>
	<c:otherwise>
		<form id="configure_license_id" width="%100" onSubmit="return validateUpdateLicenseId()">
		<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ProvidersConfigPost"/>
		<table class="width-inner-100" BORDER="0" CELLPADDING="0" CELLSPACING="0">
		<col width="%80"><col width="20%">
		<tr class='tile-row-normal'><td>
		Valid ClayTablet License Id has been configured. </td>
		<td ALIGN="RIGHT">
			<input type="button" id='change_licenseid_bt' value='Change' class="bt-image-medium" onclick='toggleLicenseDiv(${totalactivejobs});'/> </td></tr>
		<tr id='div_license_id' class='tile-row-normal' style="display: none;"><td class='form-inset'> 
			License ID :&nbsp;&nbsp;
			<input style="padding: 2px;" type="text" id='licenseid_text' name="licenseid" size='100' value="${licenseid}"
				<c:if test="${totalactivejobs gt 0}">disabled</c:if>
			/></td>
			<td ALIGN="RIGHT"><c:if test="${totalactivejobs eq 0}"><input type="Submit" name="action" class="bt-image-medium" value="Update" /></c:if>
		</td></tr></table>
		</form>
	</c:otherwise>
</c:choose>
<br/>
<div id="regularmessage" class="regular_message">${regularmessage}</div>
<table CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0" width="100%">
<tr HEIGHT="1"><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td>
<td>	
<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<col width="2%">
<col width="20%">
<col width="2%">
<col width="20%">
<col width="2%">
<col width="30%">
<col width="2%">
<col width="20%">
<col width="2%">
<tr HEIGHT="1"><td colspan="9" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Name</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Type</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Account Key</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Jobs #(Active|completed)</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr><td colspan="9" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

<c:forEach var="provider" items="${providers}">
<c:choose>
	<c:when test="${rowstyle eq 'tile-row-normal'}"><c:set var="rowstyle" value="tile-row-highlight"/></c:when>
	<c:otherwise><c:set var="rowstyle" value="tile-row-normal"/></c:otherwise>
</c:choose>
<form id="fm_providerdisplay_${provider.id}">
<input type="hidden" name="providerid" vlaue="${provider.id}"/>
<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ProvidersConfigPost"/>
<input type="hidden" name='original_licenseid' value="${licenseid}"/>
<tr class="${rowstyle}" id="providerdisplay_${provider.id}"><td><BR/></td>
<td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT">
<DIV class="small-text-inset">
<c:choose>
	<c:when test="${not empty provider.accountKey and provider.valid}">
		<a name="uneditableLink" style="display:none; visible:false;cursor:no-drop;">
			<c:out value="${provider.name}" escapeXml="true"/></a>
		<a name="editableLink" style="cursor:pointer;" href='javascript:showProviderEditor("${provider.id}");'>
			<c:out value="${provider.name}" escapeXml="true"/></a></DIV>
	</c:when>
	<c:otherwise>
		<a name="uneditableLink" style="display:none; visible:false;cursor:no-drop;">
			<c:out value="${provider.name}" escapeXml="true"/></a>
		<a name="editableLink" style="color: red;cursor:pointer;" href='javascript:showProviderEditor("${provider.id}");'>
			<c:out value="${provider.name}" escapeXml="true"/><c:if test="${not provider.valid}">&nbsp;(invalid)</c:if></a></DIV>
	</c:otherwise>
</c:choose>
</td>
<td><BR/></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT">
	<c:choose>
	<c:when test="${provider.providerType.extendedConfig eq 'true'}">
	<DIV class="small-text-inset" style="cursor:pointer;"><span onclick="switchProviderConfig('${provider.id}')">
		<c:out value="${provider.providerType.displayName}" escapeXml="true"/></span>
		<img id="providercfgfold_${provider.id}" src="js/fw/images/ui/ui/search/upArrow.png" style="display:none;cursor:pointer;" onclick="hideProviderConfig('${provider.id}')" border="0"/>
		<img id="providercfgunfold_${provider.id}" src="js/fw/images/ui/ui/search/downArrow.png" style="cursor:pointer;" onclick="showProviderConfig('${provider.id}')" border="0"/>
	<BR></DIV>
	</c:when><c:otherwise><DIV class="small-text-inset"><c:out value="${provider.providerType.displayName}" escapeXml="true"/>
	</c:otherwise></c:choose></td>
<td><BR/></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT">
	<DIV class="small-text-inset"><c:out value="${provider.accountKey}" escapeXml="true"/></DIV></td>
<td/><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT">
<DIV class="small-text-inset">${provider.jobStats.activeJobsCount} | ${provider.jobStats.completedJobsCount}</DIV></td><td/>
</tr>
<c:if test="${provider.providerType.extendedConfig eq 'true'}">
<tr class="${rowstyle}" id="providercfgdisplay_${provider.id}" style="display: none;">
<td colspan="3"><BR/></td><td colspan="2" valign="middle" NOWRAP="NOWRAP">
<table>
<tr><td>Server :&nbsp;&nbsp;</td><td>${provider.hostName}</td></tr>
<tr><td>User :&nbsp;&nbsp;</td><td>${provider.loginUser}</td></tr>
<tr><td>Password :&nbsp;&nbsp;</td>
<td onclick='showpassword("showpassword_${provider.id}", "hidepassword_${provider.id}")'>
<DIV id="hidepassword_${provider.id}">********</DIV>
<DIV id="showpassword_${provider.id}" style="display: none; "><c:out value="${provider.password}" escapeXml="true"/></DIV></td></tr>
</table>
</td>
<td colspan="3"></td>
</tr>
</c:if>
</form>

<form id="fm_provideredit_${provider.id}" onSubmit='return validateProvider("${provider.id}")'>
<input type="hidden" name="providerid" value="${provider.id}"/>
<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ProvidersConfigPost"/>
<input type="hidden" name='licenseid' value="${licenseid}"/>
<input type="hidden" id="original_name_${provider.id}" name="original_name" value="${provider.name}"/>
<input type="hidden" id="original_type_${provider.id}" value="${provider.providerType}"/>
<input type="hidden" id="original_account_${provider.id}" value="${provider.accountKey}"/>
<input type="hidden" id="original_host_${provider.id}" value="${provider.hostName}"/>
<input type="hidden" id="original_user_${provider.id}" value="${provider.loginUser}"/>
<input type="hidden" id="original_password_${provider.id}" value="${provider.password}"/>
<tr class="${rowstyle}" id="provideredit_${provider.id}" style="display: none;"><td><BR></td>
<td NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<input style="padding: 2px;" type="text" name="providername" size='30' id="providername_${provider.id}" value="${provider.name}"/></DIV></td>
<td><BR></td><td NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<select name="providertype" id="providertype_${provider.id}" 
			onChange='toggleProviderConfig(this.value, "${provider.id}");'>
		<c:forEach var="providertype" items="${providertypes}">
			<option value="${providertype}"
				<c:if test="${providertype eq provider.providerType}">selected</c:if>
			>${providertype.displayName}</option>
		</c:forEach>
	</select>
<BR/></DIV></td>
<td><BR></td><td NOWRAP="NOWRAP" ALIGN="LEFT">
<DIV class="small-text-inset">
<table width="100%" BORDER="0" CELLPADDING="0" CELLSPACING="0"><tr><td>
	<c:choose>
		<c:when test="${provider.jobStats.activeJobsCount eq 0 or empty provider.accountKey}">
		<select name="accountkey" id="provideraccount_${provider.id}" value="${provider.accountKey}">
			<option value=""/>
			<c:forEach var="disabledaccountkey" items="${disabledaccountkeys}">
				<c:if test="${disabledaccountkey eq provider.accountKey}">
					<option value="${disabledaccountkey}" selected>${disabledaccountkey}</option>
				</c:if>
			</c:forEach>
			<c:forEach var="accountkey" items="${accountkeys}">
				<option value="${accountkey}"
					<c:if test="${accountkey eq provider.accountKey}">selected</c:if>
				>${accountkey}</option>
			</c:forEach>
			<c:if test="${fn:length(disabledaccountkeys) gt 0}">
				<option disabled>===== Keys already in use =====</option>
			</c:if>
			<c:forEach var="disabledaccountkey" items="${disabledaccountkeys}">
				<c:if test="${disabledaccountkey ne provider.accountKey}">
					<option value="${accountkey}" disabled>${disabledaccountkey}</option>
				</c:if>
			</c:forEach>
		</select>
		<BR/>
		</c:when>
		<c:otherwise>
			<DIV class="small-text-inset"><c:out value="${provider.accountKey}" escapeXml="true"/>(Cannot change)&nbsp;</DIV>
			<input type="hidden" name="accountkey" id="provideraccount_${provider.id}" value="${provider.accountKey}"/>
		</c:otherwise>
</c:choose>
</td><td align="right">
<c:if test="${showValidateAccount eq 'true'}">
<input type="submit" name="Submit" value="Validate Account" class="bt-image-medium" 
		onclick='document.getElementById("action").value="validateAccount"; return validateForm();'/>
</c:if>  
</td></tr></table></DIV>
</td>
<td><BR/></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT">
<DIV class="small-text-inset" <c:if test="${provider.jobStats.activeJobsCount gt 0}"> style ="color: red;"</c:if>>
${provider.jobStats.activeJobsCount} | ${provider.jobStats.completedJobsCount}</DIV></td><td/>
</tr>
<tr class="${rowstyle}" id="providercfgedit_${provider.id}" style="display: none;"><td colspan="3"><BR></td>
<td valign="middle" NOWRAP="NOWRAP">
<table>
<tr><td>Server :</td>
	<td><input style="padding: 2px;" type="text" id="providerhost_${provider.id}" size="30" 
			name="providerhost" value="${provider.hostName}" onkeyup='resetProviderValidateResult("${provider.id}")'/></td></tr>
<tr><td>User :</td>
	<td><input style="padding: 2px;" type="text" id="provideruser_${provider.id}" size="30" 
			name="provideruser" value="${provider.loginUser}" onkeyup='resetProviderValidateResult("${provider.id}")'/></td></tr>
<tr><td>Password :</td>
	<td><input style="padding: 2px;" type="password" id="providerpassword_${provider.id}" size="30" 
			name="providerpassword" value="${provider.password}" onkeyup='resetProviderValidateResult("${provider.id}")'/></td></tr>
</table>
</td><td/>
<td NOWRAP="NOWRAP" VALIGN="BOTTOM" style="height: 100%"><DIV class="small-text-inset">
<input type="button" name="ValidateProvider" value="Validate Provider" class="bt-image-long" 
		onclick='validateProviderConnection("${provider.id}");'/> 
<label id="providervalidateresult_${provider.id}"></label>
</DIV></td><td colspan="3"></td>
</tr>
<tr class="${rowstyle}" id="providereditaction_${provider.id}" style="display: none;"><td><BR></td>
<td colspan="5" valign="middle" NOWRAP="NOWRAP">
<DIV class="small-text-inset">
<input type="submit" name="action" value="Save" class="bt-image-medium" 
		onclick='document.getElementById("action_to_validate").value="Save";'/> 
	<c:choose>
		<c:when test="${provider.jobStats.activeJobsCount eq 0}">
			<input type="submit" name="action" value="Delete" class="bt-image-medium" 
					onclick='document.getElementById("action_to_validate").value="Delete";'/> 
		</c:when>
		<c:otherwise>
			<input type="button" name="action" value="Delete" class="bt-image-medium" 
					onclick='alert("Cannot delete provider with active translation jobs.")'/> 
		</c:otherwise>
	</c:choose>
<input type="button" name="Cancel" value="Cancel" class="bt-image-medium" 
		onclick='hideProviderEditor("${provider.id}");'/> 
<BR/></DIV></td><td colspan="3"></td>

</tr>
</form>

</c:forEach>

<c:choose>
	<c:when test="${rowstyle eq 'tile-row-normal'}"><c:set var="rowstyle" value="tile-row-highlight"/></c:when>
	<c:otherwise><c:set var="rowstyle" value="tile-row-normal"/></c:otherwise>
</c:choose>

<form id="fm_provideredit_new" onSubmit="return validateNewProvider()">
<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ProvidersConfigPost"/>
<input type="hidden" name='licenseid' value="${licenseid}"/>
<tr class="${rowstyle}" id="provideredit_new" style="display: none;"><td><BR></td>
<td NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<input style="padding: 2px;" type="text" name="providername" size='30' id="providername_new" value="${provider.name}"/></DIV></td>
<td><BR></td><td NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">
	<select name="providertype" id="providertype_new" 
			onChange='toggleProviderConfig(this.value, "new");'>
		<c:forEach var="providertype" items="${providertypes}">
			<option value="${providertype}"
				<c:if test="${providertype eq provider.providerType}">selected</c:if>
			>${providertype.displayName}</option>
		</c:forEach>
	</select>
<BR/></DIV></td>
<td><BR></td><td NOWRAP="NOWRAP" ALIGN="LEFT">
<DIV class="small-text-inset">
<table width="100%" BORDER="0" CELLPADDING="0" CELLSPACING="0"><tr><td>
	<select name="accountkey" id="provideraccount_new">
		<option value=""/>
		<c:forEach var="accountkey" items="${accountkeys}">
			<option value="${accountkey}"
				<c:if test="${accountkey eq provider.accountKey}">selected</c:if>
			>${accountkey}</option>
		</c:forEach>
		<c:if test="${fn:length(disabledaccountkeys) gt 0}">
			<option disabled>===== Keys already in use =====</option>
		</c:if>
		<c:forEach var="disabledaccountkey" items="${disabledaccountkeys}">
			<c:if test="${disabledaccountkey ne provider.accountKey}">
				<option value="${accountkey}" disabled>${disabledaccountkey}</option>
			</c:if>
		</c:forEach>
	</select>
	<BR/>
</td><td align="right">
<c:if test="${showValidateAccount eq 'true'}"> 
<input type="submit" name="Submit" value="Validate Account" class="bt-image-long" 
		onclick='document.getElementById("action").value="validateAccount"; return validateForm();'/>  
</td>
</c:if>
<td/><td/>
</tr></table></DIV>
</td>
<td colspan="3"></td>
</tr>
<tr class="${rowstyle}" id="providercfgedit_new" style="display: none;"><td colspan='3'><BR></td>
<td valign="middle" NOWRAP="NOWRAP">
<table>
<tr><td>Server :</td>
	<td><input style="padding: 2px;" type="text" id="providerhost_new" size="30" 
			name="providerhost" value="" onkeyup="resetProviderValidateResult('new')"/></td></tr>
<tr><td>User :</td>
	<td><input style="padding: 2px;" type="text" id="provideruser_new" size="30" 
			name="provideruser" value="" onkeyup="resetProviderValidateResult('new')"/></td></tr>
<tr><td>Password :</td>
	<td><input style="padding: 2px;" type="password" id="providerpassword_new" size="30" 
			name="providerpassword" value="" onkeyup="resetProviderValidateResult('new')"/></td></tr>
</table>
</td><td/>
<td NOWRAP="NOWRAP" VALIGN="BOTTOM" style="height: 100%;"><DIV class="small-text-inset">
<label id="providervalidateresult_new"></label><BR/>
<input type="button" name="ValidateProvider" value="Validate Provider" class="bt-image-long" 
		onclick='validateProviderConnection("new");'/> 
</DIV></td><td colspan="3"></td>
</tr>
<tr class="${rowstyle}" id="providereditaction_new" style="display: none;"><td><BR></td>
<td colspan="5" valign="middle" NOWRAP="NOWRAP">
<DIV class="small-text-inset">
<input type="submit" name="action" value="Save" class="bt-image-medium" 
		onclick='document.getElementById("action").value="Save"; return validateForm();'/> 
<input type="button" name="Clear" value="Clear" class="bt-image-medium" 
		onclick='clearNewProviderForm();'/> 
<input type="button" name="Cancel" value="Cancel" class="bt-image-medium" 
		onclick='hideProviderEditor("new");'/> 
<BR/></DIV></td><td colspan="3"></td>
</tr>
</form>
</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr HEIGHT="1"><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr></table>
<br/>
<satellite:form>
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/ProvidersConfigPost"/>
	
	<input type="hidden" id='original_licenseid' name='original_licenseid' value="${licenseid}"/>

	<c:if test="${not empty validatedLicenseId}">
		<input type="button" id="btAddNew" value="Add new provider" class="bt-image-long" onclick='showProviderEditor("new");'/> 
	</c:if>			
</satellite:form>
</td></tr></table>
<p/>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>

<script>
	window.onload = showAlertMessage();
</script>

</body>
</html>

</gsf:root>
</cs:ftcs>
