<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="assetset" uri="futuretense_cs/assetset.tld"
%><%@ taglib prefix="commercecontext" uri="futuretense_cs/commercecontext.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="listobject" uri="futuretense_cs/listobject.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="searchstate" uri="futuretense_cs/searchstate.tld"
%><%@ taglib prefix="siteplan" uri="futuretense_cs/siteplan.tld"
%><%@ taglib prefix="workflowassignment" uri="futuretense_cs/workflowassignment.tld"
%><%@ taglib prefix="workflowasset" uri="futuretense_cs/workflowasset.tld"
%><%@ taglib prefix="workflowengine" uri="futuretense_cs/workflowengine.tld"
%><%@ taglib prefix="workflowstate" uri="futuretense_cs/workflowstate.tld"
%><%@ taglib prefix="workflowprocess" uri="futuretense_cs/workflowprocess.tld"
%><%@ taglib prefix="workflowstep" uri="futuretense_cs/workflowstep.tld"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors"
%><cs:ftcs><%--

INPUT

OUTPUT

--%>

<%

// Setup variables
String stepname1 = ics.GetVar("stepname1");
String stepname2 = ics.GetVar("stepname2");
String assettype = ics.GetVar("AssetType");
String id = ics.GetVar("id");
String siteid = ics.GetVar("pubid");

ics.RemoveVar("stepid");
%>
<workflowasset:load assettype='<%=assettype%>' id='<%=id%>' objvarname="workflowasset"  />
<workflowengine:getlegalnextworkflowsteps object="workflowasset" prefix="wfStep:" site='<%=siteid%>'/>
<%
String stepid = null;
if (Utilities.goodString(ics.GetVar("wfStep:Total"))) {
	for (int i=0; i < Integer.parseInt(ics.GetVar("wfStep:Total")); i++) {
		String stepvarname = "wfStep:"+i;
		%>
		<workflowstep:getid name='<%=stepvarname%>' varname="thisstepid" />
		<workflowstep:getname name='<%=stepvarname%>' varname="thisstepname" />
		<%
		if ((stepname1 != null && stepname1.equalsIgnoreCase(ics.GetVar("thisstepname"))) || (stepname2 != null && stepname2.equalsIgnoreCase(ics.GetVar("thisstepname")))) {
			stepid = ics.GetVar("thisstepid");
			break;
		}
	}
	if (stepid != null) {
		ics.SetVar("stepid",stepid);
	}
}
%>
</cs:ftcs>
