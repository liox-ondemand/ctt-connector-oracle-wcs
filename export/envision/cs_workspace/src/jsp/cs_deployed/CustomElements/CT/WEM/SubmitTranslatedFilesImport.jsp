<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UISubmitTranslatedFilesManager">

<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<ics:callelement element="CustomElements/CT/WEM/Header"/>

<script>
</script>

<!-- Get the roles of the user for this site -->
<usermanager:getuserfromname username='<%=ics.GetSSVar("username")%>' objvarname="u"/>
<ccuser:getsiteroles name="u" site='<%=ics.GetSSVar("pubid")%>' objvarname="roleobject"/>
<rolelist:getall name="roleobject" varname="roles"/>
<input type="hidden" name="docId" value="0"/>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Submit Translated Files
</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>
<c:if test="${not empty errorMessage}">
<span style="color:red;">${errorMessage}</span>
</c:if>
<c:if test="${not empty result}">
<p>Successfully submitted file '${originalfile}' to WCS</p>
<li>Translation files processed successfully: ${result.processedFilesCount}</li>
<c:if test="${result.errorFilesCount > 0}">
<li>Translation files processed with error: ${result.errorFilesCount}</li>
</c:if>
<li>Translation requests updated: ${result.updatedTranslationRequestsCount}</li>
<li>Target asset updated: ${result.updatedTargetItemsCount}</li>
<li>Target asset attributes updated: ${result.updatedFieldsCount}</li>
<li>Translation jobs updated: ${result.updatedJobsCount}</li>
<c:if test="${result.updatedJobsCount > 0}">
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" width="100%" style="margin: 1em;" >
<tr><td></td><td class="tile-dark"  HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
<tr><td class="tile-dark"  WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td>
<td>
<table class="width-inner-100 fixed_width_table" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<col width="2%">
<col width="1%">
<col width="15%">
<col width="1%">
<col width="20%">
<col width="1%">
<col width="10%">
<col width="1%">
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<col width="7%">
<col width="1%">
<tr><td colspan="22" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr>
<td class="tile-a" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Name</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Description</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Status</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Due Date</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Provider</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Site</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title"># of Assets</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Created</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Created Date</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Submitted</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr><td colspan="22" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

<c:forEach var="job" items="${jobs}">
	<render:getpageurl outstr="detailsurl" pagename="CustomElements/CT/JobFront">
		<render:argument name="jobid" value="${job.id}"/>
		<render:argument name="app" value="WEM"/>
	</render:getpageurl> 
	<fmt:formatDate value="${job.dueDate}" var="dueDateFormatted" type="date" pattern="yyyy-MM-dd" />
	<fmt:formatDate value="${job.createdDate}" var="createdDateFormatted" type="date" pattern="yyyy-MM-dd" />
	<fmt:formatDate value="${job.createdDate}" var="createdDateFormatted2" type="date" pattern="yyyy-MM-dd hh:mm:ss" />
	<fmt:formatDate value="${job.submittedDate}" var="submittedDateFormatted" type="date" pattern="yyyy-MM-dd" />
	<fmt:formatDate value="${job.submittedDate}" var="submittedDateFormatted2" type="date" pattern="yyyy-MM-dd hh:mm:ss" />

<tr class="tile-row-normal">
<td valign="middle" NOWRAP="NOWRAP"></td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title='<c:out value="${job.name}" escapeXml="true"/>'>
	<a href='<ics:getvar name="detailsurl"/>'><c:out value="${job.name}" escapeXml="true"/></a></td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title='<c:out value="${job.description}" escapeXml="true"/>'>
	<c:out value="${job.description}" escapeXml="true"/></td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${job.status.displayName}">${job.status.displayName}</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">
	<c:choose>
		<c:when test="${job.dueDate.time < now.time}"><b><font color="red">${dueDateFormatted}</font></b></c:when>
		<c:otherwise>${dueDateFormatted}</c:otherwise>
	</c:choose>
</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" 
		title='<c:out value="${job.translationProvider.name}" escapeXml="true"/>'>${job.translationProvider.name}</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">${job.siteName}</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset">${fn:length(job.translationRequests)}</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${job.createdUser}">${job.createdUser}</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${createdDateFormatted2}">${createdDateFormatted}</td>
<td><BR></td><td  NOWRAP="NOWRAP" ALIGN="LEFT" class="small-text-inset" title="${submittedDateFormatted2}">${submittedDateFormatted}</td>
<td><BR></td>
</tr>
</c:forEach>
</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1">
		<IMG WIDTH="1" HEIGHT="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1">
		<IMG WIDTH="1" HEIGHT="1" src="Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td></td><td background="Xcelerate/graphics/common/screen/shadow.gif">
		<IMG WIDTH="1" HEIGHT="5" src="Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td>
</tr>
</table></c:if></c:if>
<render:getpageurl outstr="backurl" pagename="CustomElements/CT/WEM/SubmitTranslatedFiles"/>
<table>
<tr><td><a href='<ics:getvar name="backurl"/>'>Back</a></td></tr>
</table>

</td></tr></TABLE>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</gsf:root>
</cs:ftcs>
