<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ page import="COM.FutureTense.Interfaces.Utilities;"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIAssignLocalesManager">
<ics:callelement element="CustomElements/CT/WEM/Header"/>
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">
<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
<c:if test="${empty step}">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script language="javascript">

function getAssetTypes(site) {
	var posturl = '${csroot}ContentServer?pagename=CustomElements/CT/GetEnabledAssetTypesForSite&site=' + site + "&requireDimension=true";
		// Make ajax call and show asset types
 		$.ajax({url:posturl,success:function(result){
 			
 			$('#selectedassettypes').find('option').remove();
 			
  			var result = result.trim();
  			var assettypes = result.split(","); 
  			for (var i = 0;i < assettypes.length; i++) {
  			    var assettype = assettypes[i];
  			    var opt = document.createElement("option");
  			  		opt.text = assettype;
  					opt.value = assettype;
  					document.getElementById("selectedassettypes").options.add(opt);
  			}
  			
  			showDiv("assettypesdiv");
  			
  			
		}});
}

function validateAssetTypesSelection() {
	var assettypelist = document.getElementById("selectedassettypes");
	
	for (var i = 0; i < assettypelist.options.length; i++) {
	    if (assettypelist.options[ i ].selected) {
			showDiv("modal_blocker");
			showDiv("loadingprogress");
	    	return true;
	    }
	}
	alert("Please select one or more asset types to continue.");
	return false;
}

function selectSiteToAssign() {
	var site = document.getElementById("selectedsite").value;
	
	if (site && site!="") {
		getAssetTypes(site);
		var siteLocales = document.getElementById("locales_" + site);
		if (siteLocales) {
			document.getElementById("selectedlocale").innerHTML=siteLocales.innerHTML;
		} else {
			document.getElementById("selectedlocale").innerHTML="";
		}
	}
}

$( document ).ready(function() {
		selectSiteToAssign();
		hideDiv("modal_blocker");
		hideDiv("loadingprogress");
	}); 
</script>
</c:if>
<c:if test="${step=='2'}">
<script>
function check(source) {
  checkboxes = document.getElementsByName('assetlist');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}

function processForm() {
	var hasError = true;
	var msg = "The following error occurred:\n";
	msg += "- You must select at least one asset.";
	var boxes = document.getElementsByTagName("input");
	for (var i=0; i<boxes.length; i++) {
		if (boxes[i].id.indexOf("assetlist") > -1 && boxes[i].checked) {
			hasError = false;
			break;
		}
	}

	if (!hasError) {
		showDiv("modal_blocker");
		showDiv("loadingprogress");
		document.forms["AppForm"].submit();
	} else {
		alert(msg);
	}
}
</script>

</c:if>

<script>
function showDiv(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'block';
}

function hideDiv(divid) {
	document.getElementById(divid).style.visibility = 'hidden';
	document.getElementById(divid).style.display = 'none';
}

function cancelForm() {
	document.getElementById("step").value="";
	document.forms["AppForm"].submit();
}

</script>

<div class="blocking_overlay" id="modal_blocker"></div>
<div id="loadingprogress" class="popup_panel_center">
	<img src="js/fw/images/ui/wem/loading.gif" style="h-align:center;"></img>
	<p>Processing request, please wait ...</p></div>
	
<satellite:form name="AppForm" method="POST">
<input type="hidden" name="pagename" value="CustomElements/CT/WEM/AssignLocalesFront"/>

<c:if test="${empty step}">

<input type="hidden" name="step" id="step" value="2"/>

<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important;">
<TR><TD><SPAN CLASS="title-text">Assign Locales:</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>Choose site and locale and then the Asset Types to which you wish to assign the locale. <br/> <br/>
<b>Note:</b> the selected site must have the Dimension asset type enabled. You can use the "Enable Locales" tool to enable Dimension asset type for sites.
<p>The asset types also need to have Dimension enabled before you can assign locale to them. 
<c:if test="${formstatus == 'complete'}"><p><strong>Assets updated successfully.</strong></p></c:if>
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>
	<TABLE CLASS="inner" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<tr><td class="form-label-text">Select Site:</td>
		<td></td>
		<td class="form-inset">
		<c:set var="siteslection" value='<%=ics.GetVar("selectedsite")%>'/>
		<select name="selectedsite" id="selectedsite" onchange="selectSiteToAssign()">
			<c:forEach var="site" items="${eligiblesites}">
          		<option value="${site.name}" <c:if test="${site.name eq siteslection}">selected</c:if>>${site.name}</option>
        	</c:forEach>
		</select>
		</td>
	</tr>

	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<tr><td width="50%" class="form-label-text">Select Locale:</td>
		<td width="10%"></td>
		<td width="40%" class="form-inset">
		<c:forEach var="site" items="${eligiblesites}">
			<select id="locales_${site.name}" style="display:none">
				<c:forEach var="locale" items="${site.locales}">
	          		<option value="${locale}">${locale}</option>
	        	</c:forEach>
			</select>
		</c:forEach>
		<select name="selectedlocale" id="selectedlocale">
			<c:forEach var="locale" items="${eligiblelocales}">
          		<option value="${locale}">${locale}</option>
        	</c:forEach>
		</select>
		</td>
	</tr>
	
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<tr><div id="assettypesdiv" style="visibility:hidden;display:none;">
		<td class="form-label-text">Select Asset Types:</td>
		<td>&nbsp;</td>
		<td class="form-inset"><select width="100" size="10" name="selectedassettypes" id="selectedassettypes" multiple></select></td>
		</div>
	</tr>
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
	<tr><td></td>
		<td></td>
		<td><input type="submit" name="submit" value="Continue" class="f1image-small" onclick='return validateAssetTypesSelection();'/></td>
	</tr>
	<tr><td colspan="3"><img height="20" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

	

	</TABLE>
</TD></TR>
</TABLE>

</c:if>



<c:if test="${step=='2'}">

<input type="hidden" name="step" id="step" value="3"/>
<input type="hidden" name="selectedlocale" value="${locale}"/>
<input type="hidden" name="selectedsite" value="${site}"/>
<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important;">
<TR><TD><SPAN CLASS="title-text">Assign Locales:</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

<c:choose>
<c:when test="${not empty assetlist}">

<TR><TD>
Select the assets in the '${site}' site that you want to assign locale '${locale}' to:
</TD></TR>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<TR><TD>

<input type="button" name="button" value="Assign" class="f1image-small" onclick="processForm();"/>&nbsp;&nbsp;&nbsp;
<input type="button" name="button" value="Cancel" class="f1image-small" onclick="cancelForm();"/><br/>&nbsp;

<table BORDER="0" CELLSPACING="0" CELLPADDING="0">
<tr><td></td><td class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
<tr><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td>
<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<tr><td colspan="12" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr>
<td class="tile-a" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title"><input name="checkall" type="checkbox" onClick="check(this)""></DIV></td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">ID</DIV></td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Type</DIV></td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Name</DIV></td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="${csroot}Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Description</DIV></td>
<td class="tile-c" background="${csroot}Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
</tr>
<tr><td colspan="12" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>


<c:forEach var="asset" items="${assetlist}">  	
<tr class="tile-row-normal">
<td valign="middle" NOWRAP="NOWRAP"></td>
<td><BR></td><td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset"><input name="assetlist" id="assetlist" type="checkbox" value="${asset.type}:${asset.id}"/><BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.id}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.type}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.name}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.description}<BR></DIV></td>
<td><BR></td>
</tr>
</c:forEach>


</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr><tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td></td><td background="${csroot}Xcelerate/graphics/common/screen/shadow.gif"><IMG WIDTH="1" HEIGHT="5" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td>
</tr>

</c:when>
<c:otherwise>
<tr><td>
	There is no asset available for update. Either there is no asset of the selected type(s) within the specified site, or all assets already have a locale assigned to them.
	<br/><input type="button" name="button" value="OK" class="f1image-small" onclick="cancelForm();"/><br/>&nbsp;
</td></tr>
</c:otherwise></c:choose>
</table>

</c:if>

<c:if test="${step=='3'}">
	<input type="hidden" name="step" id="step" value=""/>
	<input type="hidden" name="selectedlocale" value="${locale}"/>
	<input type="hidden" name="selectedsite" value="${site}"/>
<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important;">
<TR><TD><SPAN CLASS="title-text">Assign Locales:</SPAN></SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td>Locale '${locale}' has been assigned to ${fn:length(assetlist)} asset(s) in site '${site}'.</td></tr>
<tr><td><input type="button" name="button" value="OK" class="f1image-small" onclick="cancelForm();"/><br/></td></tr>
<tr> <td>&nbsp;&nbsp;</td></tr>
</TABLE>
</c:if>
</satellite:form>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
</gsf:root>
</cs:ftcs>
