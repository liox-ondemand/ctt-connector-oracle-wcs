<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIDeveloperToolManager">
<html xmlns="http://www.w3.org/1999/xhtml">    
  <head>      
    <title>Develop Tool Post result</title>
   
   <c:url value="/ContentServer" var="url">
	  <c:param name="pagename" value="CustomElements/CT/WEM/DeveloperTool" />
   </c:url>
    <meta http-equiv="refresh" content="3;URL='${url}'" />    

	<script language="javascript">
	function showAlertMessage() {
		var msg = document.getElementById("alertmessage").value;
		if (msg!=null && msg!='') {
			alert(msg);
		}
	}
	
	</script>
  </head>    
  <body> 
<ics:callelement element="CustomElements/CT/WEM/Header"/>

	<c:set var="alertmessage" value='<%=ics.GetVar("errormessage")%>'/>
	<c:set var="regularmessage" value='<%=ics.GetVar("message")%>'/>
	<input type="hidden" id="alertmessage" value="<c:out value='${alertmessage}' escapeXml='true'/>" />
	
    <p style="color: green;">${regularmessage}</p>
    <p>Redirecting to Developer Tool page in 3 seconds...</p> 

<ics:callelement element="CustomElements/CT/WEM/Footer"/>

	<script>
		window.onload = showAlertMessage();
	</script>
  </body>  
</html>     
</gsf:root>
</cs:ftcs>
