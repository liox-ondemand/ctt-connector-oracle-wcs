<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UITranslateBranchManager">

<c:if test="${showform}">
<ics:callelement element="CustomElements/CT/HeaderHtml"/>
<ics:callelement element="CustomElements/CT/HeaderTab"/>
<ics:callelement element="OpenMarket/Xcelerate/UIFramework/LoadDojo"/>


<script language="javascript"><!--
function inspectAsset(type, longid) {

	SitesApp.event(SitesApp.id('asset', type + ':' + longid), 'inspect'); // This works for .6.1
	<%--
	// SitesApp.event(fwutil.buildDocId(longid, type), 'inspect'); // This works for .8
	// Be sure to do ics.callement on OpenMarket/Xcelerate/UIFramework/LoadDojo at the top first
	--%>
}
-->
</script>

<script language="javascript"><!--

function toggleCheckboxes() {
	var hasChecked = false;
	var boxes = document.getElementsByName("page_assetids");
	for (var i=0; i < boxes.length; i++) {
		if (boxes[i].checked) {
			hasChecked = true;
			break;
		}
	}
	if (hasChecked) {
		uncheckAll();
	} else {
		checkAll();
	}
	document.getElementById("toggler").checked = false;
	var totalSelected = getNumberOfSelectedItems();	
	updateTotal(totalSelected);
}

function checkAll() {
	var boxes = document.getElementsByName("page_assetids");
	for (var i=0; i < boxes.length; i++) {
		boxes[i].checked = true;
		selectOrUnselectItem(boxes[i].value, boxes[i].checked);
	}
}

function uncheckAll() {
	var boxes = document.getElementsByName("page_assetids");
	for (var i=0; i < boxes.length; i++) {
		boxes[i].checked = false;
		selectOrUnselectItem(boxes[i].value, boxes[i].checked);
	}
}

function selectAndCheckAll() {
	selectAll();
	checkAll();
}

function unselectAndUncheckAll() {
	unselectAll();
	uncheckAll();
}

function selectAll() {
	var selectitems = document.getElementById("assetids");
	for (var i=0; i < selectitems.options.length; i++) {
		selectitems[i].selected = true;
	}
	updateTotal( selectitems.options.length );
}

function unselectAll() {
	var selectitems = document.getElementById("assetids");
	for (var i=0; i < selectitems.options.length; i++) {
		selectitems[i].selected = false;
	}
	updateTotal( "0" );
}

function updateDropdownAndTotal(thisboxvalue, ischecked) {
	// When the box is checked or unchecked, select or unselect the corresponding
	// item from the assetids drop-down menu.
	
	// Updated the drop-down menu
	selectOrUnselectItem(thisboxvalue, ischecked);
	
	var totalSelected = getNumberOfSelectedItems();	
	updateTotal(totalSelected);
}

function getNumberOfSelectedItems() {
	var selectitems = document.getElementById("assetids");
	var total = 0;
	for (var i=0; i < selectitems.options.length; i++) {
		if (selectitems[i].selected == true) {
			total++;
		}
	}
	return total;
}

function updateTotal(total) {
	document.getElementById("totalselected").innerHTML = total;
}

function selectOrUnselectItem(itemvalue, selectval) {
	var selectitems = document.getElementById("assetids");
	for (var i=0; i < selectitems.options.length; i++) {
		if (selectitems[i].value == itemvalue) {
			selectitems[i].selected = selectval;
			break;
		}
	}
}

function checkOrUncheckBox(itemvalue, checkedval) {
	var boxes = document.getElementsByName("page_assetids");
	for (var i=0; i < boxes.length; i++) {
		if (boxes[i].value == itemvalue) {
			boxes[i].checked = checkedval;
			break;
		}
	}	
}

function getCheckedRadio( radioname ) {

	var checkedval = "";
	var radios = document.getElementsByName( radioname );
	for (var i=0; i<radios.length; i++) {
		if (radios[i].checked) {
			checkedval = radios[i].value;
		}
	}
	return checkedval;

}

function getCheckedBoxes( boxname ) {

	var boxes = document.getElementsByName( boxname );
	var checked = "";
	for (var i=0; i<boxes.length; i++) {
		if (boxes[i].checked) {
			checked += ", " + boxes[i].value;
		}
	}
	//alert(checked);
}

function clearMessage() {
	activeView.clearMessage();
    alert();
}

function setFirstRecordAndProcessForm(firstrec) {
	document.getElementById("page_firstrecord").value = firstrec;
	processForm("paginate");
}

function processForm(action) {
	
	clearMessage();
	var localeFilter = getCheckedRadio( "localeFilterRadios" );
	document.getElementById("localeFilter").value = localeFilter;

	var actionelement = document.getElementById("action");
	actionelement.value = action;
	var msg = "";
	var isvalid = true;
	if (action == "translate") {
		var boxes = document.getElementById("assetids");
		for (var i=0; i < boxes.options.length; i++) {
			isvalid = false;
			msg = "You must specify at least one asset to translate";
			if (boxes[i].selected == true) {
				isvalid = true;
				break;
			}
		}
	}
	if (isvalid) {
		document.getElementsByName("AppForm")[0].submit();
	} else {
		alert(msg);
	}
}

function unselectItemsOnLoad() {

	var selectedassetids="${cs.assetids}";
	if (selectedassetids != "") {
		unselectAndUncheckAll();
		
		var saSelectedIds = selectedassetids.split(";");
		for (var i=0; i < saSelectedIds.length; i++) {
			selectOrUnselectItem( saSelectedIds[i], true );
			checkOrUncheckBox( saSelectedIds[i], true );
		}
		
		var totalSelected = getNumberOfSelectedItems();	
		updateTotal(totalSelected);
	}
	
}

//--></script>

<satellite:form name="AppForm">

	<gsf:asset-load name="thisasset" c="${cs.c}" cid="${cs.cid}" attributes="name"/>
	<input type="hidden" name="pagename" value="CustomElements/CT/TranslateBranch"/>
	<input type="hidden" name="action" id="action" value="filter"/>
	<input type="hidden" name="c" id="c" value="${cs.c}"/>
	<input type="hidden" name="cid" id="cid" value="${cs.cid}"/>
	<input type="hidden" name="localeFilter" id="localeFilter" value="${cs.localeFilter}"/>
	<input type="hidden" name="page_firstrecord" id="page_firstrecord" value="0"/>
<!--<textarea id="codeout" name="codeout"></textarea>-->


	<table border="0" cellpadding="0" cellspacing="0" width="100%"> <!-- start 3 column table -->
	<tr>
		<td valign="top" width="80%">


<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important;">
<TR><TD><SPAN CLASS="title-text">Translate Branches of Page: ${thisasset.name}</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td>

<c:if test="${empty assetlist}"><p>The selected page in the Site Tree has no branches. Select a different page to proceed.</p></c:if>
<c:if test="${not empty assetlist}">



	<c:if test="${(not hasDifferentLocales) && (not hasUnassignedLocales)}">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td>Check the box beside the assets you wish to send for translation and click "Continue". 
				On the next page, you will choose the target language and other details.
				</td>
				<td>&nbsp; &nbsp; &nbsp; &nbsp;</td>
				<td align="right" valign="top">
					<input type="button" name="continue" id="continue" value="Continue" onClick="processForm('translate');" class="f1image-medium"/>
				</td>
			</tr>
			<tr>
				<td>
					Total Selected Records (on all pages): <span id="totalselected">${fn:length(assetlist)}</span>
				</td>
				<td colspan="2" align="right" value="top" nowrap>
					<input type="button" name="selectall" id="selectall" value="Select All" onClick="selectAndCheckAll();" class="f1image-medium"/>
					<input type="button" name="unselectall" id="unselectall" value="Unselect All" onClick="unselectAndUncheckAll();" class="f1image-medium"/>
				</td>
			</tr>
			<c:if test="${fn:length(page_startrecords) > 1}">
			<tr>
				<td colspan="3">Pages:
					<c:forEach var="page_startids" items="${page_startrecords}" varStatus="status">
						<c:if test="${page_firstrecord ne page_startids}"><a href="#" onClick="setFirstRecordAndProcessForm(${page_startids});">${status.index + 1}</a></c:if>
						<c:if test="${page_firstrecord eq page_startids}">${status.index + 1}</c:if>
						<c:if test="${!status.last}"> | </c:if> 
					</c:forEach>
				</td>
			</tr>
			</c:if>
		</table>
	</c:if>
	<c:if test="${hasDifferentLocales}">
		<script language="Javascript">
			activeView.error("All assets must have the same source locale. Please select a locale to filter your selection.");
		</script>
	</c:if>	
	<c:if test="${hasUnassignedLocales}">
		<script language="Javascript">
			activeView.error("One or more assets does not have a locale assigned to it. Please ensure all assets have a source locale then rerun your search.");
		</script>
	</c:if>	
	
	<p>
<table BORDER="0" CELLSPACING="0" CELLPADDING="0" width="100%">
<tr><td></td><td class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td></tr>
<tr><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td>
<table class="width-inner-100" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<tr><td colspan="18" class="tile-highlight"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr>
<td class="tile-a" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title"><input type="checkbox" id="toggler" name="toggler" onClick="toggleCheckboxes();"/></DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Asset Type</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Asset ID</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Asset Name</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Description</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Locale</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Created By</DIV></td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
<td class="tile-b" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif" NOWRAP="NOWRAP"><DIV class="new-table-title">Created Date</DIV></td>
<td class="tile-c" background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/grad.gif">&nbsp;</td>
</tr>
<tr><td colspan="18" class="tile-dark"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>

<c:forEach var="asset" items="${assetlist}" begin="${page_firstrecord}" end="${page_lastrecord}">
<c:set var="assetname" value="${asset.name}"/>
<c:if test="${fn:length(asset.name)>20}">
	<c:set var="assetname" value="${fn:substring(asset.name,0,20)}..."/>
</c:if>
<c:set var="assetdesc" value="${asset.description}"/>
<c:if test="${fn:length(asset.description)>20}">
	<c:set var="assetdesc" value="${fn:substring(asset.description,0,20)}..."/>
</c:if>
<c:set var="datecreated" value="${asset.createdDate}" />

<tr class="tile-row-normal">
<td valign="middle" NOWRAP="NOWRAP"></td>
<td><BR></td><td VALIGN="TOP" ALIGN="LEFT"><DIV class="small-text-inset"><input checked type="checkbox" name="page_assetids" id="page_assetids" value="${asset.assetType}:${asset.id}" onClick="updateDropdownAndTotal(this.value, this.checked)"/><BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.assetType}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.id}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset"><a href="javascript:inspectAsset('${asset.assetType}', '${asset.id}');">${assetname}</a><BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${assetdesc}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.locale}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset">${asset.createdBy}<BR></DIV></td>
<td><BR></td><td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"><DIV class="small-text-inset"><fmt:formatDate pattern="MMMM dd, yyyy" value="${datecreated}" /><BR></DIV></td>
<td><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td VALIGN="TOP" NOWRAP="NOWRAP" ALIGN="LEFT"></td>
<td><BR></td>
</tr>
</c:forEach>
</table>
</td><td class="tile-dark" VALIGN="top" WIDTH="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr><tr><td colspan="3" class="tile-dark" VALIGN="TOP" HEIGHT="1"><IMG WIDTH="1" HEIGHT="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td></td><td background="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/shadow.gif"><IMG WIDTH="1" HEIGHT="5" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td><td></td>
</tr>
</table>
	</p>
</c:if>


</td></tr>


</TABLE>

		</td>
		<td width="5%">&nbsp; &nbsp; &nbsp;</td>
		<td valign="top" width="15%">
		
		<c:if test="${not empty assetlist}">
		
<TABLE CBORDER="0" CELLPADDING="0" CELLSPACING="0" style="width:95% !important; margin-top:15px;">
<TR><TD><SPAN CLASS="title-text">Refine Search</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><img height="10" width="1" src="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td>		
		
		<strong>Asset Type:</strong><br/>
			<c:set var="selectedBoxes" value="${fn:split(cs.assetTypeFilter, ';')}" />
			<c:forEach var="kid" items="${assetTypes}">
				<input type="checkbox" name="assetTypeFilter" id="assetTypeFilter" value="${kid}" onClick="processForm('filter');"
				<c:set var="contains" value="false" />
				<c:forEach var="item" items="${selectedBoxes}">
				  <c:if test="${item eq kid}">
				    <c:set var="contains" value="true" />
				  </c:if>
				</c:forEach>
          	 	<c:if test="${contains || empty cs.assetTypeFilter}"> checked</c:if>
         	 	> ${kid}<br/>
       		</c:forEach>
        </select>
		</p>	
		<p>
		<strong>Source Locale:</strong><br/>
			<c:forEach var="kid" items="${eligibleLocales}">
        	 	<input type="radio" name="localeFilterRadios" id="${kid}" value="${kid}"
         	 	<c:if test="${cs.localeFilter eq kid}"> checked</c:if>
         	 	onClick="processForm('filter');"
         	 	/> ${kid}<br/>
       		</c:forEach>
		</p>	
		
		<c:if test="${(not hasDifferentLocales) && (not hasUnassignedLocales)}">
		
		<p style="visibility:hidden;display:none;">
		<select multiple name="assetids" id="assetids" width="15" size="10">
		<c:forEach var="asset" items="${assetlist}">
			<option value="${asset.assetType}:${asset.id}" selected>${asset.name}</option>
		</c:forEach>
		</select>
		</p>
		
		</c:if>
		
</td></tr>
</TABLE>

		</c:if>

		</td>
	</tr>
	</table>


</satellite:form>

<script language="Javascript"><!--
unselectItemsOnLoad();
//--></script>

<ics:callelement element="CustomElements/CT/FooterTab"/>
<ics:callelement element="CustomElements/CT/FooterHtml"/>
</c:if>
</gsf:root>
</cs:ftcs>