<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.zip.ZipEntry" %>
<%@ page import="java.util.zip.ZipOutputStream" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>

<%
String name = (String)request.getAttribute("name");
String note = (String)request.getAttribute("note");
Date date = new Date(); 
DateFormat format = new SimpleDateFormat("_yyyyMMdd-hhmmss");

response.setContentType("application/zip");
response.setHeader("Content-Disposition", "attachment; filename=" + name+format.format(date)+ ".ZIP");

ByteArrayOutputStream bos = new ByteArrayOutputStream();
ZipOutputStream zos = new ZipOutputStream(bos);
zos.putNextEntry(new ZipEntry("info.xml"));
PrintWriter pw = new PrintWriter(zos);
pw.println("Collected at =" + date);
pw.println("Note=" + note);
pw.flush();
zos.closeEntry();
zos.close();

int anInt = 0;
Reader bReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bos.toByteArray())));
while((anInt = bReader.read()) != -1)
{
	out.write(anInt);
}
out.flush();
%>

