<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="asset" uri="futuretense_cs/asset.tld"
%><%@ taglib prefix="assetset" uri="futuretense_cs/assetset.tld"
%><%@ taglib prefix="commercecontext" uri="futuretense_cs/commercecontext.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="listobject" uri="futuretense_cs/listobject.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="searchstate" uri="futuretense_cs/searchstate.tld"
%><%@ taglib prefix="siteplan" uri="futuretense_cs/siteplan.tld"
%><%@ page import="COM.FutureTense.Interfaces.*,
                   COM.FutureTense.Util.ftMessage,
                   com.fatwire.assetapi.data.*,
                   com.fatwire.assetapi.*,
                   COM.FutureTense.Util.ftErrors"
%><cs:ftcs><%--

INPUT

OUTPUT

--%>






<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">



<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">
	

<script type="text/javascript">
	function getLocale(loc) {
		//convert from CS's locale notation format to the format Dojo NLS uses.
		//(e.g. en_US -> en-us, fr_FR -> fr-fr, etc.)
		return loc.toLowerCase().replace('_', '-');
	}
	var djConfig = {
		fw_csPath: '<%=ics.GetProperty("ft.cgipath")%>',
		pubName: 'FirstSiteII',
		parseOnLoad: true,
		locale: getLocale('en_US')
	};
</script>


	



	<script type="text/javascript" src="<%=ics.GetProperty("ft.cgipath")%>wemresources/js/WemContext.js"></script>
	<script type="text/javascript">
		//initialize wemcontext before loading dojo layers (see AppBar FIXME)
		//Unfortunately it needs to be AFTER dojo because WemContext relies on xhrGet!
		WemContext.initialize('<%=ics.GetProperty("ft.cgipath")%>','http://localhost:8080/cas/logout');
		var wemcontext = WemContext.getInstance();
		// If a new browser window is opened, as a pop up or new tab or new window, and if the Wem top bar is not there.
		// Then global sls js object would not avilable. In that scenario we load the object in two ways based on how the new window opened.
		// If as a pop-up
		if(window.opener) window.fw_sls_obj = window.opener.top.fw_sls_obj;
		// If as a browser tab or new brower window		
		if(!window.opener && window.top === window.self) document.write('<script type="text/javascript" src="<%=ics.GetProperty("ft.cgipath")%>ContentServer?user_locale=en-us&pagename=fatwire%2Fui%2Futil%2FGetSLSObj"><' + '/script>');
	</script>




<script type="text/javascript" src='<%=ics.GetProperty("ft.cgipath")%>js/dojo/dojo.js'></script>
<script type="text/javascript" src="js/fw/fw_ui_advanced.js"></script>
<script type="text/javascript" src="js/SWFUpload/swfupload.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/SWFUpload/plugins/swfupload.queue.js"></script>

<script type="text/javascript">
	dojo.require("fw.ui._advancedbase");
	dojo.addOnLoad(function() {
		var docIdInput
			, appForm = dojo.query('form[name="AppForm"]')[0]
			;
		dojo.addClass(dojo.body(), 'fw');
		docIdInput = dojo.query('input[name="docId"]')[0];
		
		// docId identifies the tab selected in UC1.
		// AdvancedView passes the docId while drawing the page and 
		//	we are carrying forward the information to every page submit.
		if (!docIdInput && appForm) {
			dojo.create('input', {
				type: 'hidden',
				name: 'docId',
				value: '1'
			}, appForm);
		}
	});
</script>
<script type="text/javascript" src='<%=ics.GetProperty("ft.cgipath")%>wemresources/js/dojopatches.js'></script>

	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
	<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<SCRIPT type="text/javascript">
if(dojo){
	dojo.addOnLoad(function() {
		if(typeof(dijit.byId('dijit_layout_BorderContainer_0')) != 'undefined'){
			dijit.byId('dijit_layout_BorderContainer_0').resize();
		}
		//The following code is needed to remove the underline from the text in the dojo buttons..
		var buttonsAndTitles = dojo.query('span.fwButton, div.new-table-title');
		for(var i=0; i < buttonsAndTitles.length; i++){
			var anchorNode = buttonsAndTitles[i].parentNode;
			if(anchorNode && anchorNode.nodeName.toLowerCase() === 'a'){
				anchorNode.className+=" button-anchor";
			}
			// buttons are inline
			var tdNode = anchorNode.parentNode;
			if(tdNode && tdNode.nodeName.toLowerCase() === 'td' && tdNode.className.indexOf("button-td") == -1){
				tdNode.className+=" button-td";
			}
		}
		}
	);
	
	dojo.addOnLoad(function() {
		// 	summary:
		//		Remove the loading screen once page is loaded. 
		var activeView, docIdElement, docId;
		
		// global ref
		SitesApp = parent.SitesApp;
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		
		if (!activeView.hideLoadingScreen) return;
		activeView.hideLoadingScreen();
		//Attach events
		var iframe = parent.document.getElementById("contentPane_" + activeView.id);
		if (iframe.attachEvent) {
	        var selectedRange = null;
	        iframe.attachEvent("onbeforedeactivate", function() {
	            var sel = iframe.contentWindow.document.selection;
	            if (sel.type != "None") {
	                selectedRange = sel.createRange();
	            }
	        });
	        iframe.contentWindow.attachEvent("onfocus", function() {
	            if (selectedRange) {
	                selectedRange.select();
	            }
	        });
    	}
	});
	
	dojo.addOnUnload(function() {
		// 	summary:
		//		Show the loading screen till the page is fully loaded. 

		var activeDoc, activeView, docIdElement, docId, LOADINGSCREEN_TIMEOUT = 5000;
		
		if (!parent.dojo || !SitesApp) return;
		
		docIdElement = dojo.query('input[name="docId"]', dojo.query('form[name="AppForm"]')[0])[0]; 
		if (docIdElement)
			docId = parseInt(docIdElement.value);
		
		activeDoc = docId !== undefined ?
			SitesApp.getDocument(docId):
			SitesApp.getActiveDocument();
		
		activeView = activeDoc.get('activeView');
		if (!activeView.showLoadingScreen) return;
		activeView.showLoadingScreen();

		//	After a while we will surely remove loading screen 
		//	and show the page even if it is not fully rendered.
		setTimeout(function() {
			if (!activeView.hideLoadingScreen) return;
			activeView.hideLoadingScreen();
		}, LOADINGSCREEN_TIMEOUT);
	});
}
</SCRIPT>

		
</head>
<body class="AdvForms">

</cs:ftcs>
