<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%><cs:ftcs>

<gsf:root action="class:com.claytablet.wcs.ui.UIAsyncOpsToolManager">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">

<style>
.regular_message {
	color: green;
}
</style>

<script language="javascript">
function alertAction() {
	return confirm("Are you sure you want to perform the following action: " + 
			document.getElementById("action").value);
}

function showAlertMessage() {
	var msg = document.getElementById("alertmessage").value;
	if (msg!=null && msg!='') {
		alert(msg);
	}
	setTimeout(resetMessage, 10000);
}

function resetMessage() {
	document.getElementById('alertmessage').innerHTML='';
}

</script>
</head>

<body>



<ics:callelement element="CustomElements/CT/WEM/Header"/>
<c:set var="csroot" value='<%=ics.GetProperty("ft.cgipath")%>'/>
<c:set var="alertmessage" value='<%=ics.GetVar("errormessage")%>'/>
<c:set var="regularmessage" value='<%=ics.GetVar("message")%>'/>
<c:set var="databasemessage" value='<%=ics.GetVar("dbqueuestatus")%>'/>
<c:set var="tableexists" value='<%=ics.GetVar("tableexists")%>'/>
<c:set var="eventflag" value='<%=ics.GetVar("eventflag")%>'/>


<input type="hidden" id="alertmessage" value="<c:out value='${alertmessage}' escapeXml='true'/>" />

<form id="AsyncOperations" onSubmit="return alertAction()">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/AsyncOpsTool"/>
	<input type="hidden" id="action" name="action" value=""/>

<TABLE CLASS="width-outer-70" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">Asynchronous Operations Manager</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td><table>
<tr><td colspan="3"><div id="alertmessage">${errormessage}</div></td></tr>
<tr>

<td>Current Event status:</td><td>&nbsp;&nbsp;</td> <td width="80%"><div id="regularmessage" class="regular_message">${regularmessage}</div></td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
<tr>
<td colspan="3">
<c:choose>
	<c:when test="${eventflag == 'NONE' }">
		<input class="f1image-medium" type="submit" name="Submit" value="Create" 
				onclick='document.getElementById("action").value="CreateEvent";' /> &nbsp;&nbsp;
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${eventflag =='DISABLED' }">
				<input class="f1image-medium" type="submit" name="Submit" value="Enable" 
					onclick='document.getElementById("action").value="EnableEvent";' /> &nbsp;&nbsp;
			</c:when>
			<c:otherwise>		
				<input class="f1image-medium" type="submit" name="Submit" value="Disable" 
					onclick='document.getElementById("action").value="DisableEvent";' /> &nbsp;&nbsp;
			</c:otherwise>
		</c:choose>
		<input class="f1image-medium" type="submit" name="Submit" value="Delete" 
				onclick='document.getElementById("action").value="DeleteEvent";' /> &nbsp;&nbsp;
	</c:otherwise>
</c:choose>

</td>
</tr>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr>
<td>Database status:</td><td>&nbsp;&nbsp; </td><td><div id="databasemessage" class="regular_message">${databasemessage}</div></td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
<tr>
<td colspan="3">


<c:choose>
      <c:when test="${tableexists ne 'true'}">
      
		<input class="f1image-medium" type="submit" name="Submit" value="Create Tables" 
			onclick='document.getElementById("action").value="CreateTables";' /> &nbsp;&nbsp;
      
      
      </c:when>

      <c:otherwise>
      
      <input class="f1image-medium" type="submit" name="Submit" value="Drop Tables" 
		onclick='document.getElementById("action").value="DropTables";' /> &nbsp;&nbsp;
      
      
      </c:otherwise>
</c:choose>



</td>
</tr>

</form>

</table></td></tr></TABLE>
<ics:callelement element="CustomElements/CT/WEM/Footer"/>

<script>
	window.onload = showAlertMessage();
</script>
</body>
</html>
</gsf:root>

</cs:ftcs>
