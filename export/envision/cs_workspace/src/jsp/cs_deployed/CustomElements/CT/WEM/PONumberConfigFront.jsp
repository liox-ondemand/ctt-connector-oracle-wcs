<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" 
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><cs:ftcs><gsf:root action="class:com.claytablet.wcs.ui.UIPONumberManager">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">

<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script> 

<script type="text/javascript">
function changeSite() {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/PONumberConfigAjax" +
		"&siteid=" + document.getElementById("selectSite").value;  
	post(posturl, false);
}

function checkRequirePONumber() {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/PONumberConfigAjax" +
		"&siteid=" + document.getElementById("selectSite").value + "&action=setPoRequired"+
		"&requireponumber=" + document.getElementById("cbRequirePoNumber").checked;
	
	post(posturl, true);
}

function addPONumber() {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/PONumberConfigAjax" +
		"&siteid=" + document.getElementById("selectSite").value + "&action=addPoNumber";
	
	var nameInput = document.getElementById("PONumberName_new");
	var descInput = document.getElementById("PONumberDesc_new");
	
	if (nameInput && descInput) {
		var name = nameInput.value;
		var desc = descInput.value;
		
		if (name=="") {
			alert("PO Number cannot be empty.");
			nameInput.focus();
			return;
		} else if (desc=="") {
			alert("PO Description cannot be empty.");
			descInput.focus();
			return;
		} else if (checkPODuplication(name)) {
			alert("The same PO Number cannot be enterred twice.");
			nameInput.focus();
			return;
		}
		posturl += "&name=" + encodeURIComponent(name) + 
				"&desc=" + encodeURIComponent(desc); 
	}

	post(posturl, true);
}

function updatePONumber(index) {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/PONumberConfigAjax" +
		"&siteid=" + document.getElementById("selectSite").value + "&action=updatePoNumber";
	
	var nameInput = document.getElementById("PONumberName_" + index);
	var descInput = document.getElementById("PONumberDesc_" + index);
	var originalNameInput = document.getElementById("originalponame_" + index);
	
	if (nameInput && descInput) {
		var name = nameInput.value;
		var desc = descInput.value;
		var originalName = originalNameInput.value;
		
		if (name=="") {
			alert("PO Number cannot be empty.");
			nameInput.focus();
			return;
		} else if (desc=="") {
			alert("PO Description cannot be empty.");
			descInput.focus();
			return;
		} else if (originalName!=name && checkPODuplication(name)) {
			alert("The same PO Number cannot be enterred twice.");
			nameInput.focus();
			return;
		}
		posturl += "&name=" + encodeURIComponent(name) + 
				"&desc=" + encodeURIComponent(desc) + 
				"&originalname=" + encodeURIComponent(originalName);
	}

	post(posturl, true);
}

function checkPODuplication(name) {
	var originalNames = document.getElementsByName("original_ponumber");
	
	for (i=0; i<originalNames.length; i++) {
		if (originalNames[i].value == name) {
			return true;
		}
	}
	return false;
}

function removePONumber(index) {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/PONumberConfigAjax" +
		"&siteid=" + document.getElementById("selectSite").value + "&action=removePoNumber";
	
	var originalNameInput = document.getElementById("originalponame_" + index);
	
	if (originalNameInput) {
		var originalName = originalNameInput.value;
		
		posturl += "&name=" + encodeURIComponent(originalName);
	}
	
	post(posturl, true);
}

function post(posturl, isEdit) {
	$.ajax({url:posturl,success:function(result){
		document.getElementById("po_number_list").innerHTML = result;
		if (document.getElementById("requireponumber").value == 'true') {
			document.getElementById("cbRequirePoNumber").checked = true;
		} else {
			document.getElementById("cbRequirePoNumber").checked = false;
		}
		var defaultChangeMessage = document.getElementById("defaultChangeMessage").value; 
		document.getElementById("message").innerHTML = defaultChangeMessage;
		
		document.getElementById("selectSite").disabled = false;
		document.getElementById("selectSite").style.cursor = "pointer";
		
		document.getElementById("cbRequirePoNumber").disabled = false;
		document.getElementById("cbRequirePoNumber").style.cursor = "pointer";
		setTimeout(clearMessage, 5000);
	}});
} 

function clearMessage() {
	var messages = document.getElementsByName("message");
	for (i=0; i<messages.length; i++) {
		messages[i].innerHTML = '';
	}
}

function getSelectValues(select) {
	var result = "";
	var options = select && select.options;
	for (var i=0, iLen=options.length; i<iLen; i++) {
	  var opt = options[i];
	  if (opt.selected) {
	    result += opt.value + ",";
	  }
	}
	return result;
}

function showRow(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'table-row';
}

function showTableCell(divid) {
	document.getElementById(divid).style.visibility = 'visible';
	document.getElementById(divid).style.display = 'table-cell';
}

function hideDiv(divid) {
	hideElement(document.getElementById(divid));
}

function hideElement(element) {
	element.style.visibility = 'hidden';
	element.style.display = 'none';
}

function showElement(element) {
	element.style.visibility = 'visible';
	element.style.display = 'inline';
}

function editPONumber(index) {
	showRow("ponumberedit_" + index);
	showRow("ponumberaction_" + index);
	if (index!='new' && document.getElementById("ponumberdisplay_" + index)){
		hideDiv("ponumberdisplay_" + index);
	}
	var editableLinks = document.getElementsByName("editableLink");
	for (i=0; i<editableLinks.length; i++) {
		hideElement(editableLinks[i]);
	}
	var uneditableLinks = document.getElementsByName("uneditableLink");
	for (i=0; i<uneditableLinks.length; i++) {
		showElement(uneditableLinks[i]);
	}
	document.getElementById("btAddNew").disabled = true;
	document.getElementById("btAddNew").style.cursor = "no-drop";

	document.getElementById("selectSite").disabled = true;
	document.getElementById("selectSite").style.cursor = "no-drop";
	
	document.getElementById("cbRequirePoNumber").disabled = true;
	document.getElementById("cbRequirePoNumber").style.cursor = "no-drop";
}

function hidePONumberEditor(index) {
	if (index!='new') {
		document.getElementById("PONumberName_" + index).value = 
				document.getElementById('originalponame_' + index).value;
		document.getElementById("PONumberDesc_" + index).value = 
				document.getElementById('originalpodesc_' + index).value;
	} else {
		document.getElementById("PONumberName_new").value = ''; 
		document.getElementById("PONumberDesc_new").value = '';
	}
	
	hideDiv("ponumberedit_" + index);
	hideDiv("ponumberaction_" + index);
	
	if (index!='new') {
		showRow("ponumberdisplay_" + index);
	}
	var uneditableLinks = document.getElementsByName("uneditableLink");
	for (i=0; i<uneditableLinks.length; i++) {
		hideElement(uneditableLinks[i]);
	}
	var editableLinks = document.getElementsByName("editableLink");
	for (i=0; i<editableLinks.length; i++) {
		showElement(editableLinks[i]);
	}
	document.getElementById("btAddNew").disabled = false;
	document.getElementById("btAddNew").style.cursor = "pointer";

	document.getElementById("selectSite").disabled = false;
	document.getElementById("selectSite").style.cursor = "pointer";
	
	document.getElementById("cbRequirePoNumber").disabled = false;
	document.getElementById("cbRequirePoNumber").style.cursor = "pointer";
}

function confirmRefresh() {
	if (confirm("You are about to refresh this page. Unsaved changes made on the page will be lost. Do you wish to proceed?")) {
		document.getElementById("refreshlink").click();
	}
}
</script>
</head>

<body>
<ics:callelement element="CustomElements/CT/WEM/Header"/>

<satellite:form>
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/WCSConfigPost"/>
</satellite:form>
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/WEM/PONumberConfigFront">
</render:getpageurl>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">PO Numbers Configuration</SPAN>&nbsp;&nbsp;
	<SPAN style="text-align:right;"><a id='refreshlink' href='<ics:getvar name="refreshurl"/>' style="display:none"/>
		<a href="javascript:confirmRefresh()">
		<img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a>
	</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>
<table style="width: 90%;">
<col width="25%"><col width="%2"><col width="35%"><col width="%2"><col width="10%">
<col width="%2">

<tr><td>
<table><tr><td>
<span CLASS="form-label-text left_align_form_label">Site: &nbsp;&nbsp;&nbsp;&nbsp;</span></td>
<td style="width: 100%;"><select id="selectSite" onchange="changeSite()" style="min-width: 98%;">
	<c:forEach var="siteinfo" items="${sites}">
		<option name="${siteinfo.name}" value="${siteinfo.id}"
			<c:if test="${siteinfo.id eq cursiteid}"> selected </c:if> 
		> ${siteinfo.name}&nbsp;&nbsp;&nbsp;&nbsp;</option>
	</c:forEach>
</select></td></tr></table></td><td></td>
<td><input type="checkbox" id="cbRequirePoNumber" onchange="checkRequirePONumber()"
		<c:if test="${requireponumber}">checked</c:if> >
	&nbsp;&nbsp;PO number required&nbsp;&nbsp;<SPAN id="message" name="message" class="regular_message"></SPAN></td>
<td/>
</tr>
<tr><td>&nbsp;</td></tr>
 
<tr><td colspan="5" id="po_number_list">loading...</td></tr></table>
<p/>
</td></tr></table>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
<script>
	window.onload = changeSite();
</script>

</body>
</html>
</gsf:root>
</cs:ftcs>
