<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%><cs:ftcs>

<gsf:root action="class:com.claytablet.wcs.ui.UIDeveloperToolManager">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">

<style>
.regular_message {
	color: green;
}
</style>

<script language="javascript">
function alertAction() {
	return confirm("Are you sure you want to perform the following action: " + 
			document.getElementById("action").value);
}

function showAlertMessage() {
	var msg = document.getElementById("alertmessage").value;
	if (msg!=null && msg!='') {
		alert(msg);
	}
	setTimeout(resetMessage, 10000);
}

function resetMessage() {
	document.getElementById('regularmessage').innerHTML='';
}

</script>

</head>

<body>
<ics:callelement element="CustomElements/CT/WEM/Header"/>

<c:set var="alertmessage" value='<%=ics.GetVar("errormessage")%>'/>
<c:set var="regularmessage" value='<%=ics.GetVar("message")%>'/>
<input type="hidden" id="alertmessage" value="<c:out value='${alertmessage}' escapeXml='true'/>" />

<h3>Developer Tools</h3>
<b style="color:red;">The tools below are only meant to be used in development environment. 
If you are seeing this page in a production environment, DO NOT use any of these tools. 
Close the page immediately and contact ClayTablet.</b>
<p/>
<p/>

<form id="DbOperations" onSubmit="return alertAction()">
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/DeveloperToolPost"/>
	<input type="hidden" id="action" name="action" value=""/>

	<div id="regularmessage" class="regular_message">${regularmessage}</div>	
	Current DB information:<br>
	<c:forEach var="info" items="${dbinfos}">
		<li><c:out value="${info}" escapeXml="true"/></li>
	</c:forEach>
	<br>
	<input type="submit" name="Submit" value="Drop CT database tables" 
		onclick='document.getElementById("action").value="DropDB"; return validateForm();' /> &nbsp;&nbsp;
	<input type="submit" name="Submit" value="Recreate CT database tables"
		onclick='document.getElementById("action").value="RecreateDB"; return validateForm();'/>
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>

<script>
	window.onload = showAlertMessage();
</script>
</body>
</html>
</gsf:root>

</cs:ftcs>
