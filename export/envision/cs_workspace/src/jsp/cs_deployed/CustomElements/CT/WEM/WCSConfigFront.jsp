<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld"
%><%@ taglib prefix="ics" uri="futuretense_cs/ics.tld"
%><%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld"
%><%@ taglib prefix="render" uri="futuretense_cs/render.tld"
%><%@ taglib prefix="gsf" uri="http://gst.fatwire.com/foundation/tags/gsf"
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" 
%><cs:ftcs>
<gsf:root action="class:com.claytablet.wcs.ui.UIWCSConfigManager">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!--  charset = UTF-8 -->

<html>
<head>
	<META http-equiv="Pragma" content="No-cache">

<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/common.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/content.css" rel="styleSheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/data/css/en_US/wemAdvancedUI.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITabContainer.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Forms.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/dnd_common.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/Calendar.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIInput.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITransferBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UITextarea.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UIComboBox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/SWFUpload.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/UILightbox.css" rel="stylesheet" type="text/css">
<link href="<%=ics.GetProperty("ft.cgipath")%>Xcelerate/../js/fw/css/ui/HoverableTooltip.css" rel="stylesheet" type="text/css">
<link href="ct/css/ct.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script> 

<script type="text/javascript">
function changeSite() {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/WCSAssetTypes" +
		"&siteid=" + document.getElementById("selectSite").value;  
	post(posturl, false);
}

function changeAssetType() {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/WCSAssetTypes" +
			"&siteid=" + document.getElementById("selectSite").value;  
	if (document.getElementById("selectAssetType")) {
		posturl+="&selectedType=" + document.getElementById("selectAssetType").value;
	}
	post(posturl, false);
} 

function changeDefinition(){
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/WCSAssetTypes" +
		"&siteid=" + document.getElementById("selectSite").value;  
	if (document.getElementById("selectAssetType")) {
		posturl+="&selectedType=" + document.getElementById("selectAssetType").value;
	}
	if (document.getElementById("selectAssetDefinition")) {
		posturl+="&selectedDefinition=" + document.getElementById("selectAssetDefinition").value;
	}
	post(posturl, false);
}

function checkSiteTranslateAll() {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/WCSAssetTypes" +
		"&siteid=" + document.getElementById("selectSite").value + 
		"&translateall=" + document.getElementById("cbTranslateAll").checked;
	
	if (document.getElementById("selectAssetType")) {
		posturl+="&selectedType=" + document.getElementById("selectAssetType").value;
	}
	if (document.getElementById("selectAssetDefinition")) {
		posturl+="&selectedDefinition=" + document.getElementById("selectAssetDefinition").value;
	}
	
	post(posturl, true);
}

function addTranslatedAttrs() {
	var selectedAttrs = '';
	if (document.getElementById("selectAvailableAttributes")) {
		selectedAttrs = getSelectValues(document.getElementById("selectAvailableAttributes"));
	}

	if (selectedAttrs!='') {
		var posturl = "ContentServer?pagename=CustomElements/CT/WEM/WCSAssetTypes" +
			"&siteid=" + document.getElementById("selectSite").value + 
			"&action=SelectAttrs";
		
		if (document.getElementById("selectAssetType")) {
			posturl+="&selectedType=" + document.getElementById("selectAssetType").value;
		}
		if (document.getElementById("selectAssetDefinition")) {
			posturl+="&selectedDefinition=" + document.getElementById("selectAssetDefinition").value;
		}
		posturl+="&selectedAttrs=" + selectedAttrs;  
	
		post(posturl, true);
	}
}

function removeTranslatedAttrs() {
	var selectedAttrs = '';
	if (document.getElementById("selectTranslatableAttributes")) {
		selectedAttrs = getSelectValues(document.getElementById("selectTranslatableAttributes"));
	}

	if (selectedAttrs!='') {
		var posturl = "ContentServer?pagename=CustomElements/CT/WEM/WCSAssetTypes" +
			"&siteid=" + document.getElementById("selectSite").value + 
			"&action=DeselectAttrs";
		
		if (document.getElementById("selectAssetType")) {
			posturl+="&selectedType=" + document.getElementById("selectAssetType").value;
		}
		if (document.getElementById("selectAssetDefinition")) {
			posturl+="&selectedDefinition=" + document.getElementById("selectAssetDefinition").value;
		}
		posturl+="&selectedAttrs=" + selectedAttrs;  
		
		post(posturl, true);
	}
}

function selectTypeDefault(useDefault) {
	var posturl = "ContentServer?pagename=CustomElements/CT/WEM/WCSAssetTypes" +
		"&siteid=" + document.getElementById("selectSite").value + 
		"&action=" + (useDefault? "EnableDefault" : "DisableDefault");
	
	if (document.getElementById("selectAssetType")) {
		posturl+="&selectedType=" + document.getElementById("selectAssetType").value;
	}
	post(posturl, true);	
}

function post(posturl, isEdit) {
	$.ajax({url:posturl,success:function(result){
		document.getElementById("asset_type_attr_config").innerHTML = result;
		document.getElementById("cbTranslateAll").checked = 
			(document.getElementById("translateall").value == 'true'); 
		if (isEdit) showMessage("Change saved.");
	}});
} 

function showMessage(message) {
	document.getElementById("message").innerHTML = message;
	setTimeout(function(){document.getElementById("message").innerHTML = ''}, 5000);
}

function getSelectValues(select) {
	var result = "";
	var options = select && select.options;
	for (var i=0, iLen=options.length; i<iLen; i++) {
	  var opt = options[i];
	  if (opt.selected) {
	    result += opt.value + ",";
	  }
	}
	return result;
}

</script>
</head>

<body>
<ics:callelement element="CustomElements/CT/WEM/Header"/>

<satellite:form>
	<input type="hidden" name="pagename" value="CustomElements/CT/WEM/WCSConfigPost"/>
</satellite:form>
<render:getpageurl outstr="refreshurl" pagename="CustomElements/CT/WEM/WCSConfigFront">
</render:getpageurl>

<TABLE CLASS="width-outer-90" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR><TD><SPAN CLASS="title-text">WebCenter Sites Translatable Attributes Configuration</SPAN>&nbsp;&nbsp;
	<SPAN style="text-align:right;"><a id='refreshlink' href='<ics:getvar name="refreshurl"/>'>
		<img style="vertical-align: middle;" src="wemresources/images/ui/ui/dashboard/refreshIcon.png" border="0" alt="Refresh"></a>
	</SPAN></TD></TR>
<tr><td><img height="5" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td class="light-line-color"><img height="2" width="1" src="${csroot}Xcelerate/graphics/common/screen/dotclear.gif"></td></tr>
<tr><td><BR/></td></tr>
<tr><td>
<input type="hidden" value="${fn:length(assettypes)}"/>

<table style="width: 90%;">
<col width="20%"><col width="%2"><col width="20%"><col width="%2"><col width="20%">
<col width="%2"><col width="10%"><col width="%2"><col width="20%">

<tr><td>
<table><tr><td>
<span CLASS="form-label-text left_align_form_label">Site: &nbsp;&nbsp;&nbsp;&nbsp;</span></td>
<td style="width: 100%;"><select id="selectSite" onchange="changeSite()" style="min-width: 98%;">
	<c:forEach var="siteinfo" items="${sites}">
		<option name="${siteinfo.name}" value="${siteinfo.id}"
			<c:if test="${siteinfo.id eq cursiteid}"> selected </c:if> 
		> ${siteinfo.name}&nbsp;&nbsp;&nbsp;&nbsp;</option>
	</c:forEach>
</select></td></tr></table></td><td></td>
<td><input type="checkbox" id="cbTranslateAll" onchange="checkSiteTranslateAll()"
		<c:if test="${enabledbydefault}">checked</c:if> >
	&nbsp;&nbsp;Translate all by default &nbsp;*</td>
<td/><td><div id="message" class="regular_message"></div></td>
</tr>
<tr><td>&nbsp;</td></tr>
 
<tr><td CLASS="left_align_form_label">Asset Types</td>
<td>&nbsp;&nbsp;</td><td CLASS="left_align_form_label">Definitions</td>
<td>&nbsp;&nbsp;</td><td CLASS="left_align_form_label">Untranslated Attributes</td>
<td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
<td>&nbsp;&nbsp;</td><td CLASS="left_align_form_label">Translated Attributes</td>
<td>&nbsp;&nbsp;</td></tr>
<tr id="asset_type_attr_config" "><td>loading...</td></tr></table>
<p/>
</td></tr></table>

<ics:callelement element="CustomElements/CT/WEM/Footer"/>
<script>
	window.onload = changeSite();
</script>

</body>
</html>
</gsf:root>
</cs:ftcs>
