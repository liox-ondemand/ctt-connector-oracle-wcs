<%@ taglib prefix="cs" uri="futuretense_cs/ftcs1_0.tld" %>
<%@ taglib prefix="ics" uri="futuretense_cs/ics.tld" %>
<%@ taglib prefix="satellite" uri="futuretense_cs/satellite.tld" %>
<%@ taglib prefix="xlat" uri="futuretense_cs/xlat.tld" %>
<%@ page import="COM.FutureTense.Interfaces.FTValList" %>
<%@ page import="COM.FutureTense.Interfaces.ICS" %>
<%@ page import="COM.FutureTense.Interfaces.IList" %>
<%@ page import="COM.FutureTense.Interfaces.Utilities" %>
<%@ page import="COM.FutureTense.Util.ftErrors" %>
<%@ page import="COM.FutureTense.Util.ftMessage"%>
<cs:ftcs>

<!--  Configuration for grid right click menus 
	  where label has the display label and action is the 
	  js function that handles the onClick event.
-->
<contextmenuconfig>
		<contextmenus>
			<menu id="edit">
				<label><xlat:stream key="dvin/Common/Edit" escape="true"/></label>
				<functionid>edit</functionid>
			</menu>
			<menu id="preview">
				<label><xlat:stream key="dvin/Common/Preview" escape="true"/></label>
				<functionid>preview</functionid>
			</menu>
			<menu id="share">
				<label><xlat:stream key="dvin/Common/Share" escape="true"/></label>
				<functionid>share</functionid>
			</menu>
			<menu id="delete">
				<label><xlat:stream key="dvin/Common/Delete" escape="true"/></label>
				<functionid>delete</functionid>
			</menu>
			<menu id="bookmark">
				<label><xlat:stream key="UI/UC1/Layout/Bookmark" escape="true"/></label>
				<functionid>bookmark</functionid>
			</menu>
			<menu id="bulktranslate">
				<label>Bulk Translate</label>
				<functionid>bulktranslate</functionid>
				<bulkoperation>yes</bulkoperation>
			</menu>
		</contextmenus>	
</contextmenuconfig>
</cs:ftcs>